const express = require("express");
require("dotenv").config();
require("./config/firebase.config.js")
require("./config/database").connect();
// const rateLimit = require('express-rate-limit');
// const {rateLimiterUsingThirdParty,customRedisRateLimiter}=require("./rateLimiter");

const cors=require("cors");
const crons=require("node-cron");
// const {checksubscription,checkifappointmentiscomplete,sendreminder}=require("./modules/cronjobs");
const path=require("path")
// const i18n = require('i18n');
// i18n.configure({

//       //define how many languages we would support in our application
//       locales:['en','fr'],

//       //define the default language
//       defaultLocale : 'fr',
//       cookie: 'i18n',
//       //define the path to language json files, default is /locales
//       directory: __dirname + '/locales',
//       autoReload: true,
//       updateFiles: true,
//       objectNotation: true,
      
    


//       // define a custom cookie name to parse locale settings from 
// });
// require("./routes/test/populate").init();
const app = express();
app.use(cors());
app.set("view engine", "ejs")
// var bodyParser = require('body-parser')

// bodyParser.json()
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));
app.use(express.static(path.join(__dirname, "views")));
app.use("/static/public",express.static("views/admin-panel/public"))
const adminpanelrouter = require("./routes/admin-panel/index");
const apiroutes = require("./routes/api");
// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
// Route to display static src images
// const limiter = rateLimit({
// 	windowMs: 15 * 60 * 1000, // 15 minutes
// 	max: 100, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
//     message:
// 		'Too many accounts created from this IP, please try again after an hour',
// 	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
// 	legacyHeaders: true, // Disable the `X-RateLimit-*` headers
// })

// Apply the rate limiting middleware to all requests
// app.use(customRedisRateLimiter)
// app.use(i18n.init);
app.use("/api", apiroutes);
app.use('/admin-panel',adminpanelrouter);

app.all('/admin-panel/*', (req, res) => {
    res.render('admin-panel/404',{
        base_url:process.env.BASE_URL+"/admin-panel/"
    });
});
// app.get('/fr', function (req, res) {
//     console.log("called ...........1")
//     res.cookie('i18n', 'fr');
//     res.redirect('back');
// });

// app.get('/en', function (req, res) {
//   console.log("called ...........2")
//     res.cookie('i18n', 'en');
//     res.redirect('back');
// });
// crons.schedule("0 0 0 * * *", async () => {
//     console.log("checksubscription cron job ran at",new Date());
//     checksubscription();
// })
// crons.schedule("*/2 * * * * *", async () => {
//     // console.log("checksubscription cron job is running");
//     sendreminder();
// })
// crons.schedule("0 0 0 * * *", async () => {
//     console.log("checkifappointmentiscomplete cron job is running")
//     checkifappointmentiscomplete();
// })


// crons.schedule("*/2 * * * * *", async () => {
//     console.log("checkifappointmentiscomplete cron job is running")
//     checkifappointmentiscomplete();
// })

// crons.schedule("0 0 0 * * *", async () => {
//     console.log("checkifappointmentiscomplete cron job is running")
//     checkifappointmentiscomplete();
// })
// crons.schedule("*/2 * * * * *", async () => {
//     console.log("called again");
//     checksubscription();
// })
// Logic goes here

module.exports = app;