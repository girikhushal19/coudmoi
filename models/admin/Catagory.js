const mongoose=require("mongoose");
const CatagorySchema=mongoose.Schema({
   mastercatagory:{type:mongoose.Types.ObjectId},
   title:{type:String,unique:true},
   thumbimage:{type:String,default:""},
   bannerimage:{type:String,default:""}
});
module.exports=mongoose.model("Catagory",CatagorySchema);