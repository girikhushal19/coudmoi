const mongoose=require('mongoose');
const Boosterpackages1Schema=new mongoose.Schema({
    type:{type:String,default:"booster"},
    price:{type:Number,default:0},
    duration:{type:Number,default:0},
    package:{type:String,default:""},
    description:{type:String,default:""},
    image:{type:String,default:""},
    
})

module.exports=mongoose.model('Boosterpackages1',Boosterpackages1Schema);