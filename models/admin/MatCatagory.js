const mongoose=require("mongoose");
const MatCatagorySchema=mongoose.Schema({
  name:{type:String},
  path:{type:String},
  image:{type:String},
});
MatCatagorySchema.index({ path: 1 });
MatCatagorySchema.statics.addcatagory=async function(data){
    const updateddata={
        name:data.name,
        path:","+data.path+",",
        image:data.image
    }
    this.create(updateddata)
    .then(()=>true)
}

MatCatagorySchema.statics.updatecatagory=async function(data){
    const updateddata={
        ...(data.name&&{name:data.name}),
        ...(data.path&&{path:","+data.path+","}),
        ...(data.image&&{image:data.image})
    }
    this.findByIdAndUpdate(data.id,updateddata)
    .then(()=>true)
}

MatCatagorySchema.statics.deletecatagory=async function(data){
   
    this.delete({_id:data.id})
    .then(()=>true)
}

MatCatagorySchema.statics.getallcatagories=async function(){
   
    return this.find({}).sort( { path: 1 } )
    
}

MatCatagorySchema.statics.getchildcatagories=async function(mcatagory){
    const path=new RegExp(mcatagory)
   const query={ path:path  }
    const docs=await this.find(query);
    console.log("query",query,"docs",docs);
    return docs
   
}

MatCatagorySchema.statics.getparentcatagoriesonly=async function(mcatagory){
    return this.find({ path:  /,Null,/ })
   
}

module.exports=mongoose.model("MatCatagory",MatCatagorySchema);

