const mongoose=require("mongoose");
const AbuseReportsSchema=new mongoose.Schema({
   
    reason:{type:String},
    
    whoisreported_Id:{type:String},
    whoisreported_Name:{type:String},
    whoisreported_type:{type:String},
    reportedby_Name:{type:String},
    reportedby_Id:{type:String},
    reportedby_type:{type:String},
    type:{type:String,default:""},
    is_resolved:{type:Boolean,default:false}
    
},
{
    timestamps:true
}
);


module.exports=mongoose.model("AbuseReports",AbuseReportsSchema);