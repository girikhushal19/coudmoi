const mongoose=require('mongoose');
const BoosterpackageSchema=new mongoose.Schema({
    booster_type:{type:String,default:""},
    start_date:{type:Date,default:null},
    end_date:{type:Date,default:null},
    payment_status:{type:Boolean,default:false},
    package_id:{type:String,default:""},
    payment_amount:{type:Number,default:0},
    transaction_id:{type:String,default:""},
    provider_id:{type:String,default:""},
    provider_name:{type:String,default:""},
    email:{type:String,default:""},
    durationindays:{type:Number,default:0},
    mode_of_payment:{type:String,default:""},
    date_of_transaction:{type:String,default:""},
    cardType:{type:String,default:""},
    cardNum:{type:String,default:""},
})

module.exports=mongoose.model('Boosterpackage',BoosterpackageSchema);