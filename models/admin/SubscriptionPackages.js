const mongoose=require('mongoose');
const SubscriptionPakagesSchema=new mongoose.Schema({
   
    package:{type:String,required:true},
    price:{type:Number,required:true},
    duration:{type:Number,required:true},
    description:{type:String,required:true},
    type:{type:String},
    is_boosted:{type:Boolean,default:false},
    is_multiple_photos_allowed:{type:Boolean,default:false}
    

});
module.exports=mongoose.model('SubscriptionPackages',SubscriptionPakagesSchema);