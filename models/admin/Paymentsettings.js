const mongoose=require("mongoose");
const paymentSettings=new mongoose.Schema({
    name:{type:String},
    liveapikey:{type:String},
    liveapisecret:{type:String},
    testapikey:{type:String},
    testapisecret:{type:String},
    active:{type:Boolean},
});

module.exports=mongoose.model("PaymentSettings",paymentSettings);