const mongoose = require("mongoose");
const CancledservicepaymentSchema = new mongoose.Schema({
   
   client_id: { type: String ,default:""},
   client_name:{ type: String ,default:""},
   provider_name:{ type: String ,default:""},
   provider_id:{type:String,default:""},
   amounttopay: { type: Number },
   cancellation_date:{type:Date,default:new Date()},
   service_id:{type:String,default:""},
   payment_status:{type:Boolean,default:false}
    
    
});
module.exports = mongoose.model("Cancledservicepayment", CancledservicepaymentSchema);