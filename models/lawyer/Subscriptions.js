const mongoose=require('mongoose');

const SubscriptionsSchema=new mongoose.Schema({
   
    subscriber_name:{type:String,required:true},
    subscriber_email:{type:String,required:true},
    type_of_subscription:{type:String},
    name_of_package:{type:String,required:true},
    package_id:{type:String,required:true},
    status:{type:Boolean,required:true},
    start_date:{type:Date,required:true},
    end_date:{type:Date,required:true},
    payment_status:{type:Boolean,default:false},
    payment_amount:{type:Number},
    mode_of_payment:{type:String},
    date_of_transaction:{type:Date},
    transaction_id:{type:String},
    provider_id:{type:String,required:true},
    is_boosted:{type:Boolean,default:false},
    is_multiple_photos_allowed:{type:Boolean,default:false},
    cardType:{type:String},
    cardNum:{type:String},
    temp_end_date:{type:Date}
    

});
module.exports=mongoose.model('Subscriptions',SubscriptionsSchema);