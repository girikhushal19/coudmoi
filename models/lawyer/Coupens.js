const mongoose = require("mongoose");
const CoupensSchema = new mongoose.Schema({
    name:{ type: String, required: true },
     type:{ type: String, required: true },
    code: { type: String, required: true },
    discountamount: { type: Number, required: true },
    limite: { type: Number, required: true },
    used: { type: Number, default:0 },
   
    start_date: { type: Date, required: true },
    end_date: { type: Date, required: true },
    status:{type:Boolean,default:true}
});
module.exports = mongoose.model("Coupens", CoupensSchema);