const mongoose = require("mongoose");
const ServicesSchema = new mongoose.Schema({
    provider_id: { type: String, required: true },
    provider_name: { type: String, required: true },
    catagory: { type: Array, default:[] },
    catagory_id: { type: Array, default:[]  },
    title:{ type: String, required: true },
    pseduoname:{ type: String,default: "" }, 
    experience: { type: String ,default: ""},
    desc:{ type: String, required: true },
   
   
    is_approved:{ type: Boolean,default:false },
    is_active:{type:Boolean,default:false},
    service_images:{type:Array},
    
   
    price:{ type: Number, required: true }
   
   
    
   
},
{
    timestamps:true
}
);
module.exports = mongoose.model("LawyerServices", ServicesSchema);