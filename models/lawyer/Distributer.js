const mongoose = require("mongoose");
const DistributerSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
});
module.exports = mongoose.model("Distributer", DistributerSchema);