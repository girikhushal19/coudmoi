const mongoose = require("mongoose");

const LawunitSchema = new mongoose.Schema({
  photo: { type: String, default: "" },
  first_name: { type: String ,default: ""},
  last_name:{ type: String, default: ""},
  email: { type: String, unique: true ,default: ""},
  password: { type: String },
  
 

  competence:{type:Boolean,default:false},
  loginType: { type: String,default:"" },
 
 
  address:{type:Array,default:[]},
  location_cor:{ 
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        default:[0.0000,0.0000]
    }
  },
  phone: { type: String ,default: ""},
  
  avalible_instant:{type:Boolean,default:false},
  
  is_deleted: { type: Boolean, default: false },
  is_active: { type: Boolean,default: true },
  token: { type: String },
  reset_password_token: {
    type: String
  },
  reset_password_expires: {
    type: Date
  },
  fcm_token:{type:String,default:""},
  totalpaidamount:{type:Number,default:0},
  totalpendingamount:{type:Number,default:0},
  extProvider:{type:Boolean,default:false},
  is_subscribed:{type:Boolean,default:false},
  subscription_id:{type:String,default:""},
  blockedchats:{type:Array,default:[]}
 
},{ timestamps: true });
LawunitSchema.index({"location_cor": '2dsphere'});
module.exports = mongoose.model("Lawunit", LawunitSchema);
