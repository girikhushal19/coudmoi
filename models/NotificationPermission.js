const mongoose = require('mongoose');
const NotificationPermissionSchema = new mongoose.Schema({
    id: {type:String, required:true},
    user_type:{type:String},
  
    
    email_offer:{type:Boolean,default:true},
    push_offer:{type:Boolean,default:true},
   
    mfc_email:{type:Boolean,default:true},
    mfc_push:{type:Boolean,default:true},
    
    pc_push:{type:Boolean,default:true},
    pc_email:{type:Boolean,default:true}


})
module.exports = mongoose.model('NotificationPermission', NotificationPermissionSchema);