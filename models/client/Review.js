const mongoose = require('mongoose');
const ReviewSchema = new mongoose.Schema({
    client_name: {type:String, required:true},
    client_id:{type:String,required:true},
    provider_id:{type:String,required:true},
    provider_name:{type:String, required:true},
    service_name:{type:String,default:""},
    service_id:{type:String,default:""},
    rating:{type:Number,required:true},
    review:{type:String,required:true},
    date: {type:Date, default:new Date().toISOString()},    
});
module.exports = mongoose.model('Review', ReviewSchema);