const mongoose=require("mongoose");
const OffersSchema=new mongoose.Schema({
    service_id:{type:String},
    provider_id:{type:String},
    price:{type:Number,default:0},
    client_id:{type:String},
    offer_date:{type:Date,default:new Date()},
    offer_time:{type:String,default:new Date().getTime()},
    offer_status:{type:Boolean,default:false},
    offer_acceptance_by_client:{type:Boolean,default:false},
    offer_acceptance_by_provider:{type:Boolean,default:true},
    
   
});

module.exports=mongoose.model("Offers",OffersSchema);