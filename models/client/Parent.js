const mongoose = require("mongoose");

const ParentSchema = new mongoose.Schema({
  
  email: { type: String, unique: true },
 
  password: { type: String },
 
  token: { type: String },
 
  date_of_registration:{type:Date,default:new Date()},
  is_active:{type:Boolean,default:true},
  is_deleted:{type:Boolean,default:false},
  fcm_token:{type:String},
  token: { type: String },
  reset_password_token: {
    type: String
  },
  reset_password_expires: {
    type: Date
  }
  
});




module.exports = mongoose.model("Parent", ParentSchema);