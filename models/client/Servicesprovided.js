const mongoose=require("mongoose");
autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection(process.env.MONGO_URI);
autoIncrement.initialize(connection);
const Servicesprovided=new mongoose.Schema({
   
    order_id:{type:Number,default:""},
    client_id:{type:String,default:""},
    client_name:{type:String,default:""},
    provider_id:{type:String,default:""},
    provider_name:{type:String,default:""},
    service_id:{type:String,default:""},
    service_name:{type:String,default:""},
    catagory_id:{type:String,default:""},
    catagory_banner:{type:String,default:""},
    catagory_thumb:{type:String,default:""},
    catagory:{type:String,default:""},
    date_availed:{type:Date,default:new Date()},
    time_availed:{type:String,default:""},
    title:{type:String,default:""},
    service_status:{type:Boolean,default:false},
    payment_status:{type:Boolean,default:false},
    payment_amount:{type:Number,default:0},
    mode_of_payment:{type:String,default:""},
    is_invite_id:{type:String,default:null},
    location:{type:String,default:""},
    location_cor:{ 
        type: {
            type: String,
            default: "Point"
        },
        coordinates: {
            type: [Number],
            default:[0.0000,0.0000]
        }
      },
    
    date_of_transaction:{type:Date,default:new Date()},
    transaction_id:{type:String,default:""},
    docs:{type:Array,default:[]},
    service_acceptance_status_by_provider:{type:Boolean,default:false},
    service_acceptance_status_by_client:{type:Boolean,default:false},
    cancelation_reason_by_user:{type:String,default:""},
    cancelation_message_by_user:{type:String,default:""},
    cardType:{type:String,default:""},
    cardNum:{type:String,default:""},
    cancelation_reason_by_provider:{type:String,default:""},
    cancelation_message_by_provider:{type:String,default:""},
    is_service_rejected:{type:Boolean,default:false},
    promo:{type:String,default:""},
    duration:{type:String,default:""},
    desc:{type:String,default:""},
    contact:{type:String,default:""},
    finalamount:{type:Number,default:0},
    commission:{type:Number,default:0},
    tax:{type:Number,default:0},
    messageSentTo:{type:Array,default:[]},
    currency:{type:String,default:"CHF"},
    payment_failed:{type:Boolean,default:false},
    is_shipping_required:{type:Boolean,default:true},
    shipping_details:{
        type:Array,default:[]
    }
});
Servicesprovided.plugin(autoIncrement.plugin, {model:'servicesprovideds',field: 'order_id',startAt: 1001});

Servicesprovided.index({"location_cor": '2dsphere'});
module.exports=mongoose.model("Servicesprovided",Servicesprovided);