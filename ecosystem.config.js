module.exports = {
  apps : [{
    name: 'slegal',
    script : "./index.js",
    exec_mode : "cluster",
    instances : 4,
    listen_timeout:20000,
    autorestart:true,
    watch:false,
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
}
