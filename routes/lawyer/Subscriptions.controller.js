const subscriptions = require("../../models/lawyer/Subscriptions");
const subpackagemodel=require("../../models/admin/SubscriptionPackages");
const boosterpackage=require("../../models/admin/Boosterpackage");
const BoosterpackagefromAdmin=require("../../models/admin/BoosterPackages");
const moment=require("moment");
const base_url = process.env.BASE_URL;
const {addmonthstodate,getdateinadvancewithstartdate,getdateinadvance}=require("../../modules/dates")
module.exports.createSubscription = async (req, res) => {
  try {
    const {
      subscriber_name,
      subscriber_email,
      type_of_subscription,
      name_of_package,
      is_boosted,
      duration,
      package_id,
    } = req.body;
    const subpackage=await subpackagemodel.findById(package_id);
    console.log("subpackage",subpackage)
    if(!subpackage){
      return res.send({
        status:false,
        message:"package with this id do not exists"
      })
    }
    await subscriptions.find({
      subscriber_email: subscriber_email,
      package_id:subpackage._id+''
    }).then(async(result) => {
      console.log("result", result);
      if (result.length > 0) {
        console.log("in 1")
        let statusofsub=moment(new Date(result[0].end_date)).isAfter(new Date(), 'day');
        console.log("result", result,"statusofsub",statusofsub);
       if(statusofsub){
        const start_date=result[0].end_date;
        const end_date=getdateinadvancewithstartdate(start_date,subpackage.duration);
         const data={
         
          temp_end_date:end_date
         }
         await subscriptions.findByIdAndUpdate(result[0]._id, data, { new: true }).then((service) => {
         return  res.send({
            status: true,
            message: "Abonnement créé avec succès",
            data: result,
            errmessage:"",
            url:
              base_url +
              "/api/payment/" +
              req.body.payment_type +"lawyer"+
              "/" +
              service._id +
              "/" +
              req.body.amount,
          });
        });
       }else{
        const start_date=new Date().toISOString();
        const end_date=getdateinadvancewithstartdate(start_date,subpackage.duration);
         const data={
          start_date:start_date,
          temp_end_date:end_date
         }
         
         await subscriptions.findByIdAndUpdate(result[0]._id, data, { new: true }).then((service) => {
         return  res.send({
            status: true,
            message: "Abonnement créé avec succès",
            data: result,
            errmessage:"",
            url:
              base_url +
              "/api/payment/" +
              req.body.payment_type +"lawyer"+
              "/" +
              service._id +
              "/" +
              req.body.amount,
          });
        });
       }
      } else {
        console.log("in 0")
        const start_date=new Date().toISOString();
        const end_date=getdateinadvance(subpackage.duration);
        console.log(end_date)
        const subscription = new subscriptions();
        subscription.subscriber_name = subscriber_name;
        subscription.subscriber_email = subscriber_email;
        // subscription.type_of_subscription = type_of_subscription;
        subscription.name_of_package = name_of_package;
        subscription.status = true;
        subscription.start_date = start_date;
        subscription.end_date = start_date;
        subscription.temp_end_date=end_date;
        subscription.package_id = package_id;
        subscription.provider_id = req.body.provider_id;
        // subscription.is_boosted=subpackage.is_boosted;
        subscription.is_multiple_photos_allowed=subpackage.is_multiple_photos_allowed;
        subscription.save().then((service) => {
          res.send({
            status: true,
            message: "Abonnement créé avec succès",
            data: result,
            errmessage:"",
            url:
              base_url +
              "/api/payment/" +
              req.body.payment_type +"lawyer"+
              "/" +
              service._id +
              "/" +
              req.body.amount,
          });
        });
      }
    });
  } catch (err) {
    console.log(err)
    res.send({
      status: false,
      message: "",
      errmessage:err.message,
      data: null,
    });
  }
};
module.exports.updateSubscription =async (req, res) => {
  const {
    subscriber_name,
    subscriber_email,
    type_of_subscription,
    name_of_package,
    status,
    start_date,
    end_date,
    subscription_id,
  } = req.body;
  const newenddate=new Date(end_date);
  const newstartdate=new Date(start_date);
 
  const data = {
    ...(subscriber_name && { subscriber_name }),
    ...(subscriber_email && { subscriber_email }),
    ...(type_of_subscription && { type_of_subscription }),
    ...(name_of_package && { name_of_package }),
    ...(status && { status }),
    ...(start_date && { start_date:newstartdate }),
    ...(end_date && { end_date:newenddate }),
  };
  console.log("data",data)
  const sub=await subscriptions.findById(subscription_id);
    sub.end_date=newenddate;
    sub.start_date=newstartdate;
    sub.payment_status=true;
    
    sub.save().then((result) => {
      res.send({
        status: true,
        message: "L'abonnement a été mis à jour avec succès",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "Abonnement non mis à jour",
        data: null,
        errmessage:err.message,
      });
    });
};
module.exports.getAllSubscriptions = (req, res) => {
  subscriptions.find({}).then((result) => {
    res.send({
      status: "success",
      message: "Subscriptions fetched successfully",
      data: result,
      errmessage:"",
    });
  });
};
module.exports.getSubscriptionById = (req, res) => {
  const subscription_id = req.params.subscription_id;
  subscriptions
    .findById(subscription_id)
    .then((result) => {
      res.send({
        status: "success",
        message: "Subscription fetched successfully",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: "error",
        message: "Subscription not fetched",
        data: err,
        errmessage:err.message,
      });
    });
};
module.exports.deleteSubscription = (req, res) => {
  const subscription_id = req.params.subscription_id;
  subscriptions
    .findByIdAndDelete(subscription_id)
    .then((result) => {
      res.send({
        status: true,
        message: "Abonnement supprimé avec succès",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "",
        data: null,
        errmessage:err.message,
      });
    });
};
module.exports.getSubscriptionbylawyerID = async(req, res) => {
  const lawyer_id = req.params.lawyer_id;
  await subscriptions
    .find({ provider_id:lawyer_id })
    .then((result) => {
      res.send({
        status: "success",
        message: "Subscriptions fetched successfully",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: "error",
        message: "",
        data: err,
        errmessage:err.message,
      });
    });
}
module.exports.getSubscriptionbylawyerIDred = async(req, res) => {
  const lawyer_id = req.params.lawyer_id;
  const subscription=await subscriptions.aggregate([
  { $match:{provider_id:lawyer_id}},
  ]).then((result) => {
      res.send({
        status: "success",
        message: "Subscriptions fetched successfully",
        data: result,
        errmessage:"",
      });
    })
    .catch((err) => {
      res.send({
        status: "error",
        message: "",
        data: err,
        errmessage:err.message,
      });
    });
}
module.exports.boostpackage=async(req,res)=>{
  const id=req.body.id;
  const type=req.body.type;
  
  const package_id=req.body.package_id;
  const booster_type=req.body.booster_type;
  const durationindays=req.body.durationindays;
  const amount=req.body.amount;
  const sub=await subscriptions.findOne({provider_id:id});
  const package=await BoosterpackagefromAdmin.findById(package_id);
  if(!package){
    return res.send({
      status:true,
      message:"no such package exists",
      
    })
  }
  if(!sub){
    return res.send({
      status:true,
      message:"need to subscribe first",
      
    })
  }
  const booster_details={
    booster_type:booster_type,
    start_date:new Date(),
    end_date:getdateinadvance(durationindays),
    subscription_id:sub._id,
    provider_id:sub.provider_id,
    provider_name:sub.subscriber_name,
    durationindays:package.duration,
    package_id:package_id,
    email:sub.subscriber_email
      }
     
    const isalreadythereandnotexpired=await boosterpackage.findOne({
      booster_type:booster_type,
      provider_id:sub.provider_id,
      package_id:package_id,
      end_date:{$gte:new Date()}
      
    });
    console.log("isalreadythereandnotexpired",isalreadythereandnotexpired)
    if(isalreadythereandnotexpired){
      await boosterpackage
      .updateOne({_id:isalreadythereandnotexpired._id,payment_status:true},{
        booster_type:booster_type,
        start_date:new Date(),
        end_date:getdateinadvancewithstartdate(isalreadythereandnotexpired.end_date,durationindays),
        subscription_id:sub._id,
        provider_id:sub.provider_id,
        durationindays:package.duration,
        provider_name:sub.subscriber_name,
        package_id:package_id,
        payment_status:false,
        email:sub.subscriber_email
          }).then((service)=>{
        return res.send({
          status:true,
          message:"booster created successfully",
          url:
                base_url +
                "/api/payment/" +
                req.body.payment_type +"lawyerboost"+
                "/" +
                isalreadythereandnotexpired._id +
                "/" +
                amount
        })
      })
    }else{
      await boosterpackage.create(booster_details).then((service)=>{
        return res.send({
          status:true,
          message:"booster created successfully",
          url:
                base_url +
                "/api/payment/" +
                req.body.payment_type +"lawyerboost"+
                "/" +
                service._id +
                "/" +
                amount
        })
      })
    }
   
    
}
module.exports.getallsubstransactions=async(req,res)=>{
  const lawyer_id = req.params.lawyer_id;
  const boosters=await boosterpackage.find({provider_id:lawyer_id});
  const subscription=await subscriptions.find({provider_id:lawyer_id});
  const allmergedthingsthatidontreallycareabout=boosters?.concat(subscription)
  return res.send({
    status: "success",
    message: "Subscriptions fetched successfully",
    data: allmergedthingsthatidontreallycareabout,
    errmessage:"",
  });
}