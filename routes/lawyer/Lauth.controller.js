const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const path = require("path");
const hbs = require("nodemailer-express-handlebars");
let lawunit = require("../../models/lawyer/Lawunit");
const User = require("../../models/auth/user");
const base_url = process.env.BASE_URL;
const apptext_cmsmodel=require("../../models/admin/Apptext");
module.exports.register = async (req, res) => {
  // Our register logic starts here
  const code=Math.floor(100000 + Math.random() * 900000);
  
 
  // Get user input
  const { email, password,fcm_token,extProvider,first_name,last_name,phone,address,latlong ,competence} = req.body;
  let encryptedPassword;
  console.log("req.body",req.body)
  // const newiscabinate=iscabinate==""?false:true;
  const newlatlong=latlong
  // JSON.parse(latlong);
  console.log("newlatlong",newlatlong)
  // Validate user input
  if (!email) {
    return res.status(400).send({
     message: "",
      status: false,
      errmessage: "Toute contribution est requise",
      data:null
    });
  }
  let user_photo;
  if(req.files.user_photo){
    user_photo="userprofile/" +req.files.user_photo[0].filename
  }
  // check if user already exist
  // Validate if user exist in our database
  const oldUser = await lawunit.findOne({ email });
 
    const oldUserP= await User.findOne({ email });
  if (oldUser) {
    return res.status(409).send({
      status: false,
      errmessage: "L'utilisateur existe déjà",
      message: "",
      data: null,
    });
  }
if(password){
   encryptedPassword = await bcrypt.hash(password, 10);
    
    }else{
      if(extProvider){
       encryptedPassword=""
      }else{
        return res.send({
        status:false,
        message:"",
        errmessage:"password is required !",
        data:null
       })
      }
    }
  //Encrypt user password
  data={
    email: email,
    password: encryptedPassword,
    // iscabinate:newiscabinate,
    fcm_token:fcm_token
  }

  // Create user in our database
  const Lawyer = await lawunit.create(data);
  
  
  const token = jwt.sign(
    { user_id: Lawyer._id, email },
    process.env.TOKEN_KEY,
    {
      expiresIn: "2h",
    }
  );
  // save user token
Lawyer.token = token;
Lawyer.photo=user_photo;
Lawyer.extProvider=extProvider;
Lawyer.verification_code=code;
Lawyer.address=address;
Lawyer.first_name=first_name;
Lawyer.last_name=last_name;
Lawyer.phone=phone;
Lawyer.competence=competence;
// Lawyer.location_cor={type:"Point","coordinates":[newlatlong[0],newlatlong[1]]}

const page_name=process.env.register_confirm_email
  const apptext=await apptext_cmsmodel.findOne({page_name:page_name});
  // Create token
  console.log(apptext);
  let emailtext;
  let emailsubject;
  if(apptext?.page_content){
  emailtext=apptext.page_content;
  emailsubject=apptext.page_heading;
  }else{
    emailtext="L'enregistrement est réussi, continuez et mettez à jour votre profil."
    emailsubject="Inscription réussie"
  }
 var data = {
              to: email,
              from: process.env.MAILER_EMAIL_ID,
              template: "confirm-register-email-lawyer",
              subject: "Confirmation de la réinitialisation du mot de passe",
              context: {
                name: email,
                text:emailtext,
                code:code
               
              },
            };
 var smtpTransport = nodemailer.createTransport({
                host: process.env.MAILER_HOST,
                port: process.env.MAILER_PORT,
                secure:false,
                auth: {
                  user: process.env.MAILER_EMAIL_ID,
                  pass: process.env.MAILER_PASSWORD,
                },
                tls: {
                  rejectUnauthorized: false
                }
              });

              const handlebarOptions = {
                viewEngine: {
                  partialsDir: path.resolve("./views/auth/"),
                  defaultLayout: false,
                },
                viewPath: path.resolve("./views/auth/"),
              };
              // console.log("data in email",data)
              smtpTransport.use("compile", hbs(handlebarOptions));
              smtpTransport.sendMail(data, function (err) {
                if (!err) {
                  // return res.status(200).json({
                  //   status: true,
                  //   message: "Veuillez vérifier votre e-mail pour de plus amples instructions",
                  //   errmessage: "",
                  //   data: null,
                  // });
                } else {
                  // console.log(err)
                  // return res.send( {
                  //   status: false,
                  //   message: "",
                  //   errmessage: err.message,
                  //   data: null,
                  // });
                }
              });
  // return new user
  Lawyer.save();
  return res.send({
    status:true,
    message:"Utilisateur créé avec succès",
    errmessage:"",
    data:Lawyer

  });

  // Our register logic ends here
};
module.exports.login = async (req, res) => {
  try {
    // Get user input
    const { email, password,fcm_token ,extProvider} = req.body;

    // Validate user input
    if (!(email )) {
      res.status(400).send("Toute contribution est requise");
    }
    // Validate if user exist in our database
    const Lawyer = await lawunit.findOne({ email:email });

    if(password!=""){
       if (
      Lawyer &&
      (await bcrypt.compare(password, Lawyer.password))
    ) {
      // Create token
      const token = jwt.sign(
        { user_id: Lawyer._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "24h",
        }
      );

       if(fcm_token){
      await lawunit.findOne({ email }).then((data)=>{
       
          data.fcm_token=fcm_token;
          data.save();
         
      })
     }
      // console.log(token)
      // save user token
      Lawyer.token = token;

      // user
    return   res.status(200).json({ 
       
        status: true, 
        message:"Connexion réussie",
        errmessage:"",
        data:Lawyer });
    } else {
      return res.status(400).send({
        message: "",
        status: false,
        errmessage: "L'adresse électronique ou le mot de passe est incorrect",
        data:null

      });
    }
    }else{
       if (Lawyer.extProvider) {
        const token = jwt.sign(
          { user_id: Lawyer._id, email },
          process.env.TOKEN_KEY,
          {
            expiresIn: "24h",
          }
        );
       if(fcm_token){
        let data=await lawunit.findOne({ email });
        data.fcm_token=fcm_token;
        data.save();
       
       }
        // save user token
        Lawyer.token = token;
        
          return res.send({
              message: "Connexion réussie",
              errmessage: "",
              data: Lawyer,
              status: true,
            });
        } else {
          return res.send({
            status: false,
            message: "",
            errmessage: "you have not singed up using this method",
            data: null,
          });
        }
    }
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
};


module.exports.socialLogin = async (req, res) => {
  try{
    let { first_name, last_name, email, phone,loginType } = req.body;
    if(!email)
    {
      return res.send({
        status:false,
        message: "L'adresse électronique est requise",
        errmessage:"L'adresse électronique est requise",
        data:null
      });
    }
    let checkEmail =  await lawunit.findOne({email});
    //console.log("checkEmail ",checkEmail);return false;
    if(!checkEmail)
    {
      const token = jwt.sign(
        { user_id:  email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "24h",
        }
      );
      const user = await lawunit.create({
        ...(first_name && { first_name:first_name }),
        ...(last_name && { last_name:last_name }),
       email: email.toLowerCase(), 
       ...(phone &&{phone:phone}), 
       ...(loginType &&{loginType:loginType}),
       ...(token &&{token:token})
     }).then((result)=>{
        return res.send({
          status:true,
          message: "Succès",
          errmessage:"Succès",
          data:result
        });
     }).catch((e)=>{
        return res.send({
          status:false,
          message: e.message,
          errmessage:e.message,
          data:null
        });
     });
    }else{
      const token = jwt.sign(
        { user_id: checkEmail._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "24h",
        }
      );
      // save user token
      //checkEmail.fcm_token=fcm_token;
      // checkEmail.token = token;
      // checkEmail.save()
      await lawunit.updateOne({_id:checkEmail._id},{token:token,loginType:loginType});
      let checkEmail2 =  await lawunit.findOne({email});
      return res.send({
        status:true,
        message: "Succès",
        errmessage:"Succès",
        data:checkEmail2
      });
    }
  }catch(e){
    return res.send({
      status:false,
      message: e.message,
      errmessage:e.message,
      data:null
    });
  }
};

module.exports.logout = async (req, res) => {
  
 const id = req.params.id;
  const Lawyer = await lawunit.findById(id);
  Lawyer.token = "";
  Lawyer.fcm_token = "";
  Lawyer.save();
  return res.send({
     status: true, 
    errmessage:"",
    message:"Deconnexion réussie",
    data:null });
  
};
module.exports.render_forgot_password_template = function (req, res) {
  return res.sendFile(path.resolve("./public/forgot-password.html"));
};
module.exports.render_reset_password_template = function (req, res) {
  return res.render(path.resolve("./public/reset-password-lawyer.ejs"), {
    url:base_url
  });
};
module.exports.forgot_password = async (req, res) => {
  console.log(req.body)
  try {
    async.waterfall(
      [
        function (done) {
          lawunit.findOne({
              email: req.body.email,
            })
            .exec(function (err, user) {
              console.log(user);
              console.log(err)
              if (user) {
                done(err, user);
              } else {
                done({
                  status: false,
                  message: "utilisateur non trouvé",
                  errmessage: "",
                  data: null,
                });
              }
            });
        },
        function (user, done, err) {
          // create a unique token
          var tokenObject = {
            email: user.email,
            id: user._id,
          };
          var secret =
            user._id + "_" + user.email + "_" + new Date().getTime();
          var token = jwt.sign(tokenObject, secret);
          done(err, user, token);
        },
        function (user, token, done) {
          lawunit
            .findByIdAndUpdate(
              { _id: user._id },
              {
                reset_password_token: token,
                reset_password_expires: Date.now() + 86400000,
              },
              { new: true }
            )
            .exec(function (err, new_user) {
              done(err, token, new_user);
            });
        },
        function (token, user, done) {
          // console.log(user)
          var data = {
            to: user.email,
            from: process.env.MAILER_EMAIL_ID,
            template: "forgot-password-email-lawyer",
            subject: "L'aide pour les mots de passe est arrivée !",
            // html: "forgot-password-email",
            context: {
              url:base_url+"/api/provider/reset_password?token="+token,
              name: user.first_name,
            },
          };
          console.log(data)
          sendemail(req, res, data, done);
        },
      ],
      function (err) {
        console.log("it failes here in forgot password",err)
        return res.status(422).json({ 
          status: false,
          message: "",
          errmessage: err.message,
          data: null,
         });
      }
    );
  } catch (err) {
    console.log(err);
  }
};
module.exports.reset_password = async (req, res) => {
  console.log(req.body);
  //  console.log("rest")
  lawunit
    .findOne({
      reset_password_token: req.body.token,
      reset_password_expires: {
        $gt: Date.now(),
      },
    })
    .exec(async function (err, user) {
      console.log("user================>",user)
      if(!user){
        return res.send({
          status:false,
          message:"page expired",
          errmessage:"La page a expiré, demandez à nouveau la réinitialisation du mot de passe"
        })
      }
      if (user && !err) {
        if (req.body.newPassword == req.body.verifyPassword) {
          console.log("password matched");
          let encryptedPassword = await bcrypt.hash(req.body.newPassword, 10);
          user.password = encryptedPassword;
          user.reset_password_expires=undefined;
          user.reset_password_token=undefined;
          user.save(function (err, user) {
            if (!err) {
              var data = {
                to: user.email,
                from: process.env.MAILER_EMAIL_ID,
                template: "reset-password-email-lawyer",
                subject: "Confirmation de la réinitialisation du mot de passe",
                context: {
                  name: user.first_name,
                  // render_reset_password_template,
                },
              };
              var smtpTransport = nodemailer.createTransport({
                host: process.env.MAILER_HOST,
                port: process.env.MAILER_PORT,
                secure:false,
                auth: {
                  user: process.env.MAILER_EMAIL_ID,
                  pass: process.env.MAILER_PASSWORD,
                },
                tls: {
                  rejectUnauthorized: false
                }
              });

              const handlebarOptions = {
                viewEngine: {
                  partialsDir: path.resolve("./views/auth/"),
                  defaultLayout: false,
                },
                viewPath: path.resolve("./views/auth/"),
              };
              // console.log("data in email",data)
              smtpTransport.use("compile", hbs(handlebarOptions));
              smtpTransport.sendMail(data, function (err) {
                if (!err) {
                  return res.status(200).json({
                    status: true,
                    message: "success",
                    errmessage: "",
                    data: null,
                  });
                } else {
                  // console.log(err)
                  return res.send( {
                    status: false,
                    message: "",
                    errmessage: err.message,
                    data: null,
                  });
                }
              });
            } else {
              console.log("it failes here in mail")
              return res.status(422).send({
                status: false,
                message: "",
                errmessage: err.message,
                data: null,
              });
            }
          });
        }
      }
    });
};
const sendemail = (req, res, data, done) => {
  // var smtpTransport = nodemailer.createTransport({
  //   // service: process.env.MAILER_SERVICE_PROVIDER,
  //   host: process.env.MAILER_HOST,
  //   port: process.env.MAILER_PORT,
  //   auth: {
  //     user: process.env.MAILER_EMAIL_ID,
  //     pass: process.env.MAILER_PASSWORD,
  //   },
  //   tls: {
  //     rejectUnauthorized: false
  //   }
  // });
  // console.log(data)
  var smtpTransport = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    secure:false,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    },
    tls: {
      rejectUnauthorized: false
    }
  });

  const handlebarOptions = {
    viewEngine: {
      partialsDir: path.resolve("./views/auth/"),
      defaultLayout: false,
    },
    viewPath: path.resolve("./views/auth/"),
  };
  // console.log("data in email",data)
  smtpTransport.use("compile", hbs(handlebarOptions));
  smtpTransport.sendMail(data, function (err) {
    if (!err) {
      return res.status(200).json({
        status:true,
        message: "success",
        data:null,
        errmessage:""
      });
    } else {
      // console.log(err)
      return done(err);
    }
  });
};
module.exports.changepassword = async (req, res) => {
  try {
    const user = await lawunit.findOne({ _id: req.body.user_id });
    if (!user) {
      return res.status(422).json({ 
        status: false,
        message: "User not found." });
    }
    const isMatch = await bcrypt.compare(req.body.oldPassword, user.password);
    if (!isMatch) {
      return res.status(422).json({ status: false,message: "Incorrect password." });
    }
    const encryptedPassword = await bcrypt.hash(req.body.newPassword, 10);
    user.password = encryptedPassword;
    await user.save();
    return res.status(200).json({ status:true,message: "Password changed successfully." });
  } catch (err) {
    console.log(err);
  }
}

module.exports.verifycode=async(req,res)=>{
  const id=req.body.id;
  const code=req.body.code;
  const user=await lawunit.findById(id);
  if(user){
    if(code.toString()==user.verification_code.toString()){
      user.verification_code="",
      user.isverfied=true;
      user.save().then((result)=>{
       return res.send({
         status:true,
         message:"La vérification est réussie",
         errmessage:"",
         data:null
       })
      })
     }else{
       return res.send({
         status:false,
         message:"",
         errmessage:"Le code n'est pas valide",
         data:null
       })
     }
  }else{
    return res.send({
      status:false,
      message:"",
      errmessage:"no user found",
      data:null
    })
  }
}
