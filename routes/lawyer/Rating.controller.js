const Ratingnreviews = require("../../models/client/Review");
const mongoose=require("mongoose")
const Servicesprovidedmodel=require("../../models/client/Servicesprovided");
module.exports.saveratingandreview = async(req, res) => {
   
  const provider_id = req.body.provider_id;
  const id=mongoose.Types.ObjectId(req.body.service_id);
  const service_id_str=req.body.service_id;
  const isjobdone=await Servicesprovidedmodel.findOne({_id:id});
  const rating=await Ratingnreviews.findOne({service_id:service_id_str});

  if(isjobdone.service_status){
    if(rating){
     await Ratingnreviews.findByIdAndUpdate(rating._id,{
        rating:req.body.rating,
        review:req.body.review
     }).then((rating) => {
     
        res.send({
          status: true,
          message: "La révision a été sauvegardée avec succès",
          data: rating,
          errmessage: "",
        });
      
    }).catch((err) => {  console.log(err); });
    }else{
        const newrating = new Ratingnreviews();
    newrating.provider_id = provider_id;
      newrating.client_id = req.body.client_id;
   
      newrating.service_id=req.body.service_id
      newrating.service_name=req.body.service_name
    newrating.rating = req.body.rating;
    newrating.review = req.body.review;
  
    newrating.client_name=req.body.client_name;
    newrating.provider_name=req.body.provider_name;
    newrating.save().then((rating) => {
     
        res.send({
          status: true,
          message: "La révision a été sauvegardée avec succès",
          data: rating,
          errmessage: "",
        });
      
    }).catch((err) => {  console.log(err); });
    }
  }
  else{
    return res.send({
        status:false,
        message:"service is not completed yet wait for it to be completed"
    })
  }
};

module.exports.saveratingandreviewforprovider = async(req, res) => {
   
  const provider_id = req.body.provider_id;
  
 
  await Ratingnreviews.findOne({
    client_id:req.body.client_id,
    provider_id:req.body.provider_id,
    service_id:{$eq:""}
 }).then((rating) => {
    if(rating){
        res.send({
            status: true,
            message: "La révision a été sauvegardée avec fail",
            data: rating,
            errmessage: "",
          });
    }else{
        const newrating = new Ratingnreviews();
    newrating.provider_id = provider_id;
      newrating.client_id = req.body.client_id;
   
     
    newrating.rating = req.body.rating;
    newrating.review = req.body.review;
  
    newrating.client_name=req.body.client_name;
    newrating.provider_name=req.body.provider_name;
    newrating.save().then((rating) => {
     
        res.send({
          status: true,
          message: "La révision a été sauvegardée avec succès",
          data: rating,
          errmessage: "",
        });
      
    }).catch((err) => {  console.log(err); });
    }
    res.send({
      status: true,
      message: "La révision a été sauvegardée avec succès",
      data: rating,
      errmessage: "",
    });
  
}).catch((err) => {  console.log(err); });
 
  
  
};
module.exports.getratingandreviewbylawyerid = async(req, res) => {
    const provider_id = req.params.teacher_id;
   await Ratingnreviews.find({provider_id:provider_id}).then((rating) => {
        res.send({
            status: true,
            message: "La revue a été récupérée avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
      res.send({
        status: false,
        message: "",
        data: null,
        errmessage: "Erreur dans la récupération des avis ",
      });
    })
}
module.exports.getallratingnreview= (req, res) => {
    Ratingnreviews.find({}).then((rating) => {
        res.send({
            status: true,
            message: "Les coordonnées bancaires ont été sauvegardées avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la mise à jour de Bank Details",
        })
    })
}
module.exports.getratingandreviewbyclientid= async(req, res) => {
    const client_id=req.params.student_id;
   await Ratingnreviews.find({client_id:client_id}).then((rating) => {
        res.send({
            status: true,
            message: "Les coordonnées bancaires ont été sauvegardées avec succès",
            data: rating,
            errmessage: "",

        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la mise à jour de Bank Details",
        })
    })
}
module.exports.getratingandreviewbybyratingid= (req, res) => {
    const rating_id=req.params.rating_id;
    Ratingnreviews.find({rating_id:rating_id}).then((rating) => { 
        res.send(rating);
    }).catch((err) => {
        res.send(err);
    })
}
module.exports.deleteratingandreview= (req, res) => {
    const rating_id=req.params.rating_id;
    Ratingnreviews.findByIdAndRemove(rating_id).then((rating) => {
        res.send({
            status: true,
            message: "La révision a été supprimée avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la récupération des avis ",
        });
    })
}
module.exports.editratingandreview= (req, res) => {
    let {rating_id,rating,review}=req.body;
    const data={
        ...(rating&&{rating}),
        ...(review&&{review})
    }
    Ratingnreviews.findByIdAndUpdate(rating_id,data,{new:true}).then((rating) => {
        res.send({
            status: true,
            message: "La révision a été modifiée avec succès",
            data: rating,
            errmessage: "",
        });
    }).catch((err) => {
        res.send({
            status: false,
            message: "",
            data: null,
            errmessage: "Erreur dans la récupération des avis ",
        });
    })
}
