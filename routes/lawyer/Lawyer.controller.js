const Services = require("../../models/lawyer/LawyerServices");
const Lawunit = require("../../models/lawyer/Lawunit");
const Adminpayoutmodel = require("../../models/admin/AdminPayout");
const Cabinatelawyer = require("../../models/lawyer/Cabinatelawyer");
const iabnmodel=require("../../models/lawyer/Iabn");
const boosterpackage=require("../../models/admin/Boosterpackage")
const {shuffle}=require("../../modules/shuffle");
const NotificationPermission=require("../../models/NotificationPermission");
const mongoose=require("mongoose");
const notificationmodel = require("../../models/Notifications");
const bcrypt = require("bcryptjs");
const Servicesprovidedmodel=require("../../models/client/Servicesprovided");
const Subscriptionpackagesmodel = require("../../models/admin/SubscriptionPackages");
const subscriptionmodel = require("../../models/lawyer/Subscriptions");
const setSchedulemodel=require("../../models/lawyer/Schedule");
const setSchedulemodelC=require("../../models/lawyer/CourseSchedule");
const contactHoursmodel=require("../../models/lawyer/ContactHours");
const {getdateinadvance,frenchDayDate}=require("../../modules/dates");
const {getdistance}=require("../../modules/getdistance");
const Adminmodel = require("../../models/admin/Admin");
const root_url=process.env.BASE_URL;
const User = require("../../models/auth/user");
const axios = require("axios");
// const fs = require('fs');
const path=require('path');
const pdf = require('html-pdf');
const ejs = require("ejs");
const Favriout = require("../../models/client/Favriout.js");

const getlatlong = async (address) => {
  const apikey = process.env.GOOGLE_MAPS_API_KEY;

  let data;
  let url=`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${apikey}`
  try {
    data = await axios
      .get(
        url
      )
      .then((result) => {
        console.log(result)
         console.log(url)
         if(result.data.results[0].geometry.location){
          return result.data.results[0].geometry.location;
        }else{
          return null
        }
        
      });
  } catch (err) {
    data= null
  }
  return data;
};
function canJSON(value) {
  try {
    JSON.parse(value);
      return true;
  } catch (ex) {
      return false;
  }
}
module.exports.createlawyer = async (req, res) => {
  try {
    const islawyeravailable = await Lawunit.findOne({ _id: req.body.id });
    if (!islawyeravailable) {
      res.send({
        status: false,
        errmessage: "Lawyer does'nt exists",
        message:"",
        data: null,
      });
    } else {
      let photo;
      let dlfront;
      let dlback;
      let passport;
      let idfront;
      let idback;

      if (req.files?.photo) {
        if (req.files?.photo) {
         
          photo = "userprofile/" + req.files.photo[0].filename;
        } 
      }
     

      const {
        latlong,
        first_name,
        email,
        id,
        last_name,
        dob,
        phone,
        address,
        competence,
        avalible_instant,
        password,
        newpassword
      } = req.body;
      
      let encryptedPassword
      if(password){
       encryptedPasswordtest = await bcrypt.hash(password, 10);
       encryptedPassword= await bcrypt.hash(newpassword, 10);
       const user = await Lawunit.findOne({ _id:id });
       console.log(user)
      const ispasswordcorrect=await bcrypt.compare(password, user.password);
      if(!ispasswordcorrect){
       return res.send({
         status:false,
         message:"",
         errmessage:"old password is not correct",
         data:null
       })
      }
      }
    let newlatlong

    if(latlong){
       try{  
     newlatlong=JSON.parse(latlong);
    }catch(e){
      newlatlong=latlong
    }
  }else{
    newlatlong=[];
  }
      
      const data = {
        ...(first_name && { 'first_name':first_name }),
       ...(email&&{email:email}),
        ...(last_name && { 'last_name':last_name }),
       
        ...(photo && { 'photo':photo }),
       
       
        ...(phone && { 'phone':phone }),
       
        ...(newpassword&&{password:encryptedPassword}),
       
        ...(newlatlong.length>0 && {'location_cor':{type:"Point","coordinates":[newlatlong[0],newlatlong[1]]}}),
        ...(avalible_instant&&{avalible_instant:true}),
        ...(competence&&{competence:competence}),
        ...(address&& { 'address':{address:address,latlong:newlatlong}}) ,
      };
      // console.log("data==============================>",data);
      // console.log(data);
      Lawunit.findByIdAndUpdate(id, data)
        .then((lawyer) => {
          res.send({
            status: true,
            data: lawyer,
            message: "Avocat mis à jour avec succès",
            errmessage: "",
          });
        })
        .catch((err) => {
          res.send({
            status: false,
            data: null,
            message: "",
            errmessage:"L'avocat n'a pas été mis à jour avec succès"
          });
        });
    }
  } catch (err) {
    console.log(err);
    res.send(err);
  }
};

module.exports.getalllawyersbackup = async (req, res) => {
    
  let filter = {};
  let cfilter={};
  let schedule=[];
  const user=await User.findById(req.body.id)
  const blocked_teachers=user.blocked_teachers.map(mongoose.Types.ObjectId);
  console.log("blocked_teacher",blocked_teachers)
  
  let cdata=req.body;
  // {
  //   "avalible_today":false,
  // "availible_in_3_days":false,
  // "lawyer_name":"",
  // "mode_of_consultation":"",
  // "expertise_in":"",
  // "language_spoken":"",
  // "sort":1,
  
  
  // "from":{"lat":"40.6971494","lng":"-74.2598745"}}

  let sort=cdata.sort;
  let getlocation=cdata.from;
    //avalibletoday schedule
    const avalible_today=cdata.avalible_today;
  // console.log("avalible_today",avalible_today)
    let avalible_today_array=[];
    if(avalible_today){
      availibletodaydate=getdateinadvance(0);
      nextdate=getdateinadvance(1)
      // console.log(availibletodaydate,nextdate);
      const todayavalibiltyschedule=await setSchedulemodel.find({
        $and:[
          {
            date:{$lt: nextdate},
          },{
            date:{$gte: availibletodaydate},
          }
        ]
      });
      console.log("todayavalibiltyschedule",todayavalibiltyschedule,availibletodaydate);
      for(let i=0;i<todayavalibiltyschedule.length;i++){
       let newid= mongoose.Types.ObjectId(todayavalibiltyschedule[i].lawyer_id);
        avalible_today_array.push(newid);
      }
      filter["_id"]={$in:avalible_today_array};
      
    }
   
    //avalible in three days schedule
    const avalible_threedays=cdata.availible_in_3_days;
    let threedayadvancedate=getdateinadvance(3);
    let avalibilityarray=[];
    if(avalible_threedays){
   
    const avaliblity=await setSchedulemodel.find({
      $and:[
        {
          date:{$lte: threedayadvancedate},
        },{
          date:{$gte: new Date().toISOString()},
        }
      ]
    });
     
      for(let i=0;i<avaliblity.length;i++){
        let newid= mongoose.Types.ObjectId(avaliblity[i].lawyer_id);
        avalibilityarray.push(newid);
        // avalibilityarray.push(avaliblity[i]._id);
      }
      filter["_id"]={$in:avalibilityarray};
      schedule=avalibilityarray;
    }
    
    if(!avalible_today && !avalible_threedays){
      filter["$and"]=[{"_id":{$ne:null}},{"_id":{$nin:blocked_teachers}}]
    }

  let servicemode;
  // if(cdata.mode_of_consultation){
  //   if(cdata.mode_of_consultation=="online"){
  //     servicemode= "modesofservice.online.isonline";
  //     filter["modesofservice.online.isonline"]=true;
  //   }else if(cdata.mode_of_consultation=="offline"){
  //     servicemode= "modesofservice.offline.isoffline";
  //     filter["modesofservice.offline.isoffline"]=true;
  //   }
  // }
  if(cdata.mode){
    if(cdata.mode=="audio"){
      // servicemode= "modesofservice.online.isonline";
      cfilter["mode"]="audio";
    }else if(cdata.mode=="video"){
      // servicemode= "modesofservice.offline.isoffline";
      cfilter["mode"]="video";
    }
  }

  let durationlend,durationhend;
 
  if(cdata.duration){
    let du=cdata.duration.split(",");
   durationlend=parseInt(du[0]);
   durationhend=parseInt(du[1]);
    console.log("typeof durationarray[0]",typeof durationlend,"typeof durationarray[1]",typeof durationhend)
    cfilter["$and"]=[
      {
          "duration":{$gte:durationlend}
      },
      {
          "duration":{$lte:durationhend}
      }
    ]
  }
  if(cdata.subject){
    cfilter["catagory"]=cdata.subject
  }
  let teacher_name=cdata.teacher_name
  if(teacher_name){
   if(teacher_name.split(" ").length==1){
      // filter["first_name"]=teacher_name;
      filter["$or"]=[
        {first_name:new RegExp(teacher_name.split(" ")[0],"i")},{last_name:new RegExp(teacher_name.split(" ")[0],"i")}
      ]
   }
   else if(teacher_name.split(" ").length==2){
    filter["first_name"]=new RegExp(teacher_name.split(" ")[0],"i");
    filter["last_name"]=new RegExp(teacher_name.split(" ")[1],"i");
   }
 }
  const languageregex=new RegExp(cdata.language_spoken,"i");
  // filter["language_spoken"]={$regex:languageregex} ;
  const expertiseregex=new RegExp(cdata.expertise_in,"i");
  filter["expertise_in"]={$regex:expertiseregex} ;
  // console.log(avalibilityarray,filter.availible_in_3_days,filter.language_spoken,servicemode)
 
  // filter["$or"]=[
  //   {"modesofservice.online.isonline":true},{"modesofservice.offline.isoffline":true}
  // ]
  
  console.log("filter",cfilter,filter,"cfilter")
  const Lawyers = await Lawunit.aggregate([
  {$match:filter},
  {$addFields:{lawyer_id:{"$toString":"$_id"}}},
  {$lookup:{from:"schedules",localField:"lawyer_id",foreignField:"teacher_id",as:"schedule"}},
  {$lookup:{
  from:"lawyerservices",
  // localField:"lawyer_id",
  // foreignField:"teacher_id",
  let: { teacher_id:"$lawyer_id" },
  pipeline:[
    {
      $match:{ $expr: { $eq: ["$teacher_id", "$$teacher_id"]}},
    
    }],
  as:"coursesr"}},
  {$lookup:{
    from:"ratingnreviews",
    // localField:"lawyer_id",
    // foreignField:"teacher_id"
    let: { nduration: "$duration",teacher_id:"$lawyer_id" },
    pipeline:[
      {
        $match:{ $expr: { $eq: ["$teacher_id", "$$teacher_id"]}},
      
      },
      {$addFields:{teacher_id:{"$toString":"$teacher_id"},
      obj_client_id:{"$toObjectId":"$student_id"}}},
      {
        $lookup:{
          from:"users",
          localField:"obj_client_id",
          foreignField:"_id",
          as:"user"
        }
      }

    ]
    ,as:"ratings"}},
  // {$lookup:{
  //   from:"lawyerservices",
  //   localField:"lawyer_id",
  //   foreignField:"teacher_id",
  //   let: { rating: "$rating" },
  //   pipeline:{
  //     avgRating:{$divide:[{$sum:{},$sum:{}]}}
  //   }

  // }},
  
  
  ]);
  if(cdata.from){
    let lat1=getlocation.lat?getlocation.lat:'';
    let long1=getlocation.lng?getlocation.lng:'';
    for(let i=0;i<Lawyers.length;i++){
      let lat2;
      let long2;
      try{
        console.log(Lawyers[i].address)
       lat2 =Lawyers[i].address[0].latlong[0]?Lawyers[i].address[0].latlong[0]["lat"]:'';
       long2=Lawyers[i].address[0].latlong[0]?Lawyers[i].address[0].latlong[0]["lng"]:'';
      }catch(e){
       
      }
      if(lat2 && long2){
        let distance=await getdistance(lat1,long1,lat2,long2);
        console.log(distance)
      Lawyers[i].distance=distance;
      }else{
        Lawyers[i].distance=0;
      }
      
    }
    Lawyers.sort(function(a,b){
      return a.distance - b.distance;
    });
  }
  let newdata=Lawyers;
  for(let i=0;i<Lawyers.length;i++){
    const lawyer=Lawyers[i];
    const courses=lawyer.ratings;
    let lrating=0;
    // console.log("courses",courses)
   if(courses.length>0){
     for(let j=0;j<courses.length;j++){
       const course=courses[j];
      //  console.log(course)
       lrating=lrating+course.rating;
       
      
      }
   }else{
     
   }
   let totalratings=courses.length;
     let avgRating=lrating/totalratings;
     Lawyers[i].avgRating=Math.ceil(avgRating);
     // newdata=Lawyers
    //  console.log("lrating",lrating,"totalratings",totalratings,"courses.length",courses.length)
    
   }
  if(cdata.rating){
    
    newdata=Lawyers?.filter((elem)=>elem.avgRating==cdata.rating);
   
  }

  // if(cdata.duration!=""){
  //  let du=cdata.duration.split(",");
  //  durationlend=du[0];
  //  durationhend=du[1];
  //  console.log("durationlend",durationlend,"durationhend",durationhend);
  // //  newdata=Lawyers.filter(function(row){
  // //   // console.log("elem",elem)
  // //   return Object.keys(row).some((key) => {    
  // //     console.log("key",key)                          
  // //    if(row[key]["courses"].lenght>0){
  // //     if(row[key]["courses"]["duration"]<=durationhend&&row[key]["courses"]["duration"]>=durationlend) {                                // if the current property is a string
  // //       return true;     // then check if it contains the search string
  // //   } 
  // //   return false; 
  // //    }            else{
  // //     return false
  // //    }                                        // return false for any other type (not really necessary as undefined will be returned implicitly)
  // // });
  // //  })
  // }else{
  //   durationlend=0;
  //  durationhend=5000;
  //  console.log("durationlend",durationlend,"durationhend",durationhend)
  // }




  return res.send({
    status:true,
    message:"teachers fetched successfully",
    errmessage:"",
    data:newdata
  })
 
};







module.exports.getallofflinecourses= async (req, res) => {
    
  let filter = {};
  let cfilter={
    
  };
  const user=await User.findById(req.body.id)
  const blocked_teachers=user.blocked_teachers;
  console.log("blocked_teacher",blocked_teachers)
  let schedule=[];
  let cdata=req.body;
  let sortit=cdata.pricesort;
  if(cdata.contenttype!=""){
    cfilter['content_type']=new RegExp(cdata.contenttype,"i");
  }
  cfilter["type"]="offline"
  // if(cdata.duration){
  //   const durationarray=cdata.duration.split(",");
  //   cfilter["$and"]=[
  //     {
  //         "duration":{$gte:durationarray[0]}
  //     },
  //     {
  //         "duration":{$lte:durationarray[1]}
  //     }
  //   ]
  // }
  if(cdata.subject){
    cfilter["catagory"]=new RegExp(cdata.subject,"i")
  }
 
  cfilter["teacher_id"]={$nin:blocked_teachers}
  const Lawyers = await Services.aggregate([
  {$match:cfilter},
  {$addFields:{teacher_id:{"$toString":"$teacher_id"},obj_client_id:{"$toObjectId":"$teacher_id"}}},
  
  {$lookup:{from:"lawunits",
  localField:"obj_client_id",
  foreignField:"_id",
 
  as:"teachers"}},
  {$lookup:{from:"ratingnreviews",localField:"teacher_id",foreignField:"teacher_id",as:"ratings"}},
  
  {$sort:{"price":sortit}},
  
  ]);
 let newdata=Lawyers;

 for(let i=0;i<Lawyers.length;i++){
  const lawyer=Lawyers[i];
  const courses=lawyer.ratings;
  let lrating=0;
  console.log("courses",courses)
 if(courses.length>0){
   for(let j=0;j<courses.length;j++){
     const course=courses[j];
     console.log(course)
     lrating=lrating+course.rating;
     
    
    }
 }else{
   
 }
 let totalratings=courses.length;
      let avgRating=lrating/totalratings;
      Lawyers[i].avgRating=Math.ceil(avgRating);
      // newdata=Lawyers
  if(cdata.rating){
    
   
    
      console.log("lrating",lrating,"totalratings",totalratings,"courses.length",courses.length)
     newdata=Lawyers?.filter((elem)=>elem.avgRating==cdata.rating);
    }
  }



  return res.send({
    status:true,
    message:"teachers fetched successfully",
    errmessage:"",
    data:newdata
  })
 
};
module.exports.getLawyerbyId = async(req, res) => {
  const id = req.params.id;
  const client_id=req.params.client_id;
  const isfavorite = await Favriout.findOne({lawyer_id:id,client_id:client_id});
  console.log(isfavorite)
  Lawunit.findOne({_id:id,is_deleted:false})
    .then(async(lawyer) => {
      console.log(lawyer);
      let newlawyer=JSON.parse(JSON.stringify(lawyer));
       newlawyer.isfavorite=isfavorite?true:false;
      if(lawyer?.cabinetname){
        console.log(lawyer.cabinetname,"it is true")
       await Cabinatelawyer.find({cabinetname:lawyer.cabinetname})
       .then((cabinatelawyer)=>{
         console.log(cabinatelawyer)
         newlawyer.cabinatelawyer=cabinatelawyer;
          res.send({
        status: true,
        data: newlawyer,
        message: "Lawyer fetched successfully",
        errmessage: "",
      });
       })
      }
     else{
       console.log(lawyer.cabinetname,"it is false")
      res.send({
        status: true,
        data: newlawyer,
        message: "Lawyer fetched successfully",
        errmessage: "",
      });
     }
    
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        data:null,
        message: "",
        errmessage:err.message,
      });
    });
};
module.exports.getLawyerbyIdbylawyer = async(req, res) => {
  const id = req.params.id;
 
 
  await Lawunit.findOne({_id:id,is_deleted:false})
    .then(async(lawyer) => {
      console.log(lawyer);
      let newlawyer=JSON.parse(JSON.stringify(lawyer));
      
      if(lawyer?.cabinetname){
        console.log(lawyer.cabinetname,"it is true")
       await Cabinatelawyer.find({cabinetname:lawyer.cabinetname})
       .then((cabinatelawyer)=>{
         console.log(cabinatelawyer)
         newlawyer.cabinatelawyer=cabinatelawyer;
          res.send({
        status: true,
        data: newlawyer,
        message: "Lawyer fetched successfully",
        errmessage: "",
      });
       })
      }
     else{
       console.log(lawyer.cabinetname,"it is false")
      res.send({
        status: true,
        data: newlawyer,
        message: "Lawyer fetched successfully",
        errmessage: "",
      });
     }
    
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        data:null,
        message: "",
        errmessage:err.message,
      });
    });
};
module.exports.updatelawyer = async (req, res) => {
  let photo;
  console.log(req.file);
  console.log(req.body);
  if (req.file) {
    photo = "lawyerprofile/" + req.file.filename;
  }
  const {
    cabinetname,
    first_name,
    last_name,
    address,
    phone,
    dob,
    expertise_in,
    presentation,
    legal_information,
    language_spoken,
    onlineprice,
    offlineprice,
    isonline,
    isoffline,
    opening_hours,
    location_serving,
    id,
  } = req.body;
  // if(persons){
  //   persons.forEach(async (person) => {
  //     let pfirst_name=person.first_name;
  //     let plast_name=person.last_name;
  //     let pexpertise_in=person.expertise_in;
  //     let plocation_serving=person.location_serving;
  //     const pdata={
  //       ...(pfirst_name && { first_name:pfirst_name }),
  //       ...(plast_name && { last_name:plast_name }),
  //       ...(cabinetname && { cabinetname }),
  //       ...(pexpertise_in && { expertise_in:pexpertise_in }),
  //       ...(location_serving && { location_serving:plocation_serving }),
  //       }
  //       Cabinatelawyer.updateOne({
  //         cabinetname:cabinetname,
  //         first_name:pfirst_name,
  //         last_name:plast_name,
  //       },pdata);
  //   })
  // }
  const data = {
    ...(first_name && { first_name }),
    ...(last_name && { last_name }),
    ...(cabinetname && { cabinetname }),
    ...(photo && { photo }),
    ...(address && { address }),
    ...(phone && { phone }),
    ...(dob && { dob }),
    ...(expertise_in && { expertise_in }),
    ...(presentation && { presentation }),
    ...(legal_information && { legal_information }),
    ...(language_spoken && { language_spoken }),
    ...(onlineprice && { "modesofservice.online.price": onlineprice }),
    ...(offlineprice && { "modesofservice.offline.price": offlineprice }),
    ...(isonline && { "modesofservice.online.isonline": isonline }),
    ...(isoffline && { "modesofservice.offline.isoffline": isoffline }),
    ...(opening_hours && { opening_hours }),
    ...(location_serving && { location_serving }),
  };
  let prevcabinatename = await Lawunit.findById(id, { cabinetname: 1, _id: 0 });
  console.log(prevcabinatename);
  Lawunit.findByIdAndUpdate(id, data)
    .then((lawyer) => {
      if (cabinetname) {
        Cabinatelawyer.updateMany(
          {
            cabinetname: prevcabinatename.cabinetname,
          },
          { cabinetname: cabinetname }
        )
          .then(() => {
            res.send({
              lawyer: lawyer,
              status: true,
              message: "Lawyer updated successfully",
            });
          })
          .catch((err) => {
            console.log(err);
            res.send(err);
          });
      } else {
        res.send({
          lawyer: lawyer,
          status: true,
          message: "Lawyer updated successfully",
        });
      }
    })
    .catch((err) => {
      res.send(err);
    });
};
module.exports.updatecabinatelawyer = (req, res) => {
  let photo;
  console.log(req.file);
  if (req.file) {
    photo="lawyerprofile/" + req.file.filename
    }
    console.log(photo);
  const {
    first_name,
    last_name,

    expertise_in,

    location_serving,
    id,
  } = req.body;
  const data = {
    ...(first_name && { first_name }),
    ...(last_name && { last_name }),
    ...(expertise_in && { expertise_in }),
    ...(location_serving && { location_serving: location_serving }),
    ...(photo && { photo }),
  };
  Cabinatelawyer.findByIdAndUpdate(id, data)
    .then((lawyer) => {
      console.log(lawyer);
      res.send({
        status: true,
        data: lawyer,
        message: "Lawyer updated successfully",
        errmessage: "",
      });
    })
    .catch((err) => {
      console.log(err)
      res.send(err);
    });
};
module.exports.deletelawyer = async (req, res) => {
  const id = req.params.id;
  const teacher=await Lawunit.findById(id);
  teacher.email=teacher.email+"_"+Math.floor(Math.random() * 10) + 1;
  teacher.is_deleted=true;
  teacher.save().then(async(result)=>{
    await Services.deleteMany({
      provider_id:id
    }).then((reu)=>{
      return res.send({
      lawyer: result,
      status: true,
      message: "Avocat supprimé avec succès",
      errmessage: "",
    });
    })
    
  }).catch((err)=>{
    console.log("err",err);
    return res.send({
      status:false,
      errmessage:err.message
    })
  })
};
module.exports.deletecabinatelawyer = (req, res) => {
  const id = req.params.id;
  Cabinatelawyer.findByIdAndUpdate(id, { email: null, is_deleted: true })
    .then((lawyer) => {
      res.send({
        status: true,
        data: lawyer,
        message: "Avocat supprimé avec succès",
        errmessage: "",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "",
        data:null,
        errmessage:"La suppression de l'avocat a échoué" ,
      });
    });
};
module.exports.getlawyersbycabinetname = (req, res) => {
  const cabinetname = req.params.cabinetname;
  Cabinatelawyer.find({ cabinetname,is_deleted:false })
    .then((lawyer) => {
      res.send({
        status: true,
        data: lawyer,
        message: "",
        errmessage: "",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "",
        data:null,
        errmessage:"La suppression de l'avocat a échoué" ,
      });
    });
};

module.exports.saveiabn = async(req, res) => {
  const {
    iabn,
    lawyer_id
  }=req.body;
  const isiabnthere = await iabnmodel.findOne({ lawyer_id: lawyer_id });
  if (isiabnthere) {
   await iabnmodel.findByIdAndUpdate(isiabnthere._id, { iabn: iabn }).then(() => {
      res.send({
        status: true,
        message: "Iabn a été mis à jour avec succès",
        errmessage: "",
        data:null
      });
    
   }).catch((err) => {
      res.send({
        status: false,
        message: "",
        data:null,
        errmessage: "La mise à jour de l'iabn a échoué"
      });
   })
  }else{
    const data = {
      ...(iabn && { iabn }),
      ...(lawyer_id && { lawyer_id }),
    };
   await iabnmodel.create(data)
      .then((iabn) => {
        res.send({
          status: true,
          message: "Iabn enregistré avec succès",
          errmessage: "",
          data:iabn
        });
      })
      .catch((err) => {
        res.send({
          status: false,
          message: "",
          data:null,
          errmessage: "La mise à jour de l'iabn a échoué"
        });
      });
  }
}
module.exports.deleteiabn = async(req, res) => {
  const lawyer_id=req.params.lawyer_id;
 await iabnmodel.findOneAndDelete({ lawyer_id: lawyer_id }).then(() => {
    res.send({
      status: true,
      message: "Iabn supprimé avec succès",
      errmessage: "",
      data:null
    });
  }).catch((err) => {
    res.send({
      status: false,
      message: "",
      data:null,
      errmessage: "La suppression de l'iabn a échoué"
    });
  })
}
module.exports.getiabn = async(req, res) => {
  const lawyer_id=req.params.lawyer_id;
 await iabnmodel.findOne({ lawyer_id: lawyer_id }).then((iabn) => {
    res.send({
      status: true,
      message: "iabn fetched successfully",
      data: iabn,
      errmessage: "",
    });
  }).catch((err) => {
    res.send({
      status: false,
      message:"",
      data:null,
      errmessage: "La récupération de l'iabn a échoué"
    });
  })
}
module.exports.getallnotifications = async(req, res) => {
  const id=req.params.id;
 await notificationmodel.find({ to_id: id }).then((notifications) => {
    res.send({
      status: true,
      message: "Notifications récupérées avec succès",
      data: notifications,
      errmessage: "",
    });
  }).catch((err) => {
    res.send({
      status: false,
      message: "",
      data:null,
      errmessage: "La récupération des notifications a échoué"
    });
  })
}
module.exports.getrevenue = async(req, res) => {
const lawyer=await Lawunit.findOne({_id:req.params.lawyer_id});
if(!lawyer){
  return  res.send({
    status: true,
    message: "Aucun enseignant trouvé",
    errmessage: "",
  })
}
const adminpayout=await Adminpayoutmodel.find({teacher_id:req.params.lawyer_id});
const totalpaidamount= lawyer.totalpaidamount;
const totalpendingamount= lawyer.totalpendingamount;
    const alltransactions = await Servicesprovidedmodel.find({teacher_id:req.params.lawyer_id,payment_status:true,is_service_rejected:false,service_status:true});
    let allamount=0;
    alltransactions.forEach(element => {
      allamount+=element.payment_amount;
    })
    const reservations = await Servicesprovidedmodel.find({teacher_id:req.params.lawyer_id,payment_status:true,service_status:true,is_service_rejected:false});
    
    const totoalnumberofreservations = alltransactions.length;
    const pendingapprovals = alltransactions.filter(
      (reservation) => reservation.service_status === false
    );
    let pendingapproval = 0;
    pendingapprovals.forEach((reservation) => {
      pendingapproval += reservation.payment_amount;
    })
    // const totalearnings = reservations.reduce(
    //   (total, reservation) => total + reservation.payment_amount?parseInt(reservation.payment_amount):0,
    //   0
    // );
    let totalearnings = 0;
    reservations.forEach((reservation) => {
      totalearnings += reservation.payment_amount?parseInt(reservation.payment_amount):0;
    })
    const totaltransections = totalearnings;
  
  
    res.send({
      status: true,
      message: "Recettes perçues avec succès",
      data:{
        reservations,
        adminpayouts:adminpayout,
        totalearnings:allamount,
        totaltransections:totalpaidamount,
        totalpendingamount:totalpendingamount,
        totoalnumberofreservations
  
      },
      errmessage: "",
    })
  }
module.exports.getallsubscriptionpackages = async(req, res) => {
  const packages = await Subscriptionpackagesmodel.find({});
  res.send({
    status: true,
    message: "packages fetched successfully",
    data:packages,
    errmessage: "",
  })
}
module.exports.getsubscriptioninvoice = async(req, res) => {
 try{
  const sub_id = req.params.sub_id;
  let invoice = await subscriptionmodel.findOne({_id:sub_id});
  const frenchdate=frenchDayDate(invoice.date_of_transaction);
  invoice=JSON.parse(JSON.stringify(invoice));
  invoice["frenchdate"]=frenchdate;
  const filepath=path.join(__dirname, "../../views/admin-panel/lawyersubscriptioninvoice.ejs");
  console.log(filepath)
  const html = await ejs.renderFile(filepath,{invoice:invoice,base_url:root_url});
  console.log(html)
  const options = { format: 'Letter' };
  
pdf.create(html, options).toFile(path.join(__dirname, "../../views/admin-panel/public/invoices/")+'invoice.pdf', function(err, result) {
  if (err) return console.log(err);
  res.sendFile(path.join(__dirname, "../../views/admin-panel/public/invoices/invoice.pdf"));
});
  
 }catch(err){
   res.send(err.message);
 }
}
module.exports.getbookeddates=async(req,res)=>{
const lawyer_id=req.params.lawyer_id;
const bookeddates=await Servicesprovidedmodel.find({lawyer_id:lawyer_id}); 
const datesarrayofonline=[];
const datesarrayofoffline=[];
for(let i=0;i<bookeddates.length;i++){
  if(bookeddates[i].mode_of_service=="online"){
    datesarrayofonline.push(bookeddates[i].date_availed);
  }else if(bookeddates[i].mode_of_service=="offline"){
    datesarrayofoffline.push(bookeddates[i].date_availed);
  }
}
res.send({
  status:true,
  message:"Rendez-vous récupéré avec succès",
  data:{
    datesarrayofonline,
    datesarrayofoffline
  },
  errmessage:""
})
}
module.exports.getalllawyersbycabinatename = async(req, res) => {
  const cabinetname = req.params.cabinate_name;
  const lawyers = await Cabinatelawyer.find({ cabinetname: cabinetname });
  res.send({
    status: true,
    message: "Avocats recherchés avec succès",
    data: lawyers,
    errmessage: "",
  });
}
module.exports.getlawyersappointments=async(req,res)=>{
  const lawyer_id=req.params.lawyer_id;
  const appointments=await Servicesprovidedmodel.find({lawyer_id:lawyer_id,payment_status:true});
  res.send({
    status:true,
    message:"appointments fetched successfully",
    errmessage:"",
    data:appointments
  })
}
module.exports.getcurrentcommisionsetbyadmin=async(req,res)=>{
  const adminmodel=await Adminmodel.findOne({});
  const commision=adminmodel.commissions;
  res.send({
    status:true,
    message:"current commision set fetched successfully",
    errmessage:"",
    data:commision
  });
}
module.exports.editlawyeraddress=async(req,res)=>{
  // console.log(req.body);
  // const updatedlawyer=await Lawunit.findOne({_id:req.body.lawyer_id});
  // console.log(updatedlawyer,updatedlawyer.address[0],updatedlawyer.address[0].address);
  const lawyer_id=req.body.lawyer_id;
  const address=req.body.address;
  const address_remark=req.body.address_remark;
  const index=req.body.index;
  let latlongarray=[];
  if(address){
    latlongarray1=await getlatlong(address);
     console.log("in if and address",latlongarray1)
     latlongarray.push(latlongarray1);
     console.log(latlongarray.length)
   }
  const updatedlawyer=await Lawunit.findOne({_id:lawyer_id});
  updatedlawyer.address[index]={address:address,address_remark:address_remark,latlong:latlongarray};
  updatedlawyer.save();
  res.send({
    status:true,
    message:"address updated successfully",
    errmessage:"",
    data:updatedlawyer
  });
}
module.exports.deletelawyeraddress=async(req,res)=>{
  const lawyer_id=req.body.lawyer_id;
  const index=req.body.index;
  const updatedlawyer=await Lawunit.findOne({_id:lawyer_id});
  updatedlawyer.address.splice(index,1);
  updatedlawyer.save();
  res.send({
    status:true,
    message:"address deleted successfully",
    errmessage:"",
    data:updatedlawyer
  });
}
module.exports.checkexpirydatesbysubscriptionID=async(req,res)=>{
  const subscription_id=req.params.sub_id;
  // const lawyer=await Lawunit.findOne({_id:lawyerid});
  const today=new Date();
  const todaydate=today.getDate();
  const todaymonth=today.getMonth()+1;
  const todayyear=today.getFullYear();
  const todaystring=todayyear+"-"+todaymonth+"-"+todaydate;
  const todaydateobject=new Date(todaystring);
  
  const lawyer_suscription=await subscriptionmodel.findById(subscription_id);
  console.log(lawyer_suscription);
  const ifsubscriptionexpired=todaydateobject>lawyer_suscription.end_date;
  console.log("ifsubscriptionexpired",ifsubscriptionexpired,"todaydateobject",todaydateobject,"end date",lawyer_suscription.end_date)
  res.send({
    status:true,
    message:"expiry dates fetched successfully",
    errmessage:"",
    data:{
      "expiry_date":lawyer_suscription.end_date,
      "ifsubscriptionexpired":ifsubscriptionexpired
    }
  });
}
module.exports.getallnotificationsbyteacherid1=async(req,res)=>{
  const lawyer_id=req.params.teacher_id;
  // const notifications=await notificationmodel.find({to_id:lawyer_id});
  const notifications=await notificationmodel.aggregate([{$match:{to_id:lawyer_id}},
  {$sort:{date:-1}}
]);
 
  res.send({
    status:true,
    message:"notifications fetched successfully",
    errmessage:"",
    data:notifications
  })
}
module.exports.getallnotificationsbyteacherid=async(req,res)=>{
  const teacher_id=req.body.provider_id;
  const type=req.body.type;
  let count=0;
 
  if(type=="unread"){
    count=(await notificationmodel.find({"to_id":teacher_id,"status":"unread"})).length;
    return res.send({
      status:true,
      message:"notifications fetched successfully",
      errmessage:"",
     
      count:count
    })
  }
  
  // const notifications=await notificationmodel.find({to_id:lawyer_id});
  const notifications=await notificationmodel.aggregate([{$match:{to_id:teacher_id}},
  {$sort:{date:-1}}
]);
if(type=="read"){
  await notificationmodel.updateMany({"to_id":teacher_id},{
    $set:{"status":"read"}
  });
  return res.send({
    status:true,
    message:"notifications fetched successfully",
    errmessage:"",
   data:notifications,
    count:count
  })
}
  
}

module.exports.getallnotificationsbystudentid1=async(req,res)=>{
  const client_id=req.params.student_id;
  // const notifications=await notificationmodel.find({to_id:client_id});
  const notifications=await notificationmodel.aggregate([{$match:{to_id:client_id}},
     {$sort:{date:-1}}
    ]);
  res.send({
    status:true,
    message:"notifications fetched successfully",
    errmessage:"",
    data:notifications
  })
}
module.exports.getallnotificationsbystudentid=async(req,res)=>{
  const student_id=req.body.client_id;
  const type=req.body.type;
  let count=0;

  if(type=="unread"){
    count=(await notificationmodel.find({"to_id":student_id,"status":"unread"})).length;
    return res.send({
      status:true,
      message:"notifications fetched successfully",
      errmessage:"",
     
      count:count
    })
  }
  // const notifications=await notificationmodel.find({to_id:client_id});
  const notifications=await notificationmodel.aggregate([{$match:{to_id:student_id}},
     {$sort:{date:-1}}
    ]);
  
    if(type=="read"){
      await notificationmodel.updateMany({"to_id":student_id},{
        $set:{"status":"read"}
      })
      return res.send({
        status:true,
        message:"notifications fetched successfully",
        errmessage:"",
        data:notifications,
        count:count
      })
    }
}
module.exports.checkifemailexists=async(req,res)=>{
  const email=req.body.email
  const oldUseru = await User.findOne({ email });
const oldUserP = await Lawunit.findOne({ email });
let type;
let userdata;
let isregisterd=false;
if(oldUseru){
  type="user";
  isregisterd=true;
  userdata=oldUseru
}else if(oldUserP){
  type="lawyer";
  isregisterd=true;
   userdata=oldUserP
}else{
  type="none"
}
 return res.send({
  status:true,
  message:"success",
  data:{
    type:type,
    isregisterd:isregisterd,
    userdata:userdata

  }
 })
}
module.exports.getsingleofflinecourse=async(req,res)=>{
  const id=mongoose.Types.ObjectId(req.params.id);
  const Lawyers = await Services.aggregate([
  {$match:{_id:id}},
  {$addFields:{teacher_id:{"$toString":"$teacher_id"},obj_client_id:{"$toObjectId":"$teacher_id"}}},
  
  {$lookup:{from:"lawunits",
  localField:"obj_client_id",
  foreignField:"_id",
 
  as:"teacher"}},
  {$lookup:{
    from:"ratingnreviews",
    // localField:"lawyer_id",
    // foreignField:"teacher_id"
    let: { nduration: "$duration",teacher_id:"$teacher_id" },
    pipeline:[
      {
        $match:{ $expr: { $eq: ["$teacher_id", "$$teacher_id"]}},
      
      },
      {$addFields:{teacher_id:{"$toString":"$teacher_id"},
      obj_client_id:{"$toObjectId":"$student_id"}}},
      {
        $lookup:{
          from:"users",
          localField:"obj_client_id",
          foreignField:"_id",
          as:"user"
        }
      }

    ]
    ,as:"ratings"}},
  
 
  
  ]);
  let newdata=Lawyers;
  for(let i=0;i<Lawyers.length;i++){
    const lawyer=Lawyers[i];
    const courses=lawyer.ratings;
    let lrating=0;
    // console.log("courses",courses)
   if(courses.length>0){
     for(let j=0;j<courses.length;j++){
       const course=courses[j];
      //  console.log(course)
       lrating=lrating+course.rating;
       
      
      }
   }else{
     
   }
   let totalratings=courses.length;
     let avgRating=lrating/totalratings;
     Lawyers[i].avgRating=Math.ceil(avgRating);
     // newdata=Lawyers
    //  console.log("lrating",lrating,"totalratings",totalratings,"courses.length",courses.length)
    
   }

  return res.send({
    status:true,
    message:"course fetched successfuly",
    data:newdata
  })
}

module.exports.getstudentsallpurchasedcourses=async(req,res)=>{
  const id=mongoose.Types.ObjectId(req.params.id);
  const allpurchasedservices=await Servicesprovidedmodel.find({student_id:id});
  const offlinecourseids=[];
  allpurchasedservices.map((service)=>{
    // if(service.type=="offline"){
    //   let course_id=mongoose.Types.ObjectId(service.course_id);
    //   offlinecourseids.push(course_id);
    // }
    let course_id=mongoose.Types.ObjectId(service.course_id);
    offlinecourseids.push(course_id);
  })
  console.log("offlinecourseids",offlinecourseids)
  
  const Lawyers = await Services.aggregate([
  {$match:{$and:[{_id:{$in:offlinecourseids}},{type:"offline"}]}},
  {$addFields:{teacher_id:{"$toString":"$teacher_id"},obj_client_id:{"$toObjectId":"$teacher_id"}}},
  
  {$lookup:{from:"lawunits",
  localField:"obj_client_id",
  foreignField:"_id",
 
  as:"teacher"}},
  // {$lookup:{
  //   from:"ratingnreviews",
  //   // localField:"lawyer_id",
  //   // foreignField:"teacher_id"
  //   let: { nduration: "$duration",teacher_id:"$teacher_id" },
  //   pipeline:[
  //     {
  //       $match:{ $expr: { $eq: ["$teacher_id", "$$teacher_id"]}},
      
  //     },
  //     {$addFields:{teacher_id:{"$toString":"$teacher_id"},
  //     obj_client_id:{"$toObjectId":"$student_id"}}},
  //     {
  //       $lookup:{
  //         from:"users",
  //         localField:"obj_client_id",
  //         foreignField:"_id",
  //         as:"user"
  //       }
  //     }

  //   ]
  //   ,as:"ratings"}},
  
 
  
  ]);
  // let newdata=Lawyers;
  // for(let i=0;i<Lawyers.length;i++){
  //   const lawyer=Lawyers[i];
  //   const courses=lawyer.ratings;
  //   let lrating=0;
  //   // console.log("courses",courses)
  //  if(courses.length>0){
  //    for(let j=0;j<courses.length;j++){
  //      const course=courses[j];
  //     //  console.log(course)
  //      lrating=lrating+course.rating;
       
      
  //     }
  //  }else{
     
  //  }
  //  let totalratings=courses.length;
  //    let avgRating=lrating/totalratings;
  //    Lawyers[i].avgRating=Math.ceil(avgRating);
  //    // newdata=Lawyers
  //   //  console.log("lrating",lrating,"totalratings",totalratings,"courses.length",courses.length)
    
  //  }

  return res.send({
    status:true,
    message:"course fetched successfuly",
    data:Lawyers
  })
}

module.exports.getallnotificationsettingofuser=async(req,res)=>{
  const id=req.params.id;
  const notipermission=await NotificationPermission.findOne({
    id:id
  });
  

  return res.send({
    status:true,
    message:"success",
    data:notipermission
  })
}
module.exports.deletenotification=async(req,res)=>{
  const id=req.params.id;
  await notificationmodel.findByIdAndDelete(id).then((result)=>{
    return res.send({
      status:true,
      message:"success",
      
    })
  })
  

 
}

module.exports.updatenotificationpermission=async(req,res)=>{
  const id=req.body.id;
  




  const email_offer=req.body.email_offer;
  const push_offer=req.body.push_offer;
  const mfc_email=req.body.mfc_email;
  const mfc_push=req.body.mfc_push;
  const pc_push=req.body.pc_push;
  const pc_email=req.body.pc_email;
  
  const notipermission=await NotificationPermission.findOne({
    id:id
  });
  if(notipermission){
  notipermission.email_offer=email_offer;
  notipermission.push_offer=push_offer;
  
  notipermission.mfc_email=mfc_email;
  notipermission.mfc_push=mfc_push;
 

  notipermission.pc_push=pc_push;
  notipermission.pc_email=pc_email;
 
  notipermission.save().then((result)=>{
    return res.send({
      status:true,
      message:"success"
    })
  })
  }else{
  const newnotipermission=new NotificationPermission();
  newnotipermission.id=id;
  newnotipermission.email_offer=email_offer;
  newnotipermission.push_offer=push_offer;
  
  newnotipermission.mfc_email=mfc_email;
  newnotipermission.mfc_push=mfc_push;
 

  newnotipermission.pc_push=pc_push;
  newnotipermission.pc_email=pc_email;
  newnotipermission.save().then((result)=>{
    return res.send({
      status:true,
      message:"success"
    })
  })

  }
}

const getboostedproviders=async()=>{
  try {
    const provider_model = Lawunit;
    const subscriber_model = subscriptionmodel;
   
    let providerswithoutboost = (await Lawunit.find({ is_active:true,is_deleted:false,is_subscribed:true })).map(e=>e._id+'')
    let providerswithboost = shuffle((await boosterpackage.find({ booster_type:"booster",end_date:{$gte:new Date()},payment_status:true })).map(e=>{if(e.provider_id!=""){return e.provider_id}}));
    console.log("providerswithboost=====>",providerswithboost)
    providerswithoutboost=providerswithoutboost.filter(r=>{
      // console.log("providerswithboost.indexOf(r)",providerswithboost.indexOf(r))
      if(providerswithboost.indexOf(r)>=0){
       
        return false
      }else{
        // console.log("r===>",r)
        return r
      }
    })
    const lot_to_cover = (providerswithoutboost.length + providerswithboost.length) * 0.1;
    providerswithboost=providerswithboost.map(e=>mongoose.Types.ObjectId(e))
    providerswithoutboost=providerswithoutboost.map(e=>mongoose.Types.ObjectId(e))
    //  console.log("providerswithoutboost",providerswithoutboost,"providerswithboost",providerswithboost)
    return [providerswithboost,providerswithoutboost];
  } catch (e) { console.log("e", e) }
}

module.exports.getalllawyersbackup1 = async (req, res) => {
  const provi=await getboostedproviders();
  const providerswithboostids=provi[0];
  const providerswithoutboostids=provi[1];
  let filter = {};
  let cfilter = {
    $expr:{ $eq: ["$provider_id", "$$service_id_str"] }
  };
  let sfilter = {}
  let schedule = [];
  
  let cdata = req.body;
  let sort = cdata.sort;
   let radius=cdata.radius;
   const latlong=cdata.latlong;
  const competence=cdata.competence;
  const avalible_today = cdata.date;

  if (cdata.catagory_id) {
      cfilter["catagory_id"] = {$elemMatch:{catagory_id:cdata.catagory_id}}
  }
  if (competence=="true" ) {
    sfilter["competence"] = true;
}else if (competence=="false" ) {
  sfilter["competence"] = false;
}

cfilter['$expr']={ $eq: ["$provider_id", "$$service_id_str"] };
sfilter["_id"]={$in:providerswithboostids};
var stack = [];
for (var i = providerswithboostids.length - 1; i >= 0; i--) {

  var rec = {
      "$cond": [
          { "$eq": [ "$_id", providerswithboostids[i-1] ] },
          i
      ]
  };

  if ( stack.length == 0 ) {
      rec["$cond"].push( i+1 );
  } else {
      var lval = stack.pop();
      rec["$cond"].push( lval );
  }

  stack.push( rec );
  console.log("rec",rec);

}

console.log("sfilter",sfilter)
const providerswithboost = await Lawunit.aggregate([
  {
    $geoNear:{
      "near":{type: "Point", coordinates:[latlong.long,latlong.lat ]},
      
      "distanceField": "dist.calculated",
      includeLocs: "dist.location",
      spherical: true,
      "maxDistance":radius,
     
    }
  },
    { $match:sfilter},
    { "$addFields": { "weight": stack[0] }},
    {$addFields:{provider_id_str:{ "$toString": "$_id" },is_boosted:true}},
    {
        $lookup: {
            from: "lawyerservices",

            let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
            pipeline: [
                {
                   
                    $match:{
                      $expr:{ $eq: ["$provider_id", "$$service_id_str"] }
                    }
                   
                },
                {
                  $lookup: {
                      from: "reviews",
                      
                      let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
                      pipeline: [
                          {
                              $match: { $expr: { $eq: ["$service_id", "$$service_id_str"] } },
                              // $match: { $expr: { $eq: ["$course_id", "$course_id"] } },
        
                          },
                        
                          {
                              $addFields: {
                                  
                                  obj_client_id: { "$toObjectId": "$client_id" }
                              }
                          },
                          {
                              $lookup: {
                                  from: "users",
                                  localField: "obj_client_id",
                                  foreignField: "_id",
                                  as: "user"
                                  
                              }
                          },
                        
                          
        
                      ]
                      , as: "ratings"
                  }
              },
              { $addFields: {
                avgRating:{$avg: "$ratings.rating"}                  
                // avgRating:{
                //   $round:[ {$avg: "$ratings.rating"},0]
                // }
            }},
              
              ],
            as: "services"
        }
    },
    {$lookup:{
      from:"favriouts",
      localField:"provider_id_str",
      foreignField:"provider_id",
      as:"favriouts"
    }},
   
    { "$sort": { "weight": 1 } }

 
]);
sfilter["_id"]={$in:providerswithoutboostids};
console.log("sfilter",sfilter)

//boosted providers





const providerswithoutboost = await Lawunit.aggregate([
  {
    $geoNear:{
      "near":{type: "Point", coordinates:[latlong.long,latlong.lat ]},
      
      "distanceField": "dist.calculated",
      includeLocs: "dist.location",
      spherical: true,
      "maxDistance":radius,
     
    }
  },
    { $match:sfilter},
    {$addFields:{provider_id_str:{ "$toString": "$_id" },is_boosted:false}},
    {
        $lookup: {
            from: "lawyerservices",

            let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
            pipeline: [
                {
                   
                    $match:{
                      $expr:{ $eq: ["$provider_id", "$$service_id_str"] }
                    }
                   
                },
                {
                  $lookup: {
                      from: "reviews",
                      
                      let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
                      pipeline: [
                          {
                              $match: { $expr: { $eq: ["$service_id", "$$service_id_str"] } },
                              // $match: { $expr: { $eq: ["$course_id", "$course_id"] } },
        
                          },
                        
                          {
                              $addFields: {
                                  
                                  obj_client_id: { "$toObjectId": "$client_id" }
                              }
                          },
                          {
                              $lookup: {
                                  from: "users",
                                  localField: "obj_client_id",
                                  foreignField: "_id",
                                  as: "user"
                                  
                              }
                          },
                        
                          
        
                      ]
                      , as: "ratings"
                  }
              },
              { $addFields: {
                 
                avgRating:{$avg: "$ratings.rating"}  
                
            }},
              
              ],
            as: "services"
        }
    },
    {$lookup:{
      from:"favriouts",
      localField:"provider_id_str",
      foreignField:"provider_id",
      as:"favriouts"
    }},
   
 {
      $lookup: {
          from: "reviews",
          localField: "provider_id_str",
          foreignField: "provider_id",
          as: "reviews"
      }
    },
    {
      $addFields: {

          avgRating: { $avg: "$reviews.rating" }
      }
    },


]);
// console.log("providerswithboost",providerswithboost,'providerswithoutboost',providerswithoutboost)
  return res.send({
      status: true,
      message: "teachers fetched successfully",
      errmessage: "",
      data: [...providerswithboost,...providerswithoutboost]
  })

};
module.exports.getalllawyers = async (req, res) => {
console.log("req.body",req.body)
 const singleid=req.body.single_id;
 const client_id=req.body.client_id;
 let singleprovider;
 if(singleid){
  singleprovider = await Lawunit.aggregate([
   
      { $match:{_id:mongoose.Types.ObjectId(singleid)}},
      {$addFields:{provider_id_str:{ "$toString": "$_id" }}},
      {
          $lookup: {
              from: "lawyerservices",
  
              let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" },provider_idstr:{"$toString":"$_id"} },
              pipeline: [
                  {
                     
                    $match:{$expr:{
                      $eq:["$provider_id",singleid]
                    }}
                     
                  },
                  {
                    $lookup: {
                        from: "reviews",
                        
                        let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
                        pipeline: [
                            {
                                $match: { $expr: { $eq: ["$service_id", "$$service_id_str"] } },
                                // $match: { $expr: { $eq: ["$course_id", "$course_id"] } },
          
                            },
                          
                            {
                                $addFields: {
                                    
                                    obj_client_id: { "$toObjectId": "$client_id" }
                                }
                            },
                            {
                                $lookup: {
                                    from: "users",
                                    localField: "obj_client_id",
                                    foreignField: "_id",
                                    as: "user"
                                    
                                }
                            },
                          
                            
          
                        ]
                        , as: "ratings"
                    }
                },
                { $addFields: {
                       
                  avgRating:{ $avg: "$ratings.rating"}
                  // avgRating:{
                  //   $round:[ {$avg: "$ratings.rating"},0]
                  // }
              }},
                
                ],
              as: "services"
          }
      },
      
      
      {$lookup:{
        from:"favriouts",
        localField:"provider_id_str",
        foreignField:"provider_id",
        as:"favriouts"
      }},
      {$addFields:{
        service:{$arrayElemAt:["$services",0]},
        is_fav:{
          
            $map: {
                input: {
                    $filter: {
                        input: '$favriouts',
                        as: 'favriout',
                        cond: { $eq: ['$$favriout.client_id', client_id] }
                    }
                },
                as: 'current',
                in: "$$current"
            
        }



         
        },
        address3:{$arrayElemAt:["$address",0]},
      }
      },
      {$lookup:{
        from:"abusereports",
        localField:"provider_id_str",
        foreignField:"provider_id",
        as:"abuse"
      }},
      {$addFields:{
        desc:"$service.desc",
        catagory:"$service.catagory",
        catagory1:{$arrayElemAt:["$service.catagory_id",0]},
        // avgRating:"$service.avgRating",
        is_boosted_profile:true
      }},
      {$addFields:{
        catagory_idOBJ:{"$toObjectId":"$catagory1.catagory_id"},
      }},
      {$lookup:{
        from:"catagories",
        localField:"catagory_idOBJ",
        foreignField:"_id",
        as:"firstcatagory"
      }},
      {$addFields:{
        
        firstcatagoryObj:{$arrayElemAt:["$firstcatagory",0]},
       
      }},
      {$addFields:{
        
        mastercatagory1:"$firstcatagoryObj.mastercatagory",
       
      }},
      {
        $lookup:{
          from:"catagories",
          localField:"mastercatagory1",
          foreignField:"_id",
          as:"pcatagory"
        }
       },
       {
        $lookup: {
            from: "reviews",
            localField: "provider_id_str",
            foreignField: "provider_id",
            as: "reviews"
        }
      },
      {
        $addFields: {
  
            avgRating: { $avg: "$reviews.rating" }
        }
      },
      
  
  ]);
  return res.send({
    status: true,
    message: "provider fetched successfully",
    errmessage: "",
    data:singleprovider 
})
 }else{
  const provi=await getboostedproviders();
  const providerswithboostids=provi[0];
  const providerswithoutboostids=provi[1];
  // console.log("providerswithoutboost",providerswithoutboostids,"providerswithboost",providerswithboostids)
  let filter = {};
   // $expr:{ $eq: ["$provider_id", "$$service_id_str"] }
  let cfilter = {
   
  };
  let sfilter = {
    is_deleted:false,is_active:true
  }
  let schedule = [];
  
  let cdata = req.body;
  let sort = cdata.sort;
  let radius=cdata.radius;
  const latlong=cdata.latlong;
  const competence=cdata.competence;
  const avalible_today = cdata.date;

  if (cdata.catagory_id) {
      cfilter["catagory_id"] = {$elemMatch:{catagory_id:cdata.catagory_id}}
  }
  if (competence=="true" ) {
    sfilter["competence"] = true;
}else if (competence=="false" ) {
  sfilter["competence"] = false;
}

cfilter['$expr']={ $eq: ["$provider_id", "$$service_id_str"] };
sfilter["_id"]={$in:providerswithboostids};
var stack = [];
for (var i = providerswithboostids.length - 1; i >= 0; i--) {

  var rec = {
      "$cond": [
          { "$eq": [ "$_id", providerswithboostids[i-1] ] },
          i
      ]
  };

  if ( stack.length == 0 ) {
      rec["$cond"].push( i+1 );
  } else {
      var lval = stack.pop();
      rec["$cond"].push( lval );
  }

  stack.push( rec );
  // console.log("rec",rec);

}

// console.log("sfilter",sfilter)
let providerswithboost=[];
if(providerswithboostids.length){
  // console.log("providerswithboostids.length=======>",1)
providerswithboost = await Lawunit.aggregate([
  {
    $geoNear:{
      "near":{type: "Point", coordinates:[latlong.long,latlong.lat ]},
      
      "distanceField": "dist.calculated",
      includeLocs: "dist.location",
      spherical: true,
      "maxDistance":radius,
     
    }
  },
    { $match:sfilter},
    { "$addFields": { "weight": stack[0] }},
    {$addFields:{provider_id_str:{ "$toString": "$_id" },is_boosted:true}},
    {
        $lookup: {
            from: "lawyerservices",

            let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
            pipeline: [
                {
                   
                    $match:cfilter
                   
                },
                {
                  $lookup: {
                      from: "reviews",
                      
                      let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
                      pipeline: [
                          {
                              $match: { $expr: { $eq: ["$service_id", "$$service_id_str"] } },
                              // $match: { $expr: { $eq: ["$course_id", "$course_id"] } },
        
                          },
                        
                          {
                              $addFields: {
                                  
                                  obj_client_id: { "$toObjectId": "$client_id" }
                              }
                          },
                          {
                              $lookup: {
                                  from: "users",
                                  localField: "obj_client_id",
                                  foreignField: "_id",
                                  as: "user"
                                  
                              }
                          },
                        
                          
        
                      ]
                      , as: "ratings"
                  }
              },
              { $addFields: {
                   
                avgRating:{ $avg: "$ratings.rating"}
                // avgRating:{
                //   $round:[ {$avg: "$ratings.rating"},0]
                // }
            }},
              
              ],
            as: "services"
        }
    },
    
    {$lookup:{
      from:"favriouts",
      localField:"provider_id_str",
      foreignField:"provider_id",
      as:"favriouts"
    }},
    
     {$addFields:{
      service:{$arrayElemAt:["$services",0]},
      is_fav:{
          
        $map: {
            input: {
                $filter: {
                    input: '$favriouts',
                    as: 'favriout',
                    cond: { $eq: ['$$favriout.client_id', client_id] }
                }
            },
            as: 'current',
            in: "$$current"
        
            }
        },
        address3:{$arrayElemAt:["$address",0]},
    }},
    
    {$addFields:{
      desc:"$service.desc",
      catagory:"$service.catagory",
      catagory1:{$arrayElemAt:["$service.catagory_id",0]},
      avgRating:"$service.avgRating",
      is_boosted_profile:true
    }},
    {$addFields:{
      catagory_idOBJ:{"$toObjectId":"$catagory1.catagory_id"},
    }},
    {$lookup:{
      from:"catagories",
      localField:"catagory_idOBJ",
      foreignField:"_id",
      as:"firstcatagory"
    }},
    {$addFields:{
      
      mastercatagory:"$firstcatagory.mastercatagory"},
      
    },
    {
      $lookup:{
        from:"catagories",
        localField:"mastercatagory",
        foreignField:"_id",
        as:"pcatagory"
      }
     },
    {$addFields:{
      
      firstcatagoryObj:{$arrayElemAt:["$firstcatagory",0]},
      pcatagory:{$arrayElemAt:["$pcatagory",0]},
      providerIDSTR:{"$toString":"$_id"}
    }},
    {
      $lookup: {
          from: "reviews",
          localField: "providerIDSTR",
          foreignField: "provider_id",
          as: "reviews"
      }
    },
    {
      $addFields: {

          avgRating: { $avg: "$reviews.rating" }
      }
    },
  { "$sort": { "weight": 1 } }

 
]);
}
sfilter["_id"]={$in:providerswithoutboostids};
// console.log("sfilter",sfilter)

//boosted providers




let providerswithoutboost=[];
if(providerswithoutboostids.length){
 providerswithoutboost = await Lawunit.aggregate([
  {
    $geoNear:{
      "near":{type: "Point", coordinates:[latlong.long,latlong.lat ]},
      
      "distanceField": "dist.calculated",
      includeLocs: "dist.location",
      spherical: true,
      "maxDistance":radius,
     
    }
  },
    { $match:sfilter},
    {$addFields:{provider_id_str:{ "$toString": "$_id" },is_boosted:false}},
    {
        $lookup: {
            from: "lawyerservices",

            let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
            pipeline: [
                {
                   
                  $match:cfilter
                   
                },
                {
                  $lookup: {
                      from: "reviews",
                      
                      let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
                      pipeline: [
                          {
                              $match: { $expr: { $eq: ["$service_id", "$$service_id_str"] } },
                              // $match: { $expr: { $eq: ["$course_id", "$course_id"] } },
        
                          },
                        
                          {
                              $addFields: {
                                  
                                  obj_client_id: { "$toObjectId": "$client_id" }
                              }
                          },
                          {
                              $lookup: {
                                  from: "users",
                                  localField: "obj_client_id",
                                  foreignField: "_id",
                                  as: "user"
                                  
                              }
                          },
                        
                          
        
                      ]
                      , as: "ratings"
                  }
              },
              { $addFields: {
                     
                avgRating:{ $avg: "$ratings.rating"}
                // avgRating:{
                //   $round:[ {$avg: "$ratings.rating"},0]
                // }
            }},
              
              ],
            as: "services"
        }
    },
    
    
    {$lookup:{
      from:"favriouts",
      localField:"provider_id_str",
      foreignField:"provider_id",
      as:"favriouts"
    }},
   
    {$addFields:{
      service:{$arrayElemAt:["$services",0]},
      is_fav:{
          
        $map: {
            input: {
                $filter: {
                    input: '$favriouts',
                    as: 'favriout',
                    cond: { $eq: ['$$favriout.client_id', client_id] }
                }
            },
            as: 'current',
            in: "$$current"
        
            }
        },
      address3:{$arrayElemAt:["$address",0]},
    }},

    {$addFields:{
      desc:"$service.desc",
      catagory:"$service.catagory",
       catagory1:{$arrayElemAt:["$service.catagory_id",0]},
      avgRating:"$service.avgRating",
      is_boosted_profile:false
    }},
    {$addFields:{
      catagory_idOBJ:{"$toObjectId":"$catagory1.catagory_id"},
    }},
    {$lookup:{
      from:"catagories",
      localField:"catagory_idOBJ",
      foreignField:"_id",
      as:"firstcatagory"
    }},
    {$addFields:{
      
      mastercatagory:"$firstcatagory.mastercatagory"},
      
    },
    {
      $lookup:{
        from:"catagories",
        localField:"mastercatagory",
        foreignField:"_id",
        as:"pcatagory"
      }
     },
    
    {$addFields:{
      
      firstcatagoryObj:{$arrayElemAt:["$firstcatagory",0]},
      pcatagory:{$arrayElemAt:["$pcatagory",0]},
      providerIDSTR:{"$toString":"$_id"}
    }},
    
    {
      $lookup: {
          from: "reviews",
          localField: "providerIDSTR",
          foreignField: "provider_id",
          as: "reviews"
      }
    },
    {
      $addFields: {

          avgRating: { $avg: "$reviews.rating" }
      }
    },
    
   
]);
}
// console.log("providerswithboost",providerswithboost,'providerswithoutboost',providerswithoutboost)
providerswithboost=providerswithboost.filter(e=>e.services.length!=0)
providerswithoutboost=providerswithoutboost.filter(e=>e.services.length!=0)
// console.log("cfilter",cfilter)  
return res.send({
      status: true,
      message: "teachers fetched successfully",
      errmessage: "",
      data: [...providerswithboost,...providerswithoutboost]
  })

 }
};

module.exports.getservicebyproviderid=async(req,res)=>{
  const id=req.params.id;
  const service=await Services.findOne({provider_id:id});
  if(service){
    return res.send({
      status:true
    })
  }else{
    return res.send({
      status:false
    })
  }
}


module.exports.getextraproviders=async(req,res)=>{
  
  const {cat_id,id}=req.body
  let cfilter={};
  cfilter["catagory_id"] = {$elemMatch:{catagory_id:cat_id}}
  const providers=await Lawunit.aggregate([
    {$match:{_id:mongoose.Types.ObjectId(id)}},
    {$addFields:{provider_id_str:{ "$toString": "$_id" }}},
    {
        $lookup: {
            from: "lawyerservices",

            let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
            pipeline: [
                {
                   
                    $match:cfilter
                   
                },
                {
                  $lookup: {
                      from: "reviews",
                      
                      let: { course_id: "$_id",service_id_str:{ "$toString": "$_id" } },
                      pipeline: [
                          {
                              $match: { $expr: { $eq: ["$service_id", "$$service_id_str"] } },
                              // $match: { $expr: { $eq: ["$course_id", "$course_id"] } },
        
                          },
                        
                          {
                              $addFields: {
                                  
                                  obj_client_id: { "$toObjectId": "$client_id" }
                              }
                          },
                          {
                              $lookup: {
                                  from: "users",
                                  localField: "obj_client_id",
                                  foreignField: "_id",
                                  as: "user"
                                  
                              }
                          },
                        
                          
        
                      ]
                      , as: "ratings"
                  }
              },
              { $addFields: {
                   
                avgRating:{ $avg: "$ratings.rating"}
                // avgRating:{
                //   $round:[ {$avg: "$ratings.rating"},0]
                // }
            }},
              
              ],
            as: "services"
        }
    },
  ]);
  return res.send({
    status:true,
    data:providers
  })
}
module.exports.getcountofallproviders=async(req,res)=>{
  
  const count=(await Lawunit.find({is_deleted:false}))?.length||0;
  return res.send({
    status:true,
    data:count
  })
}