const LawyerBandDetails = require("../../models/lawyer/LawyerBankDetails");
module.exports.savelawyerbankdetails = async (req,res)=> {
   
  const { provider_id, bank_name, account_number, account_name, bank_address,iban,bic,payment_mobile } =req.body;
  const data = {
    ...(provider_id && { provider_id:provider_id }),
    ...(bank_name && { bank_name:bank_name }),
    ...(account_number && { account_number:account_number }),
    ...(account_name && { account_name:account_name }),
    ...(bank_address && { bank_address:bank_address }),
    ...(iban && { iban:iban }),
    ...(bic && { bic:bic }),
    ...(payment_mobile && { payment_mobile:payment_mobile }),
  };
  const lawyerbankdetails = new LawyerBandDetails(data);
  await lawyerbankdetails.save().then((result) => {
    res.send({
      status: true,
      message: "Les coordonnées bancaires ont été sauvegardées avec succès",
      data: result,
      errmessage: "",
    });
  });
};
module.exports.updatebankdetails = async (req,res) => {
  const { provider_id, bank_name, account_number, account_name, bank_address,iban,bic,payment_mobile } =req.body;
    const data = {
      ...(provider_id && { provider_id:provider_id }),
      ...(bank_name && { bank_name:bank_name }),
      ...(account_number && { account_number:account_number }),
      ...(account_name && { account_name:account_name }),
      ...(bank_address && { bank_address:bank_address }),
      ...(iban && { iban:iban }),
      ...(bic && { bic:bic }),
      ...(payment_mobile && { payment_mobile:payment_mobile }),
    };
  await LawyerBandDetails.findOneAndUpdate({ provider_id }, data)
    .then((result) => {
      res.send({
        status: true,
        message: "Mise à jour réussie des coordonnées bancaires",
        data: result,
        errmessage: "",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "",
        data: err,
        errmessage: "Erreur dans la mise à jour de Bank Details",
      });
    });
};
module.exports.deletelawyerbankdetails = async (req,res) => {
    const teacher_id=req.params.lawyer_id;
    await LawyerBandDetails.findOneAndDelete({ _id:teacher_id })
        .then((result) => {
        res.send({
            status: true,
            message: "Les coordonnées bancaires ont été supprimées avec succès",
            data: result,
            errmessage: "",
        });
        })
        .catch((err) => {
        res.send({
            status: false,
            message: "",
            data: err,
            errmessage: "Erreur dans la suppression des coordonnées bancaires",
        });
        });
}
module.exports.getlawyerbankdetailsbylawyerid = async (req,res) => {
    const teacher_id = req.params.lawyer_id;
    await LawyerBandDetails.find({ provider_id:teacher_id })
        .then((result) => {
        res.send({
            status: true,
            message: "Bank Details Fetched Successfully",
            data: result,
            errmessage: "",
        });
        })
        .catch((err) => {
        res.send({
            status: false,
            message: "",
            data: err,
            errmessage: "Erreur dans la récupération des coordonnées bancaires",
        });
        });
}

