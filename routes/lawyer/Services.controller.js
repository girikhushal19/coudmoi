const Services=require('../../models/lawyer/LawyerServices');
const subscriptionmodel=require("../../models/lawyer/Subscriptions");
const boosterpackagemodel=require("../../models/admin/Boosterpackage");
const Catagory=require("../../models/admin/Catagory");
module.exports.createservice = async(req, res) => {
  console.log(req.body);
  console.log("req.body");
  const {
    provider_id,
    provider_name,
    catagory_name,
    catagory_id,
    typeofjobs,
    title,
    pseduoname,
    experience,
    desc,
    price,
    service_image,
    caption
  } = req.body;
  const service=await Services.findOne({provider_id:provider_id,title:title});
  if(service){
  return res.send({
    status:false,
    message:"service already exists"
  })
  }
  let newcatagory,newcatagory_id,newservice_image;
  let catjson;
  let catidjson;
  console.log(catagory_name)
  console.log("catagory_name")
  var s1 =catagory_name;
  var myObject1 = eval('(' + s1 + ')');
  var s2 =catagory_id;
  var myObject2 = eval('(' + s2 + ')');
  newcatagory= myObject1;
  newcatagory_id=myObject2;
  console.log(newcatagory);
  console.log("newcatagory");
  console.log(newcatagory_id);
  console.log('newcatagory_id');
  try{
    newservice_image=JSON.parse(service_image);
  }catch(e){
    newservice_image=service_image
  }
  //newservice_image=JSON.parse(service_image)
  /*for(let i=0;i<newcatagory.length;i++){
    catjson.push({
      "catagory_name":newcatagory[i].catagory_name
    })
  }
  for(let i=0;i<newcatagory_id.length;i++){
    catidjson.push({
      "catagory_id":newcatagory_id[i].catagory_id
    })
  }*/
  /*console.log(catjson)
  console.log("catjson")
  console.log(catidjson)
  console.log("catidjson")*/
  console.log("newcatagory",newcatagory,"newcatagory_id",newcatagory_id)
  const subscription=await boosterpackagemodel.findOne({provider_id:provider_id,booster_type:"image",payment_status:true,end_date:{$gte:new Date()}});
  console.log("subscription",subscription)
  // if(!subscription){
  //   return res.send({
  //     status:false,
  //     messege:"you are not subscribed, please subscribed first"
  //   })
  // }
  const images=[];
 
  if (req.files?.service_image) {
    const imageslength=req.files.service_image.length;
    // console.log("imageslength",imageslength,"req.files.service_image",req.files.service_image,"req.files",req.files)
    if(imageslength==1){
      for(let i=0;i<req.files.service_image.length;i++){
        let file=req.files.service_image[i];
        let filename="userprofile/"+file.filename
        images.push({url:filename,caption:caption})
       }
    }
    else if(imageslength>1){
      if(subscription){
        for(let i=0;i<req.files.service_image.length;i++){
          let file=req.files.service_image[i];
          images.push("userprofile/"+file.filename)
         }
      }else{
        return res.send({
          status:false,
          message:"only one service image is allowed in this subscription package"
        })
      }
    }
    }
    // if (newservice_image?.length) {
   
    //   if(newservice_image?.length==1){
        
    //   }
    //   else if(newservice_image?.length>1){
  
    //     if(subscription){
         
    //     }else{
    //       return res.send({
    //         status:false,
    //         message:"only one service image is allowed in this subscription package"
    //       })
    //     }
    //   }
    //   }

  const catagory=1
  // await Catagory.find({_id:catagory_id});
  if(catagory){
    let course=new Services();
    course.provider_id=provider_id;
    course.title=title;
    course.provider_name=provider_name;
    course.catagory=newcatagory;
    course.catagory_id=newcatagory_id;
    course.desc=desc;
    course.pseduoname=pseduoname;
    course.service_images=images;
    course.price=price;
    course.experience=experience;
    course.typeofjobs=typeofjobs;
    course.image=catagory.image;
  
    course.save().then((service) => {
      return res.send({
      status:true,
      message:"service created successfully",
      data:service
    })}).catch((err) => { console.log(err); });
  }else{
    return res.send({
      status:false,
      message:"catagory does'nt exists",
      data:null
    })
  }
 
 
};
module.exports.getallservicebyLayerId = async(req, res) => {
  let lawyer_id=req.params.lawyer_id;

  await Services.find({provider_id:lawyer_id})
    .then((services) => {
      res.send({
        status:true,
        message:"courses fetched successfully",
        errmessage:"",
        data:services
      });
    })
    .catch((err) => {
      res.send({
        status:true,
        message:"courses fetched successfully",
        errmessage:"something went wrond ,try again after sometime",
        data:services
      });
    });
};
module.exports.getServicebyId = async(req, res) => {
  const id = req.params.id;
 await Services.findById(id)
    .then((service) => {
     return res.send({
      status:true,
      data:service,
      message:"service fetched successfully"
     });
    })
    .catch((err) => {
      return res.send({
        status:true,
        data:null,
        message:err.message
       });
    });
};

module.exports.updateService = async(req, res) => {
 
  const {
    id,
    provider_id,
    service_image,
    catagory,
    catagory_id,
    title,
    pseduoname,
    experience,
    desc,
    price
    
    
  } = req.body;
  const subscription=await subscriptionmodel.findOne({provider_id:provider_id});
  // if(!subscription){
  //   return res.send({
  //     status:false,
  //     messege:"you are not subscribed, please subscribed first"
  //   })
  // }
  const subscriptionb=await boosterpackagemodel.findOne({provider_id:provider_id,booster_type:"image",payment_status:true,end_date:{$gte:new Date()}});
  const images=[];
  console.log("req.body====>",req.body)
  console.log("req.files====>",req.files)
  // if (req.files?.service_image) {
  //   const imageslength=req.files.service_image.length;
  //   // console.log("imageslength",imageslength,"req.files.service_image",req.files.service_image,"req.files",req.files)
  //   if(imageslength==1){
  //     for(let i=0;i<req.files.service_image.length;i++){
  //       let file=req.files.service_image[i];
  //       images.push("userprofile/"+file.filename)
  //      }
  //   }
  //   else if(imageslength>1){

  //     if(subscriptionb){
  //       for(let i=0;i<req.files.service_image.length;i++){
  //         let file=req.files.service_image[i];
  //         images.push("userprofile/"+file.filename)
  //        }
  //     }else{
  //       return res.send({
  //         status:false,
  //         message:"only one service image is allowed in this subscription package"
  //       })
  //     }
  //   }
  //   }
  let newservice_image=service_image;
  try{
    newservice_image=JSON.parse(service_image);
  }catch(e){
    newservice_image=service_image
  }

  if (newservice_image?.length) {
   
    if(newservice_image?.length==1){
      
    }
    else if(newservice_image?.length>1){

      if(subscriptionb){
       
      }else{
        return res.send({
          status:false,
          message:"only one service image is allowed in this subscription package"
        })
      }
    }
    }
  let data= {
   
            ...(title&&  {title:title} ),
            ...(catagory?.length>0&&  {catagory:catagory} ),
            ...(catagory_id?.length>0&&  {catagory_id:catagory_id} ),
            ...(desc&&  {desc:desc} ),
            ...(price&&  {price:price} ),
            ...(pseduoname&&{pseduoname:pseduoname}),
          
           
          
            ...(experience&&  {experience:experience} ),
            ...(newservice_image?.length &&  {"service_images":newservice_image} ),
               
        }
  console.log("Data",data,"newservice_image",newservice_image)
      await  Services.findByIdAndUpdate(
          id,
        data)
        .then((lawyer) => {
            res.send({
              status:true,
              message:"update successfully"
            });
        })
};
module.exports.deletecourse=async(req,res)=>{
  let id=req.params.id;
    let course=await Services.findByIdAndDelete(id);
    return res.send({
      status:true,
      message:"course deleted successfully",
      data:null,
      errmessage:""
    })
    
}

module.exports.updatescheduleforcourse=async(req,res)=>{
  // const date=req.body.date;
  // const id=req.body.id;
  // const time=req.body.time;
  // const dat_and_time_array=[date,time];
  // const couresemodel=Services.findById(id);
}
