const Booking = require('../../models/client/Servicesprovided');
const IABNModel=require('../../models/lawyer/Iabn');
const lawyerModel=require('../../models/lawyer/Lawunit');
const usermodel=require('../../models/auth/user');
const path = require("path");
const LawyerBandDetails = require("../../models/lawyer/LawyerBankDetails");
const notificationpermissionmodel = require("../../models/NotificationPermission");
const {sendpushnotificationtouser}=require('../../modules/Fcm');
const { sendmail } = require("../../modules/sendmail");
module.exports.cancelbooking = async(req, res) => {
   
    const student_id=req.body.client_id;
    const cancelation_reason=req.body.cancelation_reason;
    const cancelation_message=req.body.cancelation_message;
   
    const booking_id = req.params.booking_id;
    let message=process.env.BOOKING_CANCELLED_BY_PROVIDER
    console.log("req.body",req.body)
    const pathtofile = path.resolve("views/notifications/offercancelbyprovider.ejs");
   await  Booking.findByIdAndUpdate({_id:booking_id},{is_service_rejected:true})
        .then(async(booking) => {
        if(!booking.is_service_rejected){
            const notipermissions=await notificationpermissionmodel.find({id:booking.client_id});
            const user=await usermodel.findOne({_id:booking.client_id})
            if(notipermissions.length){
                if(notipermissions[0].push_offer){
                    sendpushnotificationtouser(message,user,booking.client_id);
                }else{
                    console.log("push notification is disabled for student")
                }if(notipermissions[0].email_offer){
                    sendmail(pathtofile, 
                        { provider_name: booking.provider_name,
                            reason:cancelation_reason,
                            message:cancelation_message,
                            adminmessage:message }, 
                          "Annulation du service par le client",
                         user.email);
                }else{
                    console.log("email notification is disabled for student")
                }
            }else{
                sendpushnotificationtouser(message,user,booking.client_id)
                sendmail(pathtofile, 
                    { provider_name: booking.provider_name,
                        reason:cancelation_reason,
                        message:cancelation_message,
                        adminmessage:message }, 
                      "Annulation du service par le client",
                     user.email);
            }
        res.send({
            booking:booking,
            status:true,
            message:"Booking cancelled successfully"
        })
    }else{
        res.send({
            status:false,
            message:"service cancelled already",
            errmessage:"",
            data:null
         })
    }
        }).catch((err) => {
            console.log(err)
        res.send({
            status:false,
            message:"Booking cancellation failed",
            error:err
                })
        })
}
module.exports.acceptbooking =async (req, res) => {
    const booking_id = req.params.booking_id;
    const lawyer_id=req.params.lawyer_id;
    let message=process.env.BOOKING_ACCEPTED_BY_PROVIDER
   
    const iabn=await LawyerBandDetails.findOne({provider_id:lawyer_id});
    console.log("iabn",iabn)
    const pathtofile = path.resolve("views/notifications/offersuccess.ejs");
    if(!iabn){
     return res.send({
         status:true,
         message:"",
         errmessage:"IABN non mis à jour, Veuillez mettre à jour IABN pour accepter le rendez-vous.",
         data:null
     })
    }
 await   Booking.findByIdAndUpdate({_id:booking_id},{service_acceptance_status_by_provider:true})
        .then(async(booking) => {
            const notipermissions=await notificationpermissionmodel.find({id:booking.client_id});
            const user=await usermodel.findOne({_id:booking.client_id})
            if(notipermissions.length){
                if(notipermissions[0].push_offer){
                    sendpushnotificationtouser(message,user,booking.client_id)
                }else{
                    console.log("push notification is disabled for student")
                }if(notipermissions[0].email_offer){
                    sendmail(pathtofile, 
                        { student_name: booking.client_id }, 
                        "Subscription Expires in  days",
                         user.email);
                }else{
                    console.log("email notification is disabled for teastudentcher")
                }
            }else{
                sendpushnotificationtouser(message,user,booking.client_id)
                sendmail(pathtofile, 
                    { student_name: booking.client_id }, 
                    "Subscription Expires in  days",
                     user.email);
            }
        return res.send({
            booking:booking,
            status:true,
            message:"Booking accepted successfully"
        });
        }).catch((err) => {
        res.send({
            status:false,
            message:err.message,
            error:err.message
        });
        })
}

module.exports.markserviceasdonebylawyer=async (req, res) => {   
    const offer_id=req.params.offer_id;
    await Booking.findByIdAndUpdate({_id:offer_id},{service_status:true}).then(async(booking)=>{
        const lawyer=await lawyerModel.findById(booking.provider_id);
        lawyer.totalpendingamount=lawyer.totalpendingamount+booking.finalamount;
        lawyer.save().then(async(some)=>{
            const notipermissions=await notificationpermissionmodel.find({id:booking.provider_id});
            const user=await lawyerModel.findOne({_id:booking.provider_id})
            if(notipermissions.length){
                if(notipermissions[0].push_offer){
                    sendpushnotificationtouser(`${booking.title} marqué comme fait par le ${booking.client_name} `,user,booking.provider_id,"provider","markasdone");
                }
                else{
                    console.log("push notification is disabled for student")
                }
                // if(notipermissions[0].email_offer){
                //     sendmail(pathtofile, 
                //         { provider_name: booking.provider_name,
                //             reason:cancelation_reason,
                //             message:cancelation_message,
                //             adminmessage:message }, 
                //           "Annulation du service par le client",
                //          user.email);
                // }else{
                //     console.log("email notification is disabled for student")
                // }
            }else{
                sendpushnotificationtouser(`${booking.title} marqué comme fait par le ${booking.client_name} `,user,booking.client_id,"provider","markasdone")
                // sendmail(pathtofile, 
                //     { provider_name: booking.provider_name,
                //         reason:cancelation_reason,
                //         message:cancelation_message,
                //         adminmessage:message }, 
                //       "Annulation du service par le client",
                //      user.email);
            }
            res.send({
            data:booking,
            status:true,
            message:"Service marqué comme effectué avec succès",
            errmessage:""
        });
        })
        
    }).catch((err)=>{
        res.send({
            status:false,
            message:"La mise à jour de l'état de service n'a pas abouti",
            errmessage:err.message,
            data:null
        });
    })
}

module.exports.markserviceasundonebylawyer=async (req, res) => {   
    const offer_id=req.params.offer_id;
    await Booking.findByIdAndUpdate({_id:offer_id},{service_status:false}).then((booking)=>{
        res.send({
            data:booking,
            status:true,
            message:"Service marqué comme effectué avec succès",
            errmessage:""
        });
    }).catch((err)=>{
        res.send({
            status:false,
            message:"La mise à jour de l'état de service n'a pas abouti",
            errmessage:err.message,
            data:null
        });
    })
}

