remindermodel=require('../../models/admin/Reminder');
const savereminder=async(user_id,date,remindertype)=>{
    console.log("savereminder",user_id,date,remindertype);
    const reminder=new remindermodel({
        user_id:user_id,
        date:date,
        remindertype:remindertype
    });
    await reminder.save();
}
const getreminders=async(user_id)=>{
    const reminders=await remindermodel.find({user_id:user_id});
    return reminders;
}
const getremindersbydateanduseridandremindertype=async(user_id,date,remindertype)=>{
    const reminders=await remindermodel.find({user_id:user_id,date:date,remindertype:remindertype});
    return reminders;
}
module.exports={
    savereminder,
    getreminders,
    getremindersbydateanduseridandremindertype
}