const Admin=require("../../models/admin/Admin");
const login=async(req,res)=>{
    try {
        // Get user input
        const { email, password } = req.body;
    
        // Validate user input
        if (!(email && password)) {
          res.status(400).send("All input is required");
        }
        // Validate if user exist in our database
        const user = await Admin.findOne({ email });
    
        if (user && (await bcrypt.compare(password, user.password))) {
          // Create token
          const token = jwt.sign(
            { user_id: user._id, email },
            process.env.TOKEN_KEY,
            {
              expiresIn: "24h",
            }
          );
    
          // save user token
          user.token = token;
        //   console.log(user);
          // user
          res.status(200).json(user);
        } else {
          res.status(400).send("Invalid Credentials");
        }
      } catch (err) {
        console.log(err);
      }
    
}
const changePassword=(req,res)=>{
    const {email,password}=req.body;
    Admin.findByIdAndUpdate(email,{password}).then((result)=>{
        res.send(result);
    }).catch((err)=>{
        res.send(err);
    })
}

module.exports={login,changePassword};