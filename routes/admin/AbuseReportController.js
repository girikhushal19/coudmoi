const abuseReportModel=require("../../models/admin/AbuseReport");

module.exports={
    addOrupdate:async(req,res)=>{
        const {
            reason,
           
            whoisreported_Id,
            whoisreported_Name,
            whoisreported_type,
            reportedby_Name,
            reportedby_Id,
            reportedby_type,
            id,
            type
        }=req.body;
        console.log("req.body",req.body)
        // let mediaarray=[]
        // if(req.files.photo.length){
        //     req.files.photo.map((file)=>{
        //         newfile=process.env.PHOTOS_PATH_PERFIX+"/"+file.filename;
        //         mediaarray.push(newfile);
        //     })
           
        // }
        const datatoupdate={
            ...(reason&&{reason:reason}),
           
            ...(whoisreported_Name&&{whoisreported_Name:whoisreported_Name}),
            ...(whoisreported_Id&&{whoisreported_Id:whoisreported_Id}),
            ...(whoisreported_type&&{whoisreported_type:whoisreported_type}),
            ...(reportedby_Name&&{reportedby_Name:reportedby_Name}),
            ...(reportedby_Id&&{reportedby_Id:reportedby_Id}),
            ...(reportedby_type&&{reportedby_type:reportedby_type}),
            ...(type&&{type:type}),
            
        }
       if(id){
        await abuseReportModel.findByIdAndUpdate(id,datatoupdate).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
       }else{
        await abuseReportModel.create(datatoupdate).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
       }
    },
    deleterep:async(req,res)=>{
     const id=req.params.id;
     await abuseReportModel.findByIdAndDelete(id)
     .then((result)=>{
        return res.send({
            status:true,
            message:"deleted"
        })
     })
    },
    markAsresolved:async(req,res)=>{
        const id=req.params.id;
        await abuseReportModel.findByIdAndUpdate(id,{is_resolved:true})
        .then((result)=>{
           return res.send({
               status:true,
               message:"updated"
           })
        })
    },
    markAsUnresolved:async(req,res)=>{
        const id=req.params.id;
        await abuseReportModel.findByIdAndUpdate(id,{is_resolved:false})
        .then((result)=>{
           return res.send({
               status:true,
               message:"updated"
           })
        })
    },
    getallreports:async(req,res)=>{
        const reports=await abuseReportModel.aggregate([
            {$match:{_id:{$ne:null}}},
            {$addFields:{
                
                userIDOBJ:{"$toObjectId":"$reportedbyId"},
                providerIDOBJ:{"$toObjectId":"$provider_id"},
            }},
            
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"reportedBy"
                }
            },
            {
                $lookup:{
                    from:"lawunits",
                    localField:"providerIDOBJ",
                    foreignField:"_id",
                    as:"vendor"
                }
            }
        ]);
        return res.send({
            status:true,
            data:reports
        })

    }
}