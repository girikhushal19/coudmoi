const app = require("express");
const { login, 
  changePassword 
} = require("./Adminauth.controller");
const { addcatagory
} = require("./Catagory.controller");
const {
  serviceHistory,
  getalllawyers,
  getallusers,
  scheduledHistory,
  createPromotions,
  updatePromotions,
  deletePromotions,
  getallpromotions,
  getpromotionbyid,
  paymentSettings,
  createPaymentsettings,
  getpaymentSettings,
  updateservice,
  getservicebyid,
  getallservices,
  createService,
  deleteservice,
  setAdminCommision,
  deleteadmincommision,
  setactiveadmincommision,
  getAdminCommision
} = require("./Admin.controller");
const {
  createSubscription,
  getAllSubscriptions,
  getSubscriptionById,
  updateSubscription,
  deleteSubscription,
} = require("./Subscription.controller");

const {
  addOrupdate,
deleterep,
markAsresolved,
markAsUnresolved,

getallreports
}=require("./AbuseReportController");
const adminrouter = app.Router();
adminrouter.get("/", (req, res) => {
  res.send("adming");
});
adminrouter.get("/serviceHistory", serviceHistory);
adminrouter.get("/scheduledHistory", scheduledHistory);
adminrouter.get("/getallusers", getallusers);
adminrouter.get("/getalllawyers", getalllawyers);
adminrouter.post("/paymentsettings", paymentSettings);
adminrouter.post("/createPromotions", createPromotions);
adminrouter.post("/updatePromotions", updatePromotions);
adminrouter.get("/deletePromotions/:promotion_id", deletePromotions);
adminrouter.get("/getallpromotions", getallpromotions);
adminrouter.get("/getpromotionbyid/:promotion_id", getpromotionbyid);
adminrouter.post("/createSubscription", createSubscription);
adminrouter.get("/getAllSubscriptions", getAllSubscriptions);
adminrouter.get("/getSubscriptionById/:subscription_id", getSubscriptionById);
adminrouter.post("/updateSubscription", updateSubscription);
adminrouter.get("/deleteSubscription/:subscription_id", deleteSubscription);
adminrouter.post("/login", login);
adminrouter.post("/changePassword", changePassword);
adminrouter.post("/paymentSettings", paymentSettings);
adminrouter.post("/createPaymentsettings", createPaymentsettings);
adminrouter.get("/getpaymentSettings", getpaymentSettings);
adminrouter.post("/createservice", createService);
adminrouter.get("/getallservices", getallservices);
adminrouter.get("/getservicebyid/:service_id", getservicebyid);
adminrouter.post("/updateservice", updateservice);
adminrouter.get("/deleteservice/:service_id", deleteservice);
adminrouter.post("/setAdminCommision", setAdminCommision);
adminrouter.get("/getAdminCommision", getAdminCommision);
adminrouter.post("/deleteadmincommision", deleteadmincommision);
adminrouter.post("/setactiveadmincommision", setactiveadmincommision);

adminrouter.post("/addOrupdate", addOrupdate);
adminrouter.get("/deleterep/:id", deleterep);
adminrouter.get("/markAsresolved/:id", markAsresolved);
adminrouter.get("/markAsUnresolved/:id", markAsUnresolved
);
adminrouter.get("/getallreports", getallreports);

module.exports = adminrouter;
