const Servicesprovided = require("../../models/client/Servicesprovided");
const DistributerModel=require("../../models/lawyer/Distributer");
const abuseReportModel=require("../../models/admin/AbuseReport");
const cancledrequestmodel=require("../../models/admin/cancledservicepayment");
const MatCatagoryModel=require("../../models/admin/MatCatagory");
const CoupensModel=require("../../models/lawyer/Coupens");
const Boosterpackages=require("../../models/admin/BoosterPackages");
const Boosterpackagessubs=require("../../models/admin/Boosterpackage");
const mongoose=require("mongoose");
const async=require("async")
const reasonModel=require("../../models/client/Creasons");
const commentsmodel=require("../../models/client/Review");
const coursesmodel=require("../../models/lawyer/LawyerServices");
const catagorymodel=require("../../models/admin/Catagory");
const usersmodel = require("../../models/auth/user");
const PaymentRequestmodel=require("../../models/admin/PaymentRequest");
const lawyermodel = require("../../models/lawyer/Lawunit");
const parentmodel=require("../../models/client/Parent");
const AdminPayoutmodel=require("../../models/admin/AdminPayout");
const LawyerBandDetails=require("../../models/lawyer/LawyerBankDetails");
const subscriptionpackagesmodel = require("../../models/admin/SubscriptionPackages");
const usersubscriptionmodel = require("../../models/lawyer/Subscriptions");
const base_url = process.env.BASE_URL + "/admin-panel/";
const root_url = process.env.BASE_URL;
const paymentsettingmodel = require("../../models/admin/Paymentsettings");
const cabinatelawyermodel = require("../../models/lawyer/Cabinatelawyer");
const adminmodel = require("../../models/admin/Admin");
const notificationmodel = require("../../models/Notifications");
const languagemodel = require("../../models/admin/Languages");
const expertisemodel = require("../../models/admin/Expertise");
const cmsmodel = require("../../models/admin/Cms");
const apptext_cmsmodel=require("../../models/admin/Apptext");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const path = require("path");
const nodemailer = require("nodemailer");
const {getPreviousDay,frenchDayDate}=require("../../modules/dates")
const {sendpushnotificationtotopic,sendpushnotificationtouser}=require("../../modules/Fcm")
var FCM = require("fcm-node");
var admin = require("firebase-admin/messaging");
const axios = require("axios");
const i18n = require('i18n');
//  var serverKey = "AAAAcNwxYO4:APA91bHJz4UVos8IFF4UveDUBmUUEvigfLd4khz46i8CKnCfzx4v9mmVijTBUpdm3quuuZwFq37TN9mPIsw68ZF7HOS8pHDK0Fxi18_WilD0GHBvMrxR-tkn-w2HBmGlHIbI5MpyJqHu";
const serverKey = require("../../config/prime-rainfall-321012-firebase-adminsdk-q92of-d6264a4a58.json");
var fcm = new FCM(serverKey);
const hbs = require("nodemailer-express-handlebars");
const admin_lawyer_profile_path = process.env.admin_lawyer_profile_path;
const admin_user_profile_path = process.env.admin_user_profile_path;
const moment=require("moment");


const pdf = require('html-pdf');
const ejs = require("ejs");




module.exports.sendAppNotification = async (messagedata="how's going on") => {
  // Define who to send the message to
  const topic = 'listentoadmin';
const registrationToken = 'YOUR_REGISTRATION_TOKEN';

const message = {
  data: {
    message: messagedata,
   
  },
  // token: registrationToken
topic:topic
};

// Send a message to the device corresponding to the provided
// registration token.
admin.getMessaging().send(message)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });
};

module.exports.renderhomepage = async (req, res) => {
  const reservations = await Servicesprovided.aggregate([
    {
      $match:{payment_status:true}
    },
    {
      $addFields:{
        catIDOBJ:{"$toObjectId":"$catagory_id"}
      }
    },
    {
      $lookup:{
        from:"lawyerservices",
        localField:"provider_id",
        foreignField:"provider_id",
        as:"cat"
      },
     
    },
    {$unwind:"$cat"}
  ]);
  console.log(reservations);
  const totoalnumberofreservations = reservations.length;
  const pendingapproval =(await Servicesprovided.findOne({provider_id:""}))?.length;
  
  
  
  const totalearnings = reservations.reduce(
    (total, reservation) => total + (reservation.payment_amount?reservation.payment_amount:0),
    0
  );
  const totaltransections = totoalnumberofreservations;
  console.log(totoalnumberofreservations,pendingapproval,totalearnings);
  res.render("admin-panel/index.ejs", {
    reservations,
    totoalnumberofreservations,
    pendingapproval,
    totalearnings,
    totaltransections,
    base_url,
    moment
   
  });
};
module.exports.users = async (req, res) => {
  console.log(req.query)
  const first_name=req.query.first_name?{first_name:req.query.first_name}:{};
  const last_name=req.query.last_name?{last_name:req.query.last_name}:{};
  const email=req.query.email?{email:req.query.email}:{};
  const phone=req.query.phone_number?{phone:req.query.phone_number}:{};
  const start_date=req.query.start_date?{date_of_registration:{$gte:new Date(req.query.start_date)}}:{};
  const end_date=req.query.end_date?{date_of_registration:{$lte:new Date(req.query.end_date)}}:{};
  const users=await usersmodel.find({is_deleted:false,...first_name,...last_name,...email,...phone,...start_date,...end_date});
  res.render("admin-panel/users.ejs", { users, base_url,moment });
};
module.exports.deleteuser = async (req, res) => {
  const user_id = req.params.user_id;
  const user=await usersmodel.findById(user_id);
  user.email=user?.email+(Math.random()*1000000);
  user.is_deleted=true;
  user
    .save()
    .then((result) => {
      res.send({
        status: true,
        message: "Utilisateur supprimé avec succès",
        user: result,
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: "L'utilisateur n'a pas été supprimé avec succès",
        err: err,
      });
    });
};
module.exports.viewuser = async (req, res) => {
  const google_api_key=process.env.GOOGLE_MAPS_API_KEY;
  const user_id = req.params.user_id;
  const user = await usersmodel.findById(user_id);
  res.render("admin-panel/viewuser.ejs", { user, base_url, root_url ,moment,google_api_key});
};
module.exports.updateuser = async (req, res) => {
  let user_profile;
  
  if (req.file) {
    user_profile = "userprofile/" + req.file.filename;
  }
  const {
    first_name,
    last_name,
    email,
    phone,
    date_of_registration,
    dob,
    elementid,
    profile_image,
    resetpassword,
    address,
    status
  } = req.body;
  console.log(profile_image);
  let encryptedPassword;
  if (resetpassword) {
    encryptedPassword = await bcrypt.hash(resetpassword, 10);
  }
  let newstatus;
  if(status!=""){
    if(status==0 || status=="0"){
      newstatus=false
    }else if(status==1 || status=="1"){
      newstatus=true
    }
  }
  const data = {
    ...(first_name && { first_name }),
    ...(last_name && { last_name }),
    ...(email && { email }),
    ...(phone && { phone }),
    ...(date_of_registration && { date_of_registration }),
    ...(dob && { dob }),
    ...(user_profile && { user_photo: user_profile }),
    ...(encryptedPassword && { password: encryptedPassword }),
    ...(address && { address: address }),
    ...(status && { is_active: newstatus }),

   
  };
  console.log(req.body);
  await usersmodel
    .findByIdAndUpdate(elementid, data)
    .then((result) => {
      res.send({
        status: true,
        message: "Utilisateur mis à jour avec succès",
        user: result,
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: "La mise à jour de l'utilisateur a échoué"+err.message,
        err: err,
      });
    });
};
module.exports.lawyersindividual = async (req, res) => {
  const first_name=req.query.first_name?{first_name:req.query.first_name}:{};
  const last_name=req.query.last_name?{last_name:req.query.last_name}:{};
  const email=req.query.email?{email:req.query.email}:{};
  const phone=req.query.phone_number?{phone:req.query.phone_number}:{};
  const start_date=req.query.start_date?{date_of_registration:{$gte:new Date(req.query.start_date).toISOString()}}:{};
  const end_date=req.query.end_date?{date_of_registration:{$lte:new Date(req.query.end_date).toISOString()}}:{};
  const status=req.query.status?{is_active:req.query.status=="true"?true:false}:{};
  // const lawyers = await lawyermodel.find({
  //   $or: [{ cabinetname: { $exists: false } }, { cabinetname: { $eq: "" } }],
  // });
  const lawyers=await lawyermodel.find({   $or: [{ cabinetname: { $exists: false } }, { cabinetname: { $eq: "" } }],...first_name,...last_name,...email,...phone,...start_date,...end_date,...status,is_deleted:false,});
  console.log("lawyer",lawyers)
  return res.render("admin-panel/lawyersindividual.ejs", { lawyers, base_url ,moment});
};
module.exports.lawyerscabinat = async (req, res) => {
 
  const lastname=req.query.cabinetname?{cabinetname:req.query.cabinetname}:{};
  const email=req.query.email?{email:req.query.email}:{};
  const phone=req.query.phone_number?{phone:req.query.phone_number}:{};
  const start_date=req.query.start_date?{date_of_registration:{$gte:new Date(req.query.start_date).toISOString()}}:{};
  const end_date=req.query.end_date?{date_of_registration:{$lte:new Date(req.query.end_date).toISOString()}}:{};
  const status=req.query.status?{is_active:req.query.status=="true"?true:false}:{};
  console.log(status)
  const lawyers = await parentmodel.find({
    cabinetname: { $exists: true, $ne: "" },...lastname,...email,...phone,...start_date,...end_date,...status,is_deleted:false,
  });
  res.render("admin-panel/lawyerscabinat.ejs", { lawyers, base_url,moment });
};
module.exports.servicehistory = async (req, res) => {
  const student_name=req.query.client_name;
  const teacher_name=req.query.provider_name;
  const start_date=req.query.start_date;
  const end_date=req.query.end_date;
  const status=req.query.status;
  const mode_of_service=req.query.mode_of_service;
  const mode_of_payment=req.query.mode_of_payment;

  const query={
   service_status:true,
   payment_status:true
  };
  if(student_name){
  query["client_name"]=new RegExp(student_name,"i");
  }
  if(teacher_name){
    query["provider_name"]=new RegExp(teacher_name,"i");
  }
  if(start_date && !end_date){
    query["date_availed"]={$gte:new Date(req.query.start_date)}
  }
  if(end_date && !start_date){
    query["date_availed"]={$lte:new Date(req.query.end_date)}
  }
  if(start_date&&end_date){
    query["$and"]=[
      {date_availed:{$gte:new Date(req.query.start_date)}},
      {date_availed:{$lte:new Date(req.query.end_date)}}
    ]
  }
  if(status){
    query["service_status"]=status
  }
 if(mode_of_payment){
  if(mode_of_payment.toLowerCase()=="paypal"){
    
    query["card_type"]=""
  }else if(mode_of_payment.toLowerCase()=="carte"){
    query["card_type"]={$ne:""}
  }
 }
  const servicehistory = await Servicesprovided.find(query);
  // console.log(servicehistory);
  res.render("admin-panel/servicehistory.ejs", { servicehistory, base_url,moment });
};
module.exports.scheduledservices = async (req, res) => {
  const student_name=req.query.client_name;
  const teacher_name=req.query.provider_name;
  const start_date=req.query.start_date;
  const end_date=req.query.end_date;
  const status=req.query.status;
  const mode_of_service=req.query.mode_of_service;
  const mode_of_payment=req.query.mode_of_payment;
  const date = getPreviousDay();
  const query={
    service_status:false,
    payment_status:true,
    payment_amount:{$ne:0},
    provider_id:{$ne:""}
  };
  if(student_name){
  query["client_name"]=new RegExp(student_name,"i");
  }
  if(teacher_name){
    query["provider_name"]=new RegExp(teacher_name,"i");
  }
  if(start_date && !end_date){
    query["date_availed"]={$gte:new Date(req.query.start_date)}
  }
  if(end_date && !start_date){
    query["date_availed"]={$lte:new Date(req.query.end_date)}
  }
  if(start_date&&end_date){
    query["$and"]=[
      {date_availed:{$gte:new Date(req.query.start_date)}},
      {date_availed:{$lte:new Date(req.query.end_date)}}
    ]
  }
  if(status){
    query["service_status"]=status
  }
  // if(mode_of_service){
  //   query["mode_of_service"]=mode_of_service
  // }
  if(mode_of_payment){
    if(mode_of_payment.toLowerCase()=="paypal"){
    
      query["card_type"]=""
    }else if(mode_of_payment.toLowerCase()=="carte"){
      query["card_type"]={$ne:""}
    }
  }
  
  console.log(date);
  const scheduledservices = await Servicesprovided.aggregate([
    {
      $match:query
    },
    // {
    //   $addFields:{
    //     provideridOBJ:{
    //       "$toObjectId":"provider_id"
    //     }
    //   }
    // },
    // {
    //   $lookup:{
    //     from:"lawunits",
    //     localField:"provideridOBJ",
    //     foreignField:"_id",
    //     as:"provider"
    //   },
    
    // },
    // {
    //   $unwind:"$provider"
    // }
  ])
  res.render("admin-panel/scheduledservices.ejs", {
    scheduledservices,
    base_url,
    moment
  });
};
module.exports.paymenthistory = async (req, res) => {
  const student_name=req.query.client_name;
  const teacher_name=req.query.provider_name;
  const start_date=req.query.start_date;
  const end_date=req.query.end_date;
  const status=req.query.status;
  const mode_of_service=req.query.mode_of_service;
  const mode_of_payment=req.query.mode_of_payment;

  const query={
    
    payment_status:true
  };
  if(student_name){
  query["client_name"]=new RegExp(student_name,"i");
  }
  if(teacher_name){
    query["provider_name"]=new RegExp(teacher_name,"i");
  }
  if(start_date && !end_date){
    query["date_availed"]={$gte:new Date(req.query.start_date)}
  }
  if(end_date && !start_date){
    query["date_availed"]={$lte:new Date(req.query.end_date)}
  }
  if(start_date&&end_date){
    query["$and"]=[
      {date_availed:{$gte:new Date(req.query.start_date)}},
      {date_availed:{$lte:new Date(req.query.end_date)}}
    ]
  }
  if(status){
    query["service_status"]=status
  }
  if(mode_of_service){
    query["mode_of_service"]=mode_of_service
  }
  if(mode_of_payment){
    if(mode_of_payment.toLowerCase()=="paypal"){
      
      query["card_type"]=""
    }else if(mode_of_payment.toLowerCase()=="carte"){
      query["card_type"]={$ne:""}
    }
   }
  const paymenthistory=await Servicesprovided.aggregate([
    {$match:query},
    {
      $addFields:{
        provideridOBJ:{
          "$toObjectId":"$provider_id"
        }
      }
    },
    {
      $lookup:{
        from:"lawyerservices",
        localField:"provideridOBJ",
        foreignField:"id",
        as:"serviceone"
      }
    },
    {$addFields:{
      servicetwo:{$arrayElemAt:["$serviceone",0]}
    }},
  ]);
 

  const totoalnumberofreservations = paymenthistory.length;
  const pendingapproval = paymenthistory.filter(
    (reservation) => reservation.service_status === "pending"
  ).length;
  const totalearnings = paymenthistory.reduce(
    (total, reservation) => total + (reservation.payment_amount?reservation.payment_amount:0),
    0
  );
  const totaltransections = totoalnumberofreservations;
  console.log("query",query)
  res.render("admin-panel/paymenthistory.ejs", {
    paymenthistory,
    totoalnumberofreservations,
    pendingapproval,
    totalearnings,
    totaltransections,
    base_url,
    moment
  });
};
module.exports.paymentsettings = async (req, res) => {
  const paypalpaymentsettings = await paymentsettingmodel.find({
    name: "paypal",
  });
  const stripepaymentsettings = await paymentsettingmodel.find({
    name: "stripe",
  });
  // console.log(paypalpaymentsettings,stripepaymentsettings);
  setTimeout(() => {
    res.render("admin-panel/paymentsettings.ejs", {
      base_url,
      paypalpaymentsettings,
      stripepaymentsettings,
    });
  }, 1000);
};
module.exports.managelawyersincome = async (req, res) => {
  console.log(req.query)
  const query={};
  const lawyer_name_arr = req.query.provider_name?.split(" ");
  console.log(lawyer_name_arr);
  let lawyer_first_name;
  let lawyer_last_name;
  try{
    lawyer_first_name =lawyer_name_arr[0];
    lawyer_last_name = lawyer_name_arr[1];
    if(lawyer_first_name){
      query["first_name"]=lawyer_first_name
    }
    if(lawyer_last_name){
      query["last_name"]=lawyer_last_name
    }
  }catch(e){
    lawyer_first_name={};
    lawyer_last_name={};
  }
  console.log("query",query)
 
  
  const alllawyers = await lawyermodel.find(query);
  // earingsdata = [];

  // alllawyers.forEach(async (lawyer) => {
  //   let totalearnings = await Servicesprovided.find({
  //     lawyer_id: lawyer._id,
  //   }).then((service) =>
  //     service.reduce(
  //       (total, reservation) => total + reservation.payment_amount,
  //       0
  //     )
  //   );
  //   let totaljobs = await Servicesprovided.find({ lawyer_id: lawyer._id }).then(
  //     (service) => service.length
  //   );
  //   let totalcommisionearned = await Servicesprovided.find({
  //     lawyer_id: lawyer._id,
  //   }).then((service) =>
  //     service.reduce(
  //       (total, reservation) => total + parseInt(reservation.commission_earned),
  //       0
  //     )
  //   );
  //   // console.log(lawyer)
  //   earingsdata.push([lawyer, totalearnings, totaljobs, totalcommisionearned]);
  // });
  let earningsdata=[]
  
  
  for(let i=0;i<alllawyers.length;i++){
   let lawyer=alllawyers[i];
    let provider_id=lawyer._id.toHexString();
    let revenuedata=await Servicesprovided.aggregate([
 
   
      {$match:{provider_id:provider_id,service_status:true}},
      
      {$group:{_id:"$teacher_id",total_earning:{$sum:"$payment_amount"},totaljobs:{$sum:1},total_commision_earned:{$sum:{$multiply:["$payment_amount",{$divide:["$commission_earned",100]}]}}}},
      {$project:{_id:"$_id",total_earning:"$total_earning",totaljobs:"$totaljobs",total_commision_earned:"$total_commision_earned"}},
     
     
  
    ]).exec();
   
    console.log(revenuedata)
    lawyermutable = JSON.parse(JSON.stringify(lawyer));
    lawyermutable.revenuedata=revenuedata;
   
    earningsdata.push([lawyermutable]);
  };
  setTimeout(() => {
    console.log("just wait a while");
console.log(earningsdata);
    res.render("admin-panel/managelawyersincome.ejs", {
      earningsdata,
      base_url,
      moment
    });
  }, 1000);
};
module.exports.managesubscriptionpackages = async (req, res) => {
  const subscriptionpackages = await subscriptionpackagesmodel.find({});
  //   console.log(subscriptionpackages);
  const editurl = "editsubscriptionpackage/";
  res.render("admin-panel/managesubscriptionpackages.ejs", {
    subscriptionpackages,
    editurl,
    base_url,
  });
};
module.exports.createnewsubpackage = (req, res) => {
  res.render("admin-panel/createnewsubpackage.ejs", { base_url });
};

module.exports.showproviderdetails =async (req, res) => {
  const reservations1=await lawyermodel.aggregate([
    {$match:{_id:mongoose.Types.ObjectId(req.params.id)}},
    {$addFields:{
      idstring:{"$toString":"$_id"},
    }},
    {$lookup:{from:"lawyerbanddetails",localField:"idstring",foreignField:"provider_id",as:"bank"}},
    {$addFields:{
      bank:{$arrayElemAt:["$bank",0]}
    }}
  ])
  const reservations=reservations1;
  console.log("reservations",reservations)
  res.render("admin-panel/showproviderdetails.ejs", { reservations,base_url });
};
module.exports.savesubscription = (req, res) => {
  const data = req.body;
  const newsubs = subscriptionpackagesmodel();
  newsubs.package = data.package;
  newsubs.price = data.price;
  newsubs.duration = data.duration;
  newsubs.description = data.description;
  newsubs.type=data.type;
  
  let is_multiple_photo_new=false;
  if(data.is_multiple_photo!=""){
    is_multiple_photo_new=true
  }
  let is_boosted=false;
  if(data.is_booster!=""){
    is_boosted=true
  }
  console.log("req.body",req.body)
  newsubs.is_multiple_photos_allowed=is_multiple_photo_new;
  newsubs.is_boosted=is_boosted;
  // console.log("data.is_multiple_photo",data.is_multiple_photo,"is_multiple_photo_new",is_multiple_photo_new,"newsubs.is_multiple_photos_allowed",newsubs.is_multiple_photos_allowed)
  newsubs
    .save()
    .then((result) => {
      res.send({
        status: true,
        message: "Paquet d'abonnement créé avec succès",
        data: result,
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({ status: false, message: err });
    });
};
module.exports.Editsubscriptionpackage = async (req, res) => {
  const package_id = req.params.package_id;
  const subpackage = await subscriptionpackagesmodel.findById(package_id);
  res.render("admin-panel/editsubpackage.ejs", {
    subpackage,
    base_url,
  });
};
module.exports.dummyfunction = (req, res) => {
  const subpackage = [];
  res.render("admin-panel/editsubpackage.ejs", {
    subpackage,
  });
};
module.exports.updatesubscriptionpackage = async (req, res) => {
  const data = req.body;
  const package_id = data.elementid;
  const package = data.package;
  const price = data.price;
  const duration = data.duration;
  const description = data.description;
  const type=data.type;
  const is_multiple_photo=data.is_multiple_photo;
  let is_multiple_photo_new=false;
  if(data.is_multiple_photo!=""){
    is_multiple_photo_new=true
  }
  let is_boosted=false;
  if(data.is_booster!=""){
    is_boosted=true
  }
  // newsubs.is_multiple_photos_allowed=is_multiple_photo_new;
  // newsubs.is_boosted=is_boosted;
  console.log("req.body",data)
  const newdata = {
    ...(package && { package }),
    ...(price && { price }),
    ...(duration && { duration }),
    ...(description && { description }),
    ...(type && { type })
  };
  await subscriptionpackagesmodel
    .findByIdAndUpdate(package_id, newdata)
    .then(() => {
      res.send({
        status: true,
        message: "Le paquet d'abonnement a été mis à jour avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.deletesubscriptionpackage = async (req, res) => {
  const package_id = req.params.package_id;
  await subscriptionpackagesmodel
    .findByIdAndDelete(package_id)
    .then(() => {
      res.send({
        status: true,
        message: "Paquet d'abonnement supprimé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.viewsubscriptionpackage = async (req, res) => {
  const package_id = req.params.package_id;
  const subpackage = await subscriptionpackagesmodel.findById(package_id);
  res.render("admin-panel/viewsubpackage.ejs", { subpackage, base_url });
};
module.exports.managesubscriptions = async (req, res) => {
  const query={}
  const provider_name=req.query.provider_name;
  const typeofsub=req.query.typeofsub;
  const email=req.query.email
 
  const start_date=req.query.start_date
  const end_date=req.query.end_date
  const status=req.query.status?{status:req.query.status=="true"?true:false}:{};
  if(provider_name){
    query["subscriber_name"]=new RegExp(provider_name,"i");
    
    }
    if (email) {
      query["subscriber_email"] = email;
  
  }
  
  if (start_date) {
      query["start_date"] ={"$gte": new Date(start_date),
      "$lt": new Date(start_date.setDate(date.getDate() + 1))}
  
  }
  if (end_date) {
    query["end_date"] ={"$gte": new Date(end_date),
    "$lt": new Date(end_date.setDate(date.getDate() + 1))}
  
  }
  if (status=="true") {
      query["end_date"] = {"$gte": new Date()}
  
  }else if(status=="false"){
    query["end_date"] = {"$lte": new Date()}
  }
  console.log(query)
  let subscriptions = await usersubscriptionmodel.find(query);
  res.render("admin-panel/managesubscriptions.ejs", {
    subscriptions,
    base_url,
    moment
  });
};
module.exports.manageboostersubscription = async (req, res) => {
  const query={}
  const provider_name=req.query.provider_name;
  const typeofsub=req.query.typeofsub;
  const email=req.query.email
 
  const start_date=req.query.start_date
  const end_date=req.query.end_date
  const status=req.query.status;
  if(provider_name){
    query["subscriber_name"]=new RegExp(provider_name,"i");
    
    }
    if (email) {
      query["subscriber_email"] = email;
  
  }
  
  if (start_date) {
      query["start_date"] ={"$gte": new Date(start_date),
      "$lt": new Date(start_date.setDate(date.getDate() + 1))}
  
  }
  if (end_date) {
    query["end_date"] ={"$gte": new Date(end_date),
    "$lt": new Date(end_date.setDate(date.getDate() + 1))}
  
  }
  if (status=="true") {
      query["end_date"] = {"$gte": new Date()}
  
  }else if(status=="false"){
    query["end_date"] = {"$lte": new Date()}
  }
  
  let subscriptions = await Boosterpackagessubs.find(query);
  console.log("Boosterpackagessubs",subscriptions)
  res.render("admin-panel/manageboostersubscriptions.ejs", {
    subscriptions,
    base_url,
    moment
  });
};
module.exports.viewlawyer = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  const google_api_key=process.env.GOOGLE_MAPS_API_KEY;
  const lawyer = await lawyermodel.findById(lawyer_id);
   const bank=await LawyerBandDetails.findOne({provider_id:lawyer_id});
  console.log(lawyer);
  res.render("admin-panel/viewlawyer.ejs", {google_api_key,bank,lawyer, base_url, root_url });
};
module.exports.deletelawyer = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  const lawyer = await lawyermodel.findById(lawyer_id);

  const updatedemail =
    lawyer.lawyer_email + "_deleted_" + Math.floor(Math.random() * 10000);
  await lawyer
    .updateOne({ is_deleted: true, lawyer_email: updatedemail })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat supprimé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.activatelawyer = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  await lawyermodel
    .findByIdAndUpdate(lawyer_id, { is_active: true })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat activé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.deactivateawyer = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  await lawyermodel
    .findByIdAndUpdate(lawyer_id, { is_active: false })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat désactivé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.updatelawyer = async (req, res) => {
  let photo;
  let fid="";
  let bid=""
  let fdl="";
  let bdl="";
  let passport="";

  try {
    if (req.files.photo) {
      photo = "userprofile/" + req.files.photo[0].filename;
    }
     if (req.files.fid) {
      fid = "userprofile/" + req.files.fid[0].filename;
    }
    if (req.files.bid) {
      bid = "userprofile/" + req.files.bid[0].filename;
    }
    if (req.files.fdl) {
      fdl = "userprofile/" + req.files.fdl[0].filename;
    }
    if (req.files.bdl) {
      bdl = "userprofile/" + req.files.bdl[0].filename;
    }
    if (req.files.passport) {
      passport = "userprofile/" + req.files.passport[0].filename;
    }
  
   console.log("photo",req.photo,"certificates",certificates,"identifications",identifications)
  } catch (err) {}
  let {
    first_name,
    last_name,
    email,
    password,
    proffession,
    sex,
    country,
    desc,
    pin,
    phone,

    experience,
    dob,
    expertise_in,
    presentation,
    elementid,
    resetpassword,
    address,
    bankname,
    accountname,
    iban,
    bic,
    competence,
    paymobile
  } = req.body;
  let encryptedPassword;
  if (resetpassword) {
    encryptedPassword = await bcrypt.hash(resetpassword, 10);
  }
  let latlongarray=[];
      if(address){
       let latlongresult= await getlatlong(address)

        latlongarray.push(latlongresult.lng,latlongresult.lat);
      }
      // if(address2){
      //   latlongarray.push(await getlatlong(address2));
      // }
      console.log("latlongarray",latlongarray);
      const bank=await LawyerBandDetails.findOne({lawyer_id:elementid});
      if(bank){
        bank.bank_name=bankname;
        bank.iban=iban;
        bank.account_name=accountname;
        bank.bic=bic;
        
        bank.save();
        
      }else{
          newBank=new LawyerBandDetails();
          newBank.bank_name=bankname;
          newBank.iban=iban;
          newBank.account_name=accountname;
          newBank.bic=bic;
          newBank.lawyer_id=elementid;
          newBank.save();
      }
  let data = {
    ...(photo && { photo }),
    ...(first_name && { first_name }),
    ...(last_name && { last_name }),
    ...(email && { email }),
    ...(password && { password }),
    ...(address && { address:{address:address,latlong:latlongarray} }),
  
   
    ...(phone && { phone }),
  
    ...(dob && { dob }),
    ...(competence&&{competence}),
    ...(proffession && { proffession:proffession }),
    ...(sex && { sex:sex }),
    ...(pin && { pin:pin }),
    ...(country && { country:country }),
    ...(desc && { description:desc }),
   
    
    ...(passport&&{passport:passport}),
    ...(fdl&&{'drivinglicence.front':fdl}),
    ...(bdl&&{'drivinglicence.back':bdl}),
    ...(bid&&{'identifications.back':bid}),
    ...(fid&&{'identifications.front':fid}),
    ...(resetpassword && { password: encryptedPassword }),
  
  };
  // console.log(data);
  lawyermodel
    .findByIdAndUpdate(elementid, data)
    .then((lawyer) => {
      res.send({
        status: true,
        message: "Avocat mis à jour avec succès",
        lawyer,
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};

module.exports.viewlawyercabinat = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  const lawyer = await parentmodel.findById(lawyer_id);
 
  res.render("admin-panel/viewlawyercabinet.ejs", {
    lawyer,
   
    base_url,
    root_url,
  });
};
module.exports.deletelawyercabinat = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  await parentmodel
    .findByIdAndUpdate(lawyer_id, { is_deleted: true })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat supprimé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.activatelawyercabinat = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  await parentmodel
    .findByIdAndUpdate(lawyer_id, { is_active: true })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat supprimé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.deactivateawyercabinat = async (req, res) => {
  const lawyer_id = req.params.lawyer_id;
  console.log(req.params,"in deactivateawyercabinat");
  await parentmodel
    .findByIdAndUpdate(lawyer_id, { is_active: false })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat désactivé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.updatelawyercabinat = async(req, res) => {
  
  let {
    
    email,
    password,
    elementid,
    status

    
  } = req.body;
  // console.log(req.body.onlineprice)
  if(password){
    encryptedPassword = await bcrypt.hash(password, 10);
  }
  let newstatus;
  if(status!=""){
    if(status==0 || status=="0"){
      newstatus=false
    }else if(status==1 || status=="1"){
      newstatus=true
    }
  }
  let data = {
   
    ...(email && { email }),
    ...(password && { password:encryptedPassword }),
     ...(status!="" && { is_active:newstatus })
    
   
  };
  console.log(data);
  parentmodel
    .findByIdAndUpdate(elementid, data)
    .then((lawyer) => {
      res.send({
        status: true,
        message: "avocat mis à jour avec succès",
        lawyer,
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.create_payment_setting = async (req, res) => {
  const { apikey, apisecret, paymentprovider, env_type } = req.body;
  console.log("req.body",req.body)
  let data;
  if (env_type == "live") {
    data = {
      ...(apikey && { liveapikey: apikey }),
      ...(apisecret && { liveapisecret: apisecret }),
    };
  } else if (env_type == "sandbox") {
    data = {
      ...(apikey && { testapikey: apikey }),
      ...(apisecret && { testapisecret: apisecret }),
    };
  }

  const ispaymentsetting = await paymentsettingmodel.findOne({
    name: paymentprovider,
  });

  if (ispaymentsetting) {
    await paymentsettingmodel
      .findOneAndUpdate({ name: paymentprovider }, data, { new: true })
      .then((payment_setting) => {
        res.send({
          status: true,
          message: "Les paramètres de paiement ont été mis à jour avec succès",
          payment_setting,
        });
      })
      .catch((err) => {
        console.log(err);
        res.send({
          status: false,
          message: err,
        });
      });
  } else {
    await paymentsettingmodel
      .create({
        apikey,
        apisecret,
        name: paymentprovider,
        active: true,
      })
      .then((payment_setting) => {
        res.send({
          status: true,
          message: "le paramètre de paiement a été créé avec succès",
          payment_setting,
        });
      })
      .catch((err) => {
        console.log(err);
        res.send({
          status: false,
          message: err,
        });
      });
  }
};
module.exports.carte = async (req, res) => {
  const users = await lawyermodel.find({is_deleted:false});
const google_api_key=process.env.GOOGLE_MAPS_API_KEY;
  let geocodes = [];
  
  try{
    for(let i=0;i<users.length;i++){
      let user = users[i];
      // console.log(user)
      if(user?.address.length){
        if(user.address[0].latlong){
            if(user?.address[0]?.latlong.length){
                if(user?.address[0]?.latlong[0]){
                    let lat = user.address[0].latlong[1];
            let lng = user.address[0].latlong[0];
        
            let newlatlong = { lat: lat, lng: lng }
            geocodes.push([
                newlatlong,
                user.first_name + " " + user.last_name ? user.first_name + " " + user.last_name : cabinetname,
                user.photo,
                user._id
            ]);
                }
            }
        }
    }
    }
    console.log("geocodes",geocodes)
  }catch(e){
    console.log(e)
  }
 // console.log(geocodes)
  // let newmarkerstring=markerstring.substring(1);

  // const mapurl=`https://maps.googleapis.com/maps/api/staticmap?&size=1000x400&maptype=roadmap&markers=${newmarkerstring}&key=AIzaSyC7uyZ6P0gyfKIlKKkXCKocCSee-ZFHPvQ`;
  res.render("admin-panel/carte.ejs", { google_api_key,base_url, geocodes, root_url });
};
module.exports.siteparameters = async (req, res) => {
  const siteparameters = await adminmodel.findOne({});
  res.render("admin-panel/siteparameters.ejs", { siteparameters, base_url });
};
module.exports.savesiteparameters = async (req, res) => {
  const { commissions, email, currentpassword, newpassword, confirmpassword,tax } =
    req.body;
  const admin = await adminmodel.findOne({ email: email });

  if (admin) {
    if (currentpassword != "" && newpassword != "" && confirmpassword != "") {
      console.log(
        currentpassword,
        newpassword,
        confirmpassword,
        "isok",
        bcrypt.compare(currentpassword, admin.password)
      );
      if (admin && (await bcrypt.compare(currentpassword, admin.password))) {
        const data = {
          password: bcrypt.hashSync(newpassword, 10),
          ...(commissions && { commissions: commissions }),
           ...(tax && { tax: tax }),
        };
        if (newpassword == confirmpassword) {
          await adminmodel.findOneAndUpdate({ email: email }, data);

          res.send({
            status: true,
            message: "Le mot de passe a été mis à jour avec succès",
          });
        } else {
          res.send({
            status: false,
            message: "Le nouveau mot de passe et le mot de passe de confirmation ne correspondent pas",
          });
        }
      } else {
        res.send({
          status: false,
          message: "Soit l'email n'existe pas, soit le mot de passe actuel ne correspond pas.",
        });
      }
    } else {
      await adminmodel.findOneAndUpdate(
        { email: email },
        { ...(commissions && { commissions: commissions }),
        ...(tax && { tax: tax }) }
        
      );
      res.send({
        status: true,
        message: "La Commission a été mise à jour avec succès",
      });
    }
  } else {
    res.send({
      status: false,
      message: "Admin non trouvé",
    });
  }
};
module.exports.logout = async (req, res) => {
  req.session.destroy();
  res.redirect("/admin-panel/login");
};
module.exports.cms = (req, res) => {
  res.render("admin-panel/cms.ejs", { base_url });
};
module.exports.login = (req, res) => {
  res.render("admin-panel/login.ejs", { base_url });
};

module.exports.forgotpassword = (req, res) => {
  res.render("admin-panel/forgotpassword.ejs", { base_url });
};
module.exports.checklogin = async (req, res) => {
  const { email, password } = req.body;
  const admin = await adminmodel.findOne({ email: email });
  if (admin && (await bcrypt.compare(password, admin.password))) {
    req.session.user_id = admin._id;

    return res.send({
      status: true,
      message: "Connexion réussie",
    });
  } else {
    res.send({
      status: false,
      message: "L'adresse électronique ou le mot de passe ne correspondent pas",
    });
  }
};

module.exports.saveuser = async (req, res) => {
  const { email, password } = req.body;
  try {
    console.log(email, password, req.body);
    let encryptedPassword = await bcrypt.hash(password, 10);
    adminmodel
      .create({ email, password: encryptedPassword, commissions: 0 })
      .then((result) => {
        res.send({
          status: true,
          message: "Utilisateur créé avec succès",
        });
      })
      .catch((err) => {
        console.log(err);
        res.send({
          status: false,
          message: err,
        });
      });
  } catch (err) {
    res.send({
      status: false,
      message: err,
    });
  }
};
module.exports.register = async (req, res) => {
  res.render("admin-panel/register.ejs", { base_url, root_url });
};
module.exports.notification = async (req, res) => {
  res.render("admin-panel/notification.ejs", { root_url, base_url });
};
module.exports.sendnotification = async (req, res) => {
 try{
   const { notificationtype, usertype, selecteduser, message } = req.body;
  console.log(notificationtype, usertype, selecteduser, message);
   console.log("req.body",req.body)
  let models;

  if (usertype == "user") {
    models = usersmodel;
  } else if (usertype == "lawyerind") {
    models = lawyermodel;
  } 
  let topic;
  if(usertype=="user"){
    topic=process.env.TOUSERTOPIC;
  }else if(usertype=="lawyerind"){
    topic=process.env.LAWYERINDTOPIC;
  }else if(usertype=="lawyerent"){
    topic=process.env.LAWYERENTTOPIC;
  }
  if(selecteduser[0]=="all"){
    sendpushnotificationtotopic(message,topic);
  }else{
    selecteduser?.map(async (user) => {
      await models.findById(user).then(async (usr) => {
        if (notificationtype == "email") {
          await sendemail(usr, message).then((result) => {
            notificationmodel.create({
              to_id: usr._id,
              from_email: process.env.ADMINEMAIL,
              message: message,
              status: "unread",
              notification_type: notificationtype,
              date: new Date(),
            });
          });
        } else if (notificationtype == "app") {
          topic="listentoadmin";
          console.log("in app folder")
         if(usr.fcm_token)
          { 
            
           sendpushnotificationtouser( message,usr.fcm_token,usr._id).then((result) => {
            
            console.log(result);
          }).catch((err) => {
            console.log(err);
          })}
        }
      });
    });
  }
 

  res.send({
    status: true,
    message: "Notification envoyée avec succès",
  });
}catch(err){
  console.log(err)
}
};
module.exports.sendemail = async (user, message) => {
  var data = {
    to: user.email,
    from: process.env.MAILER_EMAIL_ID,
    template: "emailnotification",
    subject: "notification from admin",
    context: {
      name: user.first_name,
      message: message,
    },
  };
  var smtpTransport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "c64a6b1cb5eec9",
      pass: "64b164b6b43eed",
    },
  });

  const handlebarOptions = {
    viewEngine: {
      partialsDir: path.resolve("./views/admin-panel/"),
      defaultLayout: false,
    },
    viewPath: path.resolve("./views/admin-panel/"),
  };
  // console.log("data in email",data)
  smtpTransport.use("compile", hbs(handlebarOptions));
  smtpTransport.sendMail(data, function (err) {
    if (!err) {
      console.log("email sent");
      return true;
    } else {
      console.log(err);
      return false;
    }
  });
};
module.exports.viewnotifications = async (req, res) => {
  const notifications = await notificationmodel.find({});
  // console.log(notifications);
  let newnotifications=JSON.parse(JSON.stringify(notifications));
  for(let i=0;i<notifications.length;i++){
    console.log(notifications[i].to_id,notifications[i])
  const user = await usersmodel.findById(notifications[i].to_id);
  const lawyer=await lawyermodel.findById(notifications[i].to_id);
  console.log(user)
  console.log(lawyer)
  if(user){
    // console.log("user",user);
    newnotifications[i].name=user?.first_name+" "+user?.last_name;
  }else{
    console.log("lawyer",lawyer);
    newnotifications[i].name=lawyer?.first_name+" "+lawyer?.last_name;
  }
  }
  console.log(newnotifications)
  res.render("admin-panel/viewnotifications.ejs", {
    base_url,
    root_url,
    notifications:newnotifications,
    moment
  });
};
module.exports.getusers = async (req, res) => {
  const user_type = req.params.user_type;
  let users;
  if (user_type == "user") {
    users = await usersmodel.find({is_deleted:false});
  } else if (user_type == "lawyerind") {
    users = await lawyermodel.find({
     
      is_deleted:false
    });
  } else if (user_type == "lawyerent") {
    users = await lawyermodel.find({ cabinetname: { $exists: true, $ne: "" } ,is_deleted:false});
  }

  res.send({
    data: users,
    status: true,
  });
};
module.exports.expertise = async (req, res) => {
  const expertise = await expertisemodel.find({});
  return res.render("admin-panel/expertise.ejs", { base_url, root_url, expertise });
};
module.exports.addexpertise = async (req, res) => {
   return res.render("admin-panel/addexpertise.ejs", { base_url, root_url });
};
module.exports.editexpertise = async (req, res) => {
  const expertise_id = req.params.expertise_id;
  const expertise = await expertisemodel.findById(expertise_id);
  res.render("admin-panel/editexpertise.ejs", {
    base_url,
    root_url,
    expertise,
  });
};
module.exports.updateexpertise = async (req, res) => {
  const { expertise_id, field } = req.body;
  console.log(req.body);
  await expertisemodel
   .find({name:field})
    .then(async (result)=>{
      if(result.length>0){
        res.send({
          status: false,
          message: "L'expertise existe déjà",
        });
      }else{
        await expertisemodel.findByIdAndUpdate(expertise_id, { name: field }).then(async (result) => {
          res.send({
            status: true,
            message: "Expertise mise à jour avec succès",
          });
        });
      }
    })
};
module.exports.deleteexpertise = async (req, res) => {
  const id = req.params.expertise_id;
  await expertisemodel
    .findByIdAndDelete(id)
    .then((result) => {
      res.send({
        status: true,
        message: "Expertise supprimée avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.saveexpertise = async (req, res) => {
  const { field } = req.body;
  console.log(req.body);

  await expertisemodel
    .find({ name: field })
    .then((result) => {
      if (result.length > 0) {
        res.send({
          status: false,
          message: "L'expertise existe déjà",
        });
      } else {
        expertisemodel
          .create({
            name: field,
          })
          .then((result) => {
            res.send({
              status: true,
              message: "Une expertise ajoutée avec succès",
            });
          })
          .catch((err) => {
            res.send({
              status: false,
              message: err,
            });
          });
      }
    })
};
module.exports.getexpertise = async (req, res) => {
  try {
    const expertise = await expertisemodel.find({}).exec(function (err, data) {
    console.log(err,data);
      if (data.length) {
        if (err) {
          res.send({
            status: false,
            expertise: null,
            message: "La recherche d'expertise a échoué",
            error: err.message,
          });
        }
        res.send({
          status: true,
          expertise: data,
          message: "Expertise récupérée avec succès",
        });
      } else {
        res.send({
          status: true,
          expertise: [],
          message: "expertise data null",
         
        });
      }
    });
  } catch (err) {
    console.log(err);
    res.send({
      status: false,
      message: err,
    });
  }
};

module.exports.language = async (req, res) => {
  const language = await languagemodel.find({});
  return res.render("admin-panel/language.ejs", { base_url, root_url, language });
};
module.exports.addlanguage = async (req, res) => {
  return res.render("admin-panel/addlanguage.ejs", { base_url, root_url });
};
module.exports.editlanguage = async (req, res) => {
  const language_id = req.params.language_id;
  const language = await languagemodel.findById(language_id);
  return res.render("admin-panel/editlanguage.ejs", { base_url, root_url, language });
};
module.exports.updatelanguage = async (req, res) => {
  const { expertise_id, field } = req.body;
  console.log(req.body);
  await languagemodel.find({ name: field }).then((result) => {
    if (result.length) {
      res.send({
        status: false,
        message: "La langue existe déjà",
      });
    } else {
      languagemodel
        .findByIdAndUpdate(expertise_id, { name: field })
        .then((result) => {
          res.send({
            status: true,
            message: "La langue a été mise à jour avec succès",
          });
        })
        .catch((err) => {
          res.send({
            status: false,
            message: err,
          });
        });
    } 
  })
};
module.exports.deletelanguage = async (req, res) => {
  const id = req.params.language_id;
  await languagemodel
    .findByIdAndDelete(id)
    .then((result) => {
      res.send({
        status: true,
        message: "Langue supprimée avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err.message,
      });
    });
};
module.exports.savelanguage = async (req, res) => {
  const { field } = req.body;
  console.log(req.body);

  await languagemodel.find({name:field}).then(async (result)=>{
    if(result.length){
      res.send({
        status: false,
        message: "La langue existe déjà",
      });
    }else{
      await languagemodel
    .create({ name: field })
    .then((result) => {
      res.send({
        status: true,
        message: "La langue a été sauvegardée avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
    }
  })
    
};
module.exports.getlanguage = async (req, res) => {
  try {
    const expertise = await languagemodel.find({}).exec(function (err, data) {
    console.log(err,data);
      if (data.length) {
        if (err) {
          res.send({
            status: false,
            expertise: null,
            message: "language fetched failed",
            error: err.message,
          });
        }
       return res.send({
          status: true,
          expertise: data,
          message: "language fetched successfully",
        });
      } else {
       return res.send({
          status: true,
          expertise: [],
          message: "language data null",
         
        });
      }
    });
  } catch (err) {
    console.log(err);
    return res.send({
      status: false,
      message: err,
    });
  }
};

const getlatlong = async (address) => {
  const apikey = process.env.GOOGLE_MAPS_API_KEY;
  let data;
  try {
    data = await axios
      .get(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${apikey}`
      )
      .then((result) => {
        return result.data.results[0].geometry.location;
      });
  } catch (err) {}
  return data;
};
module.exports.viewpaymentdetails = async (req, res) => {
  const payment_id = req.params.payment_id;
  const payment = await Servicesprovided.findById(payment_id);
  return res.render("admin-panel/viewpaymentdetails.ejs", {
    base_url,
    root_url,
    payment,
    moment
  });
};
module.exports.save_cms_page = async (req, res) => {
  const { page_name, page_content } = req.body;
  await cmsmodel
    .create({ page_name, page_content })
    .then((result) => {
      return res.send({
        status: true,
        message: "Page sauvegardée avec succès",
      });
    })
    .catch((err) => {
      return res.send({
        status: false,
        message: err.message,
      });
    });
};
module.exports.listpages = async (req, res) => {
  const pages = await cmsmodel.find({});
  return res.render("admin-panel/listpages.ejs", { base_url, root_url, pages });
};
module.exports.editpage = async (req, res) => {
  const id = req.params.page_id;
  const page = await cmsmodel.findById(id);
  return res.render("admin-panel/editpage.ejs", { base_url, root_url, page });
};
module.exports.updatepage = async (req, res) => {
  const { id, page_name, page_content } = req.body;
  console.log(req.body);
  await cmsmodel
    .findByIdAndUpdate(id, { page_name, page_content })
    .then((result) => {
      return res.send({
        status: true,
        message: "Page mise à jour avec succès",
      });
    })
    .catch((err) => {
      return res.send({
        status: false,
        message: err.message,
      });
    });
};
module.exports.deletepage = async (req, res) => {
  const id = req.params.page_id;
  await cmsmodel
    .findByIdAndDelete(id)
    .then((result) => {
      return res.send({
        status: true,
        message: "Page supprimée avec succès",
      });
    })
    .catch((err) => {
      return res.send({
        status: false,
        message: err.message,
      });
    });
};
module.exports.reservations = async (req, res) => {
  const reservations = await Servicesprovided.findById(req.params.reservation_id);
  return res.render("admin-panel/reservations.ejs", { base_url, root_url, reservations,moment });
}
module.exports.viewservicehistory = async (req, res) => {
  const service_id = req.params.service_id;
  const service = await Servicesprovided.findById(service_id);
  return res.render("admin-panel/viewservicehistory.ejs", {
    base_url,
    root_url,
    service,
    moment
  });
}
module.exports.viewserviceschedule=async (req, res) => {
  const service_id = req.params.service_id;
  const service = await Servicesprovided.findById(service_id);
  res.render("admin-panel/viewserviceschedule.ejs", {
    base_url,
    root_url,
    service,
    moment  
  });
}

module.exports.setservicestatus = async (req, res) => {

  const serviceid = req.params.serviceid;
 
  
 await Servicesprovided.find({_id:serviceid}).then((service)=>{
  {
    Servicesprovided.findByIdAndUpdate(serviceid, {service_status:service[0].service_status?false:true}).then(function(result){
     res.send({
      status: true,
      message: "Le statut du service a été mis à jour avec succès",
     })
    }).catch(function(err){
      console.log(err);
      res.send({
        status: false,
        message: err.message,
      })
    })
  
   }
 })
}
module.exports.getpages = async (req, res) => {
  await cmsmodel
    .find({})
    .then((result) => {
      res.send({
        status: true,
        message: "Page sauvegardée avec succès",
        errmessage:"",
        data: result,
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message:"",
        data:null,
        errmessage:err.message
      });
    });
}
module.exports.apptext_getpages = async (req, res) => {
  await apptext_cmsmodel
    .find({})
    .then((result) => {
      res.send({
        status: true,
        message: "Page sauvegardée avec succès",
        errmessage:"",
        data: result,
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message:"",
        data:null,
        errmessage:err.message
      });
    });
}

module.exports.updatepaidamount=async(req,res)=>{
 try{
  const id=req.params.id;
  const amount=req.params.amount;
  console.log(id,amount);
  lawyermodel.findById(id).then((doc)=>{
   
     
  //  if(doc?.totalpaidamount){
  //   doc.totalpaidamount=parseInt(doc.totalpaidamount)+parseInt(amount);
  //   doc.totalpendingamount=parseInt(doc.totalpendingamount)-parseInt(amount);
  //  }else{
  //   //  console.log(doc)
  //   doc.totalpaidamount=parseInt(amount);
  //   doc.totalpendingamount=parseInt(doc.totalpendingamount)-parseInt(amount);
  //  }
    
  //   doc.save();
  if(doc.totalpendingamount!=0){
    if(amount>doc.totalpendingamount){
     return res.send({
       status:false,
       message:"Le montant saisi est supérieur au montant en attente, veuillez saisir un montant valide.",
       errmessage:"Le montant saisi est supérieur au montant en attente, veuillez saisir un montant valide.",
       data:null
     })
    }else{
     if(doc?.totalpaidamount){
       doc.totalpaidamount=parseInt(doc.totalpaidamount)+parseInt(amount);
       doc.totalpendingamount=parseInt(doc.totalpendingamount)-parseInt(amount);
      }else{
       //  console.log(doc)
       doc.totalpaidamount=parseInt(amount);
       doc.totalpendingamount=parseInt(doc.totalpendingamount)-parseInt(amount);
      }
       
       doc.save().then(async(result)=>{
        await AdminPayoutmodel.create({
          teacher_id:id,
          payment_amount:amount,
          payment_date:new Date()
        });
        res.send({
          status:true,
          message:"Montant mis à jour avec succès",
          errmessage:"",
          data:result
        })
       })
    }
    }else{
      return res.send({
        status:false,
        message:"Il n'y a plus de montant en attente",
        errmessage:"Il n'y a plus de montant en attente",
        data:null
      })
    }
  }).catch((err)=>{
    console.log(err)
    res.send({
      status:false,
      message:err.message
    })
  })
 
 }catch(err){
  //  console.log(err);
 }
}


module.exports.downloadinvoice=async(req,res)=>{
  console.log(req.params)
  try{
    const id=req.params.id;
    // const transaction_id=req.params.transaction_id;
    let invoice = await Servicesprovided.findOne({_id:id});
    if(invoice){
      const frenchdate=frenchDayDate(invoice.date_of_transaction);
      invoice=JSON.parse(JSON.stringify(invoice));
      invoice["frenchdate"]=frenchdate;
      console.log(invoice)
      const filepath=path.join(__dirname, "../../views/admin-panel/clientinvoice.ejs");
      console.log(filepath)
      const html = await ejs.renderFile(filepath,{invoice:invoice});
      console.log(html)
      const options = { format: 'Letter' };
      
    pdf.create(html, options).toFile(path.join(__dirname, "../../views/admin-panel/public/invoices/")+'invoice'+id+'.pdf', function(err, result) {
      if (err) return console.log("in pdf create",err);
      console.log(root_url+"/static/public/invoices/invoice"+id+".pdf")
      // console.log(path.join(__dirname, "../../views/admin-panel/public/invoices/invoice"+id+".pdf"));
      res.send({
        status:true,
        url:root_url+"/static/public/invoices/invoice"+id+".pdf",
        message:"Facture téléchargée avec succès"
      })
    });
    }else{
      res.send({
        status:false,
        message:"Facture non trouvée"
      })
    }
    
   }catch(err){
     console.log("in catch block",err)
     res.send(err.message);
   }
}
module.exports.apptext=async(req,res)=>{
  res.render("admin-panel/apptext_cms.ejs", { base_url });
}
module.exports.apptext_editpage=async(req,res)=>{
  const id = req.params.page_id;
  const page = await apptext_cmsmodel.findById(id);
  res.render("admin-panel/apptext_editpage.ejs", { base_url, root_url, page });
}
module.exports.apptext_updatepage=async(req,res)=>{
  const { id, page_name, page_content,page_heading } = req.body;
  console.log(id, page_name, page_content,page_heading)
  console.log(req.body);
  await apptext_cmsmodel
    .findByIdAndUpdate(id, { page_name, page_content ,page_heading})
    .then((result) => {
      res.send({
        status: true,
        message: "Page mise à jour avec succès",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: err.message,
      });
    });
}
module.exports.apptext_deletepage=async(req,res)=>{
  const id = req.params.page_id;
  await apptext_cmsmodel
    .findByIdAndDelete(id)
    .then((result) => {
      res.send({
        status: true,
        message: "Page supprimée avec succès",
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: err.message,
      });
    });
}
module.exports.apptext_listpages=async(req,res)=>{
  const pages = await apptext_cmsmodel.find({});
  res.render("admin-panel/apptext_listpages.ejs", { base_url, root_url, pages });
}
module.exports.apptext_save_cms_page=async(req,res)=>{
  const { page_name, page_content,page_heading } = req.body;
  await apptext_cmsmodel
    .create({ page_name, page_content,page_heading })
    .then((result) => {
      res.send({
        status: true,
        message: "Page sauvegardée avec succès",
        data:null,
        errmessage:""
      });
    })
    .catch((err) => {
      res.send({
        status: false,
        message: err.message,
        data:null,
        errmessage:err.message
      });
    });
}




module.exports.addcatagoryonline=async(req,res)=>{
  let id=mongoose.Types.ObjectId(req.body.id);
  const query={};  
    let cat;
    let cat1,cat2;
    cat1=await catagorymodel.find({mastercatagory:null})
    cat=await catagorymodel.aggregate([
      // {$mathc:is_parent:true},
      {$match:{mastercatagory:null}},
      {
        $graphLookup:{
          "from": "catagories",
          "startWith": "$_id",
          "connectFromField": "_id",
          "connectToField": "mastercatagory",
          "as": "subs",
          "maxDepth": 20,
          "depthField": "level",
          
        },
       
      },
     
      { 
        "$unwind": {
        "path": "$subs",
        "preserveNullAndEmptyArrays": true
    } 
    },
   
   
      {
        $sort: {
          "subs.level": -1
        }
      },
      {
        $group: {
          _id: "$_id",
          
          parent_id: {
            $first: "$mastercatagory"
          },
          title: {
            $first: "$title"
          },
          subs: {
            $push: "$subs"
          }
        }
      },
      {
        $addFields: {
          subs: {
            $reduce: {
              input: "$subs",
              initialValue: {
                currentLevel: -1,
                currentLevelChildren: [],
                previousLevelChildren: []
              },
              in: {
                $let: {
                  vars: {
                    prev: {
                      $cond: [
                        {
                          $eq: [
                            "$$value.currentLevel",
                            "$$this.level"
                          ]
                        },
                        "$$value.previousLevelChildren",
                        "$$value.currentLevelChildren"
                      ]
                    },
                    current: {
                      $cond: [
                        {
                          $eq: [
                            "$$value.currentLevel",
                            "$$this.level"
                          ]
                        },
                        "$$value.currentLevelChildren",
                        []
                      ]
                    }
                  },
                  in: {
                    currentLevel: "$$this.level",
                    previousLevelChildren: "$$prev",
                    currentLevelChildren: {
                      $concatArrays: [
                        "$$current",
                        [
                          {
                            $mergeObjects: [
                              "$$this",
                              {
                                subs: {
                                  $filter: {
                                    input: "$$prev",
                                    as: "e",
                                    cond: {
                                      $and:[
                                        {
                                        $eq: [
                                        "$$e.mastercatagory",
                                        "$$this._id"
                                      ]
                                        }
                                      
                                    ]
                                    }
                                  }
                                }
                              }
                            ]
                          }
                        ]
                      ]
                    }
                  }
                }
              }
            }
          }
        }
      },
      {
        $addFields: {
          subs: "$subs.currentLevelChildren"
        }
      },
      {
        $match: {
          mastercatagory: null
        }
      }
    ]);
   let noparent= {
    _id:'',
    parent_id: '',
    title: 'Aucun parent',
    subs: []
  };

   cat?.unshift(noparent);
   console.log("cat2====>",cat2)
   console.log("noparent====>",noparent)
   console.log("cat====>",cat)
   console.log("type of cat2====>",typeof cat2)
  return res.render("admin-panel/addcatagoryonline.ejs", {cat, base_url, root_url });
}
module.exports.savecatagoryonline=async(req,res)=>{
  console.log("req.body",req.body)
  const catagory=req.body.catagory;
  
  let masterid;
  if(req.body.mastercatagory){
    const catagory=await catagorymodel.findOne({title:req.body.mastercatagory});
    masterid=catagory?._id;
  }else{
   masterid=null;
  }
  
 let bannerimage;
 let thumbimage;
  if (req.files?.bannerimage) {
    if (req.files?.bannerimage) {
      // console.log("req.files.photo",req.files.photo)
      bannerimage = "userprofile/" + req.files.bannerimage[0].filename;
    } 
  }
  if (req.files?.thumbimage) {
    if (req.files?.thumbimage) {
      // console.log("req.files.photo",req.files.photo)
      thumbimage = "userprofile/" + req.files.thumbimage[0].filename;
    } 
  }
 
  //  const lccatagory=catagory.toLowerCase();
  const catexists=await catagorymodel.findOne({title:catagory});
  console.log("catexists",catexists)
  if(catexists){
  return res.send({
    status:false,
    message:"La catégorie existe déjà",
    errmessage:"La catégorie existe déjà",
    data:null
  })
  }else{
  const newCatagory=new catagorymodel();
  newCatagory.title=catagory;
  newCatagory.bannerimage=bannerimage;
  newCatagory.mastercatagory=masterid;
  newCatagory.thumbimage=thumbimage;
  newCatagory.save().then(()=>{
    return res.send({
      status:true,
      message:"Catégorie ajoutée avec succès",
      errmessage:"Catégorie ajoutée avec succès",
      data:null
    })
  })
  }

}
module.exports.editcatagoryonline=async(req,res)=>{
  const id=req.params.id;
  
  let cats;
  let cat;
  cat=await catagorymodel.findById(id);
 
  cats=await catagorymodel.aggregate([
    // {$mathc:is_parent:true},
    {$match:{mastercatagory:null}},
    {
      $graphLookup:{
        "from": "catagories",
        "startWith": "$_id",
        "connectFromField": "_id",
        "connectToField": "mastercatagory",
        "as": "subs",
        "maxDepth": 20,
        "depthField": "level",
        
      },
     
    },
   
    { "$unwind": {
      "path": "$subs",
      "preserveNullAndEmptyArrays": true
  } },
  { "$match": { "subs.is_parent": true } },
    {
      $sort: {
        "subs.level": -1
      }
    },
    {
      $group: {
        _id: "$_id",
        
        parent_id: {
          $first: "$mastercatagory"
        },
        title: {
          $first: "$title"
        },
        subs: {
          $push: "$subs"
        }
      }
    },
    {
      $addFields: {
        subs: {
          $reduce: {
            input: "$subs",
            initialValue: {
              currentLevel: -1,
              currentLevelChildren: [],
              previousLevelChildren: []
            },
            in: {
              $let: {
                vars: {
                  prev: {
                    $cond: [
                      {
                        $eq: [
                          "$$value.currentLevel",
                          "$$this.level"
                        ]
                      },
                      "$$value.previousLevelChildren",
                      "$$value.currentLevelChildren"
                    ]
                  },
                  current: {
                    $cond: [
                      {
                        $eq: [
                          "$$value.currentLevel",
                          "$$this.level"
                        ]
                      },
                      "$$value.currentLevelChildren",
                      []
                    ]
                  }
                },
                in: {
                  currentLevel: "$$this.level",
                  previousLevelChildren: "$$prev",
                  currentLevelChildren: {
                    $concatArrays: [
                      "$$current",
                      [
                        {
                          $mergeObjects: [
                            "$$this",
                            {
                              subs: {
                                $filter: {
                                  input: "$$prev",
                                  as: "e",
                                  cond: {
                                    $and:[{$eq: [
                                      "$$e.mastercatagory",
                                      "$$this._id"
                                    ]},
                                    {
                                      $eq: [
                                        "$$e.is_parent",true
                                      ]
                                    }
                                  ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      ]
                    ]
                  }
                }
              }
            }
          }
        }
      }
    },
    {
      $addFields: {
        subs: "$subs.currentLevelChildren"
      }
    },
    {
      $match: {
        mastercatagory: null
      }
    }
  ]);
  
  let noparent= {
    _id:'',
    parent_id: '',
    title: 'Aucun parent',
    subs: []
  };

  cats?.unshift(noparent);
  return res.render("admin-panel/editcatagoryonline.ejs", {base_url,cats,cat, base_url, root_url });
}
module.exports.updatecatagoryonline=async(req,res)=>{
  const id=req.body.id;
  const catagory=req.body.catagory;
  const is_parent=req.body.is_parent;
  const mastercatagory=req.body.mastercatagory;
  let price;
//   if(is_parent=="true"){
//     price=null;
//  }else{
//     price=parseInt(req.body.price);
//  }
 let masterid;
 let successesor;
 let catagoryID
 if(req.body.mastercatagory){
  //  const parentcatagory=await catagorymodel.findById(id);
   const catagory=await catagorymodel.findOne({title:req.body.mastercatagory});
   catagoryID=mongoose.Types.ObjectId(id);
   
   successesor=await catagorymodel.findOne({mastercatagory:catagoryID});
   catagoryID=successesor?._id;
   masterid=catagory?._id;
 }else{
  masterid=null;
 }
 
 let bannerimage;
 let thumbimage;
  if (req.files?.bannerimage) {
    if (req.files?.bannerimage) {
      // console.log("req.files.photo",req.files.photo)
      bannerimage = "userprofile/" + req.files.bannerimage[0].filename;
    } 
  }
  if (req.files?.thumbimage) {
    if (req.files?.thumbimage) {
      // console.log("req.files.photo",req.files.photo)
      thumbimage = "userprofile/" + req.files.thumbimage[0].filename;
    } 
  }
   const lccatagory=catagory.toLowerCase();
   const catexists=await catagorymodel.findById(id);
   if(catexists){
   const data={
    ...(catagory&&{title:catagory}),
    ...(bannerimage&&{bannerimage:bannerimage}),
    ...(thumbimage&&{thumbimage:thumbimage}),
   
    ...(mastercatagory&&{mastercatagory:masterid}),
   }
   console.log("catagory====>",catagory,"<=============================================");
   console.log("catagory.mastercatagory====>",catagory.mastercatagory,"<=============================================");
   console.log("successesor====>",successesor,"<=============================================");
   console.log("id====>",id,"<=============================================");
   if(catagory.mastercatagory==null){
   
    await catagorymodel.findByIdAndUpdate(catagoryID,{mastercatagory:null});
    
   }else{

   }

   await catagorymodel.findByIdAndUpdate(catexists._id,data).then((result)=>{
   return res.send({
    status:true,
    message:"Catagorie mise à jour avec succès"
   })

   })
   }else{
    return res.send({
      status:false,
      message:"La catégorie n'existe pas",
      errmessage:"La catégorie n'existe pas",
      data:null
    })
   }
  
}
module.exports.deletecatagoryonline=async(req,res)=>{
  const id=mongoose.Types.ObjectId(req.params.id);
  console.log("id",id)

  const allcatagories=await catagorymodel.aggregate([
    {$match:{_id:id}},
    {
      $graphLookup:{
        "from": "catagories",
        "startWith": "$_id",
        "connectFromField": "_id",
        "connectToField": "mastercatagory",
        "as": "subs",
        "maxDepth": 20,
        "depthField": "level",
        
      },
     
    }
  ]);
  const deleteids=allcatagories[0].subs.map(e=>e._id);
  deleteids.push(allcatagories[0]._id);
  await catagorymodel.deleteMany({_id:{$in:deleteids}}).then(()=>{
    return res.send({
      status:true,
      message:"Catagorie supprimée avec succès"
    })
  })
  // return res.send({
  //   status:true,
  //   data:allcatagories
  // })
  // const id=req.params.id;
  // console.log("id",id)
  // await catagorymodel.findByIdAndDelete(id).then((result)=>{
  //   return res.send({
  //     status:true,
  //     message:"Catagorie supprimée avec succès"
  //   })
  // })
}
let childs={};
module.exports.getrecrcats=async(catid)=>{
 
  console.log(catid)
  if(catid.mastercatagory){
    child=await catagorymodel.findById(catid.mastercatagory);
    childs["child"]=child;
    getrecrcats(child);
    
  }
  return childs;
}
module.exports.managecatagoryonline=async(req,res)=>{
  const search=req.query.search;
   
   let id=mongoose.Types.ObjectId(req.query.id);
  const query={};
  if(search){
 query['$or']=[
  {catagory:search},
  
 ]
  }
 
  
  let cat;
  console.log("in else ","id",id)
//  if(req.query.id){
//    cat=await catagorymodel.aggregate( [
//     {$match: {"_id":mongoose.Types.ObjectId(req.query.id)}},
//     {$addFields:{sid:{$toString:"$_id"}}},
//     {$lookup:{from:"catagories",localField:"_id",foreignField:"mastercatagory",as:"subcatagories"}},
//  ] )
//  }else{
  
//    cat=await catagorymodel.aggregate( [
//     {$match: {"$or":[
//       {"mastercatagory":{$eq:""}},
//       {"mastercatagory":{ $exists: false}}
//     ]}},
//     {$addFields:{sid:{$toString:"$_id"}}},
//     {$lookup:{from:"catagories",localField:"_id",foreignField:"mastercatagory",as:"subcatagories"}},
//  ] )
//  }
  // const cat=await catagorymodel.find(query);
  // return res.send({
  //   status:true,
  //   data:cat
  // })

  cat=await catagorymodel.find({})
  return res.render("admin-panel/managecatagory.ejs", {cat, base_url, root_url });
}


module.exports.getallcatagories=async(req,res)=>{
  const search=req.query.search;
   
   let id=mongoose.Types.ObjectId(req.query.id);
  const query={};
  if(search){
 query['$or']=[
  {catagory:search},
  
 ]
  }
 
  
  let cat;
  console.log("in else ","id",id)
 if(req.query.id){
   cat=await catagorymodel.aggregate( [
    {$match: {"_id":mongoose.Types.ObjectId(req.query.id)}},
    {$addFields:{sid:{$toString:"$_id"}}},
    {$lookup:{from:"catagories",localField:"_id",foreignField:"mastercatagory",as:"subcatagories"}},
 ] )
 }else{
  
//    cat=await catagorymodel.aggregate( [
//     {$match: {"$or":[
//       {"mastercatagory":{$eq:""}},
//       {"mastercatagory":{ $exists: false}}
//     ]}},
//     {$addFields:{sid:{$toString:"$_id"}}},
//     {$lookup:{from:"catagories",localField:"_id",foreignField:"mastercatagory",as:"subcatagories"}},
//  ] )

cat=await catagorymodel.aggregate([
  // {$mathc:is_parent:true},
  {$match:{mastercatagory:""}},
  {
    $graphLookup:{
      "from": "catagories",
      "startWith": "$_id",
      "connectFromField": "_id",
      "connectToField": "mastercatagory",
      "as": "children",
      "maxDepth": 10,
      "depthField": "level",
      
    },
   
  },
  {
    $unwind: "$children"
  },
  {
    $sort: {
      "children.level": -1
    }
  },
  {
    $group: {
      _id: "$_id",
      parent_id: {
        $first: "$mastercatagory"
      },
      content: {
        $first: "$catagory"
      },
      children: {
        $push: "$children"
      }
    }
  },
  {
    $addFields: {
      children: {
        $reduce: {
          input: "$children",
          initialValue: {
            currentLevel: -1,
            currentLevelChildren: [],
            previousLevelChildren: []
          },
          in: {
            $let: {
              vars: {
                prev: {
                  $cond: [
                    {
                      $eq: [
                        "$$value.currentLevel",
                        "$$this.level"
                      ]
                    },
                    "$$value.previousLevelChildren",
                    "$$value.currentLevelChildren"
                  ]
                },
                current: {
                  $cond: [
                    {
                      $eq: [
                        "$$value.currentLevel",
                        "$$this.level"
                      ]
                    },
                    "$$value.currentLevelChildren",
                    []
                  ]
                }
              },
              in: {
                currentLevel: "$$this.level",
                previousLevelChildren: "$$prev",
                currentLevelChildren: {
                  $concatArrays: [
                    "$$current",
                    [
                      {
                        $mergeObjects: [
                          "$$this",
                          {
                            children: {
                              $filter: {
                                input: "$$prev",
                                as: "e",
                                cond: {
                                  $eq: [
                                    "$$e.mastercatagory",
                                    "$$this._id"
                                  ]
                                }
                              }
                            }
                          }
                        ]
                      }
                    ]
                  ]
                }
              }
            }
          }
        }
      }
    }
  },
  {
    $addFields: {
      children: "$children.currentLevelChildren"
    }
  },
  {
    $match: {
      mastercatagory: null
    }
  }
])
 }
  // const cat=await catagorymodel.find(query);
  return res.send({
    status:true,
    data:cat
  })
  // return res.render("admin-panel/managecatagory.ejs", {cat, base_url, root_url });
}


module.exports.addcatagoryoffline=(req,res)=>{}
module.exports.savecatagoryoffline=(req,res)=>{}
module.exports.editcatagoryoffline=(req,res)=>{}
module.exports.updatecatagoryoffline=(req,res)=>{}
module.exports.deletecatagoryoffline=(req,res)=>{}
module.exports.managecatagoryoffline=(req,res)=>{}
module.exports.pendingonlinecourses=async(req,res)=>{
  const teacher_name=req.query.teacher_name;
  const title=req.query.title;
  const catagory=req.query.catagory;
  const query={
    is_approved:false,
    type:"online"
  };
  if(teacher_name){
  query["teacher_name"]=new RegExp(teacher_name,"i");
  
  }
  if(title){
    query["title"]=new RegExp(title,"i");
  }
  if(catagory){
    query["catagory"]=new RegExp(catagory,"i");
  }
  console.log("query",query)
  const courses=await coursesmodel.find(query);
  res.render("admin-panel/pendingonlinecourses.ejs", {moment,courses, base_url, root_url });
}
module.exports.allcourses=async(req,res)=>{
  const teacher_name=req.query.teacher_name;
  const title=req.query.title;
  const catagory=req.query.catagory;
  const query={is_approved:true,type:"online"};
  if(teacher_name){
  query["teacher_name"]=new RegExp(teacher_name,"i");
  
  }
  if(title){
    query["title"]=new RegExp(title,"i");
  }
  if(catagory){
    query["catagory"]=new RegExp(catagory,"i");
  }
  console.log("query",query)
  const courses=await coursesmodel.find(query);
  res.render("admin-panel/allcourses.ejs", {moment,courses, base_url, root_url });
}
module.exports.upcomingreservations=(req,res)=>{}
module.exports.finishedreservations=(req,res)=>{}
module.exports.pendingofflinecourses=async(req,res)=>{
  const teacher_name=req.query.teacher_name;
  const title=req.query.title;
  const catagory=req.query.catagory;
  const query={
    is_approved:false,
    type:"offline"
  };
  if(teacher_name){
  query["teacher_name"]=new RegExp(teacher_name,"i");
  
  }
  if(title){
    query["title"]=new RegExp(title,"i");
  }
  if(catagory){
    query["catagory"]=new RegExp(catagory,"i");
  }
  console.log("query",query)
  const courses=await coursesmodel.find(query);
  res.render("admin-panel/pendingonlinecourses.ejs", {moment,courses, base_url, root_url });
}
module.exports.allofflinecourses=async(req,res)=>{
  const teacher_name=req.query.teacher_name;
  const title=req.query.title;
  const catagory=req.query.catagory;
  const query={type:"offline" ,  is_approved:true};
  if(teacher_name){
  query["teacher_name"]=new RegExp(teacher_name,"i");
  
  }
  if(title){
    query["title"]=new RegExp(title,"i");
  }
  if(catagory){
    query["catagory"]=new RegExp(catagory,"i");
  }
  console.log("query",query)
  const courses=await coursesmodel.find(query);
  res.render("admin-panel/allcourses.ejs", {moment,courses, base_url, root_url });
}
module.exports.purchasehistory=async(req,res)=>{
  const student_name=req.query.student_name;
  const teacher_name=req.query.teacher_name;
  const start_date=req.query.start_date;
  const end_date=req.query.end_date;
  const status=req.query.status;
  const mode_of_service=req.query.mode_of_service;
  const mode_of_payment=req.query.mode_of_payment;

  const query={
    type:"offline",
    date_availed: { $lte: new Date().toISOString() }
  };
  if(student_name){
  query["client_name"]=new RegExp(student_name,"i");
  }
  if(teacher_name){
    query["provider_name"]=new RegExp(teacher_name,"i");
  }
  if(start_date){
    query["date_availed"]={$gte:new Date(req.query.start_date).toISOString()}
  }
  if(end_date){
    query["date_availed"]={$lte:new Date(req.query.end_date).toISOString()}
  }
  if(status){
    query["service_status"]=status
  }
  if(mode_of_service){
    query["mode_of_service"]=mode_of_service
  }
  if(mode_of_payment){
    query["mode_of_payment"]=mode_of_payment
  }
  const servicehistory = await Servicesprovided.find(query);
  // console.log(servicehistory);
  res.render("admin-panel/servicehistory.ejs", { servicehistory, base_url,moment });
}
module.exports.activatecourseonline=async(req,res)=>{
  const id = req.params.id;
 
  await coursesmodel
    .findByIdAndUpdate(id, { is_active: true })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat désactivé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
}
module.exports.deactivatecourseonline=async(req,res)=>{
  const id = req.params.id;
  // console.log(req.params,"in deactivateawyercabinat");
  await coursesmodel
    .findByIdAndUpdate(id, { is_active: false })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat désactivé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
}

module.exports.deletecourseonline=async(req,res)=>{
  const id = req.params.id;
  // console.log(req.params,"in deactivateawyercabinat");
  await coursesmodel
    .findByIdAndDelete(id, { is_active: false })
    .then(() => {
      res.send({
        status: true,
        message: "Avocat désactivé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
}
module.exports.updatecourseonline=async(req,res)=>{
  const {
    id,
    teacher_name,
    
    title,
    subject,
    desc,
    catagory,
    status,
    price,
    type
    
    
  } = req.body;
  let image;
  let doc;
  if (req.files?.image) {
    if (req.files?.image) {
      // console.log("req.files.photo",req.files.photo)
      image = "userprofile/" + req.files.image[0].filename;
    } 
  }
  if (req.files?.doc) {
    if (req.files?.doc) {
      // console.log("req.files.photo",req.files.photo)
      doc = "userprofile/" + req.files.doc[0].filename;
    } 
  }
  let newstatus;
  if(status){
    if(status=="0"||status==0){
      newstatus=false
    }else if(status=="1"||status==1){
      newstatus=true
    }
  }
  let data= {
            
            ...(title&&  {title:title} ),
            ...(subject&&  {subject:subject} ),
            ...(desc&&  {desc:desc} ),
            ...(price&&  {price:price} ),
            ...(type&&  {type:type} ),
            ...(image&&  {image:image} ),
            ...(doc&&  {doc:doc} ),
            ...(status!=""&&  {is_active:newstatus} ),
            ...(catagory&&  {catagory:catagory} ),
            ...(teacher_name&&  {teacher_name:teacher_name} ),

          
            
        }
      console.log("data",data,"req.body",req.body);
      await  coursesmodel.findByIdAndUpdate(
          id,
        data)
        .then((lawyer) => {
            res.send({status:true,"message":"updated successfully"});
        })
}
module.exports.editcourseonline=async(req,res)=>{
  const id=req.params.id;
  const catagories= await catagorymodel.find({});
  const lawyer=await coursesmodel.findOne({_id:id});
  res.render("admin-panel/editcourseonline.ejs", {catagories,lawyer, base_url, root_url });
}

module.exports.approvecourseonline=async(req,res)=>{
  const id = req.params.id;
  
  // console.log(req.params,"in deactivateawyercabinat");
  const course=await coursesmodel.findById(id);
  if(course.is_approved){
    course.is_approved=false
  }else{
    course.is_approved=true
  }
  course.save()
    .then(() => {
      res.send({
        status: true,
        message: "Cours approuvé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
}

module.exports.comments=async (req,res)=>{
   const client_name=req.query.client_name
  const provider_name=req.query.provider_name
 
  const start_date=req.query.start_date
  const end_date=req.query.end_date
  const query={};
  if(client_name){
  query["client_name"]=client_name
  }
  if(provider_name){
    query["provider_name"]=provider_name
  }
  if(start_date){
    query["date"]={$gte:new Date(start_date).toISOString()}
  }
  if(end_date){
    query["date"]={$lte:new Date(end_date).toISOString()}
  }
  
  const comments = await commentsmodel.find(query);
  console.log("comments",comments)
  res.render("admin-panel/comments.ejs", { comments, base_url,moment });
} 

module.exports.updatedistributer=async (req,res)=>{
  const {
    id,
    name,
    email
  }=req.body
  const dismodel=await DistributerModel.findById(id);
  dismodel.name=name
  dismodel.email=email
  dismodel.save().then((result)=>{
    return res.send({
      status:true,
      message:"distributer updated successfully"
    })
  })
 
}
module.exports.editdistributer=async (req,res)=>{
  const id=req.params.id;
  const dismodel=await DistributerModel.findById(id);
  res.render("admin-panel/editdistributer.ejs", { dismodel,base_url, root_url });
}
module.exports.savedistributer=async (req,res)=>{
  const {
   
    name,
    email
  }=req.body
  const dismodel=await new DistributerModel();
  dismodel.name=name;
  dismodel.email=email;
  dismodel.save().then((result)=>{
    return res.send({
      status:true,
      message:"distributer created successfully"
    })
  })
  
}
module.exports.distributers=async (req,res)=>{
  const name=req.query.name;
  const email=req.query.email;
  const query={
    
  }
  if(name){
  query["name"]=name
  }
  if(email){
    query["email"]=email
  }
  const dismodel=await DistributerModel.find(query);
  res.render("admin-panel/distributers.ejs", {dismodel, base_url, root_url });
}
module.exports.createdistributer=async (req,res)=>{
   
  return res.render("admin-panel/adddistributer.ejs", { base_url, root_url });
}
module.exports.updatecoupen=async (req,res)=>{
  const {
    id,
    vdist,
  vname,
  vcode,
  start_date,
  end_date,
  vlimit,
  vtype,
  monper
  }=req.body;
  const coumodel=await CoupensModel.findById(id);
  coumodel.name=vname;
  coumodel.code=vcode;
  coumodel.discountamount=monper
  coumodel.limite=vlimit
  coumodel.distributer=vdist
  coumodel.start_date=start_date
  coumodel.end_date=end_date
  coumodel.type=vtype
  
  coumodel.save().then((result)=>{
    return res.send({
      status:true,
      message:"coupon updated successfully"
    })
  })
}
module.exports.editcoupen=async (req,res)=>{
  const id=req.params.id;
  const diss=await DistributerModel.find({});
  const coupen=await CoupensModel.findById(id);
  res.render("admin-panel/editcoupen.ejs", {coupen,diss, base_url, root_url });
}
module.exports.savecoupens=async (req,res)=>{
const {
  vdist,
vname,
vcode,
start_date,
end_date,
vlimit,
vtype,
monper
}=req.body;
console.log("req.boyd",req.body)
const isexists=await CoupensModel.findOne({code:vcode});
if(isexists){
  return res.send({
    status:false,
    message:"",
    errmessage:"Coupen existe déjà"
  })
}else{
const coumodel=new CoupensModel();
coumodel.name=vname;
coumodel.code=vcode;
coumodel.discountamount=monper
coumodel.limite=vlimit
coumodel.distributer=vdist
coumodel.start_date=start_date
coumodel.end_date=end_date
coumodel.type=vtype

coumodel.save().then((result)=>{
  return res.send({
    status:true,
    message:"coupon saved successfully"
  })
})
}
}
module.exports.coupens=async (req,res)=>{
  
  const coumodel=await CoupensModel.find({});
  res.render("admin-panel/coupens.ejs", {coumodel, base_url, root_url });
}
module.exports.createcoupne=async (req,res)=>{
const diss=await DistributerModel.find({});
  res.render("admin-panel/addcoupen.ejs", { diss,base_url, root_url });
}

module.exports.deletedistributer=async (req,res)=>{
  const id=req.params.id;
  
  console.log("id",id)
  await DistributerModel.findByIdAndDelete(id).then((result)=>{
    return res.send({
      status:true,
      message:"distributer deleted successfuly"
    })
  })
}
module.exports.deletecoupen=async (req,res)=>{
  const id=req.params.id;
  await CoupensModel.findByIdAndDelete(id).then((result)=>{
    return res.send({
      status:true,
      message:"distributer deleted successfuly"
    })
  })
}

module.exports.deactivatecoupen=async(req,res)=>{
  const id=req.params.id;
  await CoupensModel.findByIdAndUpdate(id,{status:false}).then((result)=>{
    return res.send({
      status:true,
      message:"distributer status updated successfuly"
    })
  })
}

module.exports.activatecoupen=async(req,res)=>{
  const id=req.params.id;
  await CoupensModel.findByIdAndUpdate(id,{status:true}).then((result)=>{
    return res.send({
      status:true,
      message:"distributer status updated successfuly"
    })
  })
}
module.exports.demandpayment=async(req,res)=>{
  try{
    const provider_id=req.body.provider_id;
    const provider_name=req.body.provider_name;
    const email=req.body.email;
    const bank_id=req.body.bank_id;
  const amount=req.body.amount;
  const lawyer=await lawyermodel.findById(provider_id);
  const pendingamount=lawyer.totalpendingamount;
const pastPayRequests=await PaymentRequestmodel.find({provider_id:provider_id,payment_status:false,is_rejected:false});
if(pastPayRequests.length){
  return res.send({
    status:false,
    message:"Vous ne pouvez pas demander le retrait tant que votre demande finale n'est pas acceptée par l'administrateur."
  })
}
const totalrequestamount=pastPayRequests.reduce(function(accum,current){
  return accum+parseInt(current.request_amount)
},0);
// console.log("totalrequestamount",totalrequestamount,"pendingamount",pendingamount)
if(amount<=pendingamount){
const newPayoutrequest=new PaymentRequestmodel();
newPayoutrequest.provider_id=provider_id;
newPayoutrequest.provider_name=provider_name;
newPayoutrequest.email=email;
newPayoutrequest.request_amount=amount;
newPayoutrequest.bank_id=bank_id;
lawyer.totalpendingamount=lawyer.totalpendingamount-amount;
lawyer.save();
newPayoutrequest.save((result)=>{
  return res.send({
    status:true,
    message:"Demande de paiement effectuée avec succès",
    data:result
  })
})
}else if(amount>pendingamount){
 return res.send({
  status:false,
  message:"",
  errmessage:"Le montant demandé est supérieur au montant en attente"
 })
}
  }catch(e){
    return res.send({
      status:false,
      message:"",
      errmessage:e.message
     })
  }
 
}

module.exports.getalldemandpayment=async(req,res)=>{
  const paymentrequest=await PaymentRequestmodel.find({});
  return res.send({
    status:true,
    message:"fetched successfylly",
    data:paymentrequest
  })
}

module.exports.getdemandpaymentbyteacherid=async(req,res)=>{
  const teacher_id=req.params.id;
  const paymentrequest=await PaymentRequestmodel.find({provider_id:teacher_id});
  return res.send({
    status:true,
    message:"fetched successfylly",
    data:paymentrequest
  })
}



module.exports.createreason=(req,res)=>{
  const reason=req.body.reason;
  const reasonmodel=new reasonModel();
  reasonmodel.reason=reason;
  reasonmodel.save((result)=>{
      return res.send({
          status:true,
          message:"resons fetched successfully",
          errmessage:"",
          data:result
      })
  })
  
}

module.exports.editreason=async(req,res)=>{
  const reason=req.body.reason;
  const id=req.body.id;
  const reasonmodel=await reasonModel.findById(id);
  reasonmodel.reason=reason;
  reasonmodel.save((result)=>{
      return res.send({
          status:true,
          message:"Motif mis à jour avec succès",
          errmessage:"",
          data:result
      })
  })
}

module.exports.deletereason=async(req,res)=>{
 
  const id=req.body.id;
  console.log("id",req.body.id, id)
 await reasonModel.findByIdAndDelete(id).then((result)=>{
  return res.send({
      status:true,
      message:"Raison pour laquelle il a été supprimé avec succès",
      errmessage:"",
      data:result
  })
  })

 
}

module.exports.viewreason=async(req,res)=>{
  const id=req.body.id;
  await reasonModel.findById(id).then((result)=>{
   return res.send({
       status:true,
       message:"resons fetched successfully",
       errmessage:"",
       data:result
   })
   })
}

module.exports.getallreason=async(req,res)=>{

  await reasonModel.find({}).then((result)=>{
      return res.send({
          status:true,
          message:"resons fetched successfully",
          errmessage:"",
          data:result
      })
      })
}




module.exports.reasons = async (req, res) => {
  const expertise = await reasonModel.find({});
  return res.render("admin-panel/reasons.ejs", { base_url, root_url, expertise });
};
module.exports.addreason = async (req, res) => {
   return res.render("admin-panel/addreason.ejs", { base_url, root_url });
};

module.exports.updatereason = async (req, res) => {
  const id=req.params.id;
  const reason=await reasonModel.findOne({_id:id});
  console.log(reason)
  return res.render("admin-panel/editreason.ejs", {reason, base_url, root_url });
};

module.exports.paymentrequests=async(req,res)=>{
  const requests=await PaymentRequestmodel.find({is_rejected:false,payment_status:false});
  return res.render("admin-panel/paymentrequests.ejs", { moment,requests,base_url, root_url });
}



module.exports.acceptpaymentrequest=async(req,res)=>{
  const id=req.params.id;
  const payment=await PaymentRequestmodel.findByIdAndUpdate(id,{
    payment_status:true
  });
const lawyer=await lawyermodel.findOne({_id:payment.provider_id});
  lawyer.totalpaidamount=parseInt(lawyer.totalpaidamount)+parseInt(payment.request_amount);
  lawyer.save().then((result)=>{
    return res.send({
      status:true,
      message:"La demande de paiement a été rejetée avec succès"
    })
  })

}
module.exports.rejectpaymentrequest=async(req,res)=>{
  const id=req.params.id;
  const payment=await PaymentRequestmodel.findByIdAndUpdate(id,{
    is_rejected:true
  });
  const lawyer=await lawyermodel.findOne({_id:payment.provider_id});
  
  lawyer.totalpendingamount=parseInt(lawyer.totalpendingamount)+parseInt(payment.request_amount);
  lawyer.save().then((result)=>{
    return res.send({
      status:true,
      message:"La demande de paiement a été rejetée avec succès"
    })
  })
 
}


module.exports.render_forgot_password_template = function(req, res) {
  console.log(path.resolve('./public/forgot-password.html'))
  return res.sendFile(path.resolve('./public/forgot-password.html'));
};
module.exports.render_reset_password_template = function(req, res) {
  console.log(path.resolve('./public/reset-password.ejs'))
  return res.render(path.resolve('./public/reset-password-email_admin.ejs'),{
    base_url:base_url
  });
};
module.exports.forgot_password = async (req, res) => {
  console.log("req.body",req.body)
  try {
    async.waterfall(
      [
        function (done) {
          adminmodel.findOne({
            email: req.body.email,
          }).exec(function (err, user) {
            if (user) {
              done(err, user);
            } else {
              done({
                status: false,
                message: "utilisateur non trouvé",
                errmessage: "",
                data: null,
              });
            }
          });
        },
        function (user, done,err) {
          // create a unique token
          var tokenObject = {
            email: user.email,
            id: user._id,
          };
          var secret = user._id + "_" + user.email + "_" + new Date().getTime();
          var token = jwt.sign(tokenObject, secret);
          done(err, user, token);
        },
        function (user, token, done) {
          adminmodel.findByIdAndUpdate(
            { _id: user._id },
            {
              reset_password_token: token,
              reset_password_expires: Date.now() + 86400000,
            },
            { new: true }
          ).exec(function (err, new_user) {
            done(err, token, new_user);
          });
        },
        function (token, user, done) {
          var data = {
            to: user.email,
            from: process.env.MAILER_EMAIL_ID,
            template: "forgot-password-email",
            subject: "L'aide pour les mots de passe est arrivée !",
            // html: "forgot-password-email",
            context: {
              url: base_url+"reset_password?token=" + token,
              name: user.first_name,
            },
          };
// console.log(data)
    sendemailf(req,res,data,done);
        },
      ],
      function (err) {
        return res.status(422).json({ 
          status: false,
          message: "",
          errmessage: err.message,
          data: null,
         });
      }
    );
  } catch (err) {
    console.log(err);
  }
};
module.exports.reset_password = async (req, res) => {

//  console.log(req.body)
adminmodel.findOne({
    reset_password_token: req.body.token,
    reset_password_expires: {
      $gt: Date.now(),
    },
}).exec(async function(err,user) {
  // console.log(user,err)
   if(!user){
        return res.send({
          status:false,
          message:"page expired",
          errmessage:"La page a expiré, demandez à nouveau la réinitialisation du mot de passe"
        })
      }
if(user&&!err){

  if(req.body.newPassword==req.body.verifyPassword){
    // console.log("password matched")
    let encryptedPassword = await bcrypt.hash(req.body.newPassword, 10);
    user.password=encryptedPassword;
    user.reset_password_expires=undefined;
    user.reset_password_token=undefined;
    user.save(function(err,user){
      if(!err){
        var data = {
          to: user.email,
          from: process.env.MAILER_EMAIL_ID,
          template: 'reset-password-email',
          subject: 'Confirmation de la réinitialisation du mot de passe',
          context: {
            name: user.first_name
          }
        };
        var smtpTransport = nodemailer.createTransport({
          host: process.env.MAILER_HOST,
          port:  process.env.MAILER_PORT,
          secure:false,
          auth: {
            user: process.env.MAILER_EMAIL_ID,
            pass: process.env.MAILER_PASSWORD,
          },
          tls: {
            rejectUnauthorized: false
          }
        });
      
       
        const handlebarOptions = {
          viewEngine: {
              partialsDir: path.resolve("./views/auth/"),
              defaultLayout: false,
          },
          viewPath: path.resolve("./views/auth/"),
      };
      // console.log("data in email",data)
        smtpTransport.use("compile", hbs(handlebarOptions));
        smtpTransport.sendMail(data, function (err) {
          if (!err) {
            return res.status(200).json({
              status: true,
              message: "Confirmer le succès du mot de passe",
              errmessage: "",
              data: null,
            });
          } else {
            // console.log(err)
            return res.send(
              {
                status: false,
                message: "",
                errmessage: err.message,
                data: null,
              }
            );
          }
        });
      }else{
        return res.status(422).send({
          status: false,
          message: "",
          errmessage: err.message,
          data: null,
        });
      }
    })
    
  }
}
});
};
const sendemailf = (req, res, data,done) => {
  
  // var smtpTransport = nodemailer.createTransport({
  //   // service: process.env.MAILER_SERVICE_PROVIDER,
  //   host: process.env.MAILER_HOST,
  //   port: process.env.MAILER_PORT,
  //   auth: {
  //     user: process.env.MAILER_EMAIL_ID,
  //     pass: process.env.MAILER_PASSWORD,
  //   },
  //   tls: {
  //     rejectUnauthorized: false
  //   }
  // });
  // console.log(data)
  var smtpTransport = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    secure: false,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    },
    tls: {
      rejectUnauthorized: false
    }
  });

 smtpTransport.verify(function(error, success) {

  if (error) {
    console.log(error);
  } else {
    console.log('Server is ready to take our messages');
  }

 })
  const handlebarOptions = {
    viewEngine: {
        partialsDir: path.resolve("./views/auth/"),
        defaultLayout: false,
    },
    viewPath: path.resolve("./views/auth/"),
};
// console.log("data in email",data)
  smtpTransport.use("compile", hbs(handlebarOptions));
  smtpTransport.sendMail(data, function (err) {
    if (!err) {
      return res.json({
        status: true,
        message: "Veuillez vérifier votre e-mail pour de plus amples instructions",
        errmessage: "",
        data: null,
      });
    } else {
      console.log("error in email",err)
      return done(err);
    }
  });
};

module.exports.joblist=async(req,res)=>{
  const student_name=req.query.client_name;
  const teacher_name=req.query.provider_name;
  const start_date=req.query.start_date;
  const end_date=req.query.end_date;
  const status=req.query.status;
  const mode_of_service=req.query.mode_of_service;
  const mode_of_payment=req.query.mode_of_payment;
  const date = getPreviousDay();
  const query={
    service_status:false,
    payment_status:false,
   
    
  };
  if(student_name){
  query["client_name"]=new RegExp(student_name,"i");
  }
  if(teacher_name){
    query["provider_name"]=new RegExp(teacher_name,"i");
  }
  if(start_date && !end_date){
    query["date_availed"]={$gte:new Date(req.query.start_date)}
  }
  if(end_date && !start_date){
    query["date_availed"]={$lte:new Date(req.query.end_date)}
  }
  if(start_date&&end_date){
    query["$and"]=[
      {date_availed:{$gte:new Date(req.query.start_date)}},
      {date_availed:{$lte:new Date(req.query.end_date)}}
    ]
  }
  if(status){
    query["service_status"]=status
  }
  // if(mode_of_service){
  //   query["mode_of_service"]=mode_of_service
  // }
 
  
  console.log(date);
  const scheduledservices = await Servicesprovided.aggregate([
    {
      $match:query
    },
    // {
    //   $addFields:{
    //     provideridOBJ:{
    //       "$toObjectId":"provider_id"
    //     }
    //   }
    // },
    // {
    //   $lookup:{
    //     from:"lawunits",
    //     localField:"provideridOBJ",
    //     foreignField:"_id",
    //     as:"provider"
    //   },
    
    // },
    // {
    //   $unwind:"$provider"
    // }
  ])
  res.render("admin-panel/joblist.ejs", {
    scheduledservices,
    base_url,
    moment
  });
}

module.exports.portfoliohistory=async(req,res)=>{
  console.log(req.query)
 const lawyer_name_arr = req.query.provider_name;
 const email=req.query.email;
 const query={
  payment_status:false,is_rejected:false
 };
 if(lawyer_name_arr){
  query["provider_name"]=new RegExp(lawyer_name_arr,"i");
 }
 if(email){
  query["email"]=email
 }
 
//  const paymentrequest=await PaymentRequestmodel.find({});
 const portfoliohistory=await PaymentRequestmodel.aggregate([
  {$match:query},
  {
    $addFields:{
      bankidobj:{
        "$convert": {
        "input": "$bank_id",
        "to": "objectId",
        "onError": "",
        "onNull": ""
      }}
    }
  },
  {
    $lookup:{
      from:"lawyerbanddetails",
      localField:"bankidobj",
      foreignField:"_id",
      as:"banks"
    }
  },
  {$addFields:{
    bank:{$arrayElemAt:["$banks",0]}
  }}
 ]);
 res.render("admin-panel/portfoliohistory.ejs",{base_url,root_url,portfoliohistory,moment});
}
module.exports.termsandcondition=(req,res)=>{
  return res.render("misc/termsandcondition.ejs")
}
module.exports.privacypolicy=(req,res)=>{
  return res.render("misc/privacypolicy.ejs")
}
module.exports.showabusereports=async(req,res)=>{
  const provider_name=req.query.provider_name;
  const user_name=req.query.user_name;
  const reason=req.query.reason;
  const query={}
  if(provider_name){
    query['provider_name']=provider_name
  }
   if(user_name){
    query['user_name']=user_name
  }
  if(reason){
    query['reason']=reason
  }
  // const reports=await abuseReportModel.findOne();
  const reports=await abuseReportModel.aggregate([
    {$match:query},
    {$addFields:{
        
        userIDOBJ:{"$toObjectId":"$reportedbyId"},
        providerIDOBJ:{"$toObjectId":"$provider_id"},
    }},
    
    {
        $lookup:{
            from:"users",
            localField:"userIDOBJ",
            foreignField:"_id",
            as:"reportedBy"
        }
    },
    {
        $lookup:{
            from:"lawunits",
            localField:"providerIDOBJ",
            foreignField:"_id",
            as:"provider"
        }
    }
]);
console.log("reports0",reports)
  res.render("admin-panel/abuseReports.ejs", {moment,reports,base_url });
}


module.exports.manageboosterpackages = async (req, res) => {
  const subscriptionpackages = await Boosterpackages.find({});
  //   console.log(subscriptionpackages);
  const editurl = "editsubscriptionpackage/";
  res.render("admin-panel/manageboosterpackages.ejs", {
    subscriptionpackages,
    editurl,
    base_url,
  });
};
module.exports.createnewboosterpackage = (req, res) => {
  res.render("admin-panel/createnewboosterpackage.ejs", { base_url });
};
module.exports.savebooster = (req, res) => {
  const data = req.body;
  const newsubs = Boosterpackages();
  newsubs.package = data.package;
  newsubs.price = data.price;
  newsubs.duration = data.duration;
  newsubs.description = data.description;
  newsubs.type=data.type;
  
  
  // console.log("data.is_multiple_photo",data.is_multiple_photo,"is_multiple_photo_new",is_multiple_photo_new,"newsubs.is_multiple_photos_allowed",newsubs.is_multiple_photos_allowed)
  newsubs
    .save()
    .then((result) => {
      res.send({
        status: true,
        message: "Paquet d'abonnement créé avec succès",
        data: result,
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({ status: false, message: err });
    });
};
module.exports.Editboosterpackage = async (req, res) => {
  const package_id = req.params.package_id;
  const subpackage = await Boosterpackages.findById(package_id);
  res.render("admin-panel/editboosterpackage.ejs", {
    subpackage,
    base_url,
  });
};

module.exports.updateboosterpackage = async (req, res) => {
  const data = req.body;
  const package_id = data.elementid;
  const package = data.package;
  const price = data.price;
  const duration = data.duration;
  const description = data.description;
  const package_type=data.package_type;
  const is_multiple_photo=data.is_multiple_photo;
  let is_multiple_photo_new=false;
  if(data.is_multiple_photo!=""){
    is_multiple_photo_new=true
  }
  let is_boosted=false;
  if(data.is_booster!=""){
    is_boosted=true
  }
  // newsubs.is_multiple_photos_allowed=is_multiple_photo_new;
  // newsubs.is_boosted=is_boosted;
  console.log("req.body",data)
  const newdata = {
    ...(package && { package }),
    ...(price && { price }),
    ...(duration && { duration }),
    ...(description && { description }),
    ...(package_type && { type:package_type })
   
  };
  await Boosterpackages
    .findByIdAndUpdate(package_id, newdata)
    .then(() => {
      res.send({
        status: true,
        message: "Le paquet d'abonnement a été mis à jour avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.deleteboosterpackage = async (req, res) => {
  const package_id = req.params.package_id;
  await Boosterpackages
    .findByIdAndDelete(package_id)
    .then(() => {
      res.send({
        status: true,
        message: "Paquet d'abonnement supprimé avec succès",
      });
    })
    .catch((err) => {
      console.log(err);
      res.send({
        status: false,
        message: err,
      });
    });
};
module.exports.viewboosterpackage = async (req, res) => {
  const package_id = req.params.package_id;
  const subpackage = await Boosterpackages.findById(package_id);
  res.render("admin-panel/viewboosterpackage.ejs", { subpackage, base_url });
};

module.exports.getboosterpackages=async(req,res)=>{
  const subpackage = await Boosterpackages.find({});
  // const imagepackage=subpackage.filter(e=>e.type=="image")
  // const booserpackage=subpackage.filter(e=>e.type=="booster")
  return res.send({
    status:true,
    data:subpackage
    
  })
}

module.exports.deleteadmin=async(req,res)=>{
  const subpackage = await adminmodel.deleteMany();
  return res.send({
    status:true,
    data:subpackage
  })
}

module.exports.Cancledrequests=async(req,res)=>{
  const requests=await cancledrequestmodel.aggregate([
    
    {$addFields:{service_id_obj:{"$toObjectId":"$service_id"}}},
    {$lookup:{
      from:"servicesprovideds",
      localField:"service_id_obj",
      foreignField:"_id",
      as:"service"
    }},
    {$lookup:{
      from:"lawyerbankdetails",
      localField:"service.teacher_id",
      foreignField:"teacher_id",
      as:"bankdetails"
    }},
    {$sort:{payment_status:1}}
  ]);
  
  return res.render("admin-panel/cancelledrequest.ejs", { moment,requests,base_url, root_url });
}


module.exports.paycancledrequests=async(req,res)=>{
  const id=req.params.id
  const paymentrequest=await cancledrequestmodel.findById(id);
  console.log("request",paymentrequest)
  paymentrequest.payment_status=true;
  paymentrequest.save((result)=>{
    return res.send({
      status:true,
      message:"Demande de paiement acceptée avec succès"
    })
  })
  

  

}
module.exports.getcommissions=async(req,res)=>{
  const admin=await adminmodel.findOne({});
  return res.send({
    status:true,
    message:"success",
    data:admin
  })
}

module.exports.getboostersubs=async(req,res)=>{
  const provider_id=req.params.provider_id;
  const subs=await Boosterpackagessubs.find({
    provider_id:provider_id
  });
  const imagesub=subs.filter(e=>e.booster_type=="image")||[]
  const boostersub=subs.filter(e=>e.booster_type=="booster")||[]
  return res.send({
    status:true,
    message:"success",
    imagesub:imagesub,
    boostersub:boostersub
  })
}

module.exports.downloadsubinvoice=async(req,res)=>{
  console.log(req.params)
  try{
    const id=req.params.id;
    // const transaction_id=req.params.transaction_id;
    let subs=await usersubscriptionmodel.findOne({_id:id});
    let booster=await Boosterpackagessubs.findOne({_id:id});
    let invoice;
    if(subs){
      invoice=subs
    }else if(booster){
      invoice=booster
    }
    if(invoice){
      const frenchdate=frenchDayDate(invoice.date_of_transaction);
      invoice=JSON.parse(JSON.stringify(invoice));
      invoice["frenchdate"]=frenchdate;
      console.log(invoice)
      const filepath=path.join(__dirname, "../../views/admin-panel/subinvoice.ejs");
      console.log(filepath)
      const html = await ejs.renderFile(filepath,{invoice:invoice});
      console.log(html)
      const options = { format: 'Letter' };
      
    pdf.create(html, options).toFile(path.join(__dirname, "../../views/admin-panel/public/invoices/")+'invoice'+id+'.pdf', function(err, result) {
      if (err) return console.log("in pdf create",err);
      console.log(root_url+"/static/public/invoices/invoice"+id+".pdf")
      // console.log(path.join(__dirname, "../../views/admin-panel/public/invoices/invoice"+id+".pdf"));
      res.send({
        status:true,
        url:root_url+"/static/public/invoices/invoice"+id+".pdf",
        message:"Facture téléchargée avec succès"
      })
    });
    }else{
      res.send({
        status:false,
        message:"Facture non trouvée"
      })
    }
    
   }catch(err){
     console.log("in catch block",err)
     res.send(err.message);
   }
}