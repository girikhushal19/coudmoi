const app = require('express');
const authroutes = require('./auth/index');
const clientroutes=require('./client/index');
const lawyerroutes=require('./lawyer/index');
const adminroutes=require('./admin/index');
const paymentController=require('./common/index');
const authMiddleware = require('./../middleware/auth');
const roomcontroller=require("./chat/chatRoom")
const api = app.Router();

api.use("/auth",authroutes);
api.use('/client',clientroutes);
api.use('/provider',lawyerroutes);
api.use('/admin',adminroutes);
api.use("/payment",paymentController);
api.use("/room",roomcontroller);

module.exports=api;