const mongoose=require("mongoose");
const AdminSchema=mongoose.Schema({
   
    email:{type:String,required:true},
    first_name:{type:String,required:true},
    last_name:{type:Number,default:0},
    age:{type:Number},
});
module.exports=mongoose.model("user",AdminSchema);