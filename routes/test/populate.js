const faker = require("faker");
const User = require("../../models/auth/user");
const lawyer = require("../../models/lawyer/Lawyer");
const path = require("path");
const populateUser = async (n) => {
  const users = [];
  while (n) {
    const user = {
      username: faker.internet.userName(),
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      email: faker.internet.email(),
      dob: faker.date.past(),
      password: "password123",
      phone: faker.phone.phoneNumber(),
    };
    users.push(user);

    n--;
  }
  try {
    User.insertMany(users);
  } catch (err) {
    console.log(err);
  }
};
const populateLawyer = async (n) => {
  const lawyers = [];
  while (n) {
    const lawyer = {
    //   lawyer_photo: faker.image.avatar(),
      iscabinat: faker.datatype.boolean(),
      cabinetname: faker.name.firstName(),
      lawyer_first_name: faker.name.firstName(),
      lawyer_last_name: faker.name.lastName(),
      lawyer_email: faker.internet.email(),
      lawyer_password: "password123",
      lawyer_address: faker.address.streetAddress(),
      lawyer_phone: faker.phone.phoneNumber(),
      lawyer_city: faker.address.city(),
      lawyer_state: faker.address.state(),
      lawyer_zip: faker.address.zipCode(),
      lawyer_country: faker.address.country(),
      lawyer_dob: faker.date.past(),
      lawyer_expertise_in: faker.lorem.sentence(),
      lawyer_presentation: faker.lorem.sentence(),
      lawyer_legal_information: faker.lorem.sentence(),
      lawyer_language_spoken: faker.lorem.sentence(),
    };
  }
  try {
    lawyer.insertMany(lawyers);
  } catch (err) {
    console.log(err);
  }
};

const init = () => {
//   populateUser(50);
  populateLawyer(50);
};
module.exports = { init };
