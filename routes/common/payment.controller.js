const paypal = require('paypal-rest-sdk');
const User = require('../../models/auth/user');
const Notification = require('../../models/Notifications');
const lawunit = require('../../models/lawyer/Lawunit');
const { sendmail } = require("../../modules/sendmail");
const Serviceprovided = require('../../models/client/Servicesprovided');
const adminmodel = require('../../models/admin/Admin');
const { savetransactionclient } = require('../admin/Transactions.controller');
const { sendpushnotificationtouser } = require('../../modules/Fcm');
const notificationpermissionmodel = require("../../models/NotificationPermission");
const Schedulemodel = require('../../models/lawyer/Schedule');
const CoupensModel = require("../../models/lawyer/Coupens");
//var Policies = require('../../models/policies');
// const ComBackGround = require('../../models/communitybackground');
const stripe_key = process.env.STRIPE_SECRET_KEY_TEST;
const STRIPE_PUBLISHABLE_KEY_TEST = process.env.STRIPE_PUBLISHABLE_KEY_TEST;
const stripe = require('stripe')(stripe_key);
const key = "pk_test_ZYZNOam8KTCxbRXa3RaBSeKk"
const YOUR_DOMAIN = process.env.BASE_URL;
const path = require("path");
const paymentkeys=require("../../models/admin/Paymentsettings");
let cinetpaykey;
let cinetpaysecret;


const setKeys=async()=>{
    const paypalsettings=await paymentkeys.findOne({});
    console.log("paypalsettings",paypalsettings)
    if(paypalsettings){
        cinetpaykey=paypalsettings.liveapikey;
        cinetpaysecret=paypalsettings.liveapisecret
    }else{
        cinetpaykey=process.env.CINETPAY_KEY
        cinetpaysecret=process.env.CINETPAY_SECRET
        
    }
    console.log("cinetpaykey",cinetpaykey)
}
setKeys();
module.exports.getPayPal = (req, res, next) => {
    var id = req.params.id;
    var amount = req.params.amount;

    console.log("-------")
    console.log(req.params.amount)
    var createPay = (payment) => {
        return new Promise((resolve, reject) => {
            paypal.payment.create(payment, function (err, payment) {
                if (err) {
                    reject(err);
                }
                else {
                    console.log("resolve" + resolve)
                    resolve(payment);
                }
            });
        });
    }
    paypal.configure({
        'mode': 'sandbox', //sandbox or live 
        'client_id': process.env.PAYPAL_ID,
        'client_secret': process.env.PAYPAL_SECRET
    });
    var payment = {
        'intent': 'authorize',
        'payer': {
            'payment_method': 'paypal'
        },
        'redirect_urls': {
            'return_url': YOUR_DOMAIN + "/api/payment/paypal/success/" + id + '/' + amount + "/payment_status=true",
            'cancel_url': YOUR_DOMAIN + '/api/payment/paypal/cancel/failed/'+id
        },
        'transactions': [{
            'amount': {
                'total': amount,
                'currency': "USD"
            },
            'description': 'Coopdivercite'
        }]
    }
    createPay(payment)
        .then((transaction) => {
            var id = transaction.id;
            var links = transaction.links;
            var counter = links.length;
            while (counter--) {
                if (links[counter].method == 'REDIRECT') {
                    return res.redirect(links[counter].href)
                }
            }
        })
        .catch(async (err) => {
            console.log(err);

            const service = Serviceprovided.findOne({ _id: id }, function (err, service) {
                if (err) {
                    console.log(err);

                }
                else {
                    service.payment_status = false;
                    service.payment_failed = true;
                    service.save((err, service) => {
                        if (err) {
                            console.log(err);

                        }
                        else {
                            console.log("service payment fialed saved successfully")
                        }
                    });
                }
            });
            await savetransactionclient(service, err.message);
            const user = await User.findOne({ _id: service.client_id });
            const message = process.env.MESSAGE_PAYMENT_FAILED;
            sendpushnotificationtouser(message, user, service._id,"user","payment")

            res.redirect('/err');
        });

}

module.exports.getPayPalSuccess = async (req, res) => {
    try {
        var previous_amount = 0;
        var new_amount = 0;
        const admin = await adminmodel.findOne({});
        console.log(req.params.id)
        const id = req.params.id;
        // const finalamount = req.params.finalamount;
        const promo = req.params.promo;
        const amount = req.params.amount;
        const payment_success_message=process.env.MESSAGE_PAYMENT_SUCCESS;
        const service = await Serviceprovided.findById(id);
        const pathtofileforpaymentsuccess = path.resolve("views/notifications/paymentsuccess.ejs");
        const pathtofileforbookingupdate = path.resolve("views/notifications/newbookingupdate.ejs");
        if (service) {
            //client notification permission
            const notipermissiont = await notificationpermissionmodel.find({ id: service.client_id });
            //provider notification permission
            const notipermissions = await notificationpermissionmodel.find({ id: service.provider_id });
            service.payment_status = true;
            service.mode_of_payment = "Paypal";
            service.payment_amount = amount;
            service.payment_failed=false;
            service.service_acceptance_status_by_provider=true;
            service.service_acceptance_status_by_client=true
            // service.finalamount = finalamount;
            // service.promo = promo;
            service.save().then(async (result) => {
                // deletetimeslot(service.provider_id, service.time_availed, service.date_availed, service.mode_of_service)
                await lawunit.find({ _id: service.provider_id }).exec(async (err, lawyer) => {
                   
                    const pushmessage = `${service.client_name} a effectué le paiement de ${service.title}. Vous pouvez procéder maintenant`;
                    if (notipermissions.length) {
                        if (notipermissions[0].push_reservation) {
                            sendpushnotificationtouser(pushmessage, lawyer[0], service._id,"provider","payment")
                        } else {
                            console.log("push notification is disabled for teacher")
                        } if (notipermissions[0].email_reservation) {
                            sendmail(pathtofileforbookingupdate,
                                { name: lawyer[0].first_name + " " + lawyer[0].last_name ,message:`Une réservation a été faite pour le cours ${service.course_name} par ${service.provider_name} `},
                                pushmessage,
                                lawyer[0].email);
                        } else {
                            console.log("email notification is disabled for teacher")
                        }
                    } else {
                        sendpushnotificationtouser(pushmessage, lawyer[0], service._id,"provider","payment");
                        sendmail(pathtofileforbookingupdate,
                            { name: lawyer[0].first_name + " " + lawyer[0].last_name ,message:`Une réservation a été faite pour le cours ${service.course_name} par ${service.provider_name} `},
                            pushmessage,
                            lawyer[0].email);
                    }


                })
                await savetransactionclient(service, "");
                const user = await User.findOne({ _id: service.client_id });
                
                if (notipermissiont.length) {
                    if (notipermissiont[0].push_reservation) {
                        sendpushnotificationtouser(payment_success_message, user, service._id,"user","payment");
                    } else {
                        console.log("push notification is disabled for user")
                    } if (notipermissiont[0].email_reservation) {
                        sendmail(pathtofileforpaymentsuccess,
                            { name: user.first_name + " " + user.last_name,message:`Paiement réussi pour la classe ${service.course_name}   par ${service.provider_name}` },
                            payment_success_message,
                            user.email);
                    } else {
                        console.log("email notification is disabled for teacher")
                    }
                } else {
                    sendpushnotificationtouser(payment_success_message, user, service._id,"user","payment");
                    sendmail(pathtofileforpaymentsuccess,
                        { name: user.first_name + " " + user.last_name,message:`Paiement réussi pour la classe ${service.course_name}  par ${service.provider_name}` },
                        payment_success_message,
                        user.email);
                }

                const coupen = await CoupensModel.findOne({ code: promo });
                if (coupen) {
                    coupen.used = coupen.used + 1;
                    coupen.save()
                }
                return res.send({
                    status: true,
                    message: "payemtn success"
                })
            })
        } else {
            return res.send({
                status: false,
                message: "user not found"
            })
        }




    } catch (err) {

        res.send({
            success: false,
            service: null,
            message: err.message
        });
    }
}
module.exports.getPayPalCancel = async(req, res) => {
    const id=req.params.id;

    const service = await Serviceprovided.findById(id);
    service.payment_failed=true;
    service.save().then((result)=>{
        return res.json({
            success: false,
            message: "paiement cancled"
        });
        
    })
   

}

module.exports.getStripe = (req, res, next) => {

    res.render('admin-panel/Stripe', {
        key: STRIPE_PUBLISHABLE_KEY_TEST,
        amount: req.params.amount,
        currency: "eur",
        user_id: req.params.id,
        i18n: res,
        title: "Stripe",
        url: "/api/payment/postStripe",
        base_url: process.env.BASE_URL


    })

}


module.exports.postStripe = async (req, res, next) => {



    try {
        stripe.customers.create({
            email: req.body.stripeEmail,
            source: req.body.stripeToken,
            name: req.body.first_name,

            address: {
                line1: 'test',
                postal_code: 'test',
                city: 'test',
                state: 'test',
                country: 'test',
            }
        })
            .then((customer) => {
                console.log("customerid", customer.id)
                return stripe.charges.create({
                    amount: parseFloat(req.body.amount) * 100,     // Charing Rs 25 
                    description: "making a service purchase",
                    customer: customer.id,
                    currency: "eur"

                });
            })
            .then(async (charge) => {
                // console.log(charge.status)
                // console.log("charge status")
              
                console.log("befor charge succedded")
                if (charge.status == "succeeded") {
                    console.log("after charge succedded");
                    console.log("after charge succedded req.body",req.body)
                   
                    const user= await Serviceprovided.findById(req.body.id);
                    console.log("user",user)
                    const pathtofileforpaymentsuccess = path.resolve("views/notifications/paymentsuccess.ejs");
                    const pathtofileforbookingupdate = path.resolve("views/notifications/newbookingupdate.ejs");
                    // let admin = await getmodel(adminmodel, {});
                    const notipermissiont = await notificationpermissionmodel.find({ id: user.client_id });
                    const notipermissions = await notificationpermissionmodel.find({ id: user.provider_id });
                   
                   
                   if(user){
                    console.log("after charge succedded service",user)
                        user.payment_amount = req.body.amount;
                        user.transaction_id = charge.id;
                        user.payment_status = true;
                        user.service_acceptance_status_by_provider=true;
                        user.service_acceptance_status_by_client=true
                        user.payment_failed=false;
                        user.date_of_transaction = new Date().toISOString();
                        user.mode_of_payment = "Stripe";

                        user.cardType = charge.payment_method_details.card.brand;
                        user.cardNum = charge.payment_method_details.card.last4;
                        user.save(async (err, service) => {
                            if (err) {
                                console.log(err);
                                res.json({
                                    success: false,
                                    service: null,
                                    url: YOUR_DOMAIN + "/api/payment/stripeFailed",
                                    message: "erreur dans la sauvegarde du portefeuille."
                                });
                            }
                            else {

                                // deletetimeslot(service.provider_id, service.time_availed, service.date_availed, service.mode_of_service)
                                await lawunit.find({ _id: service.provider_id }).exec(async (err, lawyer) => {
                                   
                                    const pushmessage = `${service.client_name} a effectué le paiement de ${service.title}. Vous pouvez procéder maintenant`;
                                    if (notipermissions.length) {
                                        if (notipermissions[0].push_reservation) {
                                            sendpushnotificationtouser(pushmessage, lawyer[0], service._id,"provider","payment")
                                        } else {
                                            console.log("push notification is disabled for teacher")
                                        } if (notipermissions[0].email_reservation) {
                                            sendmail(pathtofileforbookingupdate,
                                                { name: lawyer[0].first_name + " " + lawyer[0].last_name ,message:`Une réservation a été faite pour le cours ${service.course_name} par ${service.provider_name} `},
                                                pushmessage,
                                                lawyer[0].email);
                                        } else {
                                            console.log("email notification is disabled for teacher")
                                        }
                                    } else {
                                        sendpushnotificationtouser(pushmessage, lawyer[0], service._id,"provider","payment");
                                        sendmail(pathtofileforbookingupdate,
                                            { name: lawyer[0].first_name + " " + lawyer[0].last_name ,message:`Une réservation a été faite pour le cours ${service.course_name} par ${service.provider_name} `},
                                            pushmessage,
                                            lawyer[0].email);
                                    }


                                })
                                await savetransactionclient(service, "");
                                const user = await User.findOne({ _id: service.client_id });
                                const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                                if (notipermissiont.length) {
                                    if (notipermissiont[0].push_reservation) {
                                        sendpushnotificationtouser(message, user, service._id,"user","payment");
                                    } else {
                                        console.log("push notification is disabled for user")
                                    } if (notipermissiont[0].email_reservation) {
                                        sendmail(pathtofileforpaymentsuccess,
                                            { name: user.first_name + " " + user.last_name,message:`Paiement réussi pour la classe ${service.course_name}   par ${service.provider_name}` },
                                            "Succès du paiement",
                                            user.email);
                                    } else {
                                        console.log("email notification is disabled for teacher")
                                    }
                                } else {
                                    sendpushnotificationtouser(message, user, service._id,"user","payment");
                                    sendmail(pathtofileforpaymentsuccess,
                                        { name: user.first_name + " " + user.last_name,message:`Paiement réussi pour la classe ${service.course_name}   par ${service.provider_name}` },
                                        "Succès du paiement",
                                        user.email);
                                }
                                const coupen = await CoupensModel.findOne({ code: req.body.promo });
                                if (coupen) {
                                    coupen.used = coupen.used + 1;
                                    coupen.save()
                                }
                              
                                // return res.send({
                                //     status: true,
                                //     message: "payemtn success"
                                // })


                                // res.json({
                                //     success : true,
                                //     service : service,

                                //     message : "paiement effectué"
                                // });
                                return res.redirect("/api/payment/stripeSuccess");
                            }
                        });
                    }

                }
                else {
                    const service = Serviceprovided.findOne({ _id: req.params.id });
                    await savetransactionclient(service, "payment failed");
                    const user = await User.findOne({ _id: service.client_id });
                    console.log(user)

                    service.payment_failed=true;
                    service.save();
                    const message = process.env.MESSAGE_PAYMENT_FAILED;

                    sendpushnotificationtouser(message, user, service._id,"user","payment")

                    res.redirect("/api/payment/stripeFailed");

                }
                //res.send({code:200,succuss:true,charge:charge,message:"Wallet recharge successfully"})

            })
            .catch((err) => {

                res.send(err)       // If some error occurs 
            });
    } catch (err) {
        console.log(err);
    }
}


module.exports.stripeSuccess = (req, res, next) => {
    res.render('admin-panel/stripeSuccess', {
        i18n: res,
        title: "Stripe succuss",
        succuss: true
    })

}

module.exports.stripeFailed = (req, res, next) => {
    res.render('admin-panel/stripeFailed', {
        i18n: res,
        title: "Stripe failed",
        succuss: false

    })

}
async function getmodel(model, filter) {
    let response = await model.findOne(filter);
    return response;
}
async function deletetimeslot(id, timeslot, date, modesofservice) {
    console.log(id, timeslot, date, modesofservice);

    let response = await Schedulemodel.findOne({ provider_id: id, date: date });
    if (response) {
        console.log(response)
        let timeavailible = response.time;
        console.log("timeavailible", timeavailible)
        if (timeavailible) {
            let timeavailiblearray = timeavailible.split(",");
            let index = timeavailiblearray.indexOf(timeslot);
            console.log("timeavailiblearray", timeavailiblearray)
            if (index > -1) {
                timeavailiblearray.splice(index, 1);
                let newtimeavailible = timeavailiblearray.join(",");
                await Schedulemodel.findOneAndUpdate({ provider_id: id, date: date, time: newtimeavailible }).exec();
            }
        }
    }
    // if(response){
    //     console.log("response",response)
    //     if(modesofservice=="En ligne"||modesofservice=="Online"||modesofservice=="en ligne"||modesofservice=="online"){
    //         let timeavailible=response.modesofservice.online.time_availible
    //         console.log("timeavailible",timeavailible)
    //         if(timeavailible){
    //             let timeavailiblearray=timeavailible.split(",");
    //             let index=timeavailiblearray.indexOf(timeslot);
    //             console.log("timeavailiblearray",timeavailiblearray)
    //             if(index>-1){
    //                 timeavailiblearray.splice(index,1);
    //                 let newtimeavailible=timeavailiblearray.join(",");
    //                 await Schedulemodel.findOneAndUpdate({lawyer_id:id,date:date},{modesofservice:{online:{time_availible:newtimeavailible}}}).exec();
    //             }
    //         }
    //     }else if(modesofservice=="Hors ligne"||modesofservice=="hors ligne"||modesofservice=="offline"||modesofservice=="Offline"){
    //         let timeavailible=response.modesofservice.offline.time_availible;
    //         if(timeavailible){
    //             let timeavailiblearray=timeavailible.split(",");
    //             let index=timeavailiblearray.indexOf(timeslot);
    //             if(index>-1){
    //                 timeavailiblearray.splice(index,1);
    //                 let newtimeavailible=timeavailiblearray.join(",");
    //                 await Schedulemodel.findOneAndUpdate({lawyer_id:id,date:date},{modesofservice:{offline:{time_availible:newtimeavailible}}}).exec();
    //             }
    //         }
    //     }
    // }
}

module.exports.cinetpay = (req, res) => {
    const amount = req.params.amount;
    const key = cinetpaykey;
    const site_id=cinetpaysecret
    

    const user_id = req.params.id
    res.render('admin-panel/cinetpay', {
        key: key,
        amount: amount,
        currency: "eur",
        user_id: user_id,
        site_id:site_id,
        api_key:key,

        url: "/api/payment/postStripe",
        base_url: process.env.BASE_URL


    })
}

module.exports.cinetpaysuccess = async (req, res) => {
    const id = req.params.id;
    const finalamount = req.params.finalamount;
    const promo = req.params.promo;
    const amount = req.params.amount;
    const service = await Serviceprovided.findById(id);
    const pathtofile = path.resolve("views/notifications/reservationcofirmation.ejs");
    if (service) {
        const notipermissiont = await notificationpermissionmodel.find({ id: service.client_id });
        const notipermissions = await notificationpermissionmodel.find({ id: service.provider_id });
        service.payment_status = true;
        service.mode_of_payment = "Cinetpay";
        service.payment_amount = amount;
        service.finalamount = finalamount;
        service.promo = promo;
        service.save().then(async (result) => {
            deletetimeslot(service.teacher_id, service.time_availed, service.date_availed, service.mode_of_service)
            await lawunit.find({ _id: service.provider_id }).exec(async (err, lawyer) => {
                console.log("lawyer in payent controller ", lawyer);
                const message = process.env.MESSAGE_TO_UPDATE_TEACHER_FOR_BOOKED_SERVICE + " " + service.client_name;
                if (notipermissions.length) {
                    if (notipermissions[0].push_reservation) {
                        sendpushnotificationtouser(message, lawyer[0], service._id,"user","payment")
                    } else {
                        console.log("push notification is disabled for teacher")
                    } if (notipermissions[0].email_reservation) {
                        sendmail(pathtofile,
                            { name: lawyer[0].first_name + " " + lawyer[0].last_name },
                            "Subscription Expires in days",
                            lawyer[0].email);
                    } else {
                        console.log("email notification is disabled for teacher")
                    }
                } else {
                    sendpushnotificationtouser(message, lawyer[0], service._id,"user","payment");
                    sendmail(pathtofile,
                        { name: lawyer[0].first_name + " " + lawyer[0].last_name },
                        "Subscription Expires in  days",
                        lawyer[0].email);
                }


            })
            await savetransactionclient(service, "");
            const user = await User.findOne({ _id: service.client_id });
            const message = process.env.MESSAGE_PAYMENT_SUCCESS;
            if (notipermissiont.length) {
                if (notipermissiont[0].push_reservation) {
                    sendpushnotificationtouser(message, user, service._id,"user","payment");
                } else {
                    console.log("push notification is disabled for user")
                } if (notipermissiont[0].email_reservation) {
                    sendmail(pathtofile,
                        { name: user.first_name + " " + user.last_name },
                        "Subscription Expires in  days",
                        user.email);
                } else {
                    console.log("email notification is disabled for teacher")
                }
            } else {
                sendpushnotificationtouser(message, user, service._id,"user","payment");
                sendmail(pathtofile,
                    { name: user.first_name + " " + user.last_name },
                    "Subscription Expires in  days",
                    user.email);
            }

            const coupen = await CoupensModel.findOne({ code: promo });
            if (coupen) {
                coupen.used = coupen.used + 1;
                coupen.save()
            }
            return res.send({
                status: true,
                message: "payemtn success"
            })
        })
    } else {
        return res.send({
            status: false,
            message: "user not found"
        })
    }

}

module.exports.cinetpayt = (req, res) => {
    const amount = req.params.amount;
    const key = process.env.cinetpaykey;
    const user_id = req.params.id
    res.render('admin-panel/cinetpay', {
        key: key,
        amount: amount,
        currency: "eur",
        user_id: user_id,


        url: "/api/payment/postStripe",
        base_url: process.env.BASE_URL


    })
}

module.exports.cinetpaysuccesst = async (req, res) => {
    const id = req.params.id;
    const finalamount = req.params.finalamount;
    const promo = req.params.promo;
    const service = await Serviceprovided.findById(id);
    if (service) {
        service.payment_status = true;
        service.payment_amount = finalamount;
        service.promo = promo;
        service.mode_of_payment = "Cinetpay";
        service.save().then(async (result) => {
            deletetimeslot(service.teacher_id, service.time_availed, service.date_availed, service.mode_of_service)
            await lawunit.find({ _id: service.teacher_id }).exec(async (err, lawyer) => {
                console.log("lawyer in payent controller ", lawyer);
                const message = process.env.MESSAGE_TO_UPDATE_TEACHER_FOR_BOOKED_SERVICE + " " + service.client_name;
                sendpushnotificationtouser(message, lawyer[0], service._id,"user","payment")
                console.log("updated");
            })
            await savetransactionclient(service, "");
            const user = await User.findOne({ _id: service.student_id });
            const message = process.env.MESSAGE_PAYMENT_SUCCESS;
            sendpushnotificationtouser(message, user, service._id,"user","payment");


            return res.send({
                status: true,
                message: "payemtn success"
            })
        })
    } else {
        return res.send({
            status: false,
            message: "user not found"
        })
    }

}

module.exports.bypasspayment = async (req, res, next) => {



        const id=req.body.id;
        const promo=req.body.promo;
        const finalamount=req.body.finalamount;
        const commission=req.body.commission;
        const tax=req.body.tax;
        const provider_id=req.body.provider_id;
        const provider_name=req.body.provider_name;
       const payment_amount=req.body.amount
        const user= await Serviceprovided.findById(id);
        
        const pathtofileforpaymentsuccess = path.resolve("views/notifications/paymentsuccess.ejs");
        const pathtofileforbookingupdate = path.resolve("views/notifications/newbookingupdate.ejs");
        // let admin = await getmodel(adminmodel, {});
        const notipermissiont = await notificationpermissionmodel.find({ id: user.client_id });
        const notipermissions = await notificationpermissionmodel.find({ id: provider_id });
       
       
       if(user){
       
            user.payment_amount = payment_amount;
            user.transaction_id = "";
            user.provider_id = provider_id;
            user.provider_name = provider_name;
            user.promo=promo
            user.payment_status = true;
            user.service_acceptance_status_by_provider=true;
            user.service_acceptance_status_by_client=true
            user.payment_failed=false;
            user.date_of_transaction = new Date().toISOString();
            user.mode_of_payment = "Direct";
            user.finalamount = finalamount;
            user.commission = commission;
            user.tax = tax;
            user.cardType = "";
            user.cardNum = "";
            user.save(async (err, service) => {
                if (err) {
                    console.log(err);
                    res.json({
                        success: false,
                        service: null,
                        url: YOUR_DOMAIN + "/api/payment/stripeFailed",
                        message: "erreur dans la sauvegarde du portefeuille."
                    });
                }
                else {

                    // deletetimeslot(service.provider_id, service.time_availed, service.date_availed, service.mode_of_service)
                    await lawunit.find({ _id: provider_id}).exec(async (err, lawyer) => {
                       
                        const pushmessage = process.env.MESSAGE_TO_UPDATE_PROVIDER_FOR_BOOKED_SERVICE + " " + service.client_name;
                        if (notipermissions.length) {
                            if (notipermissions[0].push_reservation) {
                                sendpushnotificationtouser(pushmessage, lawyer[0], service._id,"user","payment")
                            } else {
                                console.log("push notification is disabled for teacher")
                            } if (notipermissions[0].email_reservation) {
                                sendmail(pathtofileforbookingupdate,
                                    { name: lawyer[0].first_name + " " + lawyer[0].last_name ,message:`Une réservation a été faite pour le cours ${service.course_name} par ${service.provider_name} `},
                                    pushmessage,
                                    lawyer[0].email);
                            } else {
                                console.log("email notification is disabled for teacher")
                            }
                        } else {
                            sendpushnotificationtouser(pushmessage, lawyer[0], service._id,"user","payment");
                            sendmail(pathtofileforbookingupdate,
                                { name: lawyer[0].first_name + " " + lawyer[0].last_name ,message:`Une réservation a été faite pour le cours ${service.course_name} par ${service.provider_name} `},
                                pushmessage,
                                lawyer[0].email);
                        }


                    })
                    await savetransactionclient(service, "");
                    const user = await User.findOne({ _id: service.client_id });
                    const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                    if (notipermissiont.length) {
                        if (notipermissiont[0].push_reservation) {
                            sendpushnotificationtouser(message, user, service._id,"user","payment");
                        } else {
                            console.log("push notification is disabled for user")
                        } if (notipermissiont[0].email_reservation) {
                            sendmail(pathtofileforpaymentsuccess,
                                { name: user.first_name + " " + user.last_name,message:`Paiement réussi pour la classe ${service.course_name}   par ${service.provider_name}` },
                                "Succès du paiement",
                                user.email);
                        } else {
                            console.log("email notification is disabled for teacher")
                        }
                    } else {
                        sendpushnotificationtouser(message, user, service._id,"user","payment");
                        sendmail(pathtofileforpaymentsuccess,
                            { name: user.first_name + " " + user.last_name,message:`Paiement réussi pour la classe ${service.course_name}   par ${service.provider_name}` },
                            "Succès du paiement",
                            user.email);
                    }
                    const coupen = await CoupensModel.findOne({ code: req.body.promo });
                    if (coupen) {
                        coupen.used = coupen.used + 1;
                        coupen.save()
                    }
                  
                    return res.send({
                        status: true,
                        message: "payemtn success"
                    })


                    // res.json({
                    //     success : true,
                    //     service : service,

                    //     message : "paiement effectué"
                    // });
                    // return res.redirect("/api/payment/stripeSuccess");
                }
            });
        

    }
}