const paypal = require('paypal-rest-sdk');
const User = require('../../models/auth/user');
const Notification = require('../../models/Notifications');
const boosterpackage=require("../../models/admin/Boosterpackage")
const lawunit = require('../../models/lawyer/Lawunit');
const paymentkeys=require("../../models/admin/Paymentsettings");
const Subscriptions = require('../../models/lawyer/Subscriptions');
const adminmodel = require('../../models/admin/Admin');
//var Policies = require('../../models/policies');
// const ComBackGround = require('../../models/communitybackground');
const stripe_key= process.env.STRIPE_SECRET_KEY_TEST;
const STRIPE_PUBLISHABLE_KEY_TEST = process.env.STRIPE_PUBLISHABLE_KEY_TEST;
const {savetransactionlawyer}=require('../admin/Transactions.controller');
const {sendpushnotificationtouser}=require('../../modules/Fcm');
const stripe = require('stripe')(stripe_key);
const key="pk_test_ZYZNOam8KTCxbRXa3RaBSeKk"
const YOUR_DOMAIN =process.env.BASE_URL;
let paypalkey;
let paypalsecret;
let stripekey;
let stripesecret;

let envp="sandbox";
const setKeys=async()=>{
    const paypalsettings=await paymentkeys.findOne({name:"paypal"});
    const stripesettings=await paymentkeys.findOne({name:"stripe"});
    if(paypalsettings?.currenvtype=="sandbox"){
        envp="sandbox"
        paypalkey=paypalsettings.testapikey
        paypalsecret=paypalsettings.testapisecret
        stripekey=stripesettings.testapikey
        stripesecret=stripesettings.testapisecret
    }else if(paypalsettings?.currenvtype=="live"){
        envp="live"
        paypalkey=paypalsettings.liveapikey
        paypalsecret=paypalsettings.liveapisecret
        stripekey=stripesettings.liveapikey
        stripesecret=stripesettings.liveapisecret
    }else{
        paypalkey=process.env.PAYPAL_ID
        paypalsecret=process.env.PAYPAL_SECRET
        stripekey=process.env.STRIPE_PUBLISHABLE_KEY_TEST
        stripesecret=process.env.STRIPE_SECRET_KEY_TEST
    }
}
setKeys();

module.exports.getPayPal =  async(req, res, next) => {
    var id = req.params.id;
    var amount = req.params.amount;
   
    var createPay = (payment) => {
        return new Promise((resolve, reject) => {
            paypal.payment.create(payment, function (err, payment) {
                if (err) {
                    reject(err);
                }
                else {
                    console.log("resolve"+resolve )
                    resolve(payment);
                }
            });
        });
    }
    paypal.configure({
        'mode': 'sandbox', //sandbox or live 
        'client_id': process.env.PAYPAL_ID,
        'client_secret': process.env.PAYPAL_SECRET    
    });
    var payment = {
        'intent': 'authorize',
        'payer': {
            'payment_method': 'paypal'
        },
        'redirect_urls': {
            'return_url': YOUR_DOMAIN+"/api/payment/paypallawyer/success/"+ id + "/" + amount+"/payment_status=true",
            'cancel_url': YOUR_DOMAIN+'/api/payment/paypallawyer/cancel/payment_status=false'
        },
        'transactions': [{
            'amount': {
                'total': amount,
                'currency': "USD"
            },
            'description': 'Slegal'
        }]
    }
    createPay(payment)
        .then((transaction) => {
            var id = transaction.id;
            var links = transaction.links;
            var counter = links.length;
            while (counter--) {
                if (links[counter].method == 'REDIRECT') {
                    return res.redirect(links[counter].href)
                }
            }
        })
        .catch(async(err) => {
            console.log(err);
            
            await Subscriptions.findOne({_id : id}, function (err, service) {
                if (err) {
                    console.log(err);
                    res.redirect("/api/payment/paypalfailedlawyer/payment_status=false");
                }
                else {
                    service.payment_status = false;
                    service.save(async(err, service) => {
                        if (err) {
                            console.log(err);
                            
                        }
                        else {
                            await savetransactionclient(service,err.message);
                            const user= await lawunit.findOne({_id : service.provider_id});
                            const message=process.env.MESSAGE_PAYMENT_FAILED;
                            sendpushnotificationtouser(message,user,service.provider_id,"provider","payment")
                            console.log("service payment fialed saved successfully")
                        }
                    });
                }
            });
           
               
            res.redirect('/err');
        });

}

module.exports.getPayPalSuccess = async(req, res) => {
   try{
   
   
    await Subscriptions.findOne({_id : req.params.id}).then(user=>{

        
        
        
            user.payment_amount = req.params.amount;
            user.transaction_id = req.query.paymentId;
            user.payment_status = true;
            user.status=true;
            user.end_date=user.temp_end_date;
            user.temp_end_date=null;
            user.date_of_transaction = new Date();
            user.mode_of_payment = "Paypal";
            
           
            user.save(async(err, service) => {
                if (err) {
                    console.log(err);
                    res.json({
                        success : false,
                        service : null,
                       
                        message : "erreur dans la sauvegarde du portefeuille."
                    });
                }
                else {
                await savetransactionlawyer(service,"")
                const user= await lawunit.findOne({_id : service.provider_id});
                user.is_subscribed=true;
                user.subscription_id=service._id;
                
                user.save().then(()=>{
                const message=process.env.MESSAGE_SUBSCRIPTION_SUCCESS+" "+user.first_name+" "+user.last_name;
                sendpushnotificationtouser(message,user,service.provider_id,"provider","payment")
                
                       return res.json({
                                success : true,
                                service : service,
    
                                message : "paiement effectué"
                                });
                })
            }});
           
        });
   
}catch(err){
   
    res.send({
        success : false,
        service : null,
        message : err.message
    });
  }
}

module.exports.getPayPalCancel = (req, res) => {

    res.json({
    	success : false,
    	message : "paiement cancled"
    });

}



module.exports.getStripe =  (req, res, next) => {
    
    res.render('admin-panel/Stripe', { 
                key : STRIPE_PUBLISHABLE_KEY_TEST,
                amount : req.params.amount,
                currency : "eur",
                user_id : req.params.id,
                i18n : res,
                title : "Stripe",
                url:"/api/payment/postStripelawyer"
                

             }) 

}


module.exports.postStripe =  async(req, res, next) => {
   
    
    
   try{
    stripe.customers.create({ 
        email: req.body.stripeEmail, 
        source: req.body.stripeToken,
        name: req.body.first_name, 
        
        address: { 
            line1: 'test', 
            postal_code: 'test', 
            city: 'test', 
            state: 'test', 
            country: 'test', 
        } 
    })
    .then((customer) => { 
        console.log("customerid" , customer.id)
        return stripe.charges.create({ 
            amount: parseFloat(req.body.amount) * 100,     // Charing Rs 25 
            description: "making a service purchase", 
            customer:customer.id,
            currency: "eur"
           
        }); 
    }) 
    .then(async(charge) => { 
        console.log(charge.status)
        console.log("charge status")
        if(charge.status=="succeeded"){
                      
           
           
           const user=await Subscriptions.findById(req.body.id);
           console.log("user is ===>",user)
           user.payment_amount = req.body.amount;
           user.transaction_id = charge.id;
           user.payment_status =true;
           user.status=true;
           user.end_date=user.temp_end_date;
           user.temp_end_date=null;
           user.date_of_transaction = new Date().toISOString();
           user.mode_of_payment = "stripe";
           user.cardType = charge.payment_method_details.card.brand;
           user.cardNum = charge.payment_method_details.card.last4;
            
            
            user.save().then(async(service)=>{
               
            await savetransactionlawyer(service,"");
            
            const usera= await lawunit.findById(service.provider_id);
                usera.is_subscribed=true;
                usera.subscription_id=service._id;
                usera.save(async(err, noservice) => {
                if(err){
                    return res.send({
                        status:false,
                        message:"error",
                        errmessage:"err",
                        data:null
                    })
                }
                const message=process.env.MESSAGE_SUBSCRIPTION_SUCCESS;
               
                sendpushnotificationtouser(message,usera,service.provider_id,"provider","payment")
                return res.redirect("/api/payment/stripeSuccesslawyer/payment_status=true");
                })
                
            });
           
        
        }
        else{
            const service=Subscriptions.findOne({_id : req.params.id});
            await savetransactionclient(service,"payment failed");
            const user= await lawunit.findOne({_id : service.provider_id});
            
            const message=process.env.MESSAGE_PAYMENT_FAILED;
          
            sendpushnotificationtouser(message,user,service.provider_id,"provider","payment")
           
        res.redirect("/api/payment/stripeFailedlawyer/payment_status=false");

        }
        //res.send({code:200,succuss:true,charge:charge,message:"Wallet recharge successfully"})
       
    }) 
    .catch((err) => {
        console.log(err+err);
        res.send(err)       // If some error occurs 
    }); 
   }catch(err){

    console.log(err);
   }
}
module.exports.paypalfailedlawyer =  (req, res, next) => {
    res.render('admin-panel/paypalfailedlawyer', { 
                i18n : res,
                title : "Paypal failed",
                succuss : false
             }) 

}

module.exports.stripeSuccess =  (req, res, next) => {
    res.render('admin-panel/stripeSuccess', { 
                i18n : res,
                title : "Stripe succuss",
                succuss : true
             }) 

}

module.exports.stripeFailed =  (req, res, next) => {
    res.render('admin-panel/stripeFailed', { 
                i18n : res,
                title : "Stripe failed",
                succuss : false

             }) 

}
async function getmodel(model,filter){
 let response=await   model.findOne(filter);
    return response;
}



//booster payment

module.exports.getPayPalboost =  async(req, res, next) => {
    var id = req.params.id;
    var amount = req.params.amount;
   
    var createPay = (payment) => {
        return new Promise((resolve, reject) => {
            paypal.payment.create(payment, function (err, payment) {
                if (err) {
                    reject(err);
                }
                else {
                    console.log("resolve"+resolve )
                    resolve(payment);
                }
            });
        });
    }
    paypal.configure({
        'mode': 'sandbox', //sandbox or live 
        'client_id': process.env.PAYPAL_ID,
        'client_secret': process.env.PAYPAL_SECRET    
    });
    var payment = {
        'intent': 'authorize',
        'payer': {
            'payment_method': 'paypal'
        },
        'redirect_urls': {
            'return_url': YOUR_DOMAIN+"/api/payment/paypallawyerboost/success/"+ id + "/" + amount+"/payment_status=true",
            'cancel_url': YOUR_DOMAIN+'/api/payment/paypallawyerboost/cancel/payment_status=false'
        },
        'transactions': [{
            'amount': {
                'total': amount,
                'currency': "USD"
            },
            'description': 'Slegal'
        }]
    }
    createPay(payment)
        .then((transaction) => {
            var id = transaction.id;
            var links = transaction.links;
            var counter = links.length;
            while (counter--) {
                if (links[counter].method == 'REDIRECT') {
                    return res.redirect(links[counter].href)
                }
            }
        })
        .catch(async(err) => {
            console.log(err);
            
            await Subscriptions.findOne({_id : id}, function (err, service) {
                if (err) {
                    console.log(err);
                    res.redirect("/api/payment/paypalfailedlawyerboost/payment_status=false");
                }
                else {
                    service.payment_status = false;
                    service.save(async(err, service) => {
                        if (err) {
                            console.log(err);
                            
                        }
                        else {
                            await savetransactionclient(service,err.message);
                            const user= await lawunit.findOne({_id : service.provider_id});
                            const message=process.env.MESSAGE_PAYMENT_FAILED;
                            sendpushnotificationtouser(message,user,service.provider_id,"provider","payment")
                            console.log("service payment fialed saved successfully")
                        }
                    });
                }
            });
           
               
            res.redirect('/err');
        });

}

module.exports.getPayPalSuccessboost = async(req, res) => {
   try{
   
   
    await boosterpackage.findOne({_id : req.params.id}).then(user=>{
        
        
        user.payment_amount = req.params.amount;
        user.transaction_id = req.query.paymentId;
        user.payment_status = true;
        user.date_of_transaction = new Date();
        user.mode_of_payment = "Paypal";
        
       
        user.save(async(err, service) => {
            if (err) {
                console.log(err);
                res.json({
                    success : false,
                    service : null,
                   
                    message : "erreur dans la sauvegarde du portefeuille."
                });
            }
            else {
            await savetransactionlawyer(service,"")
            const user= await lawunit.findOne({_id : service.provider_id});
            
            
            user.save().then(()=>{
            const message=process.env.MESSAGE_SUBSCRIPTION_SUCCESS+" "+user.first_name+" "+user.last_name;
            sendpushnotificationtouser(message,user,service.provider_id,"provider","payment")
            
                   return res.json({
                            success : true,
                            service : service,

                            message : "paiement effectué"
                            });
            })
        }});
       
    });
   
}catch(err){
   
    res.send({
        success : false,
        service : null,
        message : err.message
    });
  }
}

module.exports.getPayPalCancelboost = (req, res) => {

    res.json({
    	success : false,
    	message : "paiement cancled"
    });

}
module.exports.paypalfailedlawyerboost =  (req, res, next) => {
    res.render('admin-panel/paypalfailedlawyer', { 
                i18n : res,
                title : "Paypal failed",
                succuss : false
             }) 

}

module.exports.getStripeboost =  (req, res, next) => {
    
    res.render('admin-panel/Stripeboost', { 
                key : STRIPE_PUBLISHABLE_KEY_TEST,
                amount : req.params.amount,
                currency : "eur",
                user_id : req.params.id,
                i18n : res,
                title : "Stripe",
                url:"/api/payment/postStripelawyerboost"
                

             }) 

}


module.exports.postStripeboost =  async(req, res, next) => {
   
    
    
   try{
    stripe.customers.create({ 
        email: req.body.stripeEmail, 
        source: req.body.stripeToken,
        name: req.body.first_name, 
        
        address: { 
            line1: 'test', 
            postal_code: 'test', 
            city: 'test', 
            state: 'test', 
            country: 'test', 
        } 
    })
    .then((customer) => { 
        console.log("customerid" , customer.id)
        return stripe.charges.create({ 
            amount: parseFloat(req.body.amount) * 100,     // Charing Rs 25 
            description: "making a service purchase", 
            customer:customer.id,
            currency: "eur"
           
        }); 
    }) 
    .then(async(charge) => { 
        console.log(charge.status)
        console.log("charge status")
        if(charge.status=="succeeded"){
                      
           
           
           const user=await boosterpackage.findById(req.body.id);
           console.log("user is ===>",user)
            user.payment_amount = req.body.amount;
            user.transaction_id = charge.id;
            user.payment_status =true;
            user.date_of_transaction = new Date().toISOString();
            user.mode_of_payment = "stripe";
            
            user.cardType = charge.payment_method_details.card.brand;
            user.cardNum = charge.payment_method_details.card.last4;
            
            
            user.save().then(async(service)=>{
               
            await savetransactionlawyer(service,"");
            
            const usera= await lawunit.findById(service.provider_id);
                
                usera.save(async(err, noservice) => {
                if(err){
                    return res.send({
                        status:false,
                        message:"error",
                        errmessage:"err",
                        data:null
                    })
                }
                const message=process.env.MESSAGE_SUBSCRIPTION_SUCCESS;
               
                sendpushnotificationtouser(message,usera,service.provider_id,"provider","payment")
                return res.redirect("/api/payment/stripeSuccesslawyerboost/payment_status=true");
                })
                
            });
           
        
        }
        else{
            const service=Subscriptions.findOne({_id : req.params.id});
            await savetransactionclient(service,"payment failed");
            const user= await lawunit.findOne({_id : service.provider_id});
            
            const message=process.env.MESSAGE_PAYMENT_FAILED;
          
            sendpushnotificationtouser(message,user,service.provider_id,"provider","payment")
           
        res.redirect("/api/payment/stripeFailedlawyerboost/payment_status=false");

        }
        //res.send({code:200,succuss:true,charge:charge,message:"Wallet recharge successfully"})
       
    }) 
    .catch((err) => {
        console.log(err+err);
        res.send(err)       // If some error occurs 
    }); 
   }catch(err){

    console.log(err);
   }
}


module.exports.stripeSuccessboost =  (req, res, next) => {
    res.render('admin-panel/stripeSuccess', { 
                i18n : res,
                title : "Stripe succuss",
                succuss : true
             }) 

}

module.exports.stripeFailedboost =  (req, res, next) => {
    res.render('admin-panel/stripeFailed', { 
                i18n : res,
                title : "Stripe failed",
                succuss : false

             }) 

}