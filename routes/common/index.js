const express = require('express')
const router = express.Router();
const multer = require("multer");
const path=require("path");
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const admin_images_path=process.env.admin_images_path;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(admin_user_profile_path))
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
})

const upload2 = multer({ storage: storage });
//const paymentController = require('../controller/paymentController')
const paymentController = require('./payment.controller.js')
const lawyerpaymentController = require('./lawyerpayment.controller.js');
const twilliocontroller=require("./twillio.controller");
const contactuscontroller=require("./contactus.controller");
const mongotest=require("./mongotest");
router.get('/mongotest', (mongotest.mongotest));

router.get('/cinetpay/:id/:amount', (paymentController.cinetpay));
router.get('/cinetpaysuccess/:id/:finalamount/:promo/:amount', (paymentController.cinetpaysuccess));
router.get('/paypal/:id/:amount', (paymentController.getPayPal));
router.get('/paypal/success/:id/:amount/:payment_status', (paymentController.getPayPalSuccess));
router.get('/paypal/cancel/:payment_status/:id', (paymentController.getPayPalCancel));
router.post('/bypasspayment', paymentController.bypasspayment);
router.get('/stripe/:id/:amount', (paymentController.getStripe));
router.post('/postStripe', (paymentController.postStripe));
router.get('/stripeSuccess', (paymentController.stripeSuccess));
router.get('/stripeFailed', (paymentController.stripeFailed));
router.get('/paypallawyer/:id/:amount', (lawyerpaymentController.getPayPal));
router.get('/paypallawyer/success/:id/:amount/:payment_status', (lawyerpaymentController.getPayPalSuccess));
router.get('/paypallawyer/cancel/:payment_status', (lawyerpaymentController.getPayPalCancel));
router.get('/stripelawyer/:id/:amount', (lawyerpaymentController.getStripe));
router.post('/postStripelawyer', (lawyerpaymentController.postStripe));
router.get('/stripeSuccesslawyer/:payment_status', (lawyerpaymentController.stripeSuccess));
router.get('/stripeFailedlawyer/:payment_status', (lawyerpaymentController.stripeFailed));
router.get('/paypalfailedlawyer/:payment_status', (lawyerpaymentController.paypalfailedlawyer));

//booster payment
router.get('/paypallawyerboost/:id/:amount', (lawyerpaymentController.getPayPalboost));
router.get('/paypallawyerboost/success/:id/:amount/:payment_status', (lawyerpaymentController.getPayPalSuccessboost));
router.get('/paypallawyerboost/cancel/:payment_status', (lawyerpaymentController.getPayPalCancelboost));
router.get('/stripelawyerboost/:id/:amount', (lawyerpaymentController.getStripeboost));
router.post('/postStripelawyerboost', (lawyerpaymentController.postStripeboost));
router.get('/stripeSuccesslawyerboost/:payment_status', (lawyerpaymentController.stripeSuccessboost));
router.get('/stripeFailedlawyerboost/:payment_status', (lawyerpaymentController.stripeFailedboost));
router.get('/paypalfailedlawyerboost/:payment_status', (lawyerpaymentController.paypalfailedlawyerboost));
router.post('/join-room', async (req, res) => {
    // return 400 if the request has an empty body or no roomName
    if (!req.body || !req.body.roomName) {
      return res.status(400).send("Must include roomName argument.");
    }
    const roomName = req.body.roomName;
    // find or create a room with the given roomName
    twilliocontroller.findOrCreateRoom(roomName);
    // generate an Access Token for a participant in this room
    const token = twilliocontroller.getAccessToken(roomName);
    res.send({
      token: token,
    });
  });
router.post('/sendsms',twilliocontroller.sendSms);
router.post("/contactus",upload2.fields([
  { 
    name: 'docs', 
    maxCount: 10
  }
]
), contactuscontroller.contactus);

router.post('/blockuser',contactuscontroller.blockuser);
router.post('/unblockuser',contactuscontroller.unblockuser);
router.post('/deletechat',contactuscontroller.deletechat);



  
module.exports = router;