const { v4: uuidv4 } = require("uuid");
const AccessToken = require("twilio").jwt.AccessToken;
const VideoGrant = AccessToken.VideoGrant;


// create the twilioClient
const twilioClient = require("twilio")(
  process.env.TWILIO_API_KEY_SID,
  process.env.TWILIO_API_KEY_SECRET,
  { accountSid: process.env.TWILIO_ACCOUNT_SID }
);

module.exports.sendSms = (req,res) => {
    const {phone, message}=req.body;
    const client = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_ACCOUNT_AUTH);
    client.messages
      .create({
         body: message,
         mediaUrl: ['https://static01.nyt.com/images/2019/12/09/business/06technewsletter-print/merlin_163200591_5f00de6c-e351-491f-98a0-231a5103a032-superJumbo.jpg'],

         from: process.env.TWILIO_PHONE_NUMBER,
         to: phone
       })
      .then(message => {console.log(message.sid)
        return res.send({
            status:true,
            message:"done",
            data:message
        })
    }).catch((e)=> {return res.send({
      status:false,
      message:"not done",
      data:e
  })})
  }
module.exports.findOrCreateRoom = async (roomName) => {
  try {
    // see if the room exists already. If it doesn't, this will throw
    // error 20404.
    await twilioClient.video.rooms(roomName).fetch();
  } catch (error) {
    // the room was not found, so create it
    if (error.code == 20404) {
      await twilioClient.video.rooms.create({
        uniqueName: roomName,
        type: "go",
      });
    } else {
      // let other errors bubble up
      // throw error;
    }
  }
};

module.exports.getAccessToken = (roomName) => {
  // create an access token
  const token = new AccessToken(
    process.env.TWILIO_ACCOUNT_SID,
    process.env.TWILIO_API_KEY_SID,
    process.env.TWILIO_API_KEY_SECRET,
    // generate a random unique identity for this participant
    { identity: uuidv4() }
  );
  // create a video grant for this specific room
  const videoGrant = new VideoGrant({
    room: roomName,
  });

  // add the video grant
  token.addGrant(videoGrant);
  // serialize the token and return it
  return token.toJwt();
};



