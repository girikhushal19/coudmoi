const Contactus=require("../../models/admin/Cotactus");
const {sendmail}=require("../../modules/sendmail");
const path=require("path");
var Chat = require('../../models/chat/chat')
const client=require("../../models/auth/user");
const provider=require("../../models/lawyer/Lawunit");
module.exports.contactus=async(req,res)=>{
     const title=req.body.title;
     const reason=req.body.reason;
     const message=req.body.message;
     const id=req.body.id;
     const type=req.body.type;
     const name=req.body.name;
     const docs=[];
     let user;
     if(type=="user"){
     const clientv=await client.findById(id);
     if(clientv){
        user=clientv
     }else{
        return res.send({
            status:false,
            message:"user does not exists"
        })
     }
     }else if(type=="provider"){
        const providerv=await provider.findById(id);
        if(providerv){
           user=providerv
        }else{
           return res.send({
               status:false,
               message:"user does not exists"
           })
        }
     }else{
        return res.send({
            status:false,
            message:"type is required"
        })
     }
     const pathtofile = path.resolve("views/notifications/contactus.ejs");
     const subject="some weired things";
     
     if (req.files?.docs) {
      for(let i=0;i<req.files.docs.length;i++){
       let file="userprofile/"+req.files.docs[i].filename;
       docs.push(file)
      }
      }
     const newRequest=new Contactus();
     newRequest.title=title;
     newRequest.reason=reason;
     newRequest.message=message;
     newRequest.id=id;
     newRequest.name=name;
     newRequest.type=type;
     newRequest.docs=docs;
     newRequest.save().then(async(result)=>{
        
        sendmail(pathtofile, 
            { name: user.first_name+" "+user.last_name,message:message,reason:reason }, 
            subject,
            user.email);
     })
     return res.send({
        status:true,
        message:"messege sent"
     })

}

module.exports.blockuser=async(req,res)=>{
   const {
      usertypewhomtoblock,
      tobeblockeduserid,
      userid
   }=req.body;
   let model;
  if(usertypewhomtoblock=="user"){
  model=provider;
  }else if(usertypewhomtoblock=="provider"){
   model=client
  }else{
   return res.send({
      status:false,
      message:"spicify a valid type"
   })
  }
  await model.findByIdAndUpdate(userid,{$push:{blockedchats:tobeblockeduserid}}).then((rs)=>{
   return res.send({
      status:true,
      message:"blocked successfuly"
   })
  })

}
module.exports.unblockuser=async(req,res)=>{
    const {
      usertypewhomtounblock,
      tobeunblockeduserid,
      userid
   }=req.body;
   let model;
  if(usertypewhomtounblock=="user"){
  model=provider;
  }else if(usertypewhomtounblock=="provider"){
   model=client
  }else{
   return res.send({
      status:false,
      message:"spicify a valid type"
   })
  }
  const blockedusers=(await model.findById(userid))?.blockedchats||[];
  const newblockedusers=blockedusers.filter((e)=>e!=tobeunblockeduserid);
  await model.findByIdAndUpdate(userid,{
   blockedchats:newblockedusers
  }).then((rs)=>{
   return res.send({
      status:true,
      message:"unblocked successfuly"
   })
  })

}
module.exports.deletechat=async(req,res)=>{
   const {senderId,recieverId,roomId,sourceusertype}=req.body;
   const updatedoc={}
   let updateDocument
   if(sourceusertype=="user"){
      updateDocument = {
         "chats.$[item].donotshowtouser":true
         
     };
      // updatedoc["donotshowtoprovider"]=true
     
   }else if(sourceusertype=="provider"){
      updateDocument = {
         "chats.$[item].donotshowtoprovider": true 
     };
      // updatedoc["donotshowtouser"]=true
      
   }


   const query = { roomId:roomId };
                
   const options = {
   arrayFilters: [
      {
        $or:[
         {$and:[
            {"item.recieverId": senderId},
            {"item.senderId": recieverId}
           ]},
         {$and:[
            {"item.recieverId": recieverId},
            {"item.senderId": senderId}
           ]}
        ]
      
      },
   ],
   };
   console.log("query, updateDocument, options",query, updateDocument, options)
   await Chat.updateMany(query, updateDocument, options).then((rs)=>{
      console.log("rs",rs)
      return res.send({
         status:true,
         message:"deleted successfuly"
      })
     })
   // const chats=(await Chat.findOne({roomId:roomId}))?.chats;
   
   // await Chat.updateMany({
   //    recieverId:recieverId,
   //    senderId:senderId
   // },updatedoc).then((rs)=>{
   //    return res.send({
   //       status:true,
   //       message:"deleted successfuly"
   //    })
   //   })
}