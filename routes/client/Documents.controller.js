const multer = require("multer");
const path = require("path");
const jwt = require("jsonwebtoken");
const User = require("../../models/auth/user");
const Documentsforservice = require("../../models/client/Documentsforservice");
const client_documents_path=process.env.client_documents_path;
const lawyermodel=require("../../models/lawyer/Lawunit");
module.exports.senddocument = async(req, res) => {
  let filename;
  const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, path.resolve(client_documents_path));
    },
    filename: (req, file, cb) => {
     
      filename = Date.now() + path.extname(file.originalname);
      cb(null, filename);
    },
  });
  let uploadStorage = multer({ storage: storage });
  let isuploaded = uploadStorage.single("doc");
  isuploaded(req, res, async(err) => {
    if (err) {
      res.send({
        status: false,
        message: "",
        errmessage: err.message,
        data:null
        
      });
    } else {
      let client_id = req.body.client_id;
      let lawyerid = req.body.lawyer_id;
      let doc_name = req.body.doc_name;
      let from=req.body.from;
      let to =req.body.to;
      let fromtype=req.body.fromtype;
      let fromname;
      let toname;
      if(fromtype=="user"){
       let fromusernameres=await User.findById(from);
       console.log(fromusernameres);
       fromname=fromusernameres.first_name+" "+fromusernameres.last_name;
      let tonameres=await lawyermodel.findById(to);
      console.log(tonameres);
      toname=tonameres.first_name+" "+tonameres.last_name;
      console.log(fromname,toname);
      }else if(fromtype=="lawyer"){
       let  fromnameres=await lawyermodel.findById(from);
        fromname=fromnameres.first_name+" "+fromnameres.last_name;
        let tonameres=await User.findById(to);
        toname=tonameres.first_name+" "+tonameres.last_name;
        console.log(fromname,toname);
      }
      let doc_path = "clientdocuments/"+ filename;
      Documentsforservice.create({
        client_id: client_id,
        laywer_id: lawyerid,
        doc_name: doc_name,
        doc_path: doc_path,
        from:fromname,
        to:toname,
        fromid:from,
        toid:to
      }).then((doc) => {
        res.send({
          status: true,
          message: "Document téléchargé avec succès",
          errmessage: "",
          data: doc,
        });
      });
     
    }
  });
};

module.exports.getalldocument = (req, res) => {
  let token  = req.params.token;
  
  
  jwt.verify(token, process.env.TOKEN_KEY, (err, decoded) => {
    if(err){
      res.send({
        status:false,
        message:"",
         errmessage:err.message,
          data:null       

      })
    }
   else{
    let userid = decoded.user_id;
   
    Documentsforservice.find({ client_id: userid }).then((doc) => {
      res.send({
        status: true,
        message: "",
        errmessage: "",
        data: doc,
      });
    });
   }
  });
};
module.exports.getdocumentsbylawyerId = async(req, res) => {
  let lawyer_id=req.body.lawyer_id;
  // let lawyer_name;
  // if(lawyer_name){
  //   const lawyername=await Lawunit.findById(lawyer_id);
  //   lawyer_name=req.query.lawyer_name?{subscriber_name:req.query.lawyer_name}:{};
  // }
  
  const doc_name=req.body.doc_name;
  const  client_name=req.body.client_name;
  var start_date = req.body.start_date;
            var end_date = req.body.end_date;
  
  let matchdata={
    laywer_id:lawyer_id,
    is_lawyer_deleted:false
    
    
  }
  let client_ids_having_this_name;
  if(client_name){
    console.log(client_name.split(" ").length)
    if(client_name.split(" ").length==1){
    client_ids_having_this_name=await User.find(
    {$or:[{first_name:{$regex:client_name,$options:'i'}},
    {last_name:{$regex:client_name,$options:'i'}},
   ]})}
   else if(client_name.split(" ").length==2){
    client_ids_having_this_name=await User.find(
      {$and:[{first_name:{$regex:client_name.split(" ")[0],$options:'i'}},{last_name:{$regex:client_name.split(" ")[1],$options:'i'}}]}
      )
    }

   
    matchdata['client_id']={$in:client_ids_having_this_name.map(client=>client._id.toHexString())};
  }
  if(doc_name){
    matchdata['doc_name']={$regex:doc_name,$options:'i'};
  }
  if (start_date != '' || end_date != '') {
    if (start_date == '') {
        start_date = new Date(end_date);
        start_date = start_date - 1;
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['uploaded_on'] = {$gte: start_date, $lt: end_date};
    } else if (end_date == '') {
        end_date = new Date(start_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        start_date = new Date(start_date);
        start_date = start_date - 1;
        start_date = new Date(start_date);
        console.log(start_date, end_date);
        matchdata['uploaded_on'] = {$gte: start_date, $lt: end_date};
    } else {
        start_date = new Date(start_date);
        start_date = start_date.setHours(0, 0, 0, 0);
        start_date = new Date(start_date);
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['uploaded_on'] ={$gte: start_date, $lt: end_date};
    }
}
  console.log(matchdata)
  const documents=await Documentsforservice.aggregate([
    
    {$match:matchdata},
    // {$match:matchdata},
    {$project:{fromid:{"$toObjectId":"$fromid"}, 
    client_id: 1,
    laywer_id:1,
    doc_name: 1,
    doc_path: 1,
    uploaded_on: 1,
    is_lawyer_deleted:1,
    is_client_deleted:1,
    from:1,
    to: 1
  }},
    
    {$lookup:{
      from:"users",
      localField:"fromid",
      foreignField:"_id",
      as:"fromarrya"
    }},
    {$sort:{uploaded_on:-1}},
  ]);
  
  return res.send({
    status:true,
    message:"",
    errmessage:"",
    data:documents
  })
}
module.exports.getdocumentsbyclientId =async (req, res) => {

  let client_id=req.body.client_id;
  const doc_name=req.body.doc_name;
  const  client_name=req.body.client_name;
 
  var start_date = req.body.start_date;
            var end_date = req.body.end_date;
  
  let matchdata={
    client_id:client_id,
    is_client_deleted:false
   
    
  }
  let client_ids_having_this_name;
  if(client_name){
     console.log("client_name",client_name)
     if(client_name.split(" ").length==1){
      client_ids_having_this_name=await lawyermodel.find(
      {$or:[{first_name:{$regex:client_name,$options:'i'}},
      {last_name:{$regex:client_name,$options:'i'}},
     ]})}
     else if(client_name.split(" ").length==2){
      client_ids_having_this_name=await lawyermodel.find(
        {$and:[{first_name:{$regex:client_name.split(" ")[0],$options:'i'}},{last_name:{$regex:client_name.split(" ")[1],$options:'i'}}]}
        )
      }
    matchdata['laywer_id']={$in:client_ids_having_this_name.map(client=>client._id.toHexString())};
  }
  if(doc_name){
    matchdata['doc_name']={$regex:doc_name,$options:'i'};
  }
  if (start_date != '' || end_date != '') {
    if (start_date == '') {
        start_date = new Date(end_date);
        start_date = start_date - 1;
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['uploaded_on'] = {$gte: start_date, $lt: end_date};
    } else if (end_date == '') {
        end_date = new Date(start_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        start_date = new Date(start_date);
        start_date = start_date - 1;
        start_date = new Date(start_date);
        console.log(start_date, end_date);
        matchdata['uploaded_on'] = {$gte: start_date, $lt: end_date};
    } else {
        start_date = new Date(start_date);
        start_date = start_date.setHours(0, 0, 0, 0);
        start_date = new Date(start_date);
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['uploaded_on'] ={$gte: start_date, $lt: end_date};
    }
}
  console.log(matchdata)
  const documents=await Documentsforservice.aggregate([
   
    {$match:matchdata},
    {$project:{fromid:{"$toObjectId":"$fromid"}, 
    client_id: 1,
    laywer_id:1,
    doc_name: 1,
    doc_path: 1,
    uploaded_on: 1,
    is_lawyer_deleted:1,
    is_client_deleted:1,
    from:1,
    to: 1}},
    {$lookup:{
      from:"lawunits",
      localField:"fromid",
      foreignField:"_id",
      as:"from"
    }},
    {$sort:{uploaded_on:-1}}
  ]);
  return res.send({
    status:true,
    message:"",
    errmessage:"",
    data:documents
  })
  // Documentsforservice.find({client_id:client_id,is_client_deleted:false}).then((doc)=>{
  //   res.send({
  //     status:true,
  //     data:doc,
  //     message:"document fetched successfully",
  //     errmessage:""

  //   });
  // }).catch((err)=>{
  //   res.send({
  //     status:false,
  //     message:"",
  //     errmessage:err.message,
  //     data:null
  //   });
  // })
}
module.exports.deletedocumentbyclient = (req, res) => {
  let doc_id=req.params.doc_id;
  Documentsforservice.findByIdAndUpdate(doc_id,{
    is_client_deleted:true
  }).then((doc)=>{
    res.send({
      status:true,
      data:doc,
      message:"Document supprimé avec succès",
      errmessage:""
    });
  }).catch((err)=>{
    res.send({
      status:false,
      message:"",
      errmessage:err.message,
      data:null
    });
  })
}
module.exports.deletedocumentbylawyer = (req, res) => {
  let doc_id=req.params.doc_id;
  Documentsforservice.findByIdAndUpdate(doc_id,{
    is_lawyer_deleted:true
  }).then((doc)=>{
    res.send({
      status:true,
      data:doc,
      message:"Document supprimé avec succès",
      errmessage:""
    });
  }).catch((err)=>{
    res.send({
      status:false,
      message:"",
      errmessage:err.message,
      data:null
    });
  })
}
module.exports.getdocdetails=(req,res)=>{
  let doc_id=req.params.doc_id;
  Documentsforservice.find({_id:doc_id}).then((doc)=>{
    res.send({
      status:true,
      data:doc,
      message:"document fetched successfully",
      errmessage:""
    });
  }).catch((err)=>{
    res.send({
      status:false,
      message:"",
      errmessage:err.message,
      data:null
    });
  })
}
