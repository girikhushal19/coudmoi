const express = require("express");
const multer = require("multer");
const path=require("path")
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const admin_images_path=process.env.admin_images_path;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(admin_user_profile_path))
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
})

const upload2 = multer({ storage: storage });
// const upload2 = multer({ dest: path.resolve(admin_user_profile_path) });
const ParentController= require("./Parent.controller");
const  DocumentsController= require("./Documents.controller");
const ServiceProviderController= require("./ServiceProvider.controller");

const Favrioutcontroller=require("./Favriout.controller");
const Usercontroller=require("./User.controller");
const clientroutes = express.Router();

//documentcontroller
clientroutes.post("/senddocument", DocumentsController.senddocument);
clientroutes.post("/ratinsscreen", ServiceProviderController.ratinsscreen);
clientroutes.post("/getdocumentsbylawyerID", DocumentsController.getdocumentsbylawyerId);
clientroutes.post("/getdocumentsbyclientID", DocumentsController.getdocumentsbyclientId);
clientroutes.get("/deletedocumentbyclient/:doc_id", DocumentsController.deletedocumentbyclient);
clientroutes.get("/deletedocumentbylawyer/:doc_id", DocumentsController.deletedocumentbylawyer);
clientroutes.get("/getdocdetails/:doc_id", DocumentsController.getdocdetails);


// servicecontroller
clientroutes.post("/saveservice",upload2.fields([
  { 
    name: 'docs', 
    maxCount: 10
  },
  { 
    name: 'catagory_thumb', 
    maxCount: 10
  },
  { 
    name: 'catagory_banner', 
    maxCount: 10
  },
  

]
), ServiceProviderController.saveservice);

clientroutes.post("/savedocs",upload2.fields([
  { 
    name: 'docs', 
    maxCount: 10
  }
  

]
), ServiceProviderController.savedocs);
clientroutes.post("/savedocs1",upload2.fields([
  { 
    name: 'docs', 
    maxCount: 10
  }
  

]
), ServiceProviderController.savedocs1);
// clientroutes.post("/saveservice", upload2.single('docs'),ServiceProviderController.saveservice);
clientroutes.post("/updatepriceofoffer",ServiceProviderController.updatepriceofservice);
clientroutes.post("/bookaprovider",ServiceProviderController.bookaprovider);
clientroutes.post("/editservice",ServiceProviderController.editservice);
// clientroutes.post("/updatepriceofservice",ServiceProviderController.updatepriceofservice)
clientroutes.post("/trypaymentagain",ServiceProviderController.trypaymentagain)
clientroutes.post("/getfailedpayments",ServiceProviderController.getfailedpayments)
clientroutes.post("/searchallservicesbyproviderwithradius", ServiceProviderController.searchallservicesbyproviderwithradius);
clientroutes.get("/getservicesbyclientID/:client_id", ServiceProviderController.getservicesbyclientID);
clientroutes.get("/getservicesbylawyerID/:lawyer_id", ServiceProviderController.getservicesbylawyerID);
clientroutes.get(
  "/getservicesbyclientIDandlawyerID/:client_id/:lawyer_id",
  ServiceProviderController.getservicesbyclientIDandlawyerID
);
clientroutes.get("/getpricebylawyerid/:lawyer_id/:type",ServiceProviderController.getpricebylawyerid);
clientroutes.get("/getcompletedservicesbyclientID/:student_id",ServiceProviderController.getcompletedservicesbyclientID);
clientroutes.get("/getallbookeduserbylawyerID/:lawyer_id",ServiceProviderController.getallbookeduserbylawyerID);
clientroutes.get("/getongoingservicesbyclientID/:student_id",ServiceProviderController.getongoingservicesbyclientID);
clientroutes.post("/getongoingservicesbyteacherID",ServiceProviderController.getongoingservicesbylawyerID);
clientroutes.post("/getcompletedservicesbyproviderid",ServiceProviderController.getcompletedservicesbylawyerID);
clientroutes.get("/getallbookedteacherbystudentid/:student_id",ServiceProviderController.getallbookedlawyerbyclientID);
clientroutes.get("/checkpromocode/:promo",ServiceProviderController.checkpromocode);
clientroutes.get("/getallcatagoriesallatonce",ServiceProviderController.getallcatagoriesallatonce);
clientroutes.post("/getallcatagorieslevelbylevel",ServiceProviderController.getallcatagorieslevelbylevel);
clientroutes.post("/cancelservice", ServiceProviderController.cancelservice);
clientroutes.post("/makeaoffer", ServiceProviderController.makeaoffer);
clientroutes.post("/getalloffersbyserviceid", ServiceProviderController.getallofersbyserviceid);
clientroutes.post("/getallservicesbyuserid", ServiceProviderController.getallservicesbyuserid);
clientroutes.get("/getbrodcastservicetoallviableproviders", ServiceProviderController.getbrodcastservicetoallviableproviders);
clientroutes.post("/updateshippingdetails",ServiceProviderController.updateshippingdetails);
clientroutes.get("/getpostedjobs/:student_id",ServiceProviderController.getpostedjobs);
clientroutes.post("/getpostedjob",ServiceProviderController.getpostedjob);
clientroutes.post("/getallappliedjobs",ServiceProviderController.getallappliedjobs);

//favriout controller
clientroutes.post("/savefavriout", Favrioutcontroller.savefavriout);
clientroutes.get("/getallfavriouts", Favrioutcontroller.getallfavriouts);
clientroutes.get("/getfavrioutbyclientID/:client_id", Favrioutcontroller.getfavrioutbyclientID);
clientroutes.get("/deletefavorite/:client_id/:provider_id", Favrioutcontroller.deletefavorite);


//profile
clientroutes.post('/updateprofile',upload2.single('image'), Usercontroller.updateprofile);
clientroutes.get('/deleteprofile/:user_id', Usercontroller.deleteprofile);
clientroutes.get('/getprofile/:user_id', Usercontroller.getprofile);
clientroutes.post('/add_relatives', Usercontroller.add_relatives);
clientroutes.get('/getuserrelatives/:user_id', Usercontroller.getuserrelatives);






//parent controller
// clientroutes.get("/getongoingappointmentsp/:parent_email",ParentController.getongoingappointments);
// clientroutes.get("/getcompletedappointmentsp/:parent_email",ParentController.getcompletedappointments);
// clientroutes.get("/transactionsp/:parent_email",ParentController.transactions);
// clientroutes.get("/blockteacherp/:parent_email/:teacher_id",ParentController.blockteacher);
// clientroutes.get("/blockstudentp/:parent_email/:student_id",ParentController.blockstudent);

// clientroutes.get("/unblockstudentp/:parent_email/:student_id",ParentController.unblockstudent);

//test
// clientroutes.get("/test",ServiceProviderController.brodcastservicetoallviableproviders)
module.exports = clientroutes;

