const Favriout = require("../../models/client/Favriout.js");
var mongoose = require('mongoose');

module.exports.savefavriout = async(req, res) => {
    const isexists=await Favriout.findOne({
        client_id: req.body.client_id,
        provider_id: req.body.provider_id,
    })
   if(isexists){
    return res.send({
        data: null,
        status: false,
        message: "Favriout existe déjà",
        errmessage: "",
    });
   }else{
    const favriout = new Favriout({
        client_id: req.body.client_id,
        provider_id: req.body.provider_id,
    });
    favriout.save().then((favriout) => {
        return res.send({
            data: favriout,
            status: true,
            message: "Favriout sauvé avec succès",
            errmessage: "",
        });
    }).catch((err) => {
        return res.send({
            status: false,
            message: "",
            error: err,
            errmessage: "Le sauvetage de Favriout a échoué"
        });
    })
   }
}
module.exports.getallfavriouts = (req, res) => {
        Favriout.find({}, (err, favriouts) => {
            if (err) {
                res.json({
                    status: false,
                    message: "",
                    errmessage: err.message,
                    data: null
                });
            } else {
                res.json({
                    status: true,
                    data: favriouts,
                    message: "",
                    errmessage: ""
                });
            }
        });
    }
module.exports.getfavrioutbyclientID = async(req, res) => {
        
        const student_id=req.params.client_id ;
        console.log("student_id",student_id)
       await Favriout.aggregate([
            {$match: { client_id :student_id } },
            
            {
                $lookup: {
                    from: "lawunits",
                    let: { provider_id: { "$toObjectId": "$provider_id" } },
                    pipeline: [{
                        $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },

                    },
                    { $addFields: { providerIDSTR: { "$toString": "$_id" } } },
                    {
                        $lookup: {
                            from: "reviews",
                            localField: "providerIDSTR",
                            foreignField: "provider_id",
                            as: "reviews"
                        }
                    },
                    {
                        $lookup: {
                            from: "lawyerservices",
                            localField: "providerIDSTR",
                            foreignField: "provider_id",
                            as: "services"
                        }
                    },
                    { $addFields: { service: { $arrayElemAt: ["$services", 0] }, provider_idOBJ: { "$toObjectId": "$provider_id" } } },
                    // {
                    //    $lookup:{
                    //     from:"reviews",
                    //     localField:"providerIDSTR",
                    //     foreignField:"provider_id",
                    //     as:"reviews"
                    //   }
                    // },
                    {
                        $addFields: {

                            avgRating: { $avg: "$reviews.rating" }
                        }
                    },

                    ],
                    as: "provider"
                }
            },
            {$addFields:{provider:{$arrayElemAt:["$provider",0]}}},
        
        ]).exec((err, favriouts) => {
            console.log(favriouts,err)
            if (err) {
                        res.json({
                            status: false,
                            message: "",
                            errmessage: err.message,
                            data: null
                        });
                    } else {
                        res.json({
                            status: true,
                            data: favriouts,
                            message: "",
                            errmessage: ""
                        });
                    }
        })
        // console.log(req.params.student_id)
        // Favriout.find({ student_id: req.params.student_id }, (err, favriouts) => {
        //     if (err) {
        //         res.json({
        //             status: false,
        //             message: "",
        //             errmessage: err.message,
        //             data: null
        //         });
        //     } else {
        //         res.json({
        //             status: true,
        //             data: favriouts,
        //             message: "",
        //             errmessage: ""
        //         });
        //     }
        // });
    }
module.exports.deletefavorite =async (req, res) => {
        await Favriout.findOneAndDelete({  provider_id: req.params.provider_id,client_id:req.params.client_id }).then(( favriout) => {
            return res.json({
                status: true,
                message: "Favriout a été supprimé avec succès",
                errmessage: "",
                data: favriout
            });
        });
    }
   