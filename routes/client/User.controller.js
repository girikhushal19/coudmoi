const User = require("../../models/auth/user");
const Userrelativesmodel=require("../../models/client/User_relatives");
const bcrypt = require("bcryptjs");
module.exports.updateprofile = async(req, res) => {
    let user_photo;
    const admin_user_profile_folder=process.env.admin_user_profile_folder;
   if(req.file){
    console.log(req.file)
    user_photo=admin_user_profile_folder+"/"+req.file.filename;
   }
    const {
        first_name,
        last_name,
        email,
        phone,
        dob,
        user_id,
        address,
        password,
        newpassword
    }=req.body;
    let encryptedPassword
    if(password){
     encryptedPasswordtest = await bcrypt.hash(password, 10);
     encryptedPassword= await bcrypt.hash(newpassword, 10);
     const user = await User.findOne({ _id:user_id });
     console.log(user)
    const ispasswordcorrect=await bcrypt.compare(password, user.password);
    if(!ispasswordcorrect){
     return res.send({
       status:false,
       message:"",
       errmessage:"old password is not correct",
       data:null
     })
    }
    }
  const data={
      ...(first_name&&{first_name}),
        ...(last_name&&{last_name}),
        ...(email&&{email}),
        ...(phone&&{phone}),
        ...(dob&&{dob}),
        ...(user_photo&&{user_photo}),
        ...(address&&{address}),
        ...(newpassword&&{password:encryptedPassword}),

  }
   await User.findByIdAndUpdate(user_id,data).then((user) => {
       res.send({
              data:user,
                status:true,
                message:"Profil mis à jour avec succès",
                errmessage:""
       })
   }).catch((err) => {
         res.send({
              status:false,
              message:"",
              errmessage:err.message,
              data:null
         })
   })
  
}
module.exports.deleteprofile = async(req, res) => {
    const user_id=req.params.user_id;
    const user=await User.findById(user_id);
    if(user){
       user.is_deleted=true;
    user.email=user.email+"_del"+Math.random()*100000;
    user.save().then((result)=>{
      return res.send({
        status:true,
        message:"deleted successfully",

      });
    })
  }else{
    return res.send({
        status:false,
        message:"user not found",

      });
  }

}
module.exports.getprofile = async(req, res) => {
    const user_id=req.params.user_id;
    await User.findById(user_id).then((user) => {
        res.send({
            user:user,
            status:true,
            message:"Profile fetched successfully"
        })
    }).catch((err) => {
        res.send({
            status:false,
            message:"Profile fetching failed",
            error:err
        })
    })
}
module.exports.add_relatives = async(req, res) => {
    const {
        user_id,
        first_name,
        address,
        title,
        dob

    }=req.body;
    const data={
        ...(user_id&&{user_id}),
        ...(first_name&&{first_name}),
        ...(address&&{address}),
        ...(title&&{title}),
        ...(dob&&{dob}),
        ...(new Date()&&{date_of_registration:new Date()})
    }
   Userrelativesmodel.find({
       first_name:data.first_name,
   },(err,result)=>{
         if(err){
              res.send({
                status:false,
                message:"",
                data:null,
                errmessage:"L'ajout de parents a échoué"
              })
         }else if(result.length>0){
              res.send({
                status:false,
                message:"La parenté existe déjà",
                errmessage:"",
                data:null
              })
         }else{
              Userrelativesmodel.create(data).then((user) => {
                res.send({
                      data:user,
                         status:true,
                         message:"Ajout réussi de parents",
                         errmessage:""
                })
              }).catch((err) => {
                  res.send({
                         status:false,
                         message:"",
                         errmessage:"L'ajout de parents a échoué",
                         data:null
                  })
              })
         }
   })
}
module.exports.getuserrelatives=async(req,res)=>{
    const user_id=req.params.user_id;
    await Userrelativesmodel.find({user_id:user_id}).then((user) => {
        res.send({
            user:user,
            status:true,
            message:"Relatives fetched successfully"
        })
    }).catch((err) => {
        res.send({
            status:false,
            message:"Relatives fetching failed",
            error:err
        })
    })
}
