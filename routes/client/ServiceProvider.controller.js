const Servicesprovided = require('../../models/client/Servicesprovided');
const Services=require("../../models/lawyer/LawyerServices");
const boosterpackage=require("../../models/admin/Boosterpackage")
const OffersModel=require("../../models/client/Offers");
const lawyermodel=require('../../models/lawyer/Lawunit');
const ContactHours=require("../../models/lawyer/ContactHours");
const subscriptionmodel = require("../../models/lawyer/Subscriptions");
const Cancledservicepaymentmodel=require("../../models/admin/cancledservicepayment");
const Catagory=require("../../models/admin/Catagory");
const FavModel=require("../../models/client/Favriout");
const notificationpermissionmodel = require("../../models/NotificationPermission");
const {sendpushnotificationtouser}=require('../../modules/Fcm');
const adminmodel = require('../../models/admin/Admin');
const { sendmail } = require("../../modules/sendmail");
const User = require("../../models/auth/user");
const CoupensModel=require("../../models/lawyer/Coupens");

const path = require("path");
const { default: mongoose } = require('mongoose');
const base_url=process.env.BASE_URL;
module.exports.saveservice = (req, res) => {
  console.log("req.body",req.body,"req.files",req.files)
  if(!Object.keys(req.body).length){
  return res.send({
    status:false,
    message:"no req.body data"
  })
  }
    const student_id=req.body.client_id;
    const student_name=req.body.client_name;
    const provider_id=req.body.provider_id;
    const title=req.body.title;
    const is_shipping_required=req.body.is_shipping_required;
    const course_id=req.body.service_id;
    const course_name=req.body.service_name;
    const location=req.body.location;
    const catagory_id=req.body.catagory_id;
    const location_cor=req.body.location_cor;
    const contact=req.body.contact;
    const duration=req.body.duration;
    const desc=req.body.desc;
    
    const catagory=req.body.catagory;
    let is_invite="";
    if(provider_id){
      is_invite=provider_id
    }else{
      is_invite=student_id
    }
   
    // const date_availed=new Date().toISOString();
    const date_availed=new Date();
    const time_availed=req.body.time_availed;
    let is_shipping_required_new;
    if(is_shipping_required=="true"){
      is_shipping_required_new=true
    }
    if(is_shipping_required=="false"){
      is_shipping_required_new=false
    }
   
    const docs=[];
    // console.log("req.file",req.file)
    // if (req.file?.fieldname) {
    //   console.log("req.files",req.files)
    //   let file=req.file;
    //   docs.push(file.filename)
    //  }
    let bannerimage,thumbimage;
    if (req.files?.catagory_thumb) {
      // console.log("req.files",req.files)
     for(let i=0;i<req.files.catagory_thumb.length;i++){
      let file=req.files.catagory_thumb[0];
      let filename="userprofile/"+file.filename
      thumbimage=filename
     }
     }
     if (req.files?.catagory_banner) {
      // console.log("req.files",req.files)
     for(let i=0;i<req.files.catagory_banner.length;i++){
      let file=req.files.catagory_banner[0];
      let filename="userprofile/"+file.filename
      bannerimage=filename
     
     }
     }
     if (req.files?.docs) {
      console.log("req.files",req.files)
     for(let i=0;i<req.files.docs.length;i++){
      let file=req.files.docs[i];
      let filename="userprofile/"+file.filename
      docs.push(filename)
     }
     }
     console.log("docs",docs)
     let newlocation_cor=[parseFloat(location_cor.split(",")[0]),parseFloat(location_cor.split(",")[1])]
    const newService = new Servicesprovided({});
    newService.client_id=student_id;
    newService.client_name=student_name;
    newService.catagory_id=catagory_id;
    newService.catagory=catagory;
    newService.service_id=course_id;
    newService.service_name=course_name;
    newService.is_shipping_required=is_shipping_required;
    newService.title=title;
    newService.date_availed=date_availed;
    
    newService.time_availed=time_availed;
    newService.docs=docs;
    newService.catagory_banner=bannerimage;
    newService.catagory_thumb=thumbimage;

    newService.is_invite_id=is_invite;
    newService.is_shipping_required=is_shipping_required_new;
    
    newService.location=location
    const location_cor_obj={type:"Point",coordinates:newlocation_cor}
    newService.location_cor=location_cor_obj
    
    newService.contact=contact
    newService.desc=desc
    newService.duration=duration

    // console.log("location_cor_obj",location_cor_obj,"newService0",newService)
    // url:base_url+"/api/payment/"+req.body.payment_method+"/"+service._id+"/"+req.body.amount
    newService.save(async(err, service) => {
    
        if (err) {
            res.json({
                status: false,
                errmessage: err.message,
                data: null,
                message: ""
            });
        } else {
          if(provider_id){
            await OffersModel.create({
              "provider_id":provider_id,
              "service_id":service._id+''
            })
          }
         
         await brodcastservicetoallviableproviders(service,provider_id).then(async(allviableproviders)=>{
          // console.log("result",result)
          const allproviderids=allviableproviders.map(e=>e._id+'');
          const allproviders=allproviderids.filter((e)=>{
            console.log("provider in create service============e._id>",e._id+'',"provider_id",provider_id)
            if(e!=provider_id){
              return e
            } 
          
          })
          console.log("allproviders",allproviders,"allproviderids",allproviderids)
          // console.log()
          // const providerids=allproviders.map(e=>{
          //   return e._id
          // });
         await Servicesprovided.findByIdAndUpdate(service._id,{messageSentTo:allproviders}).then((reulst)=>{
            return res.json({
              status: true,
              message: 'Rendez-vous pris avec succès',
              errmessage:"",
              data: service,
              allviableproviders:allviableproviders
              
          });
          })
          
         })
          
        }
    }
    );
}

module.exports.savedocs=(req,res)=>{
  let docs=[];
  if (req.files?.docs) {
    console.log("req.files",req.files)
   for(let i=0;i<req.files.docs.length;i++){
    let file=req.files.docs[i];
    let filename="userprofile/"+file.filename
    docs.push(filename)
   }
   }
   return res.send({
    status:true,
    message:"success",
    data:docs
   })
},
module.exports.bookaprovider=async(req,res)=>{
  const service_id=req.body.service_id;
  const provider_name=req.body.provider_name;
  const provider_id=req.body.provider_id;
  const payment_method=req.body.payment_method;
  const amount=req.body.amount;
  const finalamount=req.body.finalamount;
  const promocode=req.body.promocode;
  const commission=req.body.commission;
  const tax=req.body.tax;
 
  const offer=await OffersModel.findOne({service_id:service_id,provider_id:provider_id});
  offer.offer_acceptance_by_client=true;
  offer.save();
  const service=await Servicesprovided.findById(service_id);
  
  service.provider_id=provider_id;
  service.provider_name=provider_name;
  service.payment_amount=amount;
  service.finalamount=finalamount;
  service.promo=promocode;
  service.commission=commission;
  service.tax=tax;
 
  service.save().then((result)=>{
    return res.send({
      status:true,
      message:"success",
      url:base_url+"/api/payment/"+payment_method+"/"+result._id+"/"+result.finalamount
    })
  })
}
module.exports.trypaymentagain=async(req,res)=>{
  const id=req.body.service_id;
  const payment_method=req.body.payment_method;
  const service=await Servicesprovided.findById(id);
  if(service){
   if(service.payment_status){
   return res.send({
    status:false,
    message:"payment is already done",
    data:service
   })
   }else{
    if(service.finalamount){
      let url=base_url+"/api/payment/"+payment_method+"/"+service._id+"/"+service.finalamount;
      return res.send({
       status:true,
       message:"fetched successfully",
       url:url
      })
    }else{
      return res.send({
        status:true,
        message:"service data in not complete yet",
        url:null
       })
    }
   }
  }else{
   return res.send({
    status:false,
    message:"service does not exists",
    data:null
   })

  }
}
module.exports.getfailedpayments=async(req,res)=>{
  const provider_id=req.body.provider_id;
  const allfailedtransaction=await Servicesprovided.find({
    provider_id:provider_id,
    payment_status:false,
    is_rejected:false,
    payment_failed:true
  });
  return res.send({
    status:true,
    message:"fetched success",
    data:allfailedtransaction
  })
}
module.exports.searchallservicesbyproviderwithradius=async(req,res)=>{
    const catagory=req.body.catagory;
    const competence=req.body.competence;
    const latlong=req.body.latlong;
    const radius=req.body.radius;
    const provider_id=req.body.provider_id;
    const oldoffers=await OffersModel.find({provider_id:provider_id});
   
    const sub=await subscriptionmodel.findOne({provider_id:provider_id,end_date:{$gte:new Date()}});
    // _id:{$nin:serviceids},
    const query={
      service_status:false,
     
    // location_cor: {
    //     $near: {
    //      $maxDistance: radius,
    //      $geometry: {
    //       type: "Point",
    //       coordinates:[latlong?.long, 
    //         latlong?.lat]
    //      }
    //     }
    //    }
    }
  
    if(catagory){
        query["catagory"]=catagory;
    }
  console.log(query)
    if(1){
    // await Servicesprovided.find(query)
    
    // .then((services)=>{
       
    //      return res.json({
    //             status:true,
    //             message:"services fetched successfully",
    //             errmessage:"",
    //             data:services
    //         })
        
    // })


    await Servicesprovided.aggregate([
      {
        $geoNear:{
          "near":{type: "Point", coordinates:[latlong?.long,latlong?.lat]},
          
          "distanceField": "dist.calculated",
          includeLocs: "dist.location",
          spherical: true,
          "maxDistance":radius,
         
        }
      },
      {$match:query},
      {
        $lookup:{
          from:"offers",
          let:{service_id:{"$toString":"$_id"}},
          pipeline:[
            {$match:{
              $expr:{
                $and:[
                  {$eq:["$service_id","$$service_id"]},
                  {$eq:["$provider_id",provider_id]}
                ]
              }
            }}
          ],
          as:"offer"
        }
      }
    ])
    
    .then((services)=>{
      //  let newservice=services;
       let newservice=services.filter(e=>e.offer.length==0);
         return res.json({
                status:true,
                message:"services fetched successfully",
                errmessage:"",
                data:newservice
            })
        
    })
    }else{
      return res.json({
        status:true,
        message:"services fetched successfully",
        errmessage:"",
        data:[]
    })
    }
}
module.exports.getservicesbyclientID=(req,res)=>{
    const student_id=req.params.student_id;
    Servicesprovided.find({student_id:student_id,payment_status:true},(err,services)=>{
        if(err){
            res.json({
                status:false,
                errmessage:err.message,
                data:null,
                message:""
            })
        }else{
            res.json({
                status:true,
                data:services,
                errmessage:"",
                message:"services fetched successfully"
            })
        }
    })
}
module.exports.getservicesbylawyerID=(req,res)=>{
    const teacher_id=req.params.teacher_id;
    Servicesprovided.find({teacher_id:teacher_id,payment_status:true},(err,services)=>{
        if(err){
            res.json({
                status:false,
                message:"",
                errmessage:err.message,
                data:null
            })
        }else{
            res.json({
                status:true,
                data:services,
                message:"",
                errmessage:""
            })
        }
    })
}
module.exports.getservicesbyclientIDandlawyerID=(req,res)=>{
    const student_id=req.params.student_id;
    const teacher_id=req.params.teacher_id;
    Servicesprovided.find({student_id:student_id,teacher_id:teacher_id},(err,services)=>{
        if(err){
            res.json({
                status:false,
                message:"",
                errmessage:err.message,
                data:null
            })
        }else{
            res.json({
                status:true,
                data:services,
                message:"",
                errmessage:""
            })
        }
    })
}
module.exports.cancelservice=async(req,res)=>{
    const service_id=req.body.service_id;
    const serviceidOBJ=mongoose.Types.ObjectId(service_id);
    const student_id=req.body.client_id;
    const cancelation_reason=req.body.cancelation_reason;
    const cancelation_message=req.body.cancelation_message;
    let message=process.env.BOOKING_CANCELLED_BY_STUDENT;
    console.log("req.body",req.body)
    const pathtofile = path.resolve("views/notifications/offercancelbyuser.ejs");
   await Servicesprovided.findOneAndUpdate({_id:serviceidOBJ,client_id:student_id,service_status:false,cancelation_message_by_user:{$eq:""}},
      {service_acceptance_status_by_client:false,
      cancelation_reason_by_user:cancelation_reason,
      cancelation_message_by_user:cancelation_message}).then(async(service)=>{
      if(service){
        if(0){
          return res.json({
              status:false,
              message:"err.message",
              errmessage:err.message,
              data:null
          })
      }else{
        const lawyerid=service.provider_id;
        let lawyer;
        if(lawyerid){
          lawyer=await lawyermodel.findOne({_id:lawyerid});
        }
        if(service.payment_status&&service.finalamount){
          lawyer.totalpendingamount=lawyer.totalpendingamount-service.finalamount;

        }
         

          console.log("_id",lawyerid,"service",service)
          const MESSAGE_SERVICE_CANCELED_BY_CLIENT=process.env.MESSAGE_SERVICE_CANCELLED_BY_CLIENT;
          const notipermissions=await notificationpermissionmodel.find({id:service.provider_id});
          await Cancledservicepaymentmodel.create({
            client_id: service.client_id,
            client_name:service.client_name,
            provider_name:service.provider_name,
            provider_id:service.provider_id,
            amounttopay:service.finalamount,
            service_id:service._id
         })
          if(lawyer){
            if(notipermissions.length){
              if(notipermissions[0].push_offer){
                  sendpushnotificationtouser(MESSAGE_SERVICE_CANCELED_BY_CLIENT+" "+service.client_name,lawyer,service.provider_id)
              }else{
                  console.log("push notification is disabled for student")
              }if(notipermissions[0].email_offer){
                  console.log(lawyer.email)
                  sendmail(pathtofile, 
                      { client_name: service.client_name,
                        reason:cancelation_reason,
                        message:cancelation_message,
                        adminmessage:message }, 
                      "Annulation du service par le client",
                      lawyer.email);
              }else{
                  console.log("email notification is disabled for student")
              }
          }else{
              console.log(lawyer.email)
              sendpushnotificationtouser(MESSAGE_SERVICE_CANCELED_BY_CLIENT+" "+service.client_name,lawyer,service.provider_id)
              sendmail(pathtofile, 
                { client_name: service.client_name,
                  reason:cancelation_reason,
                  message:cancelation_message,
                  adminmessage:message }, 
                "Annulation du service par le client",
                lawyer.email);
          }
          }
         
         
          // if(lawyer.fcm_token){
          //     sendpushnotificationtouser(MESSAGE_SERVICE_CANCELED_BY_CLIENT+" "+service.client_name,lawyer.fcm_token,service);
          // }
          return res.json({
            status:true,
            data:service,
            message:"",
            errmessage:""
        })
              
      }
      }else{
        return res.send({
          status:false,
          message:"Impossible d'annuler le service à ce stade"
        })
      }
    })
}
module.exports.getpricebylawyerid=async(req,res)=>{
    const type=req.params.type;
    const teacher_id=req.params.teacher_id;
    const lawyer=await lawyermodel.findOne({_id:teacher_id});
    let price;
    if(type=="online"){
      price=lawyer.modesofservice.online.price;
    }else if(type=="offline"){
     price=lawyer.modesofservice.offline.price;
    }
    res.send({
        status:true,
        message:"price fetched successfully",
        data:price,
        errmessage:""
    })
}

module.exports.getcompletedservicesbyclientID=async(req,res)=>{
    const student_id=req.params.student_id;
    // var start_date = req.body.start_date;
    // var end_date = req.body.end_date;
    var d = new Date();
    d.setHours(0,0,0,0);
    console.log("client_id",student_id)
    const services=await Servicesprovided.aggregate([
        {$match:{
            client_id:student_id,
            payment_status:true,
            service_status:true,
            service_acceptance_status_by_client:true,
            service_acceptance_status_by_provider:true,
            is_service_rejected:false,
            // // date_availed:{$lt:d}
          }},
        {$addFields:{teacher_id:{"$toObjectId":"$provider_id"},stringteacherId:{"toString":"$client_id"},serviceidstr:{"$toString":"$_id"},is_shipped:{"$size":"$shipping_details"}}},
        // {$lookup:{from:"lawunits",localField:"teacher_id",foreignField:"_id",as:"provider"}},
       
        {$lookup:{from:"reviews",
        let:{service_id_str:{"$toString":"$_id"}},
        pipeline:[
          {
            $match:{ $expr: { $eq: ["$service_id", "$$service_id_str"]}},
          
          },
         {$addFields:{obj_client_id:{"$toObjectId":"$client_id"}}},
          {
            $lookup:{
              from:"users",
              localField:"obj_client_id",
              foreignField:"_id",
              as:"user"
            }
          }
    
        ],
        as:"ratings"
      }}
    ])
    return res.send({
        status:true,
        message:"services fetched successfully",
        data:services,
        errmessage:""
    })
    // await Servicesprovided.find({student_id:student_id,service_status:true}).then((service)=>{
    //     if(service){
    //         res.json({
    //             status:true,
    //             data:service,
    //             errmessage:"",
    //             message:"services fetched successfully"
    //         })
    //     }else{
    //         res.json({
    //             status:false,
    //             message:"",
    //             errmessage:"",
    //             data:null
    //         })
    //     }
    // })
}

module.exports.getongoingservicesbyclientID=async(req,res)=>{
    const student_id=req.params.student_id;
  
    var d = new Date();
            d.setHours(0,0,0,0);
            console.log("id",student_id,"d",d);
    const services=await Servicesprovided.aggregate([
        {$match:{
            client_id:student_id,
            payment_status:true,
            service_status:false,
            is_service_rejected:false,
            service_acceptance_status_by_client:true,
            service_acceptance_status_by_provider:true,
            // date_availed:{$gte:d}
          }},
       
          {$addFields:{provider_id:{"$toObjectId":"$provider_id"},service_idSTR:{"$toString":"$_id"},is_shipped:{"$size":"$shipping_details"}}},
          // {$lookup:{from:"lawunits",localField:"provider_id",foreignField:"_id",as:"provider"}},
          // {$lookup:{
          //   from:"offers",
          //   let:{provider_id:{"$toObjectId":"$provider_id"},service_id:"$service_idSTR"},
          //   pipeline:[{
          //     $match: { $expr: { $eq: ["$service_id", "$$service_id"] } },
             
          //   },
         
          //   {$lookup:{
          //     from:"lawunits",
          //     let:{provider_id:{"$toObjectId":"$provider_id"}},
          //     pipeline:[{
          //       $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
               
          //     },
          //     {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
          //     {
          //        $lookup:{
          //         from:"reviews",
          //         localField:"providerIDSTR",
          //         foreignField:"provider_id",
          //         as:"reviews"
          //       }
          //     },
          //     { $addFields: {
                                          
          //       avgRating:{
          //         $round:[ {$avg: "$reviews.rating"},0]
          //       }
          //   }},
          
          //   ],
          //     as:"provider"
          //   }},
          // ],
          //   as:"offers"
          // }},
          // {$addFields:{acceptedoffers:{
          //   $function:{
          //     body:`function(offers,service_idSTR){
          //       const acceptedoffers=[];
          //       for(let i=0;i<offers.length;i++){
          //         let offer=offers[i];
          //         if(offer.offer_acceptance_by_client && offer.service_id==service_idSTR){
          //           acceptedoffers.push(offer);
          //         }
          //       }
          //       return acceptedoffers;
          //     }`,
          //     args: [
          //       "$offers",
          //       "$service_idSTR"
          //      ],
          //      lang: "js",
          //   }

            
          // }}},
          // { "$group": {
          //   "_id": "$catagory",
          //   "name": { "$first": "$catagory" },  //$first accumulator
          //   "count": { "$sum": 1 },  //$sum accumulator
          //   items: {$push: '$$ROOT'}
          // }},
         
         
       
    ])
   return res.send({
        status:true,
        message:"services fetched successfully",
        data:services,
        errmessage:""
    })

}


module.exports.getcompletedservicesbylawyerID=async(req,res)=>{
    const teacher_id=req.body.provider_id;
    const doc_name=req.body.doc_name;
  const  client_name=req.body.client_name;
  var start_date = req.body.start_date;
            var end_date = req.body.end_date;
            var d = new Date();
            d.setHours(0,0,0,0);
  let matchdata={
   
   
    provider_id:teacher_id,
    payment_status:true,
    
service_acceptance_status_by_client:true,
service_acceptance_status_by_provider:true,
    is_service_rejected:false,
    service_status:true
  
    
  }
  let student_ids_having_this_name;
  if(client_name){
      matchdata["client_name"]={$regex:client_name,$options:"i"}
    // student_ids_having_this_name=await User.find(
    // {$or:[{first_name:{$regex:client_name,$options:'i'}},
    // {last_name:{$regex:client_name,$options:'i'}},
    // {$and:[{first_name:{$regex:client_name.split(" ")[0],$options:'i'}},{last_name:{$regex:client_name.split(" ")[1],$options:'i'}}]}]});
    // matchdata['student_id']={$in:student_ids_having_this_name.map(client=>client._id.toHexString())};
  }
  if(doc_name){
    matchdata['doc_name']={$regex:doc_name,$options:'i'};
  }
  if (start_date != '' || end_date != '') {
    if (start_date == '') {
        start_date = new Date(end_date);
        start_date = start_date - 1;
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
    } else if (end_date == '') {
        end_date = new Date(start_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        start_date = new Date(start_date);
        start_date = start_date - 1;
        start_date = new Date(start_date);
        console.log(start_date, end_date);
        matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
    } else {
        start_date = new Date(start_date);
        start_date = start_date.setHours(0, 0, 0, 0);
        start_date = new Date(start_date);
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['date_availed'] ={$gte: start_date, $lt: end_date};
    }
}
  console.log(matchdata)
    const services=await Servicesprovided.aggregate([
        {$match:matchdata},
        {$addFields:{client_id:{"$toObjectId":"$client_id"},serviceidstr:{"$toString":"$_id"}}},
        {$lookup:{from:"users",localField:"client_id",foreignField:"_id",as:"user"}},

        {$lookup:{from:"reviews",localField:"serviceidstr",foreignField:"service_id",as:"review"}},
        {$addFields:{
          reviewOBJ:{$arrayElemAt:["$review",0]}
        }},
        {$addFields:{
          review:"$reviewOBJ.review",
          rating:"$reviewOBJ.rating",
        }},
    ])
    return res.send({
        status:true,
        message:"services fetched successfully",
        data:services,
        errmessage:""
    })
  
}

module.exports.getongoingservicesbylawyerID=async(req,res)=>{
    const teacher_id=req.body.provider_id;
    const doc_name=req.body.doc_name;
  const  client_name=req.body.client_name;
  var start_date = req.body.start_date;
            var end_date = req.body.end_date;
            var d = new Date();
            d.setHours(0,0,0,0);
  let matchdata={
   
   
    provider_id:teacher_id,
    payment_status:true,
    service_status:false,
    is_service_rejected:false,
    service_acceptance_status_by_client:true,
    service_acceptance_status_by_provider:true,
   
  
    
  }
  let student_ids_having_this_name;
  if(client_name){
      matchdata["student_name"]={$regex:client_name,$options:"i"}
    // student_ids_having_this_name=await User.find(
    // {$or:[{first_name:{$regex:client_name,$options:'i'}},
    // {last_name:{$regex:client_name,$options:'i'}},
    // {$and:[{first_name:{$regex:client_name.split(" ")[0],$options:'i'}},{last_name:{$regex:client_name.split(" ")[1],$options:'i'}}]}]});
    // matchdata['student_id']={$in:student_ids_having_this_name.map(client=>client._id.toHexString())};
  }
  if(doc_name){
    matchdata['doc_name']={$regex:doc_name,$options:'i'};
  }
  if (start_date != '' || end_date != '') {
    if (start_date == '') {
        start_date = new Date(end_date);
        start_date = start_date - 1;
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
    } else if (end_date == '') {
        end_date = new Date(start_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        start_date = new Date(start_date);
        start_date = start_date - 1;
        start_date = new Date(start_date);
        console.log(start_date, end_date);
        matchdata['date_availed'] = {$gte: start_date, $lt: end_date};
    } else {
        start_date = new Date(start_date);
        start_date = start_date.setHours(0, 0, 0, 0);
        start_date = new Date(start_date);
        end_date = new Date(end_date);
        end_date = end_date.setHours(11, 59, 59, 999);
        end_date = new Date(end_date);
        matchdata['date_availed'] ={$gte: start_date, $lt: end_date};
    }
}
  console.log(matchdata)
    const services=await Servicesprovided.aggregate([
        {$match:matchdata},
       {$addFields:{client_id:{"$toObjectId":"$client_id"}}},
        {$lookup:{from:"users",localField:"client_id",foreignField:"_id",as:"user"}},
        
    ])
    res.send({
        status:true,
        message:"services fetched successfully",
        data:services,
        errmessage:""
    })
//     const teacher_id=req.params.teacher_id;
//     console.log(teacher_id,"teacher_id")
//     await Servicesprovided.find({teacher_id:teacher_id,service_status:false}).then(service=>{
//         if(service){
//             res.json({
//                 status:true,
//                 data:service,
//                 errmessage:"",
//                 message:"services fetched successfully"
//             })
       
//     }else{
//         res.json({
//             status:false,
//             message:"",
//             errmessage:"",
//             data:null
//         })
//     }

// })
}

module.exports.getallbookeduserbylawyerID=async(req,res)=>{
    const teacher_id=req.params.teacher_id;
    const serviceprovider=await Servicesprovided.aggregate([
        {$match:{teacher_id:teacher_id, is_service_rejected:false}},
      
        {$project:{student_id:{"$toObjectId":"$student_id"},_id:0,teacher_id:1}},
        {$group:{_id:"$student_id",count:{$sum:1}}},
        {$project:{student_id:{"$toObjectId":"$_id"}}},
        {$lookup:{from:"users",localField:"student_id",foreignField:"_id",as:"user"}},
       
        

    ])
    
    res.send({
        status:true,
        message:"users fetched successfully",
        data:serviceprovider,
        errmessage:""
    })
}

module.exports.getallbookedlawyerbyclientID=async(req,res)=>{
    const student_id=req.params.student_id;
    const serviceprovider=await Servicesprovided.aggregate([
        {$match:{student_id:student_id, is_service_rejected:false}},
      

        {$project:{teacher_id:{"$toObjectId":"$teacher_id"}}},
        {$group:{_id:"$teacher_id",count:{$sum:1}}},
        {$project:{student_id:{"$toObjectId":"$_id"}}},
        {$lookup:{from:"lawunits",localField:"student_id",foreignField:"_id",as:"lawyer"}},
       
        // {$project:{teacher_id:{"$toObjectId":"$teacher_id"}}},
        // {$lookup:{from:"lawunits",localField:"teacher_id",foreignField:"_id",as:"lawyer"}},

    ])
    res.send({
        status:true,
        message:"users fetched successfully",
        data:serviceprovider,
        errmessage:""
    })
}
module.exports.checkpromocode=async(req,res)=>{
    const promo=req.params.promo;
    
    const cdate=new Date();
    cdate.setHours(0,0,0,0);
    const cdatestr=cdate.toISOString()
    console.log("cdate",cdate,"promo",promo,cdatestr)
   
    const ispromovalid=await CoupensModel.aggregate([
       { $match:{
            code:promo,
        status:true,
        // start_date:{$gte:cdate},
        // end_date:{$lte:cdate}

        }},
        {$addFields:{"islimitexahusted":{$subtract:["$limite","$used"]}}},
        {$match:{"islimitexahusted":{$gt:0}}}

    ])
    console.log(ispromovalid)
    if(ispromovalid.length>0){
        return res.send({
            status:true,
            message:"Code appliqué avec succès",
            data:ispromovalid
        })
    }else{
        return res.send({
            status:false,
            errmessage:"Code Coupen non valide ",
            data:null
        })
    }
   
}
module.exports.getallcatagorieslevelbylevel=async(req,res)=>{
    const id=req.body.id;
    const query={}
    if(id){
        query['mastercatagory']=mongoose.Types.ObjectId(id);
    }
    console.log("query",query);
    const allcatagory=await Catagory.aggregate([
        {$match:query}
    ])
    return res.send({
        status:true,
        message:"catagory fetched successfully",
        data:allcatagory
    })
}
module.exports.getallcatagoriesallatonce=async(req,res)=>{
    const allcatagory=await Catagory.aggregate([
        
        {$match:{mastercatagory:null}},
        {
          $graphLookup:{
            "from": "catagories",
            "startWith": "$_id",
            "connectFromField": "_id",
            "connectToField": "mastercatagory",
            "as": "subs",
            "maxDepth": 20,
            "depthField": "level",
            
          },
         
        },
        { "$unwind": {
          "path": "$subs",
          "preserveNullAndEmptyArrays": true
      } },
     
        {
          $sort: {
            "subs.level": -1
          }
        },
        {
          $group: {
            _id: "$_id",
            is_parent:{
                $first:"$is_parent"
            },
            parent_id: {
              $first: "$mastercatagory"
            },
            bannerimage: {
              $first: "$bannerimage"
            },
            thumbimage: {
              $first: "$thumbimage"
            },
            title: {
              $first: "$title"
            },
            subs: {
              $push: "$subs"
            }
          }
        },
        {
          $addFields: {
            subs: {
              $reduce: {
                input: "$subs",
                initialValue: {
                  currentLevel: -1,
                  currentLevelChildren: [],
                  previousLevelChildren: []
                },
                in: {
                  $let: {
                    vars: {
                      prev: {
                        $cond: [
                          {
                            $eq: [
                              "$$value.currentLevel",
                              "$$this.level"
                            ]
                          },
                          "$$value.previousLevelChildren",
                          "$$value.currentLevelChildren"
                        ]
                      },
                      current: {
                        $cond: [
                          {
                            $eq: [
                              "$$value.currentLevel",
                              "$$this.level"
                            ]
                          },
                          "$$value.currentLevelChildren",
                          []
                        ]
                      }
                    },
                    in: {
                      currentLevel: "$$this.level",
                      previousLevelChildren: "$$prev",
                      currentLevelChildren: {
                        $concatArrays: [
                          "$$current",
                          [
                            {
                              $mergeObjects: [
                                "$$this",
                                {
                                  subs: {
                                    $filter: {
                                      input: "$$prev",
                                      as: "e",
                                      cond: {
                                        $and:[
                                          {
                                          $eq: [
                                          "$$e.mastercatagory",
                                          "$$this._id"
                                        ]
                                          },
                                        
                                      ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          ]
                        ]
                      }
                    }
                  }
                }
              }
            }
          }
        },
        {
          $addFields: {
            subs: "$subs.currentLevelChildren"
          }
        },
        {
          $match: {
            mastercatagory: null
          }
        }
      ])
    return res.send({
        status:true,
        message:"catagory fetched successfully",
        data:allcatagory
    })
}
async function brodcastservicetoallviableproviders(service,provider_id){
  // console.log("service in broadcast",service);
  let radius=80000000000000000000000000;
  // let service={
  // "_id":"63771ae60c2b92de174c2230",
  // "service_status":false,
  // "payment_status":false,
  // "catagory_id":"6374973879eaf683ed06c4b0",
  // "location_cor":{"type":"Point","coordinates":[-87.1032333,45.80508939999999]},
  // "docs":[],
  // "service_acceptance_status_by_provider":false,
  // "service_acceptance_status_by_client":true,
  // "is_service_rejected":false,
  // "client_id":"6353a7df9aeff1dbc25ece32",
  // "client_name":"\"est",
  // "provider_id":"63579aba43f698e04556868f",
  // "provider_name":"rohitabc",
  // "service_id":"6329c48dfa0b79d8e8826198",
  // "service_name":"relativityvdf",
  // "date_availed":{"$date":"2022-10-31T22:00:00.000Z"},
  // "time_availed":"07H00",
  // "noofproviders":3,
  // "location":"jaipur",
  // "contact":"3625143652",
  // "desc":"omething about job",
  // "duration":"1H00"
  // ,"__v":0}
  
  //matching of schedule
  // const dayofweek=new Date(service.date_availed).getDay();
  // const matchedcontacthours=await ContactHours.find({dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")});
  // const matchedcontacthoursids=matchedcontacthours.map(hour=>hour.provider_id);
  
  //getting viable providers
  const allproviders=await Services.find({"catagory_id":{$elemMatch:{"catagory_id":service.catagory_id}}});
  // const allproviders=await lawyermodel.find({})
  const uniqueProvidersSet=new Set();
  for(let i=0;i<allproviders.length;i++){
    let providerid=allproviders[i].provider_id;
    uniqueProvidersSet.add(providerid);
  }
  const uniqueProvidersArr = [...uniqueProvidersSet];
  
  //merging both and selecting common among two arrays
  // const finalproviderids=uniqueProvidersArr.filter(c=>matchedcontacthoursids.some(s=>s==c));
  const finalproviderids=uniqueProvidersArr;
  const finalproviderOBJIDS=finalproviderids.map(e=>mongoose.Types.ObjectId(e))
  // console.log("{dayNumber:dayofweek,time_availible:",{dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")})
  // console.log("matchedcontacthoursids",matchedcontacthoursids);
  // console.log("uniqueProvidersSet",uniqueProvidersSet);
  // console.log("uniqueProvidersArr",uniqueProvidersArr);
  // console.log("finalproviderids",finalproviderids);
  // console.log("finalproviderOBJIDS",finalproviderOBJIDS);
  const query={_id:{"$in":finalproviderOBJIDS},
  location_cor: {
      $near: {
       $maxDistance: radius,
       $geometry: {
        type: "Point",
        coordinates:[service.location_cor.coordinates[0], 
        service.location_cor.coordinates[1]]
       }
      }
     }
  }
  const distancquery= {
    $geoNear: {
       near: { type: "Point",  coordinates:[service.location_cor.coordinates[0], 
       service.location_cor.coordinates[1]] },
       distanceField: "dist.calculated",
       maxDistance: radius,
    
       includeLocs: "dist.location_cor",
       spherical: true
    }
  }
  // console.log("query",distancquery)
  const allviableproviders=await lawyermodel.find(query);
  // const allviableproviders=await lawyermodel.find({});
 
  // console.log("allviableproviders",allviableproviders);
  // console.log("its all right")
  const allviableprovidersids=allviableproviders.map(e=>e._id);
  let alreadyinvited;
  if(provider_id){
   
    alreadyinvited=await lawyermodel.aggregate([
      
      {$match:{_id:mongoose.Types.ObjectId(provider_id)}},
    
      
    ]);
    // console.log("alreadyinvited",alreadyinvited)
    sendpushnotificationtouser(service.client_name+"attend le devis. Veuillez vérifier la demande.",alreadyinvited[0],service.provider_id,"provider","broadcast")
  }
  const allviableproviderswithreviews=await lawyermodel.aggregate([
    distancquery,
    {$match:{_id:{$in:allviableprovidersids},is_deleted:false,is_active:true}},
  
    {$addFields:{providerSTRId:{"$toString":"$_id"}}},
   
    {$lookup:{
      from:"servicesprovideds",
      let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
      pipeline:[
        {
          $match:{
            $expr:{
              $eq:[
                "$provider_id","$$provider_id_str"
              ]
            },
            service_status:true
          }
        }
      ],
      as:"servicesprovided"
    }},
    {$lookup:{
      from:"lawyerservices",
      let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
      pipeline:[
        {
          $match:{
            "catagory_id":{
              $elemMatch:{
                catagory_id:service.catagory_id
              }
            }
          }
        }
      ],
      as:"services"
    }},
    {$lookup:{
      from:"reviews",
      // localField:"providerSTRId",
      // foreignField:"provider_id",
      let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
      pipeline:[
        {
            $match: { $expr: { $eq: ["$provider_id", "$$provider_id_str"] }},
          
        },
        {
            $addFields: {
                
                obj_client_id: { "$toObjectId": "$client_id" }
            }
        },
        {
        $lookup: {
            from: "users",
            localField: "obj_client_id",
            foreignField: "_id",
            as: "user"
            
        }
    },
      ],
      as:"ratings"
    }},
    { $addFields: {
                                
        avgRating:{$avg: "$ratings.rating"}
    }}

  ])
  // console.log("allviableprovidersids",allviableprovidersids,"allviableproviderswithreviews",allviableproviderswithreviews)
let allproviderswithoutinvitedone=allviableproviderswithreviews;
if(alreadyinvited?.length){
  allproviderswithoutinvitedone=allviableproviderswithreviews.filter(e=>e._id+''!=alreadyinvited[0]["_id"]+'');;
  // console.log("allproviderswithoutinvitedone",allproviderswithoutinvitedone)
  // console.log("allviableproviderswithreviews",allviableproviderswithreviews)
  const alreadyinvitedinallviableproviderswithreviews=allviableproviderswithreviews.find(e=>e._id==alreadyinvited[0]["_id"]);
  if(!alreadyinvitedinallviableproviderswithreviews){
    allviableproviderswithreviews.concat(alreadyinvited)
  }
}


const MESSAGE_JOB_CREATED=process.env.MESSAGE_JOB_CREATED;
// allproviderswithoutinvitedone=allviableproviderswithreviews
for(let j=0;j<allproviderswithoutinvitedone.length;j++){
  let provider=allproviderswithoutinvitedone[j];
  sendpushnotificationtouser(MESSAGE_JOB_CREATED+" "+service.client_name,provider,service.provider_id,"provider","broadcast")
}
const newallviableproviderswithreviews=allviableproviderswithreviews?.filter(e=>e.services.length!=0)
return newallviableproviderswithreviews;
}

module.exports.getbrodcastservicetoallviableproviders=async(req,res)=>{
  let radius=80000;
  let service={
  "_id":"63771ae60c2b92de174c2230",
  "service_status":false,
  "payment_status":false,
  "catagory_id":"6381c56f1d843a17b0de501a",
  "location_cor":{"type":"Point","coordinates":[-87.1032333,45.80508939999999]},
  "docs":[],
  "service_acceptance_status_by_provider":false,
  "service_acceptance_status_by_client":true,
  "is_service_rejected":false,
  "client_id":"6353a7df9aeff1dbc25ece32",
  "client_name":"\"est",
  "provider_id":"63579aba43f698e04556868f",
  "provider_name":"rohitabc",
  "service_id":"6329c48dfa0b79d8e8826198",
  "service_name":"relativityvdf",
  "date_availed":"2022-11-30T22:00:00.000Z",
  "time_availed":"10:00",
  "noofproviders":3,
  "location":"jaipur",
  "contact":"3625143652",
  "desc":"omething about job",
  "duration":"1H00"
  ,"__v":0}
  const dayofweek=new Date(service.date_availed).getDay();
  //matching of schedule
  const matchedcontacthours=await ContactHours.find({dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")});
  const matchedcontacthoursids=matchedcontacthours.map(hour=>hour.provider_id);
  
  //getting viable providers
  const allproviders=await Services.find({"catagory_id":service.catagory_id});
  const uniqueProvidersSet=new Set();
  for(let i=0;i<allproviders.length;i++){
    let providerid=allproviders[i].provider_id;
    uniqueProvidersSet.add(providerid);
  }
  const uniqueProvidersArr = [...uniqueProvidersSet];
  
  //merging both and selecting common among two arrays
  const finalproviderids=uniqueProvidersArr.filter(c=>matchedcontacthoursids.some(s=>s==c));
  const finalproviderOBJIDS=finalproviderids.map(e=>mongoose.Types.ObjectId(e))
  console.log("{dayNumber:dayofweek,time_availible:",{dayNumber:dayofweek,time_availible:new RegExp(service.time_availed,"i")})
  console.log("matchedcontacthoursids",matchedcontacthoursids);
  // console.log("uniqueProvidersSet",uniqueProvidersSet);
  console.log("uniqueProvidersArr",uniqueProvidersArr);
  console.log("finalproviderids",finalproviderids);
  console.log("finalproviderOBJIDS",finalproviderOBJIDS);
  const query={_id:{"$in":finalproviderids},
  location_cor: {
      $near: {
       $maxDistance: radius,
       $geometry: {
        type: "Point",
        coordinates:[service.location_cor.coordinates[0], 
        service.location_cor.coordinates[1]]
       }
      }
     }
  }
  const allviableproviders=await lawyermodel.find(query);
  const allviableprovidersids=allviableproviders.map(e=>e._id);
  const allviableproviderswithreviews=await lawyermodel.aggregate([
    {$match:{_id:{$in:allviableprovidersids}}},
    {$addFields:{providerSTRId:{"$toString":"$_id"}}},
    {$lookup:{
      from:"servicesprovideds",
      let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
      pipeline:[
        {
          $match:{
            $expr:{
              $eq:[
                "$provider_id","$$provider_id_str"
              ]
            },
            service_status:true
          }
        }
      ],
      as:"services"
    }},
    {$lookup:{
      from:"reviews",
      // localField:"providerSTRId",
      // foreignField:"provider_id",
      let: { provider_id: "$providerSTRId",provider_id_str:{ "$toString": "$providerSTRId" } },
      pipeline:[
        {
            $match: { $expr: { $eq: ["$provider_id", "$$provider_id_str"] }},
          
        },
        {
            $addFields: {
                
                obj_client_id: { "$toObjectId": "$client_id" }
            }
        },
        {
        $lookup: {
            from: "users",
            localField: "obj_client_id",
            foreignField: "_id",
            as: "user"
            
        }
    },
      ],
      as:"ratings"
    }},
    { $addFields: {
                                
        avgRating:{$avg: "$ratings.rating"}
    }},
 
  ])
  // console.log("allviableprovidersids",allviableprovidersids,"allviableproviderswithreviews",allviableproviderswithreviews)
  const MESSAGE_JOB_CREATED=process.env.MESSAGE_JOB_CREATED;
  for(let j=0;j<allviableproviders.length;j++){
    let provider=allviableproviders[j];
    sendpushnotificationtouser(MESSAGE_JOB_CREATED+" "+service.client_name,provider,service.provider_id,"provider","broadcast")
  }

return allviableproviderswithreviews;
}
module.exports.makeaoffer=async(req,res)=>{
  const service_id=req.body.service_id;
  const provider_id=req.body.provider_id;
  const price=req.body.price;
  const offer=await OffersModel.findOne({
    provider_id:provider_id,
    service_id:service_id,
    
  });
 

  if(offer){
    return res.send({
      status:false,
      message:"offer alreay exists"
    })
  }
  else{
    const newDate=new Date();
    const newoffer=new OffersModel();
    newoffer.service_id=service_id;
    newoffer.provider_id=provider_id;
    newoffer.offer_date=newDate;
    newoffer.price=price;
    newoffer.offer_time=newDate.getHours()+":"+newDate.getMinutes()+":"+newDate.getSeconds()+":"+newDate.getMilliseconds();
    newoffer.save().then(async(result)=>{
      const service=await Servicesprovided.findById(service_id);
      let newmessagetosent=service.messageSentTo.filter(e=>e+''!=provider_id);
      console.log("service.messageSentTo",service.messageSentTo,"newmessagetosent",newmessagetosent)
      service.messageSentTo=newmessagetosent;
      service.save((result)=>{
        return res.send({
          status:true,
          message:"offer made successfully",
          data:null
        })
      })
      
    })
  }
 
 
}

module.exports.getallofersbyserviceid=async(req,res)=>{
  const service_id=req.body.service_id;
  const offers=await OffersModel.aggregate([
    {$match:{service_id:service_id}},
    {$lookup:{
      from:"lawunits",
      let:{provider_id:{"$toObjectId":"$provider_id"}},
      pipeline:[{
        $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
       
      },
      {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
      {
         $lookup:{
          from:"reviews",
          localField:"providerIDSTR",
          foreignField:"provider_id",
          as:"reviews"
        }
      },
      { $addFields: {
                                  
        avgRating:{$avg: "$reviews.rating"}
    }},
    ],
      as:"provider"
    }},
    {$addFields:{
      serviceIDOBJ:{"$toObjectId":"$service_id"}
    }},
    {
      $lookup:{
       from:"servicesprovideds",
       localField:"serviceIDOBJ",
       foreignField:"_id",
       as:"service"
     }
   },
   {"$unwind": {
    "path": "$service",
    "preserveNullAndEmptyArrays": true
}
},
    {$addFields:{

      clientIDOBJ:{"$toObjectId":"$service.client_id"}
    }},
   {
    $lookup:{
     from:"users",
     localField:"clientIDOBJ",
     foreignField:"_id",
     as:"client"
   }
 },
  ]);
  return res.send({
    status:false,
    data:offers,
    message:"fetched successfully"
  })
}

module.exports.getallservicesbyuserid=async(req,res)=>{
  const client_id=req.body.client_id;
  const services=await Servicesprovided.aggregate([
    {$match:{client_id:client_id}},
    {$lookup:{
      from:"offers",
      let:{provider_id:{"$toObjectId":"$provider_id"}},
      pipeline:[{
        $match: { $expr: { $eq: ["$provider_id", "$provider_id"] } },
       
      },
      
      {$lookup:{
        from:"lawunits",
        let:{provider_id:{"$toObjectId":"$provider_id"}},
        pipeline:[{
          $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
         
        },
        {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
        {
           $lookup:{
            from:"reviews",
            localField:"providerIDSTR",
            foreignField:"provider_id",
            as:"reviews"
          }
        },
        { $addFields: {
                                    
          avgRating:{$avg: "$reviews.rating"}
      }},
      ],
        as:"provider"
      }},
    ],
      as:"offers"
    }},
  ])
 
  return res.send({
    status:false,
    data:services,
    message:"fetched successfully"
  })
}
module.exports.getallappliedjobs=async(req,res)=>{
  const provider_id=req.body.provider_id;
  const services=await OffersModel.aggregate([
    {$match:{provider_id:provider_id}},
    {$lookup:{
      from:"servicesprovideds",
      let:{provider_id:{
        "$convert": {
        "input": "$provider_id",
        "to": "objectId",
        "onError": "",
        "onNull": ""
      }},provider_idstr:"$provider_id",service_idObj:{
        "$convert": {
        "input": "$service_id",
        "to": "objectId",
        "onError": "",
        "onNull": ""
      }}},
      pipeline:[{
        $match: { $expr: {$and:[
          { $eq: ["$_id", "$$service_idObj"] },
          { $eq: ["$payment_status", false] }
        ]

        } },
       
      },
      
      // {$lookup:{
      //   from:"lawunits",
      //   let:{provider_id:{
      //     "$convert": {
      //     "input": "$provider_id",
      //     "to": "objectId",
      //     "onError": "",
      //     "onNull": ""
      //   }}},
      //   pipeline:[{
      //     $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
         
      //   },
      //   {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
      //   {
      //      $lookup:{
      //       from:"reviews",
      //       localField:"providerIDSTR",
      //       foreignField:"provider_id",
      //       as:"reviews"
      //     }
      //   },
      //   { $addFields: {
                                    
      //     avgRating:{$avg: "$reviews.rating"}
      // }},
      // ],
      //   as:"provider"
      // }},

      {$lookup:{
        from:"users",
        let:{clientIDOBJ:{
          "$convert": {
          "input": "$client_id",
          "to": "objectId",
          "onError": "",
          "onNull": ""
        }}},
        pipeline:[{
          $match: { $expr: { $eq: ["$_id", "$$clientIDOBJ"] } },
         
        },
       
      ],
        as:"users"
      }},
      {$addFields:{
        user:{$arrayElemAt:["$users",0]},
       
      }},
    ],
      as:"jobs"
    }},
    
    // {$unwind:"$jobs"},
    {$addFields:{
      job:{$arrayElemAt:["$jobs",0]},
     
    }},
   
    {
      $addFields:{
        
        client_name:"$job.client_name",
        desc:"$job.desc",
        location:"$job.location",
        date_availed:"$job.date_availed",
      }
    }
  ])
  
 const newservice=services?.filter(e=>e.jobs?.length!=0);
  return res.send({
    status:true,
    data:newservice,
    message:"fetched successfully"
  })
}
module.exports.editservice=async(req,res)=>{
  
  const service_id=req.body.service_id;
    const title=req.body.title;
    const contact=req.body.contact;
    const docs=req.body.docsinstring.split(",");
    const desc=req.body.desc;
   
    // const student_id=req.body.student_id;
    // const client_name=req.body.client_name;
    // const teacher_id=req.body.teacher_id;
    // const lawyer_name=req.body.lawyer_name;
    console.log("req.body.date_availed",req.body)
    // const date_availed=new Date(req.body.date_availed).toISOString();
    // const time_availed=req.body.time_availed;
    const location=req.body.location;
    const location_cor=req.body.location_cor;
    
   
    let newlocation_cor=[parseFloat(location_cor.split(",")[0]),parseFloat(location_cor.split(",")[1])]
    const newService =await Servicesprovided.findById(service_id);
    if(!newService){
      return res.send({
        status:false,
        message:"no service found with this id"
      })
    }
    
    newService.title=title;
   
    newService.docs=docs;
   
   
    newService.location=location
    const location_cor_obj={type:"Point",coordinates:newlocation_cor}
    newService.location_cor=location_cor_obj
   
    newService.contact=contact
    newService.desc=desc
   

    console.log("location_cor_obj",location_cor_obj,"newService0",newService)
    // url:base_url+"/api/payment/"+req.body.payment_method+"/"+service._id+"/"+req.body.amount
    newService.save(async(err, service) => {
        if (err) {
            res.json({
                status: false,
                errmessage: err.message,
                data: null,
                message: ""
            });
        } else {
          return res.json({
            status: true,
            message: 'Rendez-vous pris avec succès',
            errmessage:"",
            data: service
            
            
        });
          
        }
    }
    );
}
module.exports.updateshippingdetails=async(req,res)=>{
  const service_id=mongoose.Types.ObjectId(req.body.service_id);
  const provider_id=req.body.provider_id;
  const shipping_details=req.body.shipping_details;
  const service=await Servicesprovided.findOne({_id:service_id,provider_id:provider_id});
  if(!service){
    return res.send({
      status:false,
      message:"service does not exists"
    })
  }else if(!service.is_shipping_required){
    return res.send({
      status:false,
      message:"shipping do not required on this service"
    })
  }
 
 service.shipping_details=shipping_details;
 service.save().then(async(result)=>{
  const lawyer=await User.findById(service.client_id);
  const MESSAGE_SHIPPING_UPDATED="Les détails d'expédition ont été mis à jour par le couturier.";
  sendpushnotificationtouser(MESSAGE_SHIPPING_UPDATED,lawyer,service.client_id,"user","shipping")  
  return res.send({
    status:true,
    message:"shipping details updated successfully"
  })
 })
}
module.exports.getpostedjobs=async(req,res)=>{
  const student_id=req.params.student_id;
  
    var d = new Date();
            d.setHours(0,0,0,0);
            console.log("id",student_id,"d",d);
    const services=await Servicesprovided.aggregate([
        {$match:{
            client_id:student_id,
            payment_status:false,
            service_status:false,
            is_service_rejected:false,
            service_acceptance_status_by_client:false,
            service_acceptance_status_by_provider:false,
            cancelation_message_by_user:{$eq:""}
            // date_availed:{$gte:d}
          }},
          
          {$addFields:{service_idSTR:{"$toString":"$_id"},catagory_id_OBJ:{"$toObjectId":"$catagory_id"}}},
          {
            $lookup:{
              from:"catagories",
              localField:"catagory_id_OBJ",
              foreignField:"_id",
              as:"catagorys"
            }
           },
           
           {$addFields:{
        
            catagory:{$arrayElemAt:["$catagorys",0]},
           
          }},
          {$addFields:{
            
            mastercatagory1:"$catagory.mastercatagory",
           
          }},
          {
            $lookup:{
              from:"catagories",
              localField:"mastercatagory1",
              foreignField:"_id",
              as:"pcatagory"
            }
           },
           {$addFields:{
        
            pcatagoryObj:{$arrayElemAt:["$pcatagory",0]},
           
          }}
        //  {
        //   $lookup:{
        //     from:"lawunits",
        //     let:{messageSentTo:"$messageSentTo"},
        //     pipeline:[
        //       {
        //         $unwind: '$$messageSentTo',
        //       },
        //       {$match:{
        //         // $expr:{
        //         //   _id:{$in:"$$messageSentTo"}
        //         // },
        //         // "messageSentTo":{
        //         //   $elemMatch:{
        //         //     "provider_id":"$_id"
        //         //   }
        //         // }
        //         $expr: {
        //         $eq: ['$$messageSentTo.provider_id', '_id']
                
                
                
        //         }
              
              
        //       }}
        //       ],
        //     as:"messegesentto"
        //   }
        //  },
          // { "$group": {
          //   "_id": "$catagory",
          //   "name": { "$first": "$catagory" },  //$first accumulator
          //   "count": { "$sum": 1 },  //$sum accumulator
          //   items: {$push: '$$ROOT'}
          // }},
         
         
       
    ])
   return res.send({
        status:true,
        message:"services fetched successfully",
        data:services,
        errmessage:""
    })
}
module.exports.updatepriceofservice=async(req,res)=>{
  const service_id=req.body.offer_id;
  // const service_id=req.body.service_id;
  const offeri=await OffersModel.findById(service_id);
  const price=req.body.price;
  const service=await Servicesprovided.findOne({_id:offeri.service_id});
  
  const lawyer=await User.findOne({_id:service.client_id})
  await OffersModel.findByIdAndUpdate(offeri._id,{
    price:price
  }).then((reuslt)=>{
    sendpushnotificationtouser(`Le prix a été mis à jour pour ${service.title}`,lawyer,service.client_id,"user","updateprice")  
    return res.send({
      status:true,
      message:"updated"
    })
  })
}

module.exports.getpostedjobprev=async(req,res)=>{
  const student_id=req.body.service_id;
  const client_id=req.body.client_id;
  const servicemessagesentTO=(await Servicesprovided.findById(student_id))?.messageSentTo||[]
  console.log("servicemessagesentTO00",servicemessagesentTO)  
  var d = new Date();
            d.setHours(0,0,0,0);
            console.log("id",student_id,"d",d);
    const services=await Servicesprovided.aggregate([
        {$match:{
            _id:mongoose.Types.ObjectId(student_id),
            cancelation_message_by_user:{$eq:""}
            // date_availed:{$gte:d}
          }},
          
          {$addFields:{service_idSTR:{"$toString":"$_id"},catagory_id_OBJ:{"$toObjectId":"$catagory_id"}}},
          {
            $lookup:{
              from:"catagories",
              localField:"catagory_id_OBJ",
              foreignField:"_id",
              as:"catagory"
            }
           },

           {$unwind: '$catagory'},
           {$addFields:{
      
            mastercatagory1:"$catagory.mastercatagory"},
            
          },
           {
            $lookup:{
              from:"catagories",
              localField:"mastercatagory1",
              foreignField:"_id",
              as:"pcatagory"
            }
           },
           {$unwind: '$pcatagory'},
            {$lookup:{
            from:"offers",
            let:{service_id:"$service_idSTR"},
            pipeline:[{
              $match: { $expr: { $eq: ["$service_id", "$$service_id"] } },
             
            },
         
            {$lookup:{
              from:"lawunits",
              let:{provider_id:{"$toObjectId":"$provider_id"}},
              pipeline:[{
                $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
               
              },
              {$addFields:{providerIDSTR:{"$toString":"$_id"}}},
              {
                 $lookup:{
                  from:"reviews",
                  localField:"providerIDSTR",
                  foreignField:"provider_id",
                  as:"reviews"
                }
              },
              
              // {
              //    $lookup:{
              //     from:"reviews",
              //     localField:"providerIDSTR",
              //     foreignField:"provider_id",
              //     as:"reviews"
              //   }
              // },
              { $addFields: {
                                          
                avgRating:{$avg: "$reviews.rating"}
            }},
          
            ],
              as:"provider"
            }},
            
            {
              $lookup:{
                from:"lawyerservices",
                localField:"provider_id",
                foreignField:"provider_id",
                as:"services"
              }
            },
            {$addFields:{service:{$arrayElemAt:["$services",0]},provider_idOBJ:{"$toObjectId":"$provider_id"}}},
            {
              $lookup:{
                from:"lawunits",
                localField:"provider_idOBJ",
                foreignField:"_id",
                as:"provider"
              }
            },
            {$unwind:"$provider"},
            {
            $addFields:{
              image:"$provider.photo",
              client_name:"$service.provider_name",
              exp:"$service.experience",
              desc:"$service.desc",
              provider_id:"$service.provider_id",
            }
          }
            
          ],
            as:"offers"
          }},
          { $addFields:{
                is_inviteOBJ:{"$toObjectId":"$is_invite_id"}    
          }},
         {
          $lookup:{
            from:"lawunits",
            let:{is_inviteOBJ:"$is_inviteOBJ"},
            pipeline:[
             
              {$match:{
                $expr:{
                  $eq: [ "$_id", "$$is_inviteOBJ" ]
                  
                },
              }},
              {
                $addFields:{
                  idstr:{"$toString":"$_id"}
                }
              },
              {
                $lookup:{
                  from:"lawyerservices",
                  localField:"idstr",
                  foreignField:"provider_id",
                  as:"services"
                }
              },
              {$addFields:{
                service:{$arrayElemAt:["$services",0]}
              }},
              {$addFields:{
                
                client_name:"$service.provider_name",
                exp:"$service.experience",
                desc:"$service.desc",
                provider_id:"$service.provider_id",
              }}
            ],
            
            as:"invite"
          }
         },
         {
          $lookup:{
            from:"lawunits",
            let:{idobj:"$_id",provider_id:"$provider_id"},
            pipeline:[
             
              {$match:{
                $expr:{
                  $in: [ "$_id", servicemessagesentTO ]
                  
                },
                // "messageSentTo":{
                //   $elemMatch:{
                //     "provider_id":"$_id"
                //   }
                // }
                // $expr: {
                // $eq: ['$$messageSentTo.provider_id', '_id']
                
                
                
                // }
               
              
              },
             
            },
            {
              $addFields:{
                idstr:{"$toString":"$_id"}
              }
            },
            {
              $lookup:{
                from:"lawyerservices",
                localField:"idstr",
                foreignField:"provider_id",
                as:"services"
              }
            },
            {$addFields:{
              service:{$arrayElemAt:["$services",0]}
            }},
            {
              $lookup:{
               from:"servicesprovideds",
               localField:"idobj",
               foreignField:"_id",
               as:"currentservice"
             }
           },
           {$lookup:{
            from:"favriouts",
            localField:"idstr",
            foreignField:"provider_id",
            as:"favriouts"
          }},
          {$addFields:{
            service:{$arrayElemAt:["$services",0]},
            is_fav:{
              
                $map: {
                    input: {
                        $filter: {
                            input: '$favriouts',
                            as: 'favriout',
                            cond: { $eq: ['$$favriout.client_id', client_id] }
                        }
                    },
                    as: 'current',
                    in: "$$current"
                
            }
    
    
    
             
            },
            address3:{$arrayElemAt:["$address",0]},
          }
          },
          {$addFields:{
            image:"$provider.photo",
            client_name:"$service.provider_name",
            exp:"$service.experience",
            desc:"$service.desc",
           
          }}
              ],
            as:"messegesentto"
          }
         },

          // { "$group": {
          //   "_id": "$catagory",
          //   "name": { "$first": "$catagory" },  //$first accumulator
          //   "count": { "$sum": 1 },  //$sum accumulator
          //   items: {$push: '$$ROOT'}
          // }},
         
         
       
    ])
   return res.send({
        status:true,
        message:"services fetched successfully",
        data:services,
        errmessage:""
    })
}

module.exports.getpostedjob=async(req,res)=>{
  const student_id=req.body.service_id;
  const client_id=req.body.client_id;
  const offerids=(await OffersModel.find({service_id:student_id})).map(e=>e._id+'');
  const servicemessagesentTOids=((await Servicesprovided.findById(student_id))?.messageSentTo);
  // const finalproviderids=uniqueProvidersArr.filter(c=>matchedcontacthoursids.some(s=>s==c));
  const finalmessagetosent=(servicemessagesentTOids?.filter(c=>offerids.some(s=>s!=c)))?.map(e=>mongoose.Types.ObjectId(e))||[]
  console.log("offerids",offerids)  
  console.log("servicemessagesentTOids",servicemessagesentTOids)
  console.log("student_id",student_id)
  var d = new Date();
            d.setHours(0,0,0,0);
            console.log("id",student_id,"d",d);
            const services = await Servicesprovided.aggregate([
              {
                  $match: {
                      _id: mongoose.Types.ObjectId(student_id),
                      cancelation_message_by_user: { $eq: "" }
                      
                  }
              },
          
              { 
                $addFields: 
                    { 
                      service_idSTR: 
                      { "$toString": "$_id" }, 
                      //catagory_id_OBJ: { "$toObjectId": "$catagory_id" } 
                      catagory_id_OBJ : {$convert: {input: '$catagory_id', to : 'objectId', onError: '',onNull: ''}}
                    } 
              },
              {
                  $lookup: {
                      from: "catagories",
                      localField: "catagory_id_OBJ",
                      foreignField: "_id",
                      as: "catagory1"
                  }
              },
              { $addFields:{ catagory:{$arrayElemAt: ["$catagory1", 0]} }},
             
              {
                  $addFields: {
          
                      mastercatagory1: "$catagory.mastercatagory"
                  },
          
              },
              {
                  $lookup: {
                      from: "catagories",
                      localField: "mastercatagory1",
                      foreignField: "_id",
                      as: "pcatagory1"
                  }
              },
              { $addFields:{ pcatagory:{$arrayElemAt: ["$pcatagory1", 0]} }},
              
              {
                  $lookup: {
                      from: "offers",
                      let: { service_id: "$service_idSTR" },
                      pipeline: [{
                          $match: { $expr: { $eq: ["$service_id", "$$service_id"] } },
          
                      },
          
                      {
                          $lookup: {
                              from: "lawunits",
                              //let: { provider_id: { "$toObjectId": "$provider_id" } },
                              let: {
                                    provider_id : {$convert: {input: '$provider_id', to : 'objectId', onError: '',onNull: ''}}

                              },
                              pipeline: [{
                                  $match: { $expr: { $eq: ["$_id", "$$provider_id"] } },
          
                              },
                              { $addFields: { providerIDSTR: { "$toString": "$_id" } } },
                              {
                                  $lookup: {
                                      from: "reviews",
                                      localField: "providerIDSTR",
                                      foreignField: "provider_id",
                                      as: "reviews"
                                  }
                              },
          
                              {
                                  $addFields: {
          
                                      avgRating: { $avg: "$reviews.rating" }
                                  }
                              },
          
                              ],
                              as: "provider"
                          }
                      },
          
                      {
                          $lookup: {
                              from: "lawyerservices",
                              localField: "provider_id",
                              foreignField: "provider_id",
                              as: "services"
                          }
                      },
                      { $addFields: { 
                            service: { 
                              $arrayElemAt: ["$services", 0] 
                            }, 
                            //provider_idOBJ: { "$toObjectId": "$provider_id" } 
                            provider_idOBJ : {$convert: {input: '$provider_id', to : 'objectId', onError: '',onNull: ''}}

                          } 
                      },
                      {
                          $lookup: {
                              from: "lawunits",
                              localField: "provider_idOBJ",
                              foreignField: "_id",
                              as: "provider1"
                          }
                      },
                      { $addFields:{ provider:{$arrayElemAt: ["$provider1", 0]} }},
                      
                      {
                          $addFields: {
                              image: "$provider.photo",
                              client_name: "$service.provider_name",
                              exp: "$service.experience",
                              desc: "$service.desc",
                              provider_id: "$service.provider_id",
                          }
                      },
                      {
                        $lookup: {
                            from: "reviews",
                            localField: "provider_id",
                            foreignField: "provider_id",
                            as: "reviews"
                        }
                      },
                      {
                        $addFields: {
                  
                            avgRating: { $avg: "$reviews.rating" }
                        }
                      },
          
                      ],
                      as: "offers"
                  }
              },
              {
                  $addFields: {
                      //is_inviteOBJ: { "$toObjectId": "$is_invite_id" }
                      is_inviteOBJ : {$convert: {input: '$is_invite_id', to : 'objectId', onError: '',onNull: ''}}

                  }
              },
              {
                  $lookup: {
                      from: "lawunits",
                      let: { is_inviteOBJ: "$is_inviteOBJ" },
                      pipeline: [
          
                          {
                              $match: {
                                  $expr: {
                                      $eq: ["$_id", "$$is_inviteOBJ"]
          
                                  },
                              }
                          },
                          {
                              $addFields: {
                                  idstr: { "$toString": "$_id" }
                              }
                          },
                          {
                              $lookup: {
                                  from: "lawyerservices",
                                  localField: "idstr",
                                  foreignField: "provider_id",
                                  as: "services"
                              }
                          },
                          {
                              $addFields: {
                                  service: { $arrayElemAt: ["$services", 0] }
                              }
                          },
                          {
                              $addFields: {
          
                                  client_name: "$service.provider_name",
                                  exp: "$service.experience",
                                  desc: "$service.desc",
                                  provider_id: "$service.provider_id",
                              }
                          },
                          {
                            $lookup: {
                                from: "reviews",
                                localField: "idstr",
                                foreignField: "provider_id",
                                as: "reviews"
                            }
                          },
                          {
                            $addFields: {
                      
                                avgRating: { $avg: "$reviews.rating" }
                            }
                          },
                      ],
          
                      as: "invite"
                  }
              },
              {
                  $lookup: {
                      from: "lawunits",
                      let: { idobj: "$_id", provider_id: "$provider_id" },
                      pipeline: [
          
                          {
                              $match: {
                                  $expr: {
                                      $in: ["$_id", finalmessagetosent]
          
                                  }
          
                              },
          
                          },
                          {
                              $addFields: {
                                  idstr: { "$toString": "$_id" }
                              }
                          },
                          {
                              $lookup: {
                                  from: "lawyerservices",
                                  localField: "idstr",
                                  foreignField: "provider_id",
                                  as: "services"
                              }
                          },
                          {
                              $addFields: {
                                  service: { $arrayElemAt: ["$services", 0] }
                              }
                          },
                          {
                              $lookup: {
                                  from: "servicesprovideds",
                                  localField: "idobj",
                                  foreignField: "_id",
                                  as: "currentservice"
                              }
                          },
                          {
                              $lookup: {
                                  from: "favriouts",
                                  localField: "idstr",
                                  foreignField: "provider_id",
                                  as: "favriouts"
                              }
                          },
                          {
                              $addFields: {
                                  service: { $arrayElemAt: ["$services", 0] },
                                  is_fav: {
          
                                      $map: {
                                          input: {
                                              $filter: {
                                                  input: '$favriouts',
                                                  as: 'favriout',
                                                  cond: { $eq: ['$$favriout.client_id', client_id] }
                                              }
                                          },
                                          as: 'current',
                                          in: "$$current"
          
                                      }
          
          
          
          
                                  },
                                  address3: { $arrayElemAt: ["$address", 0] },
                              }
                          },
                          {
                              $addFields: {
                                  image: "$provider.photo",
                                  client_name: "$service.provider_name",
                                  exp: "$service.experience",
                                  desc: "$service.desc",
          
                              }
                          }
                      ],
                      as: "messegesentto"
                  }
              },
              {
                $addFields:{
                  offers1:{
              
                    $map: {
                        input: {
                            $filter: {
                                input: '$offers',
                                as: 'offer',
                                cond: { $ne: ['$$offer.provider_id', "$is_invite_id"] }
                            }
                        },
                        as: 'offers2',
                        in: "$$offers2"
                    
                },
                
                },
                invite1:{
              
                  $map: {
                      input: {
                          $filter: {
                              input: '$offers',
                              as: 'offer',
                              cond: { $eq: ['$$offer.provider_id', "$is_invite_id"] }
                          }
                      },
                      as: 'invite2',
                      in: "$$invite2"
                  
              }
                }
              }
            }
          
          
          ],(err,data)=>{
                   if(err) {
                    console.log(err)
                    console.log("err.......")
                     //next(err);
                     return res.send({
                        status:false,
                        message:err
                    })
                     //return;
                   } else{
                      return res.send({
                        status:true,
                        message:"services fetched successfully",
                        data:data,
                        errmessage:""
                    })
                   }
  });
   
}


module.exports.ratinsscreen=async(req,res)=>{
  const student_id=req.body.client_id;
  const provider_id=req.body.provider_id;
  var d = new Date();
          d.setHours(0,0,0,0);
          console.log("id",student_id,"d",d);
  const services=await lawyermodel.aggregate([
      {$match:{
          _id:mongoose.Types.ObjectId(provider_id)
         
         
          // date_availed:{$gte:d}
        }},
        {$addFields:{service_idSTR:{"$toString":"$_id"},catagory_id_OBJ:{"$toObjectId":"$catagory_id"}}},
        {$lookup:{
          from:"reviews",
          // localField:"providerSTRId",
          // foreignField:"provider_id",
          let: { provider_id: "$service_idSTR" },
          pipeline:[
            {
                $match: { $expr: { $and:[{$eq: ["$provider_id", "$$provider_id"]},{$eq:["$service_id",""]}] }},
              
            },
            {
                $addFields: {
                    
                    obj_client_id: { "$toObjectId": "$client_id" }
                }
            },
            {
            $lookup: {
                from: "users",
                localField: "obj_client_id",
                foreignField: "_id",
                as: "user"
                
            }
        },
          ],
          as:"ratings"
        }},
       
        {
          $addFields: {
            isRated:{$filter:{
              input: "$ratings",
              as: "rating",
              cond: { 
                 $eq:[
                  "$$rating.client_id",student_id
                ] },
              
           
            }}, 
              avgRating: { $avg: "$ratings.rating" },
             
          onestar:{
            $filter:{
              input: "$ratings",
              as: "rating",
              cond: { 
                 $eq:[
                  "$$rating.rating",1
                ] },
              
           
            }
            
          },
          twostar:{
            $filter:{
              input: "$ratings",
              as: "rating",
              cond: { 
                 $eq:[
                  "$$rating.rating",2
                ] }
              
           
            }
            
          },
          threestar:{
            $filter:{
              input: "$ratings",
              as: "rating",
              cond: { 
                 $eq:[
                  "$$rating.rating",3
                ] },
              
           
            }
            
          },
          fourstar:{
            $filter:{
              input: "$ratings",
              as: "rating",
              cond: { 
                 $eq:[
                  "$$rating.rating",4
                ] },
              
           
            }
             
          },
          fivestar:{
            $filter:{
              input: "$ratings",
              as: "rating",
              cond: { 
                 $eq:[
                  "$$rating.rating",5
                ] },
              
           
            }
            
          }
        },
        
      },
      {
        $addFields:{
          onestarratings:{
            $multiply:[{$cond:{
            if:{
              $ne:[{$size:"$ratings"},0]
            },then:{$divide:[{$size:"$onestar"},{$size:"$ratings"}]},
            else:0
          }},100]
        },
          twostarratings:{$multiply:[{$cond:{
            if:{
              $ne:[{$size:"$ratings"},0]
            },then:{$divide:[{$size:"$twostar"},{$size:"$ratings"}]},
            else:0
          }},100]},
          threestarratings:{$multiply:[{$cond:{
            if:{
              $ne:[{$size:"$ratings"},0]
            },then:{$divide:[{$size:"$threestar"},{$size:"$ratings"}]},
            else:0
          }},100]},
          fourstarratings:{$multiply:[{$cond:{
            if:{
              $ne:[{$size:"$ratings"},0]
            },then:{$divide:[{$size:"$fourstar"},{$size:"$ratings"}]},
            else:0
          }},100]},
          fivestarratings:{$multiply:[{$cond:{
            if:{
              $ne:[{$size:"$ratings"},0]
            },then:{$divide:[{$size:"$fivestar"},{$size:"$ratings"}]},
            else:0
          }},100]}
        }
      }
       ,
       {
        $project:{
          "onestar":0,
          "twostar":0,
          "threestar":0,
          "fourstar":0,
          "fivestar":0
        }
       }
       
     
  ])
 return res.send({
      status:true,
      message:"services fetched successfully",
      data:services,
      errmessage:""
  })
}

module.exports.savedocs1=(req,res)=>{
  let docs=[];
  const caption=req.body.caption
  if (req.files?.docs) {
    console.log("req.files",req.files)
   for(let i=0;i<req.files.docs.length;i++){
    let file=req.files.docs[i];
    let filename="userprofile/"+file.filename
    docs.push({url:filename,caption:caption})
   }
   }
   return res.send({
    status:true,
    message:"success",
    data:docs
   })
}
