const Servicesprovided = require('../../models/client/Servicesprovided');
const lawyermodel=require('../../models/lawyer/Lawunit');
const {sendpushnotificationtouser}=require('../../modules/Fcm');
const User = require("../../models/auth/user");
const base_url=process.env.BASE_URL;
const mongoose=require("mongoose")





module.exports.getongoingappointments=async(req,res)=>{
const pemail=req.params.parent_email;
const childrens=await User.find({emailp:pemail});

const childrenidsarray=[];
childrens.map((child)=>{
    childrenidsarray.push(child._id.toHexString());
   
})
const services=await Servicesprovided.aggregate([
    {$match:{student_id:{$in:childrenidsarray},
    payment_status:true,
    service_acceptance_status_by_student:true,
    service_acceptance_status_by_teacher:true,
    is_service_rejected:false,
    service_status:false
}}
])
return res.send({
    status:true,
    message:"fetched successfully",
    data:services
})
}

module.exports.getcompletedappointments=async(req,res)=>{
    const pemail=req.params.parent_email;
const childrens=await User.find({emailp:pemail});
const childrenidsarray=[];
childrens.map((child)=>{
    childrenidsarray.push(child._id.toHexString());
   
})
const services=await Servicesprovided.aggregate([
    {$match:{student_id:{$in:childrenidsarray},
    payment_status:true,
    service_acceptance_status_by_student:true,
    service_acceptance_status_by_teacher:true,
    is_service_rejected:false,
    service_status:true
}}
])
return res.send({
    status:true,
    message:"fetched successfully",
    data:services
})
}

module.exports.transactions=async(req,res)=>{
    const pemail=req.params.parent_email;
    const childrens=await User.find({emailp:pemail});
    const childrenidsarray=[];
    childrens.map((child)=>{
        childrenidsarray.push(child._id.toHexString());
       
    })
    const services=await Servicesprovided.aggregate([
        {$match:{student_id:{$in:childrenidsarray},
        payment_status:true,
        service_acceptance_status_by_student:true,
        service_acceptance_status_by_teacher:true,
        is_service_rejected:false
        
    }}
    ])
    return res.send({
        status:true,
        message:"fetched successfully",
        data:services
    })
}
module.exports.blockteacher=async(req,res)=>{
    const pemail=req.params.parent_email;
    const teacher_id=req.params.teacher_id;
    const childrens=await User.find({emailp:pemail});
    const childrenidsarray=[];
    console.log("childrens",childrens)
    childrens.map((child)=>{
        
        childrenidsarray.push(child._id);
       
    });
    const services=await User.updateMany(
        {"_id":{$in:childrenidsarray}},
        {$push:{blocked_teachers:teacher_id}}
        
    
    )
    console.log("services",services,"childrenidsarray",childrenidsarray)
    return res.send({
        status:true,
        message:"teacher blocked successfully"
    })
}

module.exports.blockstudent=async(req,res)=>{
    const pemail=req.params.parent_email;
    const student_id=mongoose.Types.ObjectId(req.params.student_id)
    const user=await User.updateOne(
        {"_id":student_id},
        {$set:{is_blocked:true}}
        
    
    )
    console.log("services",user)
    return res.send({
        status:true,
        message:"teacher blocked successfully"
    })
}
module.exports.unblockstudent=async(req,res)=>{
    const pemail=req.params.parent_email;
    const student_id=mongoose.Types.ObjectId(req.params.student_id)
    const user=await User.updateOne(
        {"_id":student_id},
        {$set:{is_blocked:false}}
        
    
    )
    console.log("services",user)
    return res.send({
        status:true,
        message:"teacher blocked successfully"
    })
}
