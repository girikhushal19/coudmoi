const User = require("../../models/client/Parent");
const Parentmodel = require("../../models/client/Parent");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
let lawunit = require("../../models/lawyer/Lawunit");
const async=require("async")
const path=require("path")
const hbs = require('nodemailer-express-handlebars');
const apptext_cmsmodel=require("../../models/admin/Apptext");
const base_url=process.env.BASE_URL;



function generatePassword(digit) {
  var length = digit,
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
}
module.exports.registerp = async (req, res) => {
  // Our register logic starts here
  console.log(req.body)
  const code=Math.floor(100000 + Math.random() * 900000);
  let ppasswordrandom;
  let emailtosent;
  let user_photo;
  console.log("in register")
  try {
    // Get user input
    const { first_name,is_oldenough, last_name, email,emailp, password, phone, dob,address,extProvider } = req.body;
    console.log("req.files",req.files,"req.user_photo",req.user_photo)
    if(req.files.user_photo){
      user_photo="lawyerprofile/" +req.files.user_photo[0].filename
    }
    // Validate user input
    if (!( email  && first_name && last_name)) {
      return res.status(400).send(
       { 
        status:false,
        message:"",
        errormessage:"Toute contribution est requise",
        data:null
      }

      );
    }
    
   if(is_oldenough){
    ppasswordrandom=generatePassword(8);
   
  ppassword=await bcrypt.hash(ppasswordrandom, 10);
   const parent= await Parentmodel.findOne({email:emailp});
  console.log("parent",parent)
   if(parent){
    emailtosent=email;
    ppasswordrandom=null;
   }else{
    emailtosent=emailp;
    const newParent=new Parentmodel();
    newParent.email=emailp;
    newParent.password=ppassword;
    newParent.save();
   }
   }else{
    emailtosent=email;
   }
    // check if user already exist
    // Validate if user exist in our database
    const oldUser = await User.findOne({ email });
const oldUserP = await lawunit.findOne({ email });
    if (oldUser||oldUserP) {
      return res.status(409).send({
        status: false,
        errormessage: "L'utilisateur existe déjà",
        message: "",
        data: null,
      });
    }
    let encryptedPassword;
if(password){
   encryptedPassword = await bcrypt.hash(password, 10);
    
    }else{
      if(extProvider){
       encryptedPassword=""
      }else{
        return res.send({
        status:false,
        message:"",
        errmessage:"password is required",
        data:null
       })
      }
    }
    //Encrypt user password
   

    // Create user in our database
    const user = await User.create({
      
      first_name,
       ...(user_photo && { user_photo:user_photo }),
      
      last_name,
      email: email.toLowerCase(), // sanitize: convert email to lowercase
      password: encryptedPassword,
      phone: phone,
      dob: dob,
      date_of_registration:new Date().toISOString(),
      ...(address && { address }),
      extProvider:extProvider,
      ...(emailp&&{emailp:emailp}),
      verification_code:code,
      is_oldenough:is_oldenough,
      ...(extProvider.toString()!=""&&{extProvider:extProvider})
      
    });

    // Create token
    const token = jwt.sign(
      { user_id: user._id, email },
      process.env.TOKEN_KEY,
      {
        expiresIn: "1d",
      }
    );
    // save user token
    user.token = token;
    user.extProvider=extProvider;
const page_name=process.env.register_confirm_email
  const apptext=await apptext_cmsmodel.findOne({page_name:page_name});
  // Create token
  console.log(apptext);
  let emailtext;
  let emailsubject;
  if(apptext?.page_content){
  emailtext=apptext.page_content;
  emailsubject=apptext.page_heading;
  }else{
    emailtext="L'enregistrement est réussi, continuez et mettez à jour votre profil."
    emailsubject="Inscription réussie"
  }
 var data = {
              to: emailtosent,
              from: process.env.MAILER_EMAIL_ID,
              template: "confirm-register-email-lawyer",
              subject: emailsubject,
              context: {
                name: first_name+" "+last_name,
                text:emailtext,
                code:code,
                password:ppasswordrandom
               
              },
            };
 var smtpTransport = nodemailer.createTransport({
                host: process.env.MAILER_HOST,
                port: process.env.MAILER_PORT,
                secure:false,
                auth: {
                  user: process.env.MAILER_EMAIL_ID,
                  pass: process.env.MAILER_PASSWORD,
                },
                tls: {
                  rejectUnauthorized: false
                }
              });

              const handlebarOptions = {
                viewEngine: {
                  partialsDir: path.resolve("./views/auth/"),
                  defaultLayout: false,
                },
                viewPath: path.resolve("./views/auth/"),
              };
              // console.log("data in email",data)
              smtpTransport.use("compile", hbs(handlebarOptions));
              smtpTransport.sendMail(data, function (err) {
                if (!err) {
                  return res.status(200).json({
                    status: true,
                    message: "Veuillez vérifier votre e-mail pour de plus amples instructions",
                    errmessage: "",
                    data: null,
                  });
                } else {
                  // console.log(err)
                  return res.send( {
                    status: false,
                    message: "",
                    errmessage: err.message,
                    data: null,
                  });
                }
              });
    // return new user
    user.save();
    res.status(201).json({
      status:true,
      message:"Utilisateur créé avec succès",
      errmessage:"",
      data:user

    });
  } catch (err) {
    console.log(err);
    res.send({
      status:false,
      message:"",
      errmessage:err.message,
      data:null
    });
  }
  // Our register logic ends here
};
module.exports.loginp = async (req, res) => {
  try {
    // Get user input
    const { email, password ,fcm_token} = req.body;

    // Validate user input
    if (!(email )) {
      res.status(400).send(
        {
          status:false,
          message:"",
          errmessage:"Toute contribution est requise",
          data:null
        }
      );
    }
    // Validate if user exist in our database
    const user = await User.findOne({ email });
   if(password!=""){
     if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "24h",
        }
      );
     if(fcm_token){
      let data=await User.findOne({ email });
      data.fcm_token=fcm_token;
      data.save();
     
     }
      // save user token
      user.token = token;
      user.save();
      console.log(user);
      // user
      res.status(200).json({
        status:true,
        errmessage:"",
        message:"Connexion réussie",
        data:user
      });
    } else {
      res.status(400).send({
        status:false,
        message:"",
        errmessage:"L'adresse électronique ou le mot de passe est incorrect",
        data:null
      });
    }
   }else{
    if (user.extProvider) {
         const token = jwt.sign(
            { user_id: user._id, email },
            process.env.TOKEN_KEY,
            {
              expiresIn: "24h",
            }
          );
         if(fcm_token){
          let data=await User.findOne({ email });
          data.fcm_token=fcm_token;
          data.save();
         
         }
          // save user token
          user.token = token;
          user.save();
          console.log(user);
          return res.send({
              message: "Connexion réussie",
              errmessage: "",
              data: user,
              status: true,
            });
        } else {
          return res.send({
            status: false,
            message: "",
            errmessage: "Vous ne vous êtes pas réveillé en utilisant cette méthode",
            data: null,
          });
        }
   }
   
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
};
module.exports.logoutp = async (req, res) => {
  const id = req.params.id;
  const Lawyer = await User.findById(id);
  Lawyer.token = "";
  Lawyer.fcm_token = "";
  Lawyer.save();
  res.status(200).json({ 
    status: true, 
    errmessage:"",
    message:"Deconnexion réussie",
    data:null
  });
};
module.exports.render_forgot_password_templatep = function(req, res) {
  console.log(path.resolve('./public/forgot-password.html'))
  return res.sendFile(path.resolve('./public/forgot-password.html'));
};
module.exports.render_reset_password_templatep = function(req, res) {
  return res.render(path.resolve('./public/reset-passwordp.ejs'),{
    url:base_url
  });
};
module.exports.forgot_passwordp = async (req, res) => {
  try {
    async.waterfall(
      [
        function (done) {
          User.findOne({
            email: req.body.email,
          }).exec(function (err, user) {
            if (user) {
              done(err, user);
            } else {
              done({
                status: false,
                message: "utilisateur non trouvé",
                errmessage: "",
                data: null,
              });
            }
          });
        },
        function (user, done,err) {
          // create a unique token
          var tokenObject = {
            email: user.email,
            id: user._id,
          };
          var secret = user._id + "_" + user.email + "_" + new Date().getTime();
          var token = jwt.sign(tokenObject, secret);
          done(err, user, token);
        },
        function (user, token, done) {
          User.findByIdAndUpdate(
            { _id: user._id },
            {
              reset_password_token: token,
              reset_password_expires: Date.now() + 86400000,
            },
            { new: true }
          ).exec(function (err, new_user) {
            done(err, token, new_user);
          });
        },
        function (token, user, done) {
          var data = {
            to: user.email,
            from: process.env.MAILER_EMAIL_ID,
            template: "forgot-password-email",
            subject: "L'aide pour les mots de passe est arrivée !",
            // html: "forgot-password-email",
            context: {
              url: base_url+"/api/auth/reset_passwordp?token=" + token,
              name: user.first_name,
            },
          };
// console.log(data)
          sendemail(req,res,data,done);
        },
      ],
      function (err) {
        return res.status(422).json({ 
          status: false,
          message: "",
          errmessage: err.message,
          data: null,
         });
      }
    );
  } catch (err) {
    console.log(err);
  }
};
module.exports.reset_passwordp = async (req, res) => {

 console.log(req.body)
User.findOne({
    reset_password_token: req.body.token,
    reset_password_expires: {
      $gt: Date.now(),
    },
}).exec(async function(err,user) {
  // console.log(user,err)
   if(!user){
        return res.send({
          status:false,
          message:"page expired",
          errmessage:"La page a expiré, demandez à nouveau la réinitialisation du mot de passe"
        })
      }
if(user&&!err){

  if(req.body.newPassword==req.body.verifyPassword){
    // console.log("password matched")
    let encryptedPassword = await bcrypt.hash(req.body.newPassword, 10);
    user.password=encryptedPassword;
    user.reset_password_expires=undefined;
    user.reset_password_token=undefined;
    user.save(function(err,user){
      if(!err){
        var data = {
          to: user.email,
          from: process.env.MAILER_EMAIL_ID,
          template: 'reset-password-email',
          subject: 'Confirmation de la réinitialisation du mot de passe',
          context: {
            name: user.first_name
          }
        };
        var smtpTransport = nodemailer.createTransport({
          host: process.env.MAILER_HOST,
          port:  process.env.MAILER_PORT,
          secure:false,
          auth: {
            user: process.env.MAILER_EMAIL_ID,
            pass: process.env.MAILER_PASSWORD,
          },
          tls: {
            rejectUnauthorized: false
          }
        });
      
       
        const handlebarOptions = {
          viewEngine: {
              partialsDir: path.resolve("./views/auth/"),
              defaultLayout: false,
          },
          viewPath: path.resolve("./views/auth/"),
      };
      // console.log("data in email",data)
        smtpTransport.use("compile", hbs(handlebarOptions));
        smtpTransport.sendMail(data, function (err) {
          if (!err) {
            return res.status(200).json({
              status: true,
              message: "Confirmer le succès du mot de passe",
              errmessage: "",
              data: null,
            });
          } else {
            // console.log(err)
            return res.send(
              {
                status: false,
                message: "",
                errmessage: err.message,
                data: null,
              }
            );
          }
        });
      }else{
        return res.status(422).send({
          status: false,
          message: "",
          errmessage: err.message,
          data: null,
        });
      }
    })
    
  }
}
});
};
module.exports.sendemail = (req, res, data,done) => {
  
  // var smtpTransport = nodemailer.createTransport({
  //   // service: process.env.MAILER_SERVICE_PROVIDER,
  //   host: process.env.MAILER_HOST,
  //   port: process.env.MAILER_PORT,
  //   auth: {
  //     user: process.env.MAILER_EMAIL_ID,
  //     pass: process.env.MAILER_PASSWORD,
  //   },
  //   tls: {
  //     rejectUnauthorized: false
  //   }
  // });
  // console.log(data)
  var smtpTransport = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    secure: false,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    },
    tls: {
      rejectUnauthorized: false
    }
  });

 smtpTransport.verify(function(error, success) {

  if (error) {
    console.log(error);
  } else {
    console.log('Server is ready to take our messages');
  }

 })
  const handlebarOptions = {
    viewEngine: {
        partialsDir: path.resolve("./views/auth/"),
        defaultLayout: false,
    },
    viewPath: path.resolve("./views/auth/"),
};
// console.log("data in email",data)
  smtpTransport.use("compile", hbs(handlebarOptions));
  smtpTransport.sendMail(data, function (err) {
    if (!err) {
      return res.json({
        status: true,
        message: "Veuillez vérifier votre e-mail pour de plus amples instructions",
        errmessage: "",
        data: null,
      });
    } else {
      console.log("error in email",err)
      return done(err);
    }
  });
};
module.exports.changepasswordp = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.body.user_id });
    if (!user) {
      return res.status(422).json({ message: "User not found.",status:false });
    }
    const isMatch = await bcrypt.compare(req.body.oldPassword, user.password);
    if (!isMatch) {
      return res.status(422).json({ message: "" ,status:false,data:null,errmessage:"Mot de passe incorrect."});
    }
    const encryptedPassword = await bcrypt.hash(req.body.newPassword, 10);
    user.password = encryptedPassword;
    await user.save();
    return res.status(200).json(
  { message: "Le mot de passe a été modifié avec succès.",
  errmessage: "",
     success:true,
      data: null,
  });
  } catch (err) {
    console.log(err);
  }
}

module.exports.verifycodep=async(req,res)=>{
  const id=req.body.id;
  const code=req.body.code;
  const user=await User.findById(id);
  if(user){
    if(code.toString()==user.verification_code.toString()){
      user.verification_code="",
      user.isverfied=true;
      user.save().then((result)=>{
       return res.send({
         status:true,
         message:"La vérification est réussie",
         errmessage:"",
         data:null
       })
      })
     }else{
       return res.send({
         status:false,
         message:"",
         errmessage:"Le code n'est pas valide",
         data:null
       })
     }
  }else{
    return res.send({
      status:false,
      message:"",
      errmessage:"no user found",
      data:null
    })
  }
}

