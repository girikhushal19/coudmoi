const express = require("express");
const multer = require("multer");
const path=require("path")
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const admin_images_path=process.env.admin_images_path;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(admin_user_profile_path))
  },
  filename: function (req, file, cb) {
    cb(null, Math.floor(1000000000 + Math.random() * 9000000000) + path.extname(file.originalname)) //Appending extension
  }
})

const upload2 = multer({ storage: storage });
const AuthController = require("./Auth.controller");
const PAuthController= require("./PAuth.controller");
const authroutes = express.Router();
authroutes.post("/register",upload2.fields([
  { 
    name: 'user_photo', 
    maxCount: 10
  }
]
) ,AuthController.register);
authroutes.post("/login", AuthController.login);
authroutes.post("/socialLogin", AuthController.socialLogin);
authroutes.get("/logout/:id", AuthController.logout);
authroutes.post("/verifycode",AuthController.verifycode);
authroutes.get("/forgot_password",AuthController.render_forgot_password_template);
authroutes.post("/forgot_password",AuthController.forgot_password);

authroutes.get("/reset_password",AuthController.render_reset_password_template);
authroutes.post("/reset_password",AuthController.reset_password);
// authroutes
//   .route("/reset_password")
//   .get(render_reset_password_template)
//   .post(reset_password);
authroutes.post("/changepassword", AuthController.changepassword);

authroutes.post("/loginp", PAuthController.loginp);
authroutes.get("/logoutp/:id", PAuthController.logoutp);
authroutes.post("/verifycodep",PAuthController.verifycodep);
authroutes.get("/forgot_passwordp",PAuthController.render_forgot_password_templatep);
authroutes.post("/forgot_passwordp",PAuthController.forgot_passwordp);

authroutes.get("/reset_passwordp",PAuthController.render_reset_password_templatep);
authroutes.post("/reset_passwordp",PAuthController.reset_passwordp);
// authroutes
//   .route("/reset_password")
//   .get(render_reset_password_template)
//   .post(reset_password);
authroutes.post("/changepasswordp", PAuthController.changepasswordp);
authroutes.post("/logoutall", AuthController.logoutall);
module.exports = authroutes;
