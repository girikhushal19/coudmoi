// utils
// const makeValidation = require('@withvoid/make-validation');
// models
const  { CHAT_ROOM_TYPES ,ChatRoom} = require('../../models/chat/ChatRoom');
const {ChatMessage} = require('../../models/chat/ChatMessage.js');
const UserModel = require('../../models/auth/user');
const Chat = require('../../models/chat/chat.js');
const mongoose=require("mongoose");
module.exports=  {
  initiate: async (req, res) => {
    try {
      // const validation = makeValidation(types => ({
      //   payload: req.body,
      //   checks: {
      //     userIds: { 
      //       type: types.array, 
      //       options: { unique: true, empty: false, stringOnly: true } 
      //     },
      //     type: { type: types.enum, options: { enum: CHAT_ROOM_TYPES } },
      //   }
      // }));
      // if (!validation.success) return res.status(400).json({ ...validation });

      const { userIds, type } = req.body;
      const { chatInitiator  } = req.body;
      console.log("chatInitiator",chatInitiator)
      const allUserIds = [...userIds, chatInitiator];
      console.log("ChatRoomModel0",ChatRoom)
      const chatRoom = await ChatRoom.initiateChat(allUserIds, type, chatInitiator);
      return res.status(200).json({ success: true, chatRoom });
    } catch (error) {
      console.log(error)
      return res.status(500).json({ success: false, error: error })
    }
  },
  postMessage: async (req, res) => {
    try {
      const { roomId } = req.params;
      // const validation = makeValidation(types => ({
      //   payload: req.body,
      //   checks: {
      //     messageText: { type: types.string },
      //   }
      // }));
      // if (!validation.success) return res.status(400).json({ ...validation });

      const messagePayload = {
        messageText: req.body.messageText,
      };
      const currentLoggedUser = req.body.userId;
      const post = await ChatMessage.createPostInChatRoom(roomId, messagePayload, currentLoggedUser);
      global.io.sockets.in(roomId).emit('new message', { message: post });
      return res.status(200).json({ success: true, post });
    } catch (error) {
      console.log(error)
      return res.status(500).json({ success: false, error: error })
    }
  },
  getRecentConversation: async (req, res) => {
    try {
      const currentLoggedUser = req.params.userId;
      console.log("currentLoggedUser",currentLoggedUser)
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10,
      };
     
      const rooms = await ChatRoom.getChatRoomsByUserId(currentLoggedUser);
      // console.log("rooms",rooms)
      const roomIds = rooms.map(room => room._id);
      console.log("roomIds, options, currentLoggedUser",roomIds, options, currentLoggedUser)
      const recentConversation = await ChatMessage.getRecentConversation(
        roomIds, options, currentLoggedUser
      );
      console.log(recentConversation)
      return res.status(200).json({ success: true, conversation: recentConversation });
    } catch (error) {
      return res.status(500).json({ success: false, error: error })
    }
  },
  getConversationByRoomId: async (req, res) => {
    try {
      const { roomId } = req.params;
      console.log("roomId",roomId)
      const room = await ChatRoom.getChatRoomByRoomId(roomId)
      if (!room) {
        return res.status(400).json({
          success: false,
          message: 'No room exists for this id',
        })
      }
      const users = await UserModel.getUserByIds(room.userIds);
      const options = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10,
      };
      const conversation = await ChatMessage.getConversationByRoomId(roomId, options);
      return res.status(200).json({
        success: true,
        conversation,
        users,
      });
    } catch (error) {
      console.log(error)
      return res.status(500).json({ success: false, error });
    }
  },
  markConversationReadByRoomId: async (req, res) => {
    try {
      const { roomId,message_id,userId } = req.body;
      const messageID=mongoose.Types.ObjectId(message_id)
      const room = await Chat.findOne({roomId:req.body.roomId})
      if (!room) {
        return res.status(400).json({
          success: false,
          message: 'No room exists for this id',
        })
      }

    
      const result = await Chat.updateOne({
        messages:{"$elemMatch":{"chats.$[]._id":messageID}},
      },
      // {$push:{
      //   "messages.$.readByRecipients":userId
      // }}
        { "$set": {
        "messages.$[element].senderName":"updatedone" 
         }} 
        
      ).explain()
      console.log("result",result)
      return res.status(200).json({ success: true, data: result });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ success: false, error });
    }
  },
}

module.exports.hidechatforprovider=async(req,res)=>{
  const { id } = req.params;
  const room = await Chat.findOne({roomId:id});
  room.hide_chat_for_provider=true
  let newchats=room.chats.map(e=>{
    e.hide_chat_for_provider=true
    return e
  })
  room.chats=newchats
  room.save().then((result)=>{
    return res.send({
      status:true,
      message:"deletion successfully"
    })
  })
  if (!room) {
    return res.status(400).json({
      success: false,
      message: 'No room exists for this id',
    })
  }
}
module.exports.hidechatforuser=async(req,res)=>{
  const { id } = req.params;
  const room = await Chat.findOne({roomId:id})
  room.hide_chat_for_client=true
  let newchats=room.chats.map(e=>{
    e.hide_chat_for_client=true
    return e
  })
  room.chats=newchats
  console.log("chats",newchats)
  room.save().then((result)=>{
    return res.send({
      status:true,
      message:"deletion successfully"
    })
  })
  if (!room) {
    return res.status(400).json({
      success: false,
      message: 'No room exists for this id',
    })
  }
}

module.exports.getchatsbyroomidforprovider=async(req,res)=>{
 try{
  const { id } = req.params;
  const chats = await Chat.findOne({roomId:id,hide_chat_for_provider:false});
  await Chat.updateMany({roomId:id, chats: { $exists: true, $not: {$size: 0} }},{
    "chats.$[element].statusp":true
  },{
    arrayFilters: [
      { 'element._id': {$ne:null} },
    ],
  },)
  return res.send({
    status:true,
    message:"fetched success",
    chats:chats
  })
 }catch(e){
   return res.send({
    status:true,
    message:"fetched success",
    chats:[]
  })
 }
}

module.exports.getchatsbyroomidforuser=async(req,res)=>{
  try{
    const { id } = req.params;
  const chats = await Chat.findOne({roomId:id,hide_chat_for_client:false});
  console.log("chats",chats._id)
  await Chat.updateMany({roomId:id, chats: { $exists: true, $not: {$size: 0} }},{
    "chats.$[element].statusc":true
  },{
    arrayFilters: [
      { 'element._id': {$ne:null} },
    ],
  },)
  // const chats = await Chat.findOne({roomId:id,hide_chat_for_client:false});
  return res.send({
    status:true,
    message:"fetched success",
    chats:chats
  })
  }catch(e){
    return res.send({
    status:true,
    message:"fetched success",
    chats:[]
  })
  }
}


module.exports.getallchatsbyclientid=async(req,res)=>{
  const { id } = req.params;
  const chats = await Chat.aggregate([
    {
      $match:{
        userID:id
      }
    },
    {
      $addFields:{
        idOBJ:{
          "$toObjectId":"$providerID"
        }
      }
    },
    {
      $lookup:{
        from:"lawunits",
        localField:"idOBJ",
        foreignField:"_id",
        as:"chatter"
      }
    },
    {
      $lookup: {
          from: "abusereports",
          let:{whoisreported_Id1:"$providerID",reportedbyId1:"$userID"},
            pipeline:[
              {$match:{
                $expr:{
                  $and:[
                    {$eq:["$whoisreported_Id","$$whoisreported_Id1"]},
                    {$eq:["$reportedby_Id","$$reportedbyId1"]},
                   
                  ]
                }
              }}
            ],
          
          as: "abuses"
      }
    },

    {
      $addFields:{
        lastchat:{ $arrayElemAt: [ "$chats", -1 ] }
      }
    },
    {$addFields:{
      
      isallread:{
        
          $map: {
              input: {
                  $filter: {
                      input: '$chats',
                      as: 'chat',
                      cond: { $eq: ['$$chat.statusc', false] }
                  }
              },
              as: 'isallread',
              in: "$$isallread._id"
          
      }



       
      },
     
    }
    },
  {
    $project:{
      "chats":0
    }
  }
  ]);
  // const chats = await Chat.findOne({"userId":id});
  return res.send({
    status:true,
    message:"fetched success",
    chats:chats
  })
}

module.exports.getallchatsbyproviderid=async(req,res)=>{
  const { id } = req.params;
  
  const chats = await Chat.aggregate([
    {
      $match:{
        providerID:id
      }
    },
    {
      $addFields:{
        idOBJ:{
          "$toObjectId":"$userID"
        }
      }
    },
    {
      $lookup:{
        from:"users",
        localField:"idOBJ",
        foreignField:"_id",
        as:"chatter"
      }
    },
    {
      $lookup: {
          from: "abusereports",
          let:{whoisreported_Id1:"$userID",reportedbyId1:"$providerID"},
            pipeline:[
              {$match:{
                $expr:{
                  $and:[
                    {$eq:["$whoisreported_Id","$$whoisreported_Id1"]},
                    {$eq:["$reportedby_Id","$$reportedbyId1"]},
                   
                  ]
                }
              }}
            ],
          
          as: "abuses"
      }
  },
  {
    $addFields:{
      lastchat:{ $arrayElemAt: [ "$chats", -1 ] }
    }
  },
  {$addFields:{
      
    isallread:{
      
        $map: {
            input: {
                $filter: {
                    input: '$chats',
                    as: 'chat',
                    cond: { $eq: ['$$chat.statusp', false] }
                }
            },
            as: 'isallread',
            in: "$$isallread._id"
        
    }



     
    },
   
  }
  },
{
  $project:{
    "chats":0
  }
}
  ]);
  return res.send({
    status:true,
    message:"fetched success",
    chats:chats
  })
}


module.exports.saveimages=async(req,res)=>{
  const { id } = req.params;
  const images=[];
 
  if (req.files?.images) {
    for(let i=0;i<req.files.images.length;i++){
      let file=req.files.service_image[i];
      images.push("userprofile/"+file.filename)
     }
    }
  const chats = await Chat.findOne({})
  return res.send({
    status:true,
    message:"fetched success",
    chats:chats
  })
}