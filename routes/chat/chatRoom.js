const express =require('express');
// controllers
const chatRoom = require('./chatRoom.controller');

const router = express.Router();
const multer = require("multer");
const path=require("path")
const admin_lawyer_profile_path=process.env.admin_lawyer_profile_path;
const admin_user_profile_path=process.env.admin_user_profile_path;
const admin_images_path=process.env.admin_images_path;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(admin_user_profile_path))
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
})

const upload2 = multer({ storage: storage });
router
  .get('/:userId', chatRoom.getRecentConversation)
  .get('/newroom/:roomId', chatRoom.getConversationByRoomId)
  .post('/initiate', chatRoom.initiate)
  .post('/:roomId/message', chatRoom.postMessage)
  .post('/:roomId/mark-read', chatRoom.markConversationReadByRoomId)
  .get('/hidechatforuser/:id',chatRoom.hidechatforuser)
  .get('/hidechatforprovider/:id',chatRoom.hidechatforprovider)
  .get('/getchatsbyroomidforuser/:id',chatRoom.getchatsbyroomidforuser)
  .get('/getchatsbyroomidforprovider/:id',chatRoom.getchatsbyroomidforprovider)
  .get('/getallchatsbyclientid/:id',chatRoom.getallchatsbyclientid)
  .get('/getallchatsbyproviderid/:id',chatRoom.getallchatsbyproviderid)
  .get('/saveimages',upload2.fields([
    { 
      name: 'images', 
      maxCount: 10
    }
    
  
  ]
  ),chatRoom.saveimages)  

module.exports=router;