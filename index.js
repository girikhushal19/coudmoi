const http = require("http");
const app = require("./app");
const server = http.createServer(app);
var Chat = require('./models/chat/chat')
const Notification = require('./models/Notifications')
var User = require('./models/auth/user');
const provider=require("./models/lawyer/Lawunit");
const socketio = require("socket.io");
// const WebSockets =require("./utils/WebSockets.js");


// io.of('/').adapter.on('error', (err)=>{console.log('adapter error',err)});
const {sendpushnotificationtouser} =require("./modules/Fcm");
// io.on("connection", WebSockets.connection)
// console.log("io",io)
const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;
// module.exports={io}
// server listening 
// global.io = socketio.listen(server);
server.listen(port,function () {
  console.log(`Server listening on port ${port}`);
 
 });
 console.log("Listening on port ..........", port)
 server.on('error', onError);
 server.on('listening', onListening);
 const io = socketio(server);
 function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  // debug('Listening on ' + bind);
}


function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}
const whoissender=(type)=>{
  let model;
  if(type=="user"){
  model=User;
  }else if(type="provider"){
  model=provider;
  }
  return model;
}
 io.on('connection', socket => {
  const transport = socket.conn.transport;
  console.log(transport.conn);
  console.log("in connection ......")
       socket.on('chat', async(data) => {
        console.log("data",data)
            let message = data.message;
            let recieverId = data.recieverId;
            let receiverName = data.receiverName;
            let receiverProfile = data.receiverProfile;
            let senderId = data.senderId;
            let senderName = data.senderName;
            let senderProfile = data.senderProfile;
            let totype=data.totype;
            var count = 0;
            var roomId1 = data.roomID;
            var roomId2 = data.roomID;
            var userID =data.userID;
            var providerID=data.providerID;
            let model=whoissender(totype);
            const senderBlocked=(await model.findById(recieverId))?.blockedchats||[];
            if(senderBlocked.includes(senderId)){
              console.log("this id is blocked",)
              io.sockets.emit("blocked", "you have been blocked,you can't send messege to user");
              // return res.send({
              //   status:false,
              //   message:""
              // })
              // var room=data.roomID;
              //   var today = new Date();
              //   var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
              //   var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
              //   var dateTime = today;
              //   Chat.findOne({
              //     roomId: roomId1
              //     }).exec(function getTabacs(err, roomDetails) {
                    
              //       // console.log("roomDetails",roomDetails)
                  
              //     if(roomDetails){
              //      let isnewsender= !roomDetails?.users.find(e=>e==data.senderId);
              //      if(isnewsender){
              //       roomDetails.users.push(data.senderId);
              //      }
              //      console.log("isnewsender",isnewsender,data.senderId)
              //           roomDetails.chats.unshift({ 
              //             message: data.message, 
              //             by: data.senderId,
              //             senderId : data.senderId,
              //             senderName : data.senderName,
              //             senderProfile : data.senderProfile,
              //             recieverId : data.recieverId,
              //             receiverName : data.receiverName,
              //             receiverProfile : data.receiverProfile,
              //             readByRecipients:[senderId],
              //             time : dateTime 
              //           });
              //           if(roomDetails.status == true){
              //            console.log(roomDetails.count)
              //            console.log("roomDetails.count")
              //            count = roomDetails.count + 1 ;  
              //           }
              //           else {
              //            count = 1;  
              //           }
              //           roomDetails.status = true;
              //           roomDetails.count = count;
              //           roomDetails.updated_at = new Date();  
              //           roomDetails.save().then(() => {
              //           var notification = new Notification();
              //           notification.createdAt = new Date();
              //           notification.adminID = recieverId;
              //           notification.status = 'unread';
              //           notification.details = '';
              //           notification.title = 'Vous avez un nouveau message de' + data.name ;
              //           notification.sentTo = recieverId;
              //           notification.sentBy =  senderId;
              //           notification.notification_type = 'Message';
              //           notification.message=data.message;
              //           notification.save(function(err) {
              //             if (err) {
              //               console.log(err);
              //             }
                         
              //             console.log("whoissender(totype)",whoissender(totype))
              //             // model.findById(recieverId, function getUser(err, user1) {
              //             //     if (err) {
              //             //         console.log(err);
              //             //         console.log("in error of user",err);
              //             //         errorMessage = err;
              //             //     } else {
              //             //       if (user1.fcm_token != undefined || user1.fcm_token != null) {
              //             //         sendpushnotificationtouser(data.message,user1,recieverId)
              //             //         // io.sockets.emit(room, roomDetails);
              //             //       }
              //             //         io.sockets.emit(room, roomDetails);
              //             //     }
              //             // });
              //         })
              //         });
              //     } else {
              //       var roomDetails = new Chat();
              //       roomDetails.roomId = roomId1;
              //       roomDetails.users.unshift(data.senderId);
              //       roomDetails.users.unshift(data.recieverId);
              //       roomDetails.updated_at = new Date();
              //       roomDetails.chats.unshift({ 
              //             message: data.message, 
              //             by: data.senderId,
              //             senderId : data.senderId,
              //             senderName : data.senderName,
              //             senderProfile : data.senderProfile,
              //             recieverId : data.recieverId,
              //             receiverName : data.receiverName,
              //             receiverProfile : data.receiverProfile,
              //             readByRecipients:[data.senderId],
              //             time : dateTime 
              //           });  
              //           roomDetails.save().then(() => {
              //           var notification = new Notification();
              //           notification.createdAt = new Date();
              //           notification.adminID = recieverId;
              //           notification.status = 'unread';
              //           notification.details = '';
              //           notification.title = 'Vous avez un nouveau message de' + data.name ;
              //           notification.sentTo = recieverId;
              //           notification.sentBy =  senderId;
              //           notification.notification_type = 'Message';
              //           notification.message=data.message;
              //           notification.save(function(err) {
              //             if (err) {
              //                 res.json({
              //                     success: false,
              //                     message: err
              //                 });
              //             }
              //             // model.findById(recieverId, function getUser(err, user1) {
              //             //     if (err) {
              //             //         console.log(err);
              //             //          console.log("in error of user",err);
              //             //         errorMessage = err;
              //             //     } else {
              //             //       console.log("user41",user1)
              //             //         if (user1.fcm_token != undefined || user1.fcm_token != null) {
              //             //           sendpushnotificationtouser(data.message,user1,recieverId)
              //             //           // io.sockets.emit(room, roomDetails);
              //             //         }
              //             //         io.sockets.emit(room, roomDetails);
              //             //     }
              //             // });
              //         })
              //         });

              //     }

                   
                
              //   })
            }else{
              var room=data.roomID;
              var today = new Date();
              var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
              var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
              var dateTime = today;
              Chat.findOne({
                roomId: roomId1
                }).exec(function getTabacs(err, roomDetails) {
                  
                  // console.log("roomDetails",roomDetails)
                
                if(roomDetails){
                 let isnewsender= !roomDetails?.users.find(e=>e==data.senderId);
                 if(isnewsender){
                  roomDetails.users.push(data.senderId);
                 }
                 console.log("isnewsender",isnewsender,data.senderId)
                      roomDetails.chats.unshift({ 
                        message: data.message, 
                        by: data.senderId,
                        senderId : data.senderId,
                        senderName : data.senderName,
                        senderProfile : data.senderProfile,
                        recieverId : data.recieverId,
                        receiverName : data.receiverName,
                        images:data.images,
                        hide_chat_for_client:false,
                        hide_chat_for_provider:false,
                        receiverProfile : data.receiverProfile,
                        readByRecipients:[senderId],
                        time : dateTime 
                      });
                      if(roomDetails.status == true){
                       console.log(roomDetails.count)
                       console.log("roomDetails.count")
                       count = roomDetails.count + 1 ;  
                      }
                      else {
                       count = 1;  
                      }
                      roomDetails.status = true;
                      roomDetails.userID = userID;
                      roomDetails.providerID = providerID;
                      roomDetails.count = count;
                      roomDetails.hide_chat_for_client=false,
                      roomDetails.hide_chat_for_provider=false,
                      roomDetails.updated_at = new Date();  
                      roomDetails.save().then(() => {
                      var notification = new Notification();
                      notification.createdAt = new Date();
                      notification.adminID = recieverId;
                      notification.status = 'unread';
                      notification.details = '';
                      notification.title = 'Vous avez un nouveau message de' + data.name ;
                      notification.sentTo = recieverId;
                      notification.to_id = recieverId;
                      notification.sentBy =  senderId;
                      notification.notification_type = 'Message';
                      notification.message=data.message;
                      notification.save(function(err) {
                        if (err) {
                          console.log(err);
                        }
                       
                        console.log("whoissender(totype)",whoissender(totype))
                        model.findById(recieverId, function getUser(err, user1) {
                            if (err) {
                                console.log(err);
                                console.log("in error of user",err);
                                errorMessage = err;
                            } else {
                              // if (user1.fcm_token != undefined || user1.fcm_token != null) {
                              //   sendpushnotificationtouser(data.message,user1,recieverId)
                              //   // io.sockets.emit(room, roomDetails);
                              // }
                                io.sockets.emit(room, roomDetails);
                            }
                        });
                    })
                    });
                } else {
                  var roomDetails = new Chat();
                  roomDetails.roomId = roomId1;
                  roomDetails.userID = userID;
                  roomDetails.providerID = providerID;
                  

                  roomDetails.users.unshift(data.senderId);
                  roomDetails.users.unshift(data.recieverId);
                  roomDetails.updated_at = new Date();
                  roomDetails.chats.unshift({ 
                        message: data.message, 
                        by: data.senderId,
                        senderId : data.senderId,
                        senderName : data.senderName,
                        senderProfile : data.senderProfile,
                        recieverId : data.recieverId,
                        images:data.images,
                        hide_chat_for_client:false,
                        hide_chat_for_provider:false,
                        receiverName : data.receiverName,
                        receiverProfile : data.receiverProfile,
                        readByRecipients:[data.senderId],
                        time : dateTime 
                      });  
                      roomDetails.save().then(() => {
                      var notification = new Notification();
                      notification.createdAt = new Date();
                      notification.adminID = recieverId;
                      notification.status = 'unread';
                      notification.details = '';
                      notification.title = 'Vous avez un nouveau message de' + data.name ;
                      notification.sentTo = recieverId;
                      notification.to_id = recieverId;
                      notification.sentBy =  senderId;
                      notification.notification_type = 'Message';
                      notification.message=data.message;
                      notification.save(function(err) {
                        if (err) {
                            res.json({
                                success: false,
                                message: err
                            });
                        }
                        model.findById(recieverId, function getUser(err, user1) {
                            if (err) {
                                console.log(err);
                                 console.log("in error of user",err);
                                errorMessage = err;
                            } else {
                              console.log("user41",user1)
                                // if (user1.fcm_token != undefined || user1.fcm_token != null) {
                                //   sendpushnotificationtouser(data.message,user1,recieverId)
                                //   // io.sockets.emit(room, roomDetails);
                                // }
                                io.sockets.emit(room, roomDetails);
                            }
                        });
                    })
                    });

                }

                 
              
              })
            }
               
            
        });


        socket.on('typing', data => {
          socket.broadcast.emit('typing', data); // return data
        });
        
});