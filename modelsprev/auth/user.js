const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  user_photo: {type:String,default:""},
  first_name: { type: String,required:true },
  last_name: { type: String, required:true },
  email: { type: String, unique: true },
  phone:{type:String},
  password: { type: String },
  address: { type: String,default:"" },
  token: { type: String },
  dob:{type:String},
  date_of_registration:{type:Date},
  is_active:{type:Boolean,default:true},
  is_deleted:{type:Boolean,default:false},
  fcm_token:{type:String},
  reset_password_token: {
    type: String
  },
  reset_password_expires: {
    type: Date
  },
  extProvider:{type:Boolean,default:false}
});




module.exports = mongoose.model("user", userSchema);