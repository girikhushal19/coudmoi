const mongoose = require("mongoose");
const DocumentsforserviceSchema = new mongoose.Schema({
    client_id: { type: String, required: true },
    laywer_id: { type: String, required: true },
    doc_name: { type: String, required: true },
    doc_path: { type: String, required: true },
    uploaded_on: { type: Date, default: new Date().toISOString() },
    is_lawyer_deleted:{type:Boolean,default:false},
    is_client_deleted:{type:Boolean,default:false},
    from: { type: String },
    to: { type: String },
    fromid: { type: String },
    toid: { type: String }
});
module.exports = mongoose.model("Documentsforservice", DocumentsforserviceSchema);