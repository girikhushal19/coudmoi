const mongoose=require("mongoose");
const Servicesprovided=new mongoose.Schema({
   
    
    client_id:{type:String},
    client_name:{type:String},
    lawyer_id:{type:String},
    lawyer_name:{type:String},
    for_whom:{type:String},
    if_for_whom:{type:Object},
    address:{type:String,default:""},
    date_availed:{type:Date},
    time_availed:{type:String},
    service_status:{type:Boolean,default:false},
    payment_status:{type:Boolean,default:false},
    payment_amount:{type:Number},
    mode_of_payment:{type:String},
    mode_of_service:{type:String},
    commission_earned:{type:Number},
    date_of_transaction:{type:Date},
    transaction_id:{type:String},
    service_acceptance_status_by_lawyer:{type:Boolean,default:false},
    service_acceptance_status_by_client:{type:Boolean,default:true},
    is_service_rejected:{type:Boolean,default:false}

})
module.exports=mongoose.model("Servicesprovided",Servicesprovided);