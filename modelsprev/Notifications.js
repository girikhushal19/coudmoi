const mongoose = require('mongoose');
const NotificationSchema = new mongoose.Schema({
    to_id: {type:String, required:true},
    message: {type:String, required:true},
    status: {type:String, required:true},
    notification_type: {type:String, required:true},
    date: {type:Date, required:true},
    from_email: {type:String},
})
module.exports = mongoose.model('Notification', NotificationSchema);