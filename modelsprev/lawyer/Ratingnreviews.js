const mongoose=require("mongoose");
const RatingnreviewsSchema=new mongoose.Schema({
    client_id:{type:String,required:true},
    lawyer_id:{type:String,required:true},
    rating:{type:Number,required:true},
    review:{type:String,required:true},
});
module.exports=mongoose.model("Ratingnreviews",RatingnreviewsSchema);