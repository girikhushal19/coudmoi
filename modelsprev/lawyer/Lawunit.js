const mongoose = require("mongoose");

const LawunitSchema = new mongoose.Schema({
  photo: { type: String, default: "" },
  iscabinate: { type: Boolean, default: false },
  latlong: { type: Array ,default: []},
  cabinetname: { type: String ,default: ""},
  first_name: { type: String ,default: ""},
  last_name:{ type: String, default: ""},
  email: { type: String, unique: true ,default: ""},
  password: { type: String },
  address1: { type: String },
  address2: { type: String },
  address:{type:Array,default:[]},
  phone: { type: String ,default: ""},
  dob: { type: String ,default: ""},
  expertise_in: { type: String ,default: ""},
  presentation: { type: String ,default: ""},
  legal_information: { type: String ,default: ""},
  language_spoken: { type: String ,default: ""},
  date_of_registration: { type: Date,Defaule:new Date().toISOString() },
  experience: { type: String ,default: ""},
  is_deleted: { type: Boolean, default: false },
  is_active: { type: Boolean,Default: true },
  modesofservice:{
    online:{isonline:{type:Boolean,default:false},price:{type:Number,default:0}},
    offline:{isoffline:{type:Boolean,default:false},price:{type:Number,default:0}},
},
location_serving:{type:String,default:""},
opening_hours:{type:String,default:""},
  token: { type: String },
  reset_password_token: {
    type: String
  },
  reset_password_expires: {
    type: Date
  },
  fcm_token:{type:String,default:""},
  payment_frequency:{type:String,default:""},
  totalpaidamount:{type:Number,default:0},
  totalpendingamount:{type:Number,default:0},
  extProvider:{type:Boolean,default:false}
  
});

module.exports = mongoose.model("Lawunit", LawunitSchema);
