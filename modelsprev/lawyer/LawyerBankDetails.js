const mongoose=require("mongoose");
const LawyerBandDetailsSchema = new mongoose.Schema({
lawyer_id: { type: String },
bank_name: { type: String },
account_number: { type: String },
account_name: { type: String },
bank_address: { type: String },
})
module.exports = mongoose.model("LawyerBandDetails", LawyerBandDetailsSchema);