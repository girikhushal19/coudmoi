const subscriptionsmodel = require('../models/lawyer/Subscriptions');
const mongoose = require('mongoose');
const moment = require("moment")

const { sendmail } = require("../modules/sendmail");
const { sendpushnotificationtouser } = require('../modules/Fcm');
const lawyersmodel = require('../models/lawyer/Lawunit');
const notificationpermissionmodel = require("../models/NotificationPermission");
const studentmodel = require("../models/auth/user")
const appointmentmodel = require('../models/client/Servicesprovided');

const { savereminder,
    getremindersbydateanduseridandremindertype
} = require("../routes/admin/Reminder.controller");
const path = require("path");
const { v4: uuidv4 } = require("uuid");
const AccessToken = require("twilio").jwt.AccessToken;
const VideoGrant = AccessToken.VideoGrant;


// create the twilioClient
const twilioClient = require("twilio")(
  process.env.TWILIO_API_KEY_SID,
  process.env.TWILIO_API_KEY_SECRET,
  { accountSid: process.env.TWILIO_ACCOUNT_SID }
);
const checksubscription = async () => {
    const subscriptions = await subscriptionsmodel.find({ payment_status: true });

    try {
        subscriptions.forEach(async (subscription) => {

            const { _id, end_date } = subscription;

            const currentdate = new Date();
            const enddate = new Date(end_date);
            // console.log("subsddddddcccccription.lawyer_id",subscription.lawyer_id);
            const lawyerIDtoObjectID = mongoose.Types.ObjectId(subscription.lawyer_id);
            const lawyer_fcm_token = await getfcmtoken(lawyerIDtoObjectID);
            if (currentdate > enddate) {
                const pathtofile = path.resolve("views/notifications/subscriptionexpired.ejs");

                const isalreadyreminded = await getremindersbydateanduseridandremindertype(subscription.lawyer_id, new Date().toDateString(), "subscriptionexpired");
                if (subscription.status == true) {
                    const update = await subscriptionsmodel.findByIdAndUpdate(_id, { status: false });
                    const { subscriber_email, subscriber_name } = update;
                    // console.log("subscriber_email",subscriber_email,"subscriber_name",subscriber_name);
                    const days = Math.round((enddate - currentdate) / (1000 * 60 * 60 * 24));

                    sendmail(pathtofile, { name: subscriber_name, days: days }, "Subscription Expired", subscriber_email);
                    sendpushnotificationtouser(`${subscriber_name}' Your subscription has expired.`, lawyer_fcm_token, subscription.lawyer_id);
                    savereminder(subscription.lawyer_id, new Date().toDateString(), "subscriptionexpired");
                } else {
                    if (isalreadyreminded.length == 0) {
                        const dayscountsinceexpired = Math.round((currentdate - enddate) / (1000 * 60 * 60 * 24));
                        if (dayscountsinceexpired <= 7) {
                            sendmail(pathtofile, { name: subscription.subscriber_name, days: dayscountsinceexpired }, "Subscription Expired", subscription.subscriber_email);
                            sendpushnotificationtouser(`${subscription.subscriber_name}' Your subscription has expired.please renew it`, lawyer_fcm_token, subscription.lawyer_id);
                            savereminder(subscription.lawyer_id, new Date().toDateString(), "subscriptionexpired");
                        }
                    }
                }
            }
            else if (currentdate < enddate) {
                const pathtofile = path.resolve("views/notifications/subscriptionalert.ejs");
                const isalreadyreminded = await getremindersbydateanduseridandremindertype(subscription.lawyer_id, new Date().toDateString(), "subscriptionexpiryalert");
                let days = Math.round((enddate - currentdate) / (1000 * 60 * 60 * 24));
                if (days <= 7) {
                    if (isalreadyreminded.length == 0) {
                        sendmail(pathtofile, { name: subscription.subscriber_name, days: days }, "Subscription Expires in " + days + " days", subscription.subscriber_email);
                        sendpushnotificationtouser(`${subscription.subscriber_name}' Your subscription has expired.please renew it`, lawyer_fcm_token, subscription.lawyer_id);
                        savereminder(subscription.lawyer_id, new Date().toDateString(), "subscriptionexpiryalert");
                    }
                }
            }
        }
        )
    } catch (err) {
        console.log(err);
    }
}


const getfcmtoken = async (lawyer_id) => {
    try {


        const lawyer = await lawyersmodel.find({ _id: lawyer_id });
        return lawyer?.fcm_token;
    } catch (err) {
        console.log(err);
        return null;
    }
}

const checkifappointmentiscomplete = async () => {
    const appointments = await appointmentmodel.find({});
    appointments.forEach(async (appointment) => {
        const current_date = new Date().toISOString().split("T")[0];
        let appointment_date = appointment.date_availed;
        let appointment_date_to_string = appointment_date.toISOString().split("T")[0];
        //  console.log( appointment_date_to_string,"appointment_date", current_date,"current_date",appointment.service_status,"appointment.service_status");
        console.log("is current date greater than appointment date", current_date > appointment_date);
        if (appointment.service_status == false && current_date > appointment_date_to_string) {

            appointment.service_status = true;
            appointment.save();

        }
    })
}

const sendSms = (phone,message) => {
    
    const client = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_ACCOUNT_AUTH);
    client.messages
      .create({
         body: message,
        //  mediaUrl: ['https://static01.nyt.com/images/2019/12/09/business/06technewsletter-print/merlin_163200591_5f00de6c-e351-491f-98a0-231a5103a032-superJumbo.jpg'],

         from: process.env.TWILIO_PHONE_NUMBER,
         to: phone
       })
      .then(message => {console.log(message.sid)
        console.log("messagr sent")
    }).catch((e)=> {
        console.log("messagr sent",e)
    })
  }
const sendreminder = async () => {
    const appointments = await appointmentmodel.find({});
    const pathtofile = path.resolve("views/notifications/reminderemail.ejs");
    appointments.forEach(async (appointment) => {
        // console.log()
        const current_date = new Date().toISOString().split("T")[0];
        const student = await studentmodel.findOne({ _id: appointment.student_id });
        const teacher = await lawyersmodel.findOne({ _id: appointment.teacher_id });
        //    console.log("student",student,"teacher",teacher)
        let appointment_date = appointment.date_availed;
        let intervals = [717];
        let appointment_date_to_string = appointment_date.toISOString().split("T")[0];
        //  console.log( appointment_date_to_string,"appointment_date", current_date,"current_date",appointment.service_status,"appointment.service_status");
        // console.log("is current date greater t16,517,han appointment date",current_date>appointment_date);
        if (appointment.service_status == false && current_date == appointment_date_to_string) {
            // console.log("matched id",appointment._id)
            let crudate = new Date()
            let curtime = moment(crudate.getHours() + ":" + crudate.getMinutes(), "HH:mm:");
            let apptime = moment(appointment.time_availed.split("H")[0] + ":" + appointment.time_availed.split("H")[1], "HH:mm")
            let diffrence;
            // console.log(curtime, apptime)
            if (curtime > apptime) {

                
                duration = moment.duration(curtime.diff(apptime));
                let timediff = duration._data.hours * 60 + duration._data.minutes * 1;
                console.log(timediff)
                if (intervals.includes(timediff)) {
                    let sreminder;
                    let treminder;
                    sreminder = await notificationpermissionmodel.find({ id: appointment.student_id })
                    treminder = await notificationpermissionmodel.find({ id: appointment.teacher_id })
                    console.log("appointment.student_id",appointment.student_id,"appointment.teacher_id",appointment.teacher_id)
                    if (sreminder.length) {
                        // console.log("student phoen",student.phone)
                    //    console.log(sreminder)
                        if (sreminder[0].email_reminder) {
                            sendmail(pathtofile, { name: appointment.student_name, days: timediff }, "Class reminder", student.email);
                            console.log("in if otu")
                            savereminder(appointment.student_id, new Date().toDateString(), "reminder");
                        }else{
                            console.log("student email is disabled")
                        }
                        if (sreminder[0].push_reminder) {
                            sendpushnotificationtouser(`${appointment.student_name}' Your class is about to start in .${timediff}`, student,appointment.student_id);
                        }else{
                            console.log("student push is disabled")
                        }if(sreminder[0].sms_reminder){
                            sendSms(student.phone,"yeas it expired")
                        }else{
                            console.log("student phone is disabled")
                        }
                    }
                    else {
                        sendmail(pathtofile, { name: appointment.student_name, days: timediff }, "Class reminder", student.email);
                        sendSms(student.phone,"yeas it expired")
                        savereminder(appointment.student_id, new Date().toDateString(), "reminder");
                        sendpushnotificationtouser(`${appointment.student_name}' Your class is about to start in .${timediff}`, student,appointment.student_id);
                    }
                    if (treminder.length) {
                        if (treminder[0].email_reminder) {
                            sendmail(pathtofile, { name: appointment.teacher_name, days: timediff }, "Class reminder", teacher.email);
                            
                            savereminder(appointment.student_id, new Date().toDateString(), "reminder");
                        }else{
                            console.log("teacher email is disabled")
                        }
                        if (treminder[0].push_reminder) {
                            sendpushnotificationtouser(`${appointment.teacher_name}' Your class is about to start in .${timediff}`, teacher,appointment.teacher_id);
                        }else{
                            console.log("teacher push is disabled")
                        }
                    } else {
                        sendmail(pathtofile, { name: appointment.teacher_name, days: timediff }, "Class reminder", teacher.email);
                            
                        savereminder(appointment.student_id, new Date().toDateString(), "reminder");
                        sendpushnotificationtouser(`${appointment.teacher_name}' Your class is about to start in .${timediff}`, teacher,appointment.teacher_id);
                    }







                }
                
            }
        }
    })
}
module.exports = {
    checksubscription,
    sendreminder,
    checkifappointmentiscomplete
}
