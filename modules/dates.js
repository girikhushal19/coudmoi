function getPreviousDay(date = new Date()) {
  const previous = new Date(date.getTime());
  previous.setDate(date.getDate() - 1);

  return previous.toISOString();
}
  function getdateinadvance(num){
    Date.prototype.addDays = function (days) {
      let date = new Date(this.valueOf());
     
      date.setHours(0,0,0);
      date.setDate(date.getDate() + days);
      
      return date;
    }
    let date = new Date();
    return date.addDays(num);
    // return date.addDays(num).toISOString();
    // return date
  }

  function getdateinadvancewithstartdate(start_date,num){
    Date.prototype.addDays = function (days) {
      let date = new Date(start_date);
     
      date.setHours(0,0,0);
      date.setDate(date.getDate() + days);
      
      return date;
    }
    let date = new Date();
    return date.addDays(num);
    // return date.addDays(num).toISOString();
    // return date
  }
  const addmonthstodate=(datetoadd,duration)=>{
    Date.isLeapYear = function (year) { 
      return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
  };
  
  Date.getDaysInMonth = function (year, month) {
      return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
  };
  
  Date.prototype.isLeapYear = function () { 
      return Date.isLeapYear(this.getFullYear()); 
  };
  
  Date.prototype.getDaysInMonth = function () { 
      return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
  };
  
  Date.prototype.addMonths = function (value) {
      var n = this.getDate();
      this.setDate(1);
      this.setMonth(this.getMonth() + value);
      this.setDate(Math.min(n, this.getDaysInMonth()));
      return this;
  };
  var newdate=new Date(datetoadd);
  var addmonths=newdate.addMonths(duration);
  
  return addmonths;
  }
  function frenchDayDate(date) {
    const mois = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre" ]
    
    let today = new Date(date);
    let year = today.getFullYear()
    let dayNumber = today.getDate()
    let month = mois[today.getMonth()]
    let weekday = today.toLocaleDateString("fr-FR", { weekday: "long" });
     let frenchdate=weekday+" ,"+month+" "+dayNumber+","+year;
    return frenchdate;
  }

  
  module.exports={getdateinadvancewithstartdate,getPreviousDay,getdateinadvance,addmonthstodate,frenchDayDate};