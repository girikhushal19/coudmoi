const nodemailer = require("nodemailer");
const ejs = require("ejs");

const sendmail = (pathtofile,idata,subject,to_email) => {
  // console.log("sendmail", pathtofile, idata, subject, to_email);
    var smtpTransport = nodemailer.createTransport({
      name: "example.com",
      host: process.env.MAILER_HOST,
      port: process.env.MAILER_PORT,
      secure: false,
      auth: {
        user: process.env.MAILER_EMAIL_ID,
        pass: process.env.MAILER_PASSWORD,
      },
       tls: {
      rejectUnauthorized: false
    }
    });
  
    // const handlebarOptions = {
    //   viewEngine: {
    //     partialsDir: path.resolve("./views/notifications/"),
    //     defaultLayout: false,
    //   },
    //   viewPath: path.resolve("./views/notifications/"),
    // };
    smtpTransport.verify((error, success) => {
        if (error) {
          console.log(error);
        } else {
          console.log('Server is rehady to take messages');
        }
      });
      const fromis=process.env.admin_email?process.env.admin_email:"Admin"
      ejs.renderFile(pathtofile, idata, function (err, data) {
        if (err) {
            console.log(err);
        } else {
            var mainOptions = {
                from:process.env.MAILER_EMAIL_ID ,
                to: to_email,
                subject:subject ,
                html: data
            };
            // console.log("html data ======================>", mainOptions);
    
            smtpTransport.sendMail(mainOptions, function (err, info) {
              if (err) {
                console.log(err);
              } else {
               console.log('Message sent: ' + info);
              }
          });
          }
      });
  };
  module.exports = {sendmail}